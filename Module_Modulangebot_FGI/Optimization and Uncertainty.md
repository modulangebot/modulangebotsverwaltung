## Optimization and Uncertainty - NEU (1231552)

### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](../Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Moduleigenschaft            | Inhalt                                                 |
|-----------------------------|--------------------------------------------------------|
| (!) Modultitel (DE)         | Optimization and Uncertainty                               |
| (!) Modultitel (EN)         | Optimization and Uncertainty                               | 
| (!) ECTS                    | 6                                                      |
| (!) Gültig ab               | SoSe 2025                             |
| Gültig bis                  |                                            |
| (!) ModulanbieterIn         | Prof. Dr. Martin Hoefer                                 |
| (!) Sprache                 | Englisch                                               |
| (!) Turnus                  | Unregelmäßig                                           |
| (!) Moduldauer              | Einsemestrig                                           |
| (!) Modulniveau             | Master                                                 |
| (!) Fachsemester            | 1                                                      |



### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](../Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)       | (!) ECTS | (!) Präsenzzeit (SWS) | 
|-------------------------------|-------------------------------------|----------|-----------------------|
| Vorlesung Optimization and Uncertainty | Lecture Optimization and Uncertainty  | 0     | 3         |
| Übung Optimization and Uncertainty | Excercise Optimization and Uncertainty  | 0     | 2         |
| Prüfung Optimization and Uncertainty | Exam Optimization and Uncertainty  | 6     | 0         |

<!-- Zeilen bei Bedarf kopieren -->


### 3. Studien- und Prüfungsleistungen
*[Erläuterungen zu Abschnitt 3](../Anleitungen_Hilfen/3_Leistungen.md)*

**(!) Teilnahmevoraussetzungen (DE)**  
Keine.

**(!) Teilnahmevoraussetzungen (EN)**  
None.

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Hausaufgaben.


**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the module examination.



### 4. Informationen für das Modulhandbuch
*[Erläuterungen zu Abschnitt 4](../Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*

<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
-->

**(!) Empfohlene Voraussetzungen (DE)**  
Good knowledge of data structures and algorithms, stochastics and linear programming.

**(!) Empfohlene Voraussetzungen (EN)**  
Good knowledge of data structures and algorithms, stochastics and linear programming.

**(!) Lernziele (DE)**  
**Knowledge:** After successful completion of the module students know
* Theoretical understanding of stochastic uncertainty models and optimization algorithms along with their limitations

**Skills:** After successful completion of the module, students will be able to
* Clear conception of the assumptions underlying models of uncertainty and their algorithms
* Ability to give estimates on the approximation quality of the derived solutions

**Competencies:** Based on the knowledge and skills acquired in the module, students will be able to
* Application of theoretical insights to support the selection of suitable algorithms in generalized scenarios and for new optimization tasks

**(!) Lernziele (EN)**  
**Knowledge:** After successful completion of the module students know
* Theoretical understanding of stochastic uncertainty models and optimization algorithms along with their limitations

**Skills:** After successful completion of the module, students will be able to
* Clear conception of the assumptions underlying models of uncertainty and their algorithms
* Ability to give estimates on the approximation quality of the derived solutions

**Competencies:** Based on the knowledge and skills acquired in the module, students will be able to
* Application of theoretical insights to support the selection of suitable algorithms in generalized scenarios and for new optimization tasks

**(!) Inhalt (DE)**  
Optimization problems with uncertainty about the input arise in many areas of modern computing systems. Often probabilistisc assumptions about (parts of) the input is available. In this course, we discuss algorithmic techniques that can be used for such scenarios and come with provable performance guarantees. Topics include, e.g., stochastic online optimization, elementary stopping problems and combinatorial extensions, Markov decision processes, probing problems, online convex optimization and multi-armed bandit problems, or game-theoretic scenarios.

**(!) Inhalt (EN)**  
Optimization problems with uncertainty about the input arise in many areas of modern computing systems. Often probabilistisc assumptions about (parts of) the input is available. In this course, we discuss algorithmic techniques that can be used for such scenarios and come with provable performance guarantees. Topics include, e.g., stochastic online optimization, elementary stopping problems and combinatorial extensions, Markov decision processes, probing problems, online convex optimization and multi-armed bandit problems, or game-theoretic scenarios.

**(!) Literatur**  




### 5a. SPOen, in denen das Modul verankert werden soll

*[Erläuterungen zu Abschnitt 5a/5b](../Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) Studiengangskürzel | (!) SPO-Version | (!) Modulbereich                                     |
|------------------------|-----------------|------------------------------------------------------|
| MSDaSci                | 2018            | Vertiefungsbereich > Computer Science and Mathematics > Computer Science |
| MSDaSci                | 2018            | Vertiefungsbereich > Computer Science > Wahlpflichtmodule aus Modulkatalog Computer Science|
| MSInf                  | 2023            | Wahlpflichtbereiche > Theoretische Informatik > Module aus dem Bereich Theoretische Informatik |
| MSInf                  | 2009            | Theoretische Informatik                    |
| BSInf                  | 2022            | Mastervorzug Theoretische Informatik                       |
| BSInf                  | 2018            | Mastervorzug Theoretische Informatik                       |
<!-- Zeilen bei Bedarf kopieren -->



### 5b. SPOen mit abweichenden Verwendungsspezifika

<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

Studiengangskürzel/SPO-Version/Modulbereich:  
...

Abweichende(s) Verwendungsspezifikum/-a:  
...



### Interne Kommentare (gehören NICHT zur Moduldefinition)

Verknüpfungen:
* AK mit GHK 186502
* PK:
    * Übung mit GHK 186503
    * Prüfung mit GHK 186504
