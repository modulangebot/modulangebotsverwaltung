
## Bachelorarbeit (1215682)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**BSInf 2010** in der Version **Angelegt über RWTH API als 1**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Bachelorarbeit
| (!) Modultitel (EN)     | Bachelor Thesis
| (!) ECTS                | 15
| (!) Gültig ab           | 2008-04-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Modulangebotsorganisator: Modulangebotsverantwortlicher InformatikModellierungsteamverantwortlicher: Dr. rer. nat. Katja PetzoldtModulverantworlicher:  Fachgruppe Informatik
| (!) Sprache             | Deutsch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Bachelor
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Bachelorarbeit | Bachelor Thesis | 15 | 0 |



### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| See examination regulations. | BSInf2018 | Bachelorarbeit |
| See examination regulations. | BSInf2022 | Bachelorarbeit |
| Zum Bachelor-Projekt wird zugelassen, wer mindestens 120 ECTS aus den Modulen der vorhergehenden Semester <br>erreicht hat. Für konkrete Aufgabenstellungen werden unterschiedliche Vorkenntnisse benötigt, die vom jeweiligen Betreuer festgelegt werden. | BSInf2010 | Bachelorarbeit |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | BSInf2018 | Bachelorarbeit |
| keine Angabe | BSInf2022 | Bachelorarbeit |
| keine Angabe | BSInf2010 | Bachelorarbeit |

**(!) Prüfungsbedingungen (DE)**  
Die Modulprüfung besteht aus den folgenden Teilleistungen: Schriftliche Ausarbeitung der Bachelorarbeit (12 CP) und Kolloquium (3 CP).

**(!) Prüfungsbedingungen (EN)**  
The module examination consists of the following components: Written thesis (12 CP) and colloquium (3 CP).


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Zum Bachelor-Projekt wird zugelassen, wer mindestens 120 ECTS aus den Modulen der vorhergehenden Semester erreicht hat. Für konkrete Aufgabenstellungen werden unterschiedliche Vorkenntnisse benötigt, die vom jeweiligen Betreuer festgelegt werden.

**Empfohlene Voraussetzungen (EN)**  
For the admission to the Bachelor's project, a minimum of 120 ECTS from the modules of previous semesters are required. For concrete topics varying prior knowledge is required, which is determined by the corresponding thesis supervisor.

**(!) Lernziele (DE)**  
Kenntnisse und Fähigkeiten:  Fähigkeit, sich in das Thema einzuarbeiten, es einzuordnen, einzugrenzen, kritisch zu bewerten und weiter zu entwickeln.    Fähigkeit, das Thema anschaulich und formal angemessen in einem bestimmten Umfang schriftlich darzustellen.    Fähigkeit, das Thema fachgerecht und anschaulich in einem Vortrag einer bestimmten Dauer zu präsentieren.    Fähigkeit, aktiv zu Diskussionen über wissenschaftsnahe Themen der Informatik beizutragen.  Kompetenzen:   Eigenständige Ausarbeitung eines wissenschaftsnahen Themas der Informatik und dessen Darstellung.  

**(!) Lernziele (EN)**  
Acquisition of the following skills to independently develop a scientific theme of computer science:  Ability to familiar oneself with the topic, to categorize it, to narrow it down, to evaluate it critically, and to further develop it. Ability to treat the topic clearly and formal adequate in writing meeting a specific length. Ability to treat the topic professionally and descriptive in an oral presentation of given length. Ability to actively contribute to discussions on science-oriented topics of computer science. 

**(!) Inhalt (DE)**  
Für das Bachelor-Projekt wird ein wissenschaftsnahes Thema zu Konzepten, Vorgehensweisen und Ergebnissen der Informatik mit dem Betreuer vereinbart. Das Thema kann theoretisch oder praktisch orientiert sein, in jedem Fall ist eine kritische Auseinandersetzung und Bewertung gefordert. Beispiele sind etwa:  Literaturüberblick und Bewertung bestehender Ansätze zu einem aktuellen wissenschaftlichen Themengebiet; vertiefte Bewertung und analytischer oder empirischer Vergleich von ausgewählten Lösungskonzepten.    Implementierung, Weiterentwicklung und Evaluierung von bestehenden Verfahren und Konzepten der Informatik zur wissenschaftlichen Analyse (Evaluierungsprototyp) oder zur didaktischen Verwendung (Demonstrationsprototyp); Evaluierung der Leistungsfähigkeit von Systemen in Bezug auf bestimmte Aufgabenstellungen und Arbeitslasten.   Themengebiete können gemeinschaftlich bearbeitet werden, müssen jedoch in Absprache mit dem Betreuer in individuell vertieften und abgegrenzten Leistungen resultieren.

**(!) Inhalt (EN)**  
For the Bachelor's project, a science-oriented topic on concepts, practices and results of computer science will be agreed on with the supervisor. The topic can be theoretically or practically oriented, in each case, in any case, a critical analysis and evaluation is required. Examples include:  Literature review and assessment of existing approaches on a current scientific topic, in-depth assessment and analytical or empirical comparison of selected solution statements. Implementation, development and evaluation of existing methods and concepts of computer science for scientific analysis (evaluation of prototype), or for educational use (prototype demonstration), evaluating the performance of systems in relation to specific tasks and work loads.   Topics may be treated collectively, but must result, in consultation with the supervisor, in distinct and individual in-depth performances.

**(!) Literatur**  
Themenabhängig; wird vorgegeben bzw.selbst recherchiert


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| BSInf2018 | Bachelorarbeit | Angelegt über RWTH API als 1 |
| BSInf2022 | Bachelorarbeit | Angelegt über RWTH API als 1 |
| BSInf2010 | Bachelorarbeit | Angelegt über RWTH API als 1 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|