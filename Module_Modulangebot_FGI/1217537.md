
## Algorithmische Lerntheorie (1217537)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **Angelegt über RWTH API als 1**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Algorithmische Lerntheorie
| (!) Modultitel (EN)     | Algorithmic Learning Theory
| (!) ECTS                | 6
| (!) Gültig ab           | 2018-04-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Modulangebotsorganisator: Modulangebotsverantwortlicher InformatikModellierungsteamverantwortlicher: Dr. rer. nat. Katja PetzoldtModulverantworlicher: Universitätsprofessor Dr. rer. nat. Martin Grohe
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Bachelor/Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Vorlesung Algorithmische Lerntheorie | Lecture Algorithmic Learning Theory |  | 3 |
| Übung Algorithmische Lerntheorie | Exercise Algorithmic Learning Theory | 0 | 2 |
| Prüfung Algorithmische Lerntheorie | Exam Algorithmic Learning Theory | 6 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflichtbereich Theoretische Informatik |
| keine Angabe | BSInf2018 | Wahlpflicht Theoretische Informatik |
| None | MSMath2018 | Anwendungsfach Informatik |
| Keine | MSMath2018 | Subsidiary Subject Informatics |
| keine Angabe | BSInf2022 | Wahlpflicht Theoretische Informatik |
| keine Angabe | MSInf2023 | Module aus dem Bereich KI & Daten |
| None | MSMath2024 | Anwendungsfach Informatik |
| Keine | BSMath2010 | Anwendungsfach Informatik |
| Keine | MSMath2009 | Anwendungsfach Informatik |
| keine Angabe | MSMath2009 | Zusätzliche Prüfungsleistungen |
| None. | MSInf2009 | Theoretische Informatik |
| keine Angabe | MSInf2009 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSSSE2011 | Wahlpflichtbereich Theoretische Grundlagen des SSE |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflichtbereich Theoretische Informatik |
| keine Angabe | BSInf2018 | Wahlpflicht Theoretische Informatik |
| keine Angabe | MSMath2018 | Anwendungsfach Informatik |
| keine Angabe | MSMath2018 | Subsidiary Subject Informatics |
| keine Angabe | BSInf2022 | Wahlpflicht Theoretische Informatik |
| keine Angabe | MSInf2023 | Module aus dem Bereich KI & Daten |
| keine Angabe | MSMath2024 | Anwendungsfach Informatik |
| keine Angabe | BSMath2010 | Anwendungsfach Informatik |
| keine Angabe | MSMath2009 | Anwendungsfach Informatik |
| keine Angabe | MSMath2009 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSInf2009 | Theoretische Informatik |
| keine Angabe | MSInf2009 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSSSE2011 | Wahlpflichtbereich Theoretische Grundlagen des SSE |

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the module examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Gute Kenntnisse aus 'Lineare Algebra', 'Stochastik', 'Datenstrukturen und Algorithmen'. Kenntnisse grundlegender Konzepte das Machine Learning (z.B. aus den Kursen 'Foundations of Data Science' oder 'Machine Learning').

**Empfohlene Voraussetzungen (EN)**  
Good knowledge of linear algebra, stochastics, data structures and algorithms. Familiarity with basic machine learning concepts (e.g. from the course 'Foundations of Data Science' or 'Machine Learning').

**(!) Lernziele (DE)**  
 Knowledge:   Theoretical understanding of machine models and algorithms and their limitations   Skills:   Clear conception of the assumptions underlying machine learning algorithms   Ability to give estimates on the generalisation error and the quality of hypotheses   Competence:   Application of theoretical insights to support the selection of   suitable machine learning algorithms and models 

**(!) Lernziele (EN)**  
 Knowledge:   Theoretical understanding of machine models and algorithms and their limitations   Skills:   Clear conception of the assumptions underlying machine learning algorithms   Ability to give estimates on the generalisation error and the quality of hypotheses   Competence:   Application of theoretical insights to support the selection of   suitable machine learning algorithms and models 

**(!) Inhalt (DE)**  
  PAC learning uniform convergence and sample size bounds VC dimension theoretical analysis of machine learning algorithms advanced theoretical topics in machine learning  

**(!) Inhalt (EN)**  
 PAC learning uniform convergence and sample size bounds VC dimension theoretical analysis of machine learning algorithms advanced theoretical topics in machine learning 

**(!) Literatur**  
 Shalev-Schwartz and Ben David. Understanding Machine Learning. Cambridge University Press, 2014. 


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science | Angelegt über RWTH API als 1 |
| MSDaSci2018 | Computer Science | Angelegt über RWTH API als 1 |
| MSDaSci2018 | Computer Science | Angelegt über RWTH API als 1 |
| BSInf2018 | Wahlpflichtbereich Theoretische Informatik | Angelegt über RWTH API als 1 |
| BSInf2018 | Wahlpflicht Theoretische Informatik | Angelegt über RWTH API als 1 |
| MSMath2018 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
| MSMath2018 | Subsidiary Subject Informatics | Angelegt über RWTH API als 1 |
| BSInf2022 | Wahlpflicht Theoretische Informatik | Angelegt über RWTH API als 1 |
| MSInf2023 | Module aus dem Bereich KI & Daten | Angelegt über RWTH API als 1 |
| MSMath2024 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
| BSMath2010 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
| MSMath2009 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
| MSMath2009 | Zusätzliche Prüfungsleistungen | Angelegt über RWTH API als 1 |
| MSInf2009 | Theoretische Informatik | Angelegt über RWTH API als 1 |
| MSInf2009 | Zusätzliche Prüfungsleistungen | Angelegt über RWTH API als 1 |
| MSSSE2011 | Wahlpflichtbereich Theoretische Grundlagen des SSE | Angelegt über RWTH API als 1 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|
| Gültig ab  | 2018-04-01 -> 2021-10-01 | BSInf2018 | Wahlpflichtbereich Theoretische Informatik | Angelegt über RWTH API als 1
| ECTS | 6 -> 7 | MSMath2018 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2018-04-01 -> 2018-10-01 | MSMath2018 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| ECTS | 6 -> 7 | MSMath2018 | Subsidiary Subject Informatics | Angelegt über RWTH API als 1
| Gültig ab  | 2018-04-01 -> 2018-10-01 | MSMath2018 | Subsidiary Subject Informatics | Angelegt über RWTH API als 1
| ECTS | 6 -> 7 | MSMath2024 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2018-04-01 -> 2018-10-01 | MSMath2024 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| ECTS | 6 -> 7 | BSMath2010 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2018-04-01 -> 2018-10-01 | BSMath2010 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| ECTS | 6 -> 7 | MSMath2009 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2018-04-01 -> 2018-10-01 | MSMath2009 | Anwendungsfach Informatik | Angelegt über RWTH API als 1