
## Seminar I Software und Kommunikation (1212696)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V2**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Seminar I Software und Kommunikation
| (!) Modultitel (EN)     | Seminar I Software and Communication
| (!) ECTS                | 4
| (!) Gültig ab           | 2018-04-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Fachgruppe Informatik
| (!) Sprache             | Deutsch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Seminar I Software und Kommunikation | Seminar I Software and Communication | 4 | 2 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| In den Seminarveranstaltungen besteht Anwesenheitspflicht. | BSInf2010 | Seminare Software und Kommunikation |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | BSInf2010 | Seminare Software und Kommunikation |

**(!) Prüfungsbedingungen (DE)**  
Schriftliche Hausarbeit mit Referat (100 %). Im Seminar besteht Anwesenheitspflicht.

**(!) Prüfungsbedingungen (EN)**  
Written homework and presentation (100 %). Attendance in the seminar is mandatory.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Keine.

**Empfohlene Voraussetzungen (EN)**  
None.

**(!) Lernziele (DE)**  
Kenntnisse: Beim erfolgreichen Abschluss des Moduls sollen Studierende in der Lage sein aktuelle Probleme in der Forschung zu benennen und den State-of-the-Art zu beschreiben. | Fertigkeiten: Sie sollen in der Lage sein zwischen relevanten und irrelevantem Material zu unterscheiden, die einschlägige Literatur zu durchsuchen, das Erlernte in einer Präsentation visuell ansprechend aufzubereiten und das Erlernte einer breiten Zuhörerschaft verständlich zu präsentieren. | Kompetenzen: Basierend auf dem Wissen und den Fähigkeiten, sollen Studierende in der Lage sein wissenschaftliche Publikationen zu verstehen, Forschungsergebnisse kritisch zu beurteilen und eine wissenschaftliche Präsentation zu halten.


**(!) Lernziele (EN)**  
Knowledge: Upon successful completion of the module, students should be able to identify current research problems and describe the state of the art. | Skills: They should be able to distinguish between relevant and irrelevant material, to search the relevant literature, to present what they have learned in a visually appealing way and to present what they have learned to a broad audience in an understandable way. | Competences: Based on knowledge and skills, students should be able to understand scientific publications, critically evaluate research results and give a scientific presentation.


**(!) Inhalt (DE)**  
Das Erreichen der Lernziele wird durch Einübung an Hand persönlich zugeordneter vertiefter wissenschaftlicher Themen sowie die aktive Teilnahme an den Präsentationsterminen verfolgt. Die Wahl der Themengebiete obliegt dem jeweiligen Veranstalter.


**(!) Inhalt (EN)**  
The achievement of the learning objectives is pursued by practice on the basis of personally assigned in-depth scientific topics as well as active participation in the presentation dates. The choice of topics is up to the respective instructor.


**(!) Literatur**  
Themenabhängig; wird vorgegeben bzw. selbst recherchiert.



### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| BSInf2010 | Seminare Software und Kommunikation | V2 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|