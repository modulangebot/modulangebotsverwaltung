## 	Introduction to Program Synthesis - NEU (1231481)

### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](../Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Moduleigenschaft            | Inhalt                                                 |
|-----------------------------|--------------------------------------------------------|
| (!) Modultitel (DE)         | Introduction to Program Synthesis                               |
| (!) Modultitel (EN)         | Introduction to Program Synthesis                               | 
| (!) ECTS                    | 6                                                      |
| (!) Gültig ab               | WiSe 2024/2025                             |
| Gültig bis                  |                                            |
| (!) ModulanbieterIn         | Prof. Dr. Holger Hoos                                 |
| (!) Sprache                 | Englisch                                               |
| (!) Turnus                  | Wintersemester                                           |
| (!) Moduldauer              | Einsemestrig                                           |
| (!) Modulniveau             | Master                                                 |
| (!) Fachsemester            | 1                                                      |



### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](../Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)       | (!) ECTS | (!) Präsenzzeit (SWS) | 
|-------------------------------|-------------------------------------|----------|-----------------------|
| Vorlesung Introduction to Program Synthesis| Lecture Introduction to Program Synthesis  | 0        | 2                     |
| Übung Introduction to Program Synthesis | Exercise Introduction to Program Synthesis | 0        | 2       |             
| Prüfung Introduction to Program Synthesis  | Exam Introduction to Program Synthesis     | 6        | 0                     |
<!-- Zeilen bei Bedarf kopieren -->


### 3. Studien- und Prüfungsleistungen
*[Erläuterungen zu Abschnitt 3](../Anleitungen_Hilfen/3_Leistungen.md)*

**(!) Teilnahmevoraussetzungen (DE)**  
Keine.

**(!) Teilnahmevoraussetzungen (EN)**  
None.

**(!) Prüfungsbedingungen (DE)**  

Klausur (100 %). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.


**(!) Prüfungsbedingungen (EN)**  

Written exam (100 %). Students must pass written homework to be admitted to the examination.



### 4. Informationen für das Modulhandbuch
*[Erläuterungen zu Abschnitt 4](../Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*

<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
-->

**(!) Empfohlene Voraussetzungen (DE)**  

Grundlegende Kenntnisse in der Programmiersprache Python

**(!) Empfohlene Voraussetzungen (EN)**  

Fundamental programming skills in Python

**(!) Lernziele (DE)**  
**Kenntnisse:** Nach erfolgreichem Abschluss des Moduls kennen Studierende
* Formale Definitionen, Darstellung und Aufbau von Computerprogrammen, traditionelle und moderne Dogmen sowie Methodiken der Programmsynthese.

**Fähigkeiten:** Nach erfolgreichem Abschluss des Moduls können Studierende
* Implementierung und Anwendung grundlegender Programmsynthesemethoden
* Einsatz moderner Frameworks zur Programmiersynthese für die Anwendung ; von state-of-the-art Methoden

**Kompetenzen:** Auf der Basis der im Modul erworbenen Kenntnisse und Fähigkeiten sind Studierende in der Lage,
* Programme für Zwecke in verschiedenen Problembereichen zu synthetisieren

**(!) Lernziele (EN)**  

**Knowledge:** After successful completion of the module students know
* Formal definition and fundamental design as well as representations of computer programs, traditional and modern principles and techniques in program synthesis.

**Skills:** After successful completion of the module, students will be able to
* Implement and use fundamental program synthesis methods
* Use modern programming synthesis frameworks to apply more sophisticated methods

**Competencies:** Based on the knowledge and skills acquired in the module, students will be able to
* Synthesize programs for purposes in various problem domain

**(!) Inhalt (DE)**  

Die Vorlesung „Einführung in die Programmsynthese“ deckt den historischen Hintergrund, die Grundlagen sowie die modernen Methodiken der Programmsynthese ab.

Die Vorlesung behandelt zuerst den historischen Hintergrund von Computerprogrammen und der fundamentalen Methodiken der Programmsynthese, gibt dann eine Einführung in die grundlegenden Dogmen der Programmsynthese und stellt zentrale Grundlagen des maschinellen Lernens sowie der Optimierung vor. Die Vorlesung konzentriert sich dann auf das grundlegende Verständnis der grundlegenden (traditionellen) Methoden und Konzepte, die im Laufe der Zeit vorgeschlagen wurden. Der letzte Teil der Vorlesung befasst sich mit modernen Techniken, die auf zeitgemäßen Konzepten des maschinellen Lernens beruhen und für moderne Anwendungen wie z.B. die Programmoptimierung eingesetzt werden.

Der Umfang der Vorlesung kombiniert daher ein grundlegendes und formales Verständnis von Computerprogrammen im Allgemeinen, grundlegende und traditionelle Konzepte der Programmsynthese, welche in der Vergangenheit verwendet wurden, sowie moderne (state-of-the-art) Methoden.

Gliederung der Vorlesung:
1. Grundlagen von Computerprogrammen (Historischer Hintergrund, Definition und Repräsentation)
2. Allgemeine Prinzipien der Programmsynthese (Paradigmen und Suchräume)
3. Traditionelle Konzepte (enumerative, induktive/deduktive und stochastische Suche)
4. Methodiken basierend auf maschinellem Lernen (Neuronale Programmsynthese, Genetische Programmierung, GenAI/LLMs)

**(!) Inhalt (EN)**  

The lecture "Introduction to Program Synthesis" covers its historical background, fundamentals and state-of-the-art methods used for program synthesis. It first sketches the historical background of computer programs and program synthesis, introduces the most fundamental dogmas prevalent in program synthesis, and covers the core basics of machine learning and optimization. The lecture then focuses on the fundamental understanding of basic (traditional) methods and concepts that have been proposed over time. The last part of the lecture addresses modern techniques that are based on contemporary machine learning and optimization concepts and are used for state-of-the-art applications.

The scope of the lecture therefore combines a fundamental and formal understanding of computer programs in general, basic and traditional concepts that have been used in program synthesis, as well as modern (state-of-the-art) methods.

Lecture outline:
1. Fundamentals of computer programs (history, definition and representation)
2. General principles of program synthesis (paradigms and search spaces)
3. Traditional concepts (enumerative, inductive/deductive, and stochastic search)
4. Machine-learning methods (Neural Program Synthesis, Genetic Programming, GenAI/LLMs)

**(!) Literatur**  

* Sumit Gulwani, Oleksandr Polozov, and Rishabh Singh. “Program Synthesis”, 2017 doi: https://10.1561/2500000010 
* Saurabh Srivastava, Sumit Gulwani, and Jeffrey S. Foster. “From Program Verification to Program Synthesis”, 2010. doi: https://10.1145/1706299.170633
* John R. Koza. Genetic Programming: On the Programming of Computers by Means of Natural Selection, 1992. url: https://mitpress.mit.edu/books/genetic-programming
* Hassoun Soha and Tsutomu Sasao: Logic Synthesis and Verification, Springer, 2002. doi: https://doi.org/10.1007/978-1-4615-0817-5



### 5a. SPOen, in denen das Modul verankert werden soll

*[Erläuterungen zu Abschnitt 5a/5b](../Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) Studiengangskürzel | (!) SPO-Version | (!) Modulbereich                                     |
|------------------------|-----------------|------------------------------------------------------|
| MSDaSci                | 2018            | Vertiefungsbereich > Computer Science and Mathematics > Computer Science |
| MSDaSci                | 2018            | Vertiefungsbereich > Computer Science > Wahlpflichtmodule aus Modulkatalog Computer Science|
| MSInf                  | 2023            | Wahlpflichtbereiche > KI & Daten > Module aus dem Bereich KI & Daten |
| MSInf                  | 2009            | Daten- und Informationsmanagement                    |
| BSInf                  | 2022            | Mastervorzug KI & Daten                       |
| BSInf                  | 2018            | Mastervorzug KI & Daten                       |
<!-- Zeilen bei Bedarf kopieren -->



### 5b. SPOen mit abweichenden Verwendungsspezifika

<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

Studiengangskürzel/SPO-Version/Modulbereich:  
...

Abweichende(s) Verwendungsspezifikum/-a:  
...



### Interne Kommentare (gehören NICHT zur Moduldefinition)
