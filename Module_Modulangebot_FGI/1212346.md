
## Mobile Internet Technology (1212346)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V2**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Mobile Internet Technology
| (!) Modultitel (EN)     | Mobile Internet Technology
| (!) ECTS                | 6
| (!) Gültig ab           | 2018-04-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Universitätsprofessor Dr.-Ing. Klaus Wehrle & Dr. rer. nat. Dirk Thißen
| (!) Sprache             | Englisch
| (!) Turnus              | Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Bachelor/Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Vorlesung Mobile Internet Technology | Lecture Mobile Internet Technology |  | 3 |
| Übung Mobile Internet Technology | Exercise Mobile Internet Technology | 0 | 1 |
| Prüfung Mobile Internet Technology | Exam Mobile Internet Technology | 6 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| None. | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| None. | MSDaSci2018 | Computer Science |
| None. | MSDaSci2018 | Computer Science |
| None. | BSInf2018 | Wahlpflichtbereich Software und Kommunikation |
| keine Angabe | BSInf2018 | Wahlpflicht Software und Kommunikation |
| keine Angabe | BSInf2018 | Zusätzliche Prüfungsleistungen |
| None. | MSMI2019 | Rechner- und Kommunikationstechnologie |
| keine Angabe | LABErwGyGeInf2022 | Module à 6 CP |
| None. | MEdErwGyGeInf2022 | Module à 6 CP |
| None. | MEdErwBKInf2022 | Module à 6 CP |
| keine Angabe | LABErwBKInf2022 | Module à 6 CP |
| None. | BSInf2022 | Wahlpflichtbereich Software und Kommunikation |
| keine Angabe | BSInf2022 | Wahlpflicht Software und Kommunikation |
| keine Angabe | MSAT2023 | Verteilte Systeme und Kommunikation |
| keine Angabe | MSAT2023 | Freie Module |
| Basic knowledge in data communication, e.g. lecture 'Data Communication and Internet Technology' & Contents of the lecture "Secure Distributed Systems". Students must pass the exercises to be admitted to the examination. Details will be provided in the lecture. | MSInf2023 | Module aus dem Bereich Hardware/Software-Systeme |
| None. | MSInf2023 | Auflagen |
| keine Angabe | LABBKInf2023 | Module à 6 CP |
| keine Angabe | LABGyGeInf2023 | Module à 6 CP |
| keine Angabe | LABErwBKInf2023 | Module à 6 CP |
| keine Angabe | LABErwGyGeInf2023 | Module à 6 CP |
| keine Angabe | BSMTIKI2023 | Mastervorzugsfächer |
| None. | MSMTIKI2023 | Vertiefungsbereich Software und Kommunikation |
| Inhalt  der  Vorlesung  "Sichere  verteilte  Systeme"  bzw.  "Datenkommunikation und Sicherheit" Voraussetzung für die Zulassung zur Prüfung ist das Bestehen  von  Übungsaufgaben.  Details  werden  in  der  Vorlesung bekanntgegeben. | BSInf2010 | Wahlpflichtbereich Software und Kommunikation |
| Basic knowledge in data communication, e.g. lecture 'Data Communication and Internet Technology' & Contents of the lecture "Secure Distributed Systems". Students must pass the exercises to be admitted to the examination. Details will be provided in the lecture. | BSInf2010 | Wahlpflicht Software und Kommunikation |
| Voraussetzung für die Zulassung zur Prüfung ist das Bestehen wöchentlicher Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | LABGyGeInf2011 | Module à 6 CP |
| keine Angabe | LABBKInf2017 | Module à 6 CP |
| keine Angabe | LABGyGeInf2017 | Module à 6 CP |
| None. | MEdBKInf2017 | Module à 6 CP |
| Voraussetzung für die Zulassung zur Prüfung ist das Bestehen wöchentlicher Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | MEdGyGeInf2014 | Bereich Software und Kommunikation |
| None. | MEdGyGeInf2017 | Module à 6 CP |
| keine Angabe | MEdGyGeInf2017 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| Basic knowledge in data communication, e.g. lecture 'Data Communication and Internet Technology' & Contents of the lecture "Secure Distributed Systems". Students must pass the exercises to be admitted to the examination. Details will be provided in the lecture. | MSInf2009 | Software und Kommunikation |
| None. | MSInf2009 | Auflagen |
| None. | MSInf2009 | Zusätzliche Prüfungsleistungen |
| None. | MSSSE2011 | Communication |
| None. | BSTKI2013 | Vertiefungsbereich Software und Kommunikation |
| None. | MSTKI2013 | Vertiefungsbereich Software und Kommunikation |
| None. | MSMI2005 | Rechner- und Kommunikationstechnologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflichtbereich Software und Kommunikation |
| keine Angabe | BSInf2018 | Wahlpflicht Software und Kommunikation |
| keine Angabe | BSInf2018 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSMI2019 | Rechner- und Kommunikationstechnologie |
| keine Angabe | LABErwGyGeInf2022 | Module à 6 CP |
| keine Angabe | MEdErwGyGeInf2022 | Module à 6 CP |
| keine Angabe | MEdErwBKInf2022 | Module à 6 CP |
| keine Angabe | LABErwBKInf2022 | Module à 6 CP |
| keine Angabe | BSInf2022 | Wahlpflichtbereich Software und Kommunikation |
| keine Angabe | BSInf2022 | Wahlpflicht Software und Kommunikation |
| keine Angabe | MSAT2023 | Verteilte Systeme und Kommunikation |
| keine Angabe | MSAT2023 | Freie Module |
| keine Angabe | MSInf2023 | Module aus dem Bereich Hardware/Software-Systeme |
| keine Angabe | MSInf2023 | Auflagen |
| keine Angabe | LABBKInf2023 | Module à 6 CP |
| keine Angabe | LABGyGeInf2023 | Module à 6 CP |
| keine Angabe | LABErwBKInf2023 | Module à 6 CP |
| keine Angabe | LABErwGyGeInf2023 | Module à 6 CP |
| keine Angabe | BSMTIKI2023 | Mastervorzugsfächer |
| keine Angabe | MSMTIKI2023 | Vertiefungsbereich Software und Kommunikation |
| keine Angabe | BSInf2010 | Wahlpflichtbereich Software und Kommunikation |
| keine Angabe | BSInf2010 | Wahlpflicht Software und Kommunikation |
| keine Angabe | LABGyGeInf2011 | Module à 6 CP |
| keine Angabe | LABBKInf2017 | Module à 6 CP |
| keine Angabe | LABGyGeInf2017 | Module à 6 CP |
| keine Angabe | MEdBKInf2017 | Module à 6 CP |
| keine Angabe | MEdGyGeInf2014 | Bereich Software und Kommunikation |
| keine Angabe | MEdGyGeInf2017 | Module à 6 CP |
| keine Angabe | MEdGyGeInf2017 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSInf2009 | Software und Kommunikation |
| keine Angabe | MSInf2009 | Auflagen |
| keine Angabe | MSInf2009 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSSSE2011 | Communication |
| keine Angabe | BSTKI2013 | Vertiefungsbereich Software und Kommunikation |
| keine Angabe | MSTKI2013 | Vertiefungsbereich Software und Kommunikation |
| keine Angabe | MSMI2005 | Rechner- und Kommunikationstechnologie |

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100%). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100%). Students must pass written homework to be admitted to the module examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Grundkenntnisse in der Datenkommunikation.

**Empfohlene Voraussetzungen (EN)**  
Basic knowledge in data communication is recommended

**(!) Lernziele (DE)**  
Kenntnisse:  Kenntnis der Funktionsprinzipien drahtloser Netze, speziell 802.11 (WLAN) und Mobilfunk Kenntnis der Probleme der Internetprotokolle (IP, TCP) in mobilen Szenarien  Fertigkeiten:  Fähigkeit, Problemquellen in mobilen Szenarien zu identifizieren und geeignet zu lösen Fähigkeit zur Identifikation wichtiger gemeinsamer Aspekte verschiedener drahtloser Netzwerklösungen  Kompetenzen: ;  Fähigkeit zur methodischen Analyse der Anwendbarkeit von Architekturen drahtloser, mobiler Systeme auf zukünftige Internetszenarien Fähigkeit zur Diskussion von Anforderungen an Internetprotokolle in drahtlosen, mobilen Systemen und die Entwicklung von Lösungen zur Erfüllung der Anforderungen 

**(!) Lernziele (EN)**  
Knowledge: On successful completion of this module, students should be able to:  describe the principles of wireless networks, especially 802.11 (WLAN) and telecommunication networks state problems of the Internet protocols (IP, TCP) in mobile scenarios  Skills: They should be able to:  use the gained knowledge to identify sources of problems in mobile scenarios and to deal with them appropriately  ;identify important common aspects in wireless network approaches  Competences: Based on the knowledge and skills acquired they should be able to:  analyze the applicability of mobile system architectures for future mobile Internet scenarios methodically discuss requirements to the Internet protocols in wireless systems and propose solutions how to fulfil these requirements 

**(!) Inhalt (DE)**  
Dieser Kurs befasst sich mit Architekturen, Protokollen und Algorithmen für mobile Internet-Systeme:  Physical Layer: Modulation, Codierung und Signalausbreitung MAC Layer: Herausforderungen beim Medienzugriff auf Funkkanälen Drahtlose, datenorientierte Netze: 802.11 (WLAN) Routing in Ad-hoc-Netzen Mobilfunk: GSM, GPRS, UMTS, LTE, 5G Mobiliät im Internet: Mobile IP, HIP, TCP 

**(!) Inhalt (EN)**  
This course addresses architectures, protocols, and algorithms for mobile Internet systems:  Physical layer: modulation, coding and signal propagation MAC layer: challenges in wireless medium access Wireless, data-oriented networks: 802.11 (WLAN) Routing in Ad-hoc networks Mobile networks: GSM, GPRS, UMTS, LTE, 5G Mobility in the Internet: Mobile IP, HIP, TCP 

**(!) Literatur**  
 Folien zur Vorlesung / Lecture Slides J. Schiller: Mobile Communications, 2. Auflage, Addison Wesley, 2004 W. Stallings: "Wireless Communications and Networks&;quot;. Pearson, 2nd Ed., 2014 Weitere Spezialliteratur wird in den Vorlesungsfolien bekannt gegeben 


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| BSInf2018 | Wahlpflichtbereich Software und Kommunikation | V2 |
| BSInf2018 | Wahlpflicht Software und Kommunikation | V2 |
| BSInf2018 | Zusätzliche Prüfungsleistungen | V2 |
| MSMI2019 | Rechner- und Kommunikationstechnologie | V2 |
| LABErwGyGeInf2022 | Module à 6 CP | V2 |
| MEdErwGyGeInf2022 | Module à 6 CP | V2 |
| MEdErwBKInf2022 | Module à 6 CP | V2 |
| LABErwBKInf2022 | Module à 6 CP | V2 |
| BSInf2022 | Wahlpflichtbereich Software und Kommunikation | V2 |
| BSInf2022 | Wahlpflicht Software und Kommunikation | V2 |
| MSAT2023 | Verteilte Systeme und Kommunikation | V2 |
| MSAT2023 | Freie Module | V2 |
| MSInf2023 | Module aus dem Bereich Hardware/Software-Systeme | V2 |
| MSInf2023 | Auflagen | V2 |
| LABBKInf2023 | Module à 6 CP | V2 |
| LABGyGeInf2023 | Module à 6 CP | V2 |
| LABErwBKInf2023 | Module à 6 CP | V2 |
| LABErwGyGeInf2023 | Module à 6 CP | V2 |
| BSMTIKI2023 | Mastervorzugsfächer | V2 |
| MSMTIKI2023 | Vertiefungsbereich Software und Kommunikation | V2 |
| BSInf2010 | Wahlpflichtbereich Software und Kommunikation | V2 |
| BSInf2010 | Wahlpflicht Software und Kommunikation | V2 |
| LABGyGeInf2011 | Module à 6 CP | V2 |
| LABBKInf2017 | Module à 6 CP | V2 |
| LABGyGeInf2017 | Module à 6 CP | V2 |
| MEdBKInf2017 | Module à 6 CP | V2 |
| MEdGyGeInf2014 | Bereich Software und Kommunikation | V2 |
| MEdGyGeInf2017 | Module à 6 CP | V2 |
| MEdGyGeInf2017 | Individuelle Module | V2 |
| MSAT2013 | Individuelle Module | V2 |
| MSAT2013 | Individuelle Module | V2 |
| MSAT2013 | Individuelle Module | V2 |
| MSInf2009 | Software und Kommunikation | V2 |
| MSInf2009 | Auflagen | V2 |
| MSInf2009 | Zusätzliche Prüfungsleistungen | V2 |
| MSSSE2011 | Communication | V2 |
| BSTKI2013 | Vertiefungsbereich Software und Kommunikation | V2 |
| MSTKI2013 | Vertiefungsbereich Software und Kommunikation | V2 |
| MSMI2005 | Rechner- und Kommunikationstechnologie | V2 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|
| Gültig ab  | 2018-04-01 -> 2018-10-01 | BSInf2018 | Wahlpflicht Software und Kommunikation | V2
| Gültig ab  | 2018-04-01 -> 2018-10-01 | BSInf2022 | Wahlpflicht Software und Kommunikation | V2
| Gültig ab  | 2018-04-01 -> 2023-10-01 | MSAT2023 | Verteilte Systeme und Kommunikation | V2
| Gültig ab  | 2018-04-01 -> 2023-10-01 | MSAT2023 | Freie Module | V2
| ECTS | 6 -> 4 | MEdGyGeInf2017 | Individuelle Module | V2
| Gültig ab  | 2018-04-01 -> 2018-10-01 | BSTKI2013 | Vertiefungsbereich Software und Kommunikation | V2