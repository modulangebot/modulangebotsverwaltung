
## Betriebssysteme und Systemsoftware (1214960)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V2**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Betriebssysteme und Systemsoftware
| (!) Modultitel (EN)     | Operating Systems and System Software
| (!) ECTS                | 6
| (!) Gültig ab           | 2018-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Universitätsprofessor Dr.-Ing. Klaus Wehrle Universitätsprofessor Dr.-Ing. Hermann Ney
| (!) Sprache             | Deutsch/Englisch
| (!) Turnus              | Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Bachelor
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Vorlesung Betriebssysteme und Systemsoftware | Lecture Operating Systems and System Software |  | 3 |
| Übung Betriebssysteme und Systemsoftware | Exercises Operating Systems and System Software | 0 | 2 |
| Prüfung Betriebssysteme und Systemsoftware | Exam Operating Systems and System Software | 6 | 0 |
| Globalübung Betriebssysteme und Systemsoftware | Global Exercise Operating Systems and System Software |  | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | MSDaSci2018 | Auflagen |
| None. | BSInf2018 | Modulbereich Technische Informatik |
| keine Angabe | LABGyGeTech2018 | Vorgezogene Mastermodule |
| keine Angabe | ConRo2020 | Computer Science |
| Keine | BSMath2021 | Wahlmodul Informatik |
| keine Angabe | BSMath2021 | Zusätzliche Prüfungsleistungen |
| None. | LABErwGyGeInf2022 | Pflichtfächer |
| None. | MEdErwGyGeInf2022 | Auflagen |
| none | MEdErwGyGeTech2022 | Fachwissenschaft Technik |
| None. | MEdErwBKInf2022 | Auflagen |
| None. | LABErwBKInf2022 | Pflichtfächer |
| None. | BSInf2022 | Modulbereich Technische Informatik |
| keine Angabe | MSAT2023 | Informatik |
| keine Angabe | MSAT2023 | frei wählbare Auflagenmodule |
| None. | MSInf2023 | Auflagen |
| None. | LABBKInf2023 | Pflichtfächer |
| None. | LABGyGeInf2023 | Pflichtfächer |
| None. | LABErwBKInf2023 | Pflichtfächer |
| None. | LABErwGyGeInf2023 | Pflichtfächer |
| keine Angabe | BSMTIKI2023 | Pflichtbereich |
| keine Angabe | MSMTIKI2023 | Auflagen |
| Inhalte der Vorlesung/Übung Technische Informatik. Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | BSInf2010 | Modulbereich Technische Informatik |
| Voraussetzung für die Zulassung zur Prüfung ist das Bestehen wöchentlicher Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | LABGyGeInf2011 | Pflichtfächer |
| Keine | BSMath2010 | Wahlmodul Informatik |
| Keine | BSMath2016 | Wahlmodul Informatik |
| keine Angabe | BSMath2016 | Zusätzliche Prüfungsleistungen |
| None. | LABBKInf2017 | Pflichtfächer |
| None. | LABGyGeInf2017 | Pflichtfächer |
| None. | MEdBKInf2017 | Auflagen |
| None. | MEdGyGeInf2017 | Auflagen |
| none | LABGyGeTech2016 | Vorgezogene Mastermodule |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Informatik |
| keine Angabe | MSAT2013 | frei wählbare Auflagenmodule  |
| None. | MSInf2009 | Auflagen |
| None. | MSSSE2011 | Auflagen |
| None. | BSTKI2013 | Aufbaumodule |
| none | MEdGyGeTech2018 | Fachwissenschaft Technik |
| None. | MSTKI2013 | Auflagen |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSDaSci2018 | Auflagen |
| keine Angabe | BSInf2018 | Modulbereich Technische Informatik |
| keine Angabe | LABGyGeTech2018 | Vorgezogene Mastermodule |
| keine Angabe | ConRo2020 | Computer Science |
| keine Angabe | BSMath2021 | Wahlmodul Informatik |
| keine Angabe | BSMath2021 | Zusätzliche Prüfungsleistungen |
| keine Angabe | LABErwGyGeInf2022 | Pflichtfächer |
| keine Angabe | MEdErwGyGeInf2022 | Auflagen |
| keine Angabe | MEdErwGyGeTech2022 | Fachwissenschaft Technik |
| keine Angabe | MEdErwBKInf2022 | Auflagen |
| keine Angabe | LABErwBKInf2022 | Pflichtfächer |
| keine Angabe | BSInf2022 | Modulbereich Technische Informatik |
| keine Angabe | MSAT2023 | Informatik |
| keine Angabe | MSAT2023 | frei wählbare Auflagenmodule |
| keine Angabe | MSInf2023 | Auflagen |
| keine Angabe | LABBKInf2023 | Pflichtfächer |
| keine Angabe | LABGyGeInf2023 | Pflichtfächer |
| keine Angabe | LABErwBKInf2023 | Pflichtfächer |
| keine Angabe | LABErwGyGeInf2023 | Pflichtfächer |
| keine Angabe | BSMTIKI2023 | Pflichtbereich |
| keine Angabe | MSMTIKI2023 | Auflagen |
| keine Angabe | BSInf2010 | Modulbereich Technische Informatik |
| keine Angabe | LABGyGeInf2011 | Pflichtfächer |
| keine Angabe | BSMath2010 | Wahlmodul Informatik |
| keine Angabe | BSMath2016 | Wahlmodul Informatik |
| keine Angabe | BSMath2016 | Zusätzliche Prüfungsleistungen |
| keine Angabe | LABBKInf2017 | Pflichtfächer |
| keine Angabe | LABGyGeInf2017 | Pflichtfächer |
| keine Angabe | MEdBKInf2017 | Auflagen |
| keine Angabe | MEdGyGeInf2017 | Auflagen |
| keine Angabe | LABGyGeTech2016 | Vorgezogene Mastermodule |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Informatik |
| keine Angabe | MSAT2013 | frei wählbare Auflagenmodule  |
| keine Angabe | MSInf2009 | Auflagen |
| keine Angabe | MSSSE2011 | Auflagen |
| keine Angabe | BSTKI2013 | Aufbaumodule |
| keine Angabe | MEdGyGeTech2018 | Fachwissenschaft Technik |
| keine Angabe | MSTKI2013 | Auflagen |

**(!) Prüfungsbedingungen (DE)**  
Klausur (100 %). Voraussetzung für die Zulassung zur Prüfung ist das erfolgreiche Absolvieren des Modulbausteins "C- und Shellprogrammierung" und das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Written Exam (100 %). Students must successfully complete the module component "C- und Shellprogrammierung" and pass written homework to be admitted to the examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Inhalte der Vorlesung/Übung Technische Informatik.

**Empfohlene Voraussetzungen (EN)**  
Contents of the Lecture/Exercises Technical Computer Science.

**(!) Lernziele (DE)**  
Kenntnisse:Nach erfolgreichem Abschluss des Moduls kennen Studierende  grundlegende Konzepte des Aufbaus von Betriebssystemen grundlegende Konzepte des Zusammenwirkens der Bestandteile eines Rechners das Zusammenspiel zwischen Hardware und Software  Fähigkeiten:Nach erfolgreichem Abschluss des Moduls können Studierende  selbstständig mit Shell-Utilities umgehen, um Betriebssystemfunktionalität zu nutzen Betriebssystemnahme Funktionalitäten in der Programmiersprache C implementieren Betriebssysteme verwalten  Kompetenzen:Auf der Basis der im Modul erworbenen Kenntnisse und Fähigkeiten sind Studierende in der Lage,  Betriebssystemeigenschaften bei der Implementierung und Auswahl zu berücksichtigen bereitgestellte Funktionalität eines Betriebssystem auch im beruflichen Umfeld zu nutzen 

**(!) Lernziele (EN)**  
Knowledge:Upon successful completion of the module, students will know.  basic concepts of the structure of operating systems basic concepts of the interaction of the components of a computer the interaction between hardware and software  Skills:Upon successful completion of the module, students will be able to.  independently use shell utilities to utilize operating system functionality. implement operating system functionality in the C programming language manage operating systems  Competencies:Based on the knowledge and skills acquired in the module, students will be able to,  consider operating system characteristics during implementation and selection. use provided functionality of an operating system also in the professional environment 

**(!) Inhalt (DE)**  
 Aufgaben und Struktur von Betriebssystemen Das Betriebssystem Unix Systemaufrufe und Shellprogrammierung Einführung in die Programmiersprache C Prozessverwaltung: Prozesse, Threads und Interprozesskommunikation Prozess-Synchronisation, Nebenläufigkeit und Deadlocks CPU-Scheduling Speicherverwaltung: Segmentierung, Paging, Fragmentierung, virtueller Speicher Stack- und Heap-Verwaltung, Garbage Collection Dateisystem und Rechteverwaltung I/O-System Verteilte Systeme Socket-Programmierung 

**(!) Inhalt (EN)**  
 Tasks and Structure of Operating Systems The Operating System Unix System Calls and Shell Programming Introduction to the C Programming Language Process Management: Processes, Threads, and Inter-Process Communication Synchronization of Processes, Concurrency, and Deadlocks CPU Scheduling Memory Management: Segmentation, Paging, Fragmentation, Virtual Memory Stack and Heap Management, Garbage Collection File System and Access Control I/O System Distributed Systems Network Programming with Sockets 

**(!) Literatur**  
  ;A. Silberschatz, G. Gagne, P. B. Galvin: Operating System Concepts. 9th Edition, Wiley, 2013 A. S. Tanenbaum: Modern Operating Systems. 4th Edition, Prentice Hall, 2014. O. Spaniol: Systemprogrammierung - Skript zur Vorlesung an der RWTH Aachen. Aachener Beiträge zur Informatik, Band 14. 3. Auflage, Mainz-Verlag, 2002. Folien zur Vorlesung / Lecture Slides 


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSDaSci2018 | Auflagen | V2 |
| BSInf2018 | Modulbereich Technische Informatik | V2 |
| LABGyGeTech2018 | Vorgezogene Mastermodule | V2 |
| ConRo2020 | Computer Science | V2 |
| BSMath2021 | Wahlmodul Informatik | V2 |
| BSMath2021 | Zusätzliche Prüfungsleistungen | V2 |
| LABErwGyGeInf2022 | Pflichtfächer | V2 |
| MEdErwGyGeInf2022 | Auflagen | V2 |
| MEdErwGyGeTech2022 | Fachwissenschaft Technik | V2 |
| MEdErwBKInf2022 | Auflagen | V2 |
| LABErwBKInf2022 | Pflichtfächer | V2 |
| BSInf2022 | Modulbereich Technische Informatik | V2 |
| MSAT2023 | Informatik | V2 |
| MSAT2023 | frei wählbare Auflagenmodule | V2 |
| MSInf2023 | Auflagen | V2 |
| LABBKInf2023 | Pflichtfächer | V2 |
| LABGyGeInf2023 | Pflichtfächer | V2 |
| LABErwBKInf2023 | Pflichtfächer | V2 |
| LABErwGyGeInf2023 | Pflichtfächer | V2 |
| BSMTIKI2023 | Pflichtbereich | V2 |
| MSMTIKI2023 | Auflagen | V2 |
| BSInf2010 | Modulbereich Technische Informatik | V2 |
| LABGyGeInf2011 | Pflichtfächer | V2 |
| BSMath2010 | Wahlmodul Informatik | V2 |
| BSMath2016 | Wahlmodul Informatik | V2 |
| BSMath2016 | Zusätzliche Prüfungsleistungen | V2 |
| LABBKInf2017 | Pflichtfächer | V2 |
| LABGyGeInf2017 | Pflichtfächer | V2 |
| MEdBKInf2017 | Auflagen | V2 |
| MEdGyGeInf2017 | Auflagen | V2 |
| LABGyGeTech2016 | Vorgezogene Mastermodule | V2 |
| MSAT2013 | Individuelle Module | V2 |
| MSAT2013 | Individuelle Module | V2 |
| MSAT2013 | Informatik | V2 |
| MSAT2013 | frei wählbare Auflagenmodule  | V2 |
| MSInf2009 | Auflagen | V2 |
| MSSSE2011 | Auflagen | V2 |
| BSTKI2013 | Aufbaumodule | V2 |
| MEdGyGeTech2018 | Fachwissenschaft Technik | V2 |
| MSTKI2013 | Auflagen | V2 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|
| ECTS | 6 -> 7 | BSInf2022 | Modulbereich Technische Informatik | V2
| ECTS | 6 -> 7 | MSInf2023 | Auflagen | V2
| ECTS | 6 -> 5 | LABBKInf2023 | Pflichtfächer | V2
| ECTS | 6 -> 5 | LABGyGeInf2023 | Pflichtfächer | V2
| ECTS | 6 -> 7 | BSMTIKI2023 | Pflichtbereich | V2
| ECTS | 6 -> 7 | MSMTIKI2023 | Auflagen | V2