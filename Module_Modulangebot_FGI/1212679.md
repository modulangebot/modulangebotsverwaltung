
## Indexstrukturen für Datenbanken (1212679)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **Angelegt über RWTH API als 1**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Indexstrukturen für Datenbanken
| (!) Modultitel (EN)     | Index Structures for Databases
| (!) ECTS                | 6
| (!) Gültig ab           | 2009-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Modulangebotsorganisator: Modulangebotsverantwortlicher InformatikModellierungsteamverantwortlicher: Dr. rer. nat. Katja PetzoldtModulverantworlicher: Universitätsprofessor Dr. rer. nat. Thomas Seidl
| (!) Sprache             | Deutsch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Übung Indexstrukturen für Datenbanken | Exercise Index Structures for Databases |  | 2 |
| Vorlesung Indexstrukturen für Datenbanken | Lecture Index Structures for Databases |  | 3 |
| Prüfung Indexstrukturen für Datenbanken | Exam Index Structures for Databases | 6 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| Kenntnisse aus dem Modul Datenstrukturen und Algorithmen;<br>empfohlen sind Kenntnisse aus dem Modul Datenbanken<br>und Informationssysteme. | MSSSE2011 | Data and Information Management |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSSSE2011 | Data and Information Management |

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the module examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Kenntnisse aus den Modulen "Datenstrukturen und Algorithmen" und "Datenbanken und Informationssysteme".

**Empfohlene Voraussetzungen (EN)**  
Knowledge from the modules "Data structures and algorithms" and "Databases and information systems".

**(!) Lernziele (DE)**  
Erwerb der folgenden Kenntnisse und Fähigkeiten:  Kenntnis grundlegender Konzepte und Einsatzbereiche von Indexstrukturen für große Datenbanken. Kenntnis verschiedener Lösungsansätze für unterschiedliche Datentypen 

**(!) Lernziele (EN)**  


**(!) Inhalt (DE)**  
Indexstrukturen für verschiedene Datentypen  Motivation / Grundlagen Eindimensionale Daten Mehrdimensionale und räumliche Daten Hochdimensionale Daten Dimensionslose Daten (metrische Räume) Intervalldaten Relationale Indexierung 

**(!) Inhalt (EN)**  


**(!) Literatur**  
Folienskript zur Vorlesung mit zahlreichen Verweisen auf die Originalliteratur


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSSSE2011 | Data and Information Management | Angelegt über RWTH API als 1 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|