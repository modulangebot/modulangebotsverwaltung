
## iOS Application Development (1215681)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V2**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | iOS Application Development
| (!) Modultitel (EN)     | iOS Application Development
| (!) ECTS                | 6
| (!) Gültig ab           | 2018-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Universitätsprofessor Dr. rer. nat. Jan Oliver Borchers &; Dr. rer. nat. Simon Völker
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Bachelor/Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Vorlesung iOS Application Development | Lecture iOS Application Development |  | 3 |
| Prüfung iOS Application Development | Exam iOS Application Development | 6 | 0 |
| Übung iOS Application Development | Exercise iOS Application Development | 0 | 2 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSETITTI2018 | Individuelle Module |
| Knowledge on basics in object-oriented software development<br><br>Students must pass the exercises to be admitted to the examination. Details will be provided in the lecture. | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| Knowledge on basics in objectoriented software development<br><br>Students must pass the exercises to be admitted to the examination. Details will be provided in the lecture. | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| None. | BSInf2018 | Wahlpflichtbereich Angewandte Informatik |
| keine Angabe | BSInf2018 | Wahlpflicht Angewandte Informatik |
| None. | MSMI2019 | Multimedia-Technologie |
| None | MSCSS2021 | Electives Computer Science |
| None | MSCSS2021 | Electives Computer Science |
| None | MSCSS2021 | Electives Computer Science |
| keine Angabe | MEdErwGyGeInf2022 | Individuelle Module |
| None. | BSInf2022 | Wahlpflichtbereich Angewandte Informatik |
| keine Angabe | BSInf2022 | Wahlpflicht Angewandte Informatik |
| Knowledge on basics in object-oriented software development <br><br>Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben.   | MSInf2023 | Module aus dem Bereich Grafik und Interaktion |
| None. | MSInf2023 | Auflagen |
| keine Angabe | BSMTIKI2023 | Mastervorzugsfächer |
| None. | MSMTIKI2023 | Vertiefungsbereich Angewandte Informatik |
| Knowledge on basics in object-oriented software development.<br><br>Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | BSInf2010 | Wahlpflichtbereich Angewandte Informatik |
| Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | BSInf2010 | Wahlpflicht Angewandte Informatik |
| keine Angabe | MEdGyGeInf2017 | Individuelle Module |
| Knowledge on basics in object-oriented software development <br><br>Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben.   | MSInf2009 | Angewandte Informatik |
| None. | MSInf2009 | Auflagen |
| None. | MSSSE2011 | Applied Computer Science |
| None. | BSTKI2013 | Vertiefungsbereich Angewandte Informatik |
| None. | MSTKI2013 | Vertiefungsbereich Angewandte Informatik |
| None. | MSMI2005 | Multimedia-Technologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSETITTI2018 | Individuelle Module |
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflichtbereich Angewandte Informatik |
| keine Angabe | BSInf2018 | Wahlpflicht Angewandte Informatik |
| keine Angabe | MSMI2019 | Multimedia-Technologie |
| keine Angabe | MSCSS2021 | Electives Computer Science |
| keine Angabe | MSCSS2021 | Electives Computer Science |
| keine Angabe | MSCSS2021 | Electives Computer Science |
| keine Angabe | MEdErwGyGeInf2022 | Individuelle Module |
| keine Angabe | BSInf2022 | Wahlpflichtbereich Angewandte Informatik |
| keine Angabe | BSInf2022 | Wahlpflicht Angewandte Informatik |
| keine Angabe | MSInf2023 | Module aus dem Bereich Grafik und Interaktion |
| keine Angabe | MSInf2023 | Auflagen |
| keine Angabe | BSMTIKI2023 | Mastervorzugsfächer |
| keine Angabe | MSMTIKI2023 | Vertiefungsbereich Angewandte Informatik |
| keine Angabe | BSInf2010 | Wahlpflichtbereich Angewandte Informatik |
| keine Angabe | BSInf2010 | Wahlpflicht Angewandte Informatik |
| keine Angabe | MEdGyGeInf2017 | Individuelle Module |
| keine Angabe | MSInf2009 | Angewandte Informatik |
| keine Angabe | MSInf2009 | Auflagen |
| keine Angabe | MSSSE2011 | Applied Computer Science |
| keine Angabe | BSTKI2013 | Vertiefungsbereich Angewandte Informatik |
| keine Angabe | MSTKI2013 | Vertiefungsbereich Angewandte Informatik |
| keine Angabe | MSMI2005 | Multimedia-Technologie |

**(!) Prüfungsbedingungen (DE)**  
Die Modulprüfung besteht aus den folgenden Teilleistungen: Referat mit Schriftlicher Hausarbeit (20 %); Projektarbeit mit Referat (50 %); Klausur (30 %). Für die Referate und die Projektarbeit gilt Anwesenheitspflicht.

**(!) Prüfungsbedingungen (EN)**  
The module examination consists of the following partial qualifications: ;Presentation with written paper (20 %); project work with presentation (50 %); written exam (30 %). Attendance is mandatory for the presentations and the project work. 


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Grundkenntnisse in der objektorientierten Softwareentwicklung.

**Empfohlene Voraussetzungen (EN)**  
Knowledge on basics in object-oriented software development.

**(!) Lernziele (DE)**  
Kenntnisse: Nach erfolgreichem Abschluss dieses Moduls sollten die Teilnehmer in der Lage sein, die Struktur eines modernen SDK für mobile Anwendungen zu definieren, die Designrichtlinien für mobile Anwendungen abzurufen und wichtige Konzepte der Softwarearchitektur zu erläutern, die häufig im iOS SDK verwendet werden. Darüber hinaus werden die Unterschiede zwischen mobilem und Desktop-Geräten aufgezeigt und ein Überblick über die vom iOS SDK bereitgestellten Frameworks gegeben. | Fähigkeiten: Studierende werden nach dem Kurs in der Lage sein, ihre eigenen iOS-Apps effektiv zu implementieren, die iOS-Entwicklungsumgebung umfassend zu nutzen und einen iterativen Softwareentwicklungsprozess anzuwenden. | Kompetenzen: Basierend auf den erworbenen Kenntnissen und Fähigkeiten erwerben die Studierenden die Kompetenz, in einem Team zu kommunizieren / zu arbeiten, die Gestaltungsrichtlinien auf ein bestimmtes Anwendungsszenario anzuwenden und einen Entwicklungsplan für eine definierte Anwendung zu erstellen, um ihre Ergebnisse überzeugend zu präsentieren.

**(!) Lernziele (EN)**  
Knowledge: On successful completion of this module, students should be able to define the structure of a modern mobile application SDK, recall mobile application design guidelines and explain key software architecture concepts heavily used in the iOS SDK. Furthermore, the will be able to state the differences between mobile and desktop computing and provide an overview of the frameworks provided by the iOS SDK | Skills: They should be able to effectively implement their own iOS Apps, use the iOS development environment in depth and apply an iterative software development process. | Competences: Based on the knowledge and skills acquired students will reach the competence of communicating/working in a team, of applying the design guidelines to a specific application scenario and of setting up a development plan for a defined application to convincingly present their results.

**(!) Inhalt (DE)**  
In diesem Kurs lernen Studierende, wie Sie mobile Anwendungen auf iOS-Geräten entwickeln. Es behandelt folgende Themen: Einführung in die Programmiersprache Swift, Xcode, Storyboards, Model-View-Controller für iOS, App-Frameworks (zB UiKit, Foundation), Debugging mit Instrumenten, Basis-iOS-Entwicklungs-Frameworks (zB MapKit, CoreData, Core Location) ), iOS-Grafik- und Spiele-Frameworks (z. B. Sprite Kit, Scene Kit) und Apps im AppStore veröffentlichen.

**(!) Inhalt (EN)**  
In this class you learn how to develop mobile application on iOS devices. It covers the following topics: Introduction of the programming language Swift, Xcode, Storyboards, Model-View-Controller for iOS, App Frameworks (e.g. UiKit, Foundation), Debugging with Instruments, Basis iOS Development Frameworks (e.g. MapKit, CoreData, Core Location), iOS Graphics and Games Frameworks (e.g. Sprite Kit, Scene Kit), Publish Apps in the AppStore.

**(!) Literatur**  
Neuste Version “Programming Fundamentals with Swift” von Matt Neuburg, Verleger: O’Reilly Media


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSETITTI2018 | Individuelle Module | V2 |
| MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| BSInf2018 | Wahlpflichtbereich Angewandte Informatik | V2 |
| BSInf2018 | Wahlpflicht Angewandte Informatik | V2 |
| MSMI2019 | Multimedia-Technologie | V2 |
| MSCSS2021 | Electives Computer Science | V2 |
| MSCSS2021 | Electives Computer Science | V2 |
| MSCSS2021 | Electives Computer Science | V2 |
| MEdErwGyGeInf2022 | Individuelle Module | V2 |
| BSInf2022 | Wahlpflichtbereich Angewandte Informatik | V2 |
| BSInf2022 | Wahlpflicht Angewandte Informatik | V2 |
| MSInf2023 | Module aus dem Bereich Grafik und Interaktion | V2 |
| MSInf2023 | Auflagen | V2 |
| BSMTIKI2023 | Mastervorzugsfächer | V2 |
| MSMTIKI2023 | Vertiefungsbereich Angewandte Informatik | V2 |
| BSInf2010 | Wahlpflichtbereich Angewandte Informatik | V2 |
| BSInf2010 | Wahlpflicht Angewandte Informatik | V2 |
| MEdGyGeInf2017 | Individuelle Module | V2 |
| MSInf2009 | Angewandte Informatik | V2 |
| MSInf2009 | Auflagen | V2 |
| MSSSE2011 | Applied Computer Science | V2 |
| BSTKI2013 | Vertiefungsbereich Angewandte Informatik | V2 |
| MSTKI2013 | Vertiefungsbereich Angewandte Informatik | V2 |
| MSMI2005 | Multimedia-Technologie | V2 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|
| Gültig ab  | 2018-10-01 -> 2024-04-01 | LABGyGeInf2023 | weiteres Fach je nach 2. Fach | V2