
## Seminar: Topics in Automation, Compilers and Code-Generation (1215729)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSSiSc 2010** in der Version **Angelegt über RWTH API als 1**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Seminar: Topics in Automation, Compilers and Code-Generation
| (!) Modultitel (EN)     | Seminar: Topics in Automation, Compilers and Code-Generation
| (!) ECTS                | 4
| (!) Gültig ab           | 2014-04-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Modulangebotsorganisator: Modulangebotsverantwortlicher InformatikModellierungsteamverantwortlicher: Dr. rer. nat. Katja PetzoldtModulverantworlicher: Universitätsprofessor Paolo Bientinesi Ph. D.Prüfungsamt ZPA
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Seminar Topics in Automation, Compilers and Code-Generation |  |  | 2 |
| Prüfung Topics in Automation, Compilers and Code-Generation | Exam Topics in Automation, Compilers and Code-Generation | 4 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSSiSc2010 | Computer Science |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSSiSc2010 | Computer Science |

**(!) Prüfungsbedingungen (DE)**  
Die Modulprüfung besteht aus den folgenden Teilleistungen: Mündliche Prüfung (50 %); Schriftliche Hausarbeit (50 %).

**(!) Prüfungsbedingungen (EN)**  
The module examination consists of the following partial qualifications: Oral examination (50 %); Written homework (50 %).


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Keine.

**Empfohlene Voraussetzungen (EN)**  
None.

**(!) Lernziele (DE)**  
Fachbezogene Lernziele: Advanced topics within the areas of automation, compilers and code generation will be chosen every semester. Students are first expected to read up or research on a personally assigned subject, and then to synthesize the acquired knowledge in the form of oral presentation and written report. Additionally, through class participation the students will be exposed to a range of cutting edge techniques and topics. Nicht fachbezogene Lernziele:  Students will acquire the following skills and knowledge, in order to be able to prepare and present concepts, approaches and results of a scientific topic of computer science:    Ability to independently investigate and learn an advanced topic in computer science on the basis of appropriate literature, especially scientific original articles  Ability to identify and critically assess unfamiliar computer science concepts and topics Ability to elaborate -in written form and within a given timeframe- concepts, approaches and results of a given topic, following a clear structure. Proof of independent thinking through construction of suitable examples. Ability to timely prepare an oral presentation using appropriate media and independently developed examples. Delivery of a scientific talk in front of a computer science audience  Ability to actively participate and contribute to in-depth discussions on topics in computer science.   

**(!) Lernziele (EN)**  


**(!) Inhalt (DE)**  
 Students attending this seminar will be assigned topics related to state-of-the-art technology in the field of high-performance and scientific computing. Based on the assignment, every student will perform the following tasks under the guidance of a supervisor:    Search for relevant literature Write a topic summary Prepare a talk of 40 min during which the topic is presented to other participants  

**(!) Inhalt (EN)**  


**(!) Literatur**  
Research papers to be determined according to the topics covered in the semester.


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSSiSc2010 | Computer Science | Angelegt über RWTH API als 1 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|