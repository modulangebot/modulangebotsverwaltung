
## Automatic Generation and Analysis of Algorithms (1212693)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V2**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Automatic Generation and Analysis of Algorithms
| (!) Modultitel (EN)     | Automatic Generation and Analysis of Algorithms
| (!) ECTS                | 6
| (!) Gültig ab           | 2018-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Modulangebotsorganisator: Modulangebotsverantwortlicher InformatikModellierungsteamverantwortlicher: Dr. rer. nat. Katja PetzoldtModulverantworlicher: Universitätsprofessor Paolo Bientinesi Ph. D.
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Lecture Automatic Generation and Analysis of Algorithms | Lecture Automatic Generation and Analysis of Algorithms |  | 4 |
| Exam Automatic Generation and Analysis of Algorithms | Exam Automatic Generation and Analysis of Algorithms | 6 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| Basic knowledge of Numerical Linear Algebra: matrix product, linear systems, factorizations;<br><br>Knowledge of programming: iteration, recursion, function definitions and calls;<br><br>Familiarity with at least one of the following languages: Mathematica, Maple, Matlab, C.<br><br>Students must pass the exercises to be admitted to the examination. Details will be provided in the lecture. | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| Basic knowledge of Numerical Linear Algebra: matrix product, linear systems, factorizations;<br><br>Knowledge of programming: iteration, recursion, function definitions and calls;<br><br>Familiarity with at least one of the following languages: Mathematica, Maple, Matlab, C.<br><br>Students must pass the exercises to be admitted to the examination. Details will be provided in the lecture. | MSDaSci2018 | Computer Science |
| Basic knowledge of Numerical Linear Algebra: matrix product, linear systems, factorizations;<br><br>Knowledge of programming: iteration, recursion, function definitions and calls;<br><br>Familiarity with at least one of the following languages: Mathematica, Maple, Matlab, C.<br><br>Students must pass the exercises to be admitted to the examination. Details will be provided in the lecture. | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflicht Angewandte Informatik |
| keine Angabe | BSInf2022 | Wahlpflicht Angewandte Informatik |
| keine Angabe | BSInf2010 | Wahlpflicht Angewandte Informatik |
| keine Angabe | MSSiSc2010 | Computer Science |
| None. | MSInf2009 | Angewandte Informatik |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflicht Angewandte Informatik |
| keine Angabe | BSInf2022 | Wahlpflicht Angewandte Informatik |
| keine Angabe | BSInf2010 | Wahlpflicht Angewandte Informatik |
| keine Angabe | MSSiSc2010 | Computer Science |
| keine Angabe | MSInf2009 | Angewandte Informatik |

**(!) Prüfungsbedingungen (DE)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the examination.

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Basic knowledge of Numerical Linear Algebra: matrix product, linear systems, factorizations; Knowledge of programming: iteration, recursion, function definitions and calls; Familiarity with at least one of the following languages: Mathematica, Maple, Matlab, C.

**Empfohlene Voraussetzungen (EN)**  
Basic knowledge of Numerical Linear Algebra: matrix product, linear systems, factorizations; Knowledge of programming: iteration, recursion, function definitions and calls; Familiarity with at least one of the following languages: Mathematica, Maple, Matlab, C.

**(!) Lernziele (DE)**  
Knowledge: On successful completion of this module, students will have knowledge about  Techniques for automation in scientific computing, including: autotuning, search, transformations, decompositions. Linear algebra compilers Partitioned Matrix Expressions &; Loop Invariants  Skills: They should be able to  Find Partitioned Matrix Expressions &; Loop Invariants Design pattern matching and rewrite rules Build heuristics-based and theorem-based tools for the generation of algorithms  Competences: Based on the knowledge and skills acquired, they should be able to  Identify and use existing tools for automation Propose solutions for domain specific compilers 

**(!) Lernziele (EN)**  
Knowledge: On successful completion of this module, students will have knowledge about  Techniques for automation in scientific computing, including: autotuning, search, transformations, decompositions. Linear algebra compilers Partitioned Matrix Expressions &; Loop Invariants  Skills: They should be able to  Find Partitioned Matrix Expressions &; Loop Invariants Design pattern matching and rewrite rules Build heuristics-based and theorem-based tools for the generation of algorithms  Competences: Based on the knowledge and skills acquired, they should be able to  Identify and use existing tools for automation Propose solutions for domain specific compilers 

**(!) Inhalt (DE)**  
This course is research-oriented; it covers novel techniques in computer automation. In this course, "automation" means that a computer makes decisions and performs operations much like a human would. The objective is a software system that creates algorithms without human intervention.

Starting from a problem expressed in symbolic (algebraic) form (example: Lx = b), it is possible to automatically generate algorithms and code to solve the problem with only minimal human intevention. Not only are the algorithms generated automatically, but they are provably correct!

We introduce the concepts of automation, autotuning and formal correctness of programs.

We first review a variety of methodologies to automatically generate algorithms in different fields. Then we restrict ourselves to linear algebra and introduce two different approaches towards automation. The first approach, based on program correctness and pattern matching, relies on Partitioned Matrix Expressions and Loop Invariants. The second approach, also based on pattern matching, originates a linear algebra compiler. Such techniques generate algorithms as well as cost and error analyses.

The course is research oriented, participation is crucial.

**(!) Inhalt (EN)**  
This course is research-oriented; it covers novel techniques in computer automation. In this course, "automation" means that a computer makes decisions and performs operations much like a human would. The objective is a software system that creates algorithms without human intervention.

Starting from a problem expressed in symbolic (algebraic) form (example: Lx = b), it is possible to automatically generate algorithms and code to solve the problem with only minimal human intevention. Not only are the algorithms generated automatically, but they are provably correct!

We introduce the concepts of automation, autotuning and formal correctness of programs.

We first review a variety of methodologies to automatically generate algorithms in different fields. Then we restrict ourselves to linear algebra and introduce two different approaches towards automation. The first approach, based on program correctness and pattern matching, relies on Partitioned Matrix Expressions and Loop Invariants. The second approach, also based on pattern matching, originates a linear algebra compiler. Such techniques generate algorithms as well as cost and error analyses.

The course is research oriented, participation is crucial.

**(!) Literatur**  
 Lecture slides Diaries from the computer sessions Research Manuscripts:  "The Science of Deriving Dense Linear Algebra Algorithms" "Mechanical Derivation and Systematic Analysis of Correct Linear Algebra Algorithms" "A Goal-Oriented and Modular Approach to Stability Analysis" "Knowledge-Based Automatic Generation of Algorithms and Code"  


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| BSInf2018 | Wahlpflicht Angewandte Informatik | V2 |
| BSInf2022 | Wahlpflicht Angewandte Informatik | V2 |
| BSInf2010 | Wahlpflicht Angewandte Informatik | V2 |
| MSSiSc2010 | Computer Science | V2 |
| MSInf2009 | Angewandte Informatik | V2 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|