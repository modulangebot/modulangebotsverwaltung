
## Objektorientierte Softwarekonstruktion (1212354)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V2**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Objektorientierte Softwarekonstruktion
| (!) Modultitel (EN)     | Object Oriented Software Construction
| (!) ECTS                | 6
| (!) Gültig ab           | 2018-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Universitätsprofessor Dr. rer. nat. Horst Lichter
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Vorlesung Objekt-orientierte Softwarekonstruktion | Lecture Object Oriented Software Construction |  | 3 |
| Übung Objekt-orientierte Softwarekonstruktion | Exercise Object Oriented Software Construction | 0 | 2 |
| Prüfung Objekt-orientierte Softwarekonstruktion | Exam Object Oriented Software Construction | 6 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| Learning outcomes of the module Softwaretechnik<br><br>Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| Learning outcomes of the module Softwaretechnik<br><br>Students must pass the exercises to be admitted to the examination. Details will be provided in the lecture. | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Zusätzliche Prüfungsleistungen |
| keine Angabe | BSInf2018 | Wahlpflicht Software und Kommunikation |
| None. | MSMI2019 | Rechner- und Kommunikationstechnologie |
| None | ConRo2020 | Computer Science |
| keine Angabe | LABErwGyGeInf2022 | Module à 6 CP |
| None. | MEdErwGyGeInf2022 | Module à 6 CP |
| None. | MEdErwBKInf2022 | Module à 6 CP |
| keine Angabe | LABErwBKInf2022 | Module à 6 CP |
| keine Angabe | BSInf2022 | Wahlpflicht Software und Kommunikation |
| keine Angabe | MSInf2023 | Module aus dem Bereich Software-Entwicklungsmethoden und -werkzeuge |
| keine Angabe | LABBKInf2023 | Module à 6 CP |
| keine Angabe | LABGyGeInf2023 | Module à 6 CP |
| keine Angabe | LABErwBKInf2023 | Module à 6 CP |
| keine Angabe | LABErwGyGeInf2023 | Module à 6 CP |
| keine Angabe | BSMTIKI2023 | Mastervorzugsfächer |
| None. | MSMTIKI2023 | Vertiefungsbereich Software und Kommunikation |
| keine Angabe | BSInf2010 | Wahlpflicht Software und Kommunikation |
| Voraussetzung für die Zulassung zur Prüfung ist das Bestehen wöchentlicher Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | LABGyGeInf2011 | Module à 6 CP |
| keine Angabe | LABBKInf2017 | Module à 6 CP |
| keine Angabe | LABGyGeInf2017 | Module à 6 CP |
| None. | MEdBKInf2017 | Module à 6 CP |
| Voraussetzung für die Zulassung zur Prüfung ist das Bestehen wöchentlicher Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | MEdGyGeInf2014 | Bereich Software und Kommunikation |
| None. | MEdGyGeInf2017 | Module à 6 CP |
| keine Angabe | MSVT2011 | Informatik |
| keine Angabe | MSCES2011 | Software und eingebettete Systeme |
| keine Angabe | MSCES2011 | Software und eingebettete Systeme |
| keine Angabe | MSCES2011 | Software und eingebettete Systeme |
| keine Angabe | MSSiSc2010 | Computer Science |
| None. | MSInf2009 | Software und Kommunikation |
| None. | MSInf2009 | Software und Kommunikation |
| None. | MSSSE2011 | Core Subjects Software Engineering |
| None. | BSTKI2013 | Vertiefungsbereich Software und Kommunikation |
| None. | MSTKI2013 | Vertiefungsbereich Software und Kommunikation |
| None. | MSMI2005 | Rechner- und Kommunikationstechnologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Zusätzliche Prüfungsleistungen |
| keine Angabe | BSInf2018 | Wahlpflicht Software und Kommunikation |
| keine Angabe | MSMI2019 | Rechner- und Kommunikationstechnologie |
| keine Angabe | ConRo2020 | Computer Science |
| keine Angabe | LABErwGyGeInf2022 | Module à 6 CP |
| keine Angabe | MEdErwGyGeInf2022 | Module à 6 CP |
| keine Angabe | MEdErwBKInf2022 | Module à 6 CP |
| keine Angabe | LABErwBKInf2022 | Module à 6 CP |
| keine Angabe | BSInf2022 | Wahlpflicht Software und Kommunikation |
| keine Angabe | MSInf2023 | Module aus dem Bereich Software-Entwicklungsmethoden und -werkzeuge |
| keine Angabe | LABBKInf2023 | Module à 6 CP |
| keine Angabe | LABGyGeInf2023 | Module à 6 CP |
| keine Angabe | LABErwBKInf2023 | Module à 6 CP |
| keine Angabe | LABErwGyGeInf2023 | Module à 6 CP |
| keine Angabe | BSMTIKI2023 | Mastervorzugsfächer |
| keine Angabe | MSMTIKI2023 | Vertiefungsbereich Software und Kommunikation |
| keine Angabe | BSInf2010 | Wahlpflicht Software und Kommunikation |
| keine Angabe | LABGyGeInf2011 | Module à 6 CP |
| keine Angabe | LABBKInf2017 | Module à 6 CP |
| keine Angabe | LABGyGeInf2017 | Module à 6 CP |
| keine Angabe | MEdBKInf2017 | Module à 6 CP |
| keine Angabe | MEdGyGeInf2014 | Bereich Software und Kommunikation |
| keine Angabe | MEdGyGeInf2017 | Module à 6 CP |
| keine Angabe | MSVT2011 | Informatik |
| keine Angabe | MSCES2011 | Software und eingebettete Systeme |
| keine Angabe | MSCES2011 | Software und eingebettete Systeme |
| keine Angabe | MSCES2011 | Software und eingebettete Systeme |
| keine Angabe | MSSiSc2010 | Computer Science |
| keine Angabe | MSInf2009 | Software und Kommunikation |
| keine Angabe | MSInf2009 | Software und Kommunikation |
| keine Angabe | MSSSE2011 | Core Subjects Software Engineering |
| keine Angabe | BSTKI2013 | Vertiefungsbereich Software und Kommunikation |
| keine Angabe | MSTKI2013 | Vertiefungsbereich Software und Kommunikation |
| keine Angabe | MSMI2005 | Rechner- und Kommunikationstechnologie |

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Kenntnisse der Softwaretechnik.

**Empfohlene Voraussetzungen (EN)**  
 Knowledge of “Software Engineering”.

**(!) Lernziele (DE)**  
Nach Beendigung des Moduls verfügen die Studierenden über das folgende Wissen und über die folgenden Kompetenzen: sie wissen, wie objekt-orientierte Modellierungskonzepte anzuwenden sind | sie sind in der Lage Use-Case basiert zu analysieren und Domänenwissen zu modellieren | sie kennen die Konzepte zur Entwicklung von Rahmenwerken | sie sind in der Lage, Code und Architektur auf Basis von Smells zu verbessern | sie kennen die Architektur von Java basierten Komponentenmodellen. Nutzen für die zukünftige Karriere / vermittelte Soft Skills: Alle Kompetenzen werden im Rahmen der begleitenden Übungen trainiert. In diesen arbeiten die Studierenden in Gruppen zusammen, um ihre Lösungen zu entwickeln. Weiterhin präsentieren und diskutieren sie im Rahmen der Übungen die von ihnen entwickelten Lösungen. Da in diesem Modul professionelles Wissen im Bereich der systematischen Softwarekonstruktion vermittelt wird, verfügen die Studierenden am Ende über relevantes Wissen und Kompetenzen um als Softwarearchitekt erfolgreich arbeiten zu können.

**(!) Lernziele (EN)**  
Study goals: After completing the module the students have the following knowledge and competencies: they know how to apply important object oriented modeling concepts| they are able to perform use case based and domain driven design | they know the concepts of framework based development | they know important design patterns and are able to apply patterns in architectural design | they know how to improve code and architecture based on smells and refactoring | they know the architecture of Java based component models. Benefits for future professional life / soft skills: All competencies are trained in the exercises, where small teams of students have to create typical analysis, design and implementation artefacts. They have to present and discuss their solutions and ideas in front of the class. As professional knowledge on software construction is provided, students gain personal and professional competencies that enable to work as software architects.

**(!) Inhalt (DE)**  
Dieses Modul führt in zentrale Methoden, Techniken und Prozesse einer systematischen Softwareentwicklung basierend auf objekt-orientierten Konzepten ein. Die Vorlesung beschäftigt sich mit den folgenden Themen: Grundlagen der Objektorientierung | Software Wiederverwendung | Design by Contract | Vererbung, Polymorphismus und Generische Einheiten | Software Entwurfsprinzipien | Domain Modelling, Domain Driven Design | Komponententechnologie | Muster und Rahmenwerke | Smells und Refactoring

**(!) Inhalt (EN)**  
This module introduces central methods, techniques and processes of systematic software construction based on object-oriented concepts. The lecture covers the following topics: Foundations of object orientation | Software reuse | Design by Contract | Inheritance, polymorphism and generics | Software design principles | Domain modelling, domain driven design | Component technology - Advanced patterns and frameworks | Smells and Refactoring

**(!) Literatur**  
Meyer, B. (1997): Object Oriented Software Construction, 2nd edition, Prentice Hall | Züllighoven, H. (2005): Object-Oriented Construction Handbook – Developing Application-Oriented Software with the Tools and Materials Approach. dpunkt.verlag, Heidelberg | Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides (1995): Design Patterns, Addison-Wesley | Fowler Martin (1999): Refactoring - Improving the design of existing code, Addison Wesley.


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| MSDaSci2018 | Zusätzliche Prüfungsleistungen | V2 |
| BSInf2018 | Wahlpflicht Software und Kommunikation | V2 |
| MSMI2019 | Rechner- und Kommunikationstechnologie | V2 |
| ConRo2020 | Computer Science | V2 |
| LABErwGyGeInf2022 | Module à 6 CP | V2 |
| MEdErwGyGeInf2022 | Module à 6 CP | V2 |
| MEdErwBKInf2022 | Module à 6 CP | V2 |
| LABErwBKInf2022 | Module à 6 CP | V2 |
| BSInf2022 | Wahlpflicht Software und Kommunikation | V2 |
| MSInf2023 | Module aus dem Bereich Software-Entwicklungsmethoden und -werkzeuge | V2 |
| LABBKInf2023 | Module à 6 CP | V2 |
| LABGyGeInf2023 | Module à 6 CP | V2 |
| LABErwBKInf2023 | Module à 6 CP | V2 |
| LABErwGyGeInf2023 | Module à 6 CP | V2 |
| BSMTIKI2023 | Mastervorzugsfächer | V2 |
| MSMTIKI2023 | Vertiefungsbereich Software und Kommunikation | V2 |
| BSInf2010 | Wahlpflicht Software und Kommunikation | V2 |
| LABGyGeInf2011 | Module à 6 CP | V2 |
| LABBKInf2017 | Module à 6 CP | V2 |
| LABGyGeInf2017 | Module à 6 CP | V2 |
| MEdBKInf2017 | Module à 6 CP | V2 |
| MEdGyGeInf2014 | Bereich Software und Kommunikation | V2 |
| MEdGyGeInf2017 | Module à 6 CP | V2 |
| MSVT2011 | Informatik | V2 |
| MSCES2011 | Software und eingebettete Systeme | V2 |
| MSCES2011 | Software und eingebettete Systeme | V2 |
| MSCES2011 | Software und eingebettete Systeme | V2 |
| MSSiSc2010 | Computer Science | V2 |
| MSInf2009 | Software und Kommunikation | V2 |
| MSInf2009 | Software und Kommunikation | V2 |
| MSSSE2011 | Core Subjects Software Engineering | V2 |
| BSTKI2013 | Vertiefungsbereich Software und Kommunikation | V2 |
| MSTKI2013 | Vertiefungsbereich Software und Kommunikation | V2 |
| MSMI2005 | Rechner- und Kommunikationstechnologie | V2 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|