
## SiSc Laboratory (1212320)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSSiSc 2010** in der Version **Angelegt über RWTH API als 1_neu**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | SiSc Laboratory
| (!) Modultitel (EN)     | SiSc Laboratory
| (!) ECTS                | 6
| (!) Gültig ab           | 2021-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Universitätsprofessor Dr. rer. nat. Uwe Naumann
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester
| (!) Moduldauer          | Zweisemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Vorlesung/Übung SiSc Laboratory | Lecture/Exercise SiSc Laboratory |  | 1 |
| Praktikum SiSc-Laboratory | Lab SiSc Laboratory | 6 | 2 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSSiSc2010 | Compulsory Courses |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSSiSc2010 | Compulsory Courses |

**(!) Prüfungsbedingungen (DE)**  
The module examination consists of the following partial qualifications: Course assignment (60%); Presentation (20%); Written exam (20%). It is not possible to transfer parts of the examinations in another semester.

**(!) Prüfungsbedingungen (EN)**  
The module examination consists of the following partial qualifications: Course assignment (60%); Presentation (20%); Written exam (20%). It is not possible to transfer parts of the examinations in another semester.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Knowledge of the modules 'Parallel Programming I', 'Numerical Methods for PDEs', and 'Parallel Computing in SiSc'

**Empfohlene Voraussetzungen (EN)**  
Knowledge of the modules 'Parallel Programming I', 'Numerical Methods for PDEs', and 'Parallel Computing in SiSc'

**(!) Lernziele (DE)**  
 Knowledge: understanding of algorithmic differentiation for parameter sensitivity analysis Skills: ability to design, implement, run and test a numerical simulation software for a practically relevant problem Competences: requirements engineering, project management, teamwork 

**(!) Lernziele (EN)**  
 Knowledge: understanding of algorithmic differentiation for parameter sensitivity analysis Skills: ability to design, implement, run and test a numerical simulation software for a practically relevant problem Competences: requirements engineering, project management, teamwork 

**(!) Inhalt (DE)**  
 Essential Algorithmic Differentiation Software Project  requirements analysis design implementation tests documentation  

**(!) Inhalt (EN)**  
 Essential Algorithmic Differentiation Software Project  requirements analysis design implementation tests documentation  

**(!) Literatur**  
 Set of slides Example programs References to relevant current literature and online materials 


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSSiSc2010 | Compulsory Courses | Angelegt über RWTH API als 1_neu |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|