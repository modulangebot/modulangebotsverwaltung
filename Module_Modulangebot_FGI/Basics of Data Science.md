## Basics of Data Science - NEU (1231554)

### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](../Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Moduleigenschaft            | Inhalt                                                 |
|-----------------------------|--------------------------------------------------------|
| (!) Modultitel (DE)         | Basics of Data Science                                 |
| (!) Modultitel (EN)         | Basics of Data Science                                 | 
| (!) ECTS                    | 3                                                      |
| (!) Gültig ab               | SoSe 2025                                              |
| Gültig bis                  |                                                        |
| (!) ModulanbieterIn         | Universitätsprofessor Professor h. c. Dr. h. c. Dr. ir. Wil van der Aalst|
| (!) Sprache                 | Englisch                                               |
| (!) Turnus                  | Unregelmäßig                                           |
| (!) Moduldauer              | Einsemestrig                                           |
| (!) Modulniveau             | Bachelor                                               |
| (!) Fachsemester            | 1                                                      |



### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](../Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)       | (!) ECTS | (!) Präsenzzeit (SWS) | 
|-------------------------------|-------------------------------------|----------|-----------------------|
| Vorlesung/Übung Basics of Data Science | Lecture/Excercise Basics of Data Science  | 0     | 2         |
| Prüfung Basics of Data Science | Exam Basics of Data Science  | 3     | 0         |

<!-- Zeilen bei Bedarf kopieren -->


### 3. Studien- und Prüfungsleistungen
*[Erläuterungen zu Abschnitt 3](../Anleitungen_Hilfen/3_Leistungen.md)*

**(!) Teilnahmevoraussetzungen (DE)**  
Keine.

**(!) Teilnahmevoraussetzungen (EN)**  
None.

**(!) Prüfungsbedingungen (DE)**  
Klausur (100 %). Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Hausaufgaben.


**(!) Prüfungsbedingungen (EN)**  
Written exam (100 %). Students must pass written homework to be admitted to the module examination.



### 4. Informationen für das Modulhandbuch
*[Erläuterungen zu Abschnitt 4](../Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*

<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
-->

**(!) Empfohlene Voraussetzungen (DE)**  
Keine.

**(!) Empfohlene Voraussetzungen (EN)**  
None.

**(!) Lernziele (DE)**  
**Kenntnisse:**
Nach erfolgreichem Abschluss des Moduls kennen Studierende verschiedene Herausforderungen und Lösungsansätze aus dem Bereich Data Science. Sie kennen Probleme und Strategien aus dem Bereich der Dateninfrastruktur und Datenmanagement,  verschiedene Methoden, Modelle und Konzepte zur Datenanalyse sowie Erwägungen und Ansätze in Bezug auf Privatsphäre und verantwortungsvollen Umgang mit Daten. Die Vor- und Nachteile sowie Beschränkungen der verschiedenen Strategien und Methoden sind bekannt.

**Fähigkeiten:**
Nach erfolgreichem Abschluss des Moduls können Studierende verschiedene Methoden im Bereich Data Science verstehen und implementieren. Dazu gehören Methoden zur Datenvisualisierung, Datenvorbereitung, Datenreduktion sowie Datenanalyse sowie die Evaluierung der Ergebnisse.  Sie sind in der Lage Daten zu anonymisieren und die Fairness von Algorithmen zu analysieren.

**Kompetenzen:**
Auf der Basis der im Modul erworbenen Kenntnisse und Fähigkeiten sind Studierende in der Lage, Probleme und Herausforderungen im Bereich Data Science zu analysieren, geeignete Methoden zu deren Lösung auszuwählen und einzusetzen, die Ergebnisse korrekt zu interpretieren, sowie die Qualität und Auswirkungen der Lösung zu bewerten. 

**(!) Lernziele (EN)**  
**Knowledge.**
After successfully completing the module, students will be familiar with various challenges and solution approaches in the field of data science. They are familiar with problems and strategies in the area of data infrastructure and data management, various methods, models and concepts for data analysis as well as considerations and approaches related to privacy and responsible data handling. The advantages, disadvantages and limitations of the various strategies and methods are known.

**Skills:**
After successfully completing the module, students will be able to understand and implement various methods in the field of data science. This includes methods for data visualization, data preparation, data reduction and data analysis as well as the evaluation of the results.  They are able to anonymize data and analyze the fairness of algorithms.

**Competencies:**
Based on the knowledge and skills acquired in the module, students are able to analyze problems and challenges in the field of data science, select and use suitable methods to solve them, correctly interpret the results, as well as evaluate the quality and effects of the solution.

**(!) Inhalt (DE)**  
Der Kurs vermittelt den Studierenden einen umfassenden Überblick über die grundlegenden Herausforderungen, Konzepte und Werkzeuge der Datenwissenschaft. Der Inhalt lässt sich in drei Hauptbereiche einteilen:
Es wird ein Überblick über die Data-Science-Infrastruktur gegeben, die sich mit Volumen und Geschwindigkeit befasst. Zu den Themen gehören Instrumentierung, Big-Data-Infrastrukturen und verteilte Systeme, Datenbanken und Datenmanagement. Die größte Herausforderung besteht in Skalierbarkeit und sofortiger Verfügbarkeit.
Der Schwerpunkt des Kurses liegt auf der Datenanalyse, bei der es darum geht, Wissen aus Daten zu extrahieren. Zu den behandelten Themen gehören beispielsweise Datenexploration und -visualisierung, Datenvorverarbeitung, Datenqualitätsprobleme und -transformationen, überwachte Lerntechniken und deren Bewertung (z. B. Entscheidungsbäume, lineare und logistische Regression, Support-Vektor-Maschinen), unüberwachtes Lernen (z. B. Clustering, Pattern Mining), Process Mining und Text Mining. Die größte Herausforderung der Datenanalyse besteht darin, Antworten auf bekannte und unbekannte Unbekannte zu geben.
Darüber hinaus untersucht der Kurs, wie sich Data Science auf Menschen, Organisationen und die Gesellschaft auswirkt, indem er Herausforderungen diskutiert und Richtlinien und Techniken für die verantwortungsvolle Anwendung von Data Science-Techniken bereitstellt. Themen sind beispielsweise Ethik & Datenschutz, IT-Recht und Mensch-Technik-Interaktion. Die größte Herausforderung besteht darin, Data-Science-Techniken verantwortungsvoll und produktiv anzuwenden.

**(!) Inhalt (EN)**  
The course is designed to provide students with a comprehensive overview of the fundamental challenges, concepts and tools of data science. The content can be organized in three main areas:
An overview is given to data science infrastructure concerned with volume and velocity. Topics include instrumentation, big data infrastructures and distributed systems, databases and data management. The main challenge is to make things scalable and instant.
The main focus of the course is on data analysis concerned with extracting knowledge from data. Topics included are, for example, data exploration and visualization, data preprocessing, data quality issues and transformations, supervised learning techniques and their evaluation (e.g. decision trees, linear and logistic regression, support vector machines), unsupervised learning (e.g. clustering, pattern mining), process mining and text mining. The main challenge of data analysis is to provide answers to known and unknown unknowns.
Furthermore, the course considers how data science affects people, organizations, and society by discussing challenges and providing guidelines and techniques to apply data science techniques responsibly. Topics include, for example, ethics & privacy, IT law and human-technology interaction. The main challenge is to apply data science techniques productively in a responsible manner.

**(!) Literatur**  




### 5a. SPOen, in denen das Modul verankert werden soll

*[Erläuterungen zu Abschnitt 5a/5b](../Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) Studiengangskürzel | (!) SPO-Version | (!) Modulbereich                                     |
|------------------------|-----------------|------------------------------------------------------|
| MSInf                  | 2023            | Auflagen                                             |

<!-- Zeilen bei Bedarf kopieren -->



### 5b. SPOen mit abweichenden Verwendungsspezifika

<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

Studiengangskürzel/SPO-Version/Modulbereich:  
...

Abweichende(s) Verwendungsspezifikum/-a:  
...



### Interne Kommentare (gehören NICHT zur Moduldefinition)

Verknüpfungen:
* AK mit GHK 186507
* PK mit GHK 186508
