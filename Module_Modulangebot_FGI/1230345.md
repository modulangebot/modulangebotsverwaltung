## Near-term Quantum Computation - NEU (1230345)


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](../Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Moduleigenschaft            | Inhalt                                                 |
|-----------------------------|--------------------------------------------------------|
| (!) Modultitel (DE)         | Near-term Quantum Computation                          |
| (!) Modultitel (EN)         | Near-term Quantum Computation                          |
| (!) ECTS                    | 6                                                      |
| (!) Gültig ab               | SoSe 2024                                              |
| Gültig bis                  |                                                        |
| (!) ModulanbieterIn         | Univ.-Prof. Dr. rer. nat. Dominique Unruh              |
| (!) Sprache                 | Englisch                                               |
| (!) Turnus                  | Sommersemester                                         |
| (!) Moduldauer              | Einsemestrig                                           |
| (!) Modulniveau             | Master                                                 |
| (!) Fachsemester            | 1                                                      |



### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN) | (!) ECTS | (!) Präsenzzeit (SWS) | 
|-------------------------------|-------------------------------|----------|-----------------------|
| Vorlesung Near-term Quantum Computation | Lecture Near-term Quantum Computation | 0 | 3 |
| Übung Near-term Quantum Computation | Excercise Near-term Quantum Computation | 0 | 1 |
| Prüfung Near-term Quantum Computation | Exam Near-term Quantum Computation | 6 | 0 |




### 3. Studien- und Prüfungsleistungen
*[Erläuterungen zu Abschnitt 3](../Anleitungen_Hilfen/3_Leistungen.md)*

**(!) Teilnahmevoraussetzungen (DE)**  
Keine.

**(!) Teilnahmevoraussetzungen (EN)**  
None.

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.


**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the module examination.




### 4. Informationen für das Modulhandbuch
*[Erläuterungen zu Abschnitt 4](../Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*


**(!) Empfohlene Voraussetzungen (DE)**  
Linear Algebra; Quantum Information/Computing Fundamentals

**(!) Empfohlene Voraussetzungen (EN)**  
Linear Algebra; Quantum Information/Computing Fundamentals

**(!) Lernziele (DE)**  
Knowledge
After successful completion of the module students know:
* basics and limitations variational quantum algorithms on near-term (noisy) quantum computers as well as quantum annealers
* how to map applications to near-term (noisy) quantum computers
* basic concepts of quantum compilation

Skills
After successful completion of the module, students will be able to:
* implement quantum algorithms on real quantum computing devices
* assess the computational capabilities of near-term (noisy) quantum computers
* represent and manipulate quantum circuits with the diagrammatic calculus (ZX-calculus)

Competencies
Based on the knowledge and skills acquired in the module, students will be able to:
* orient themselves in field of near-term (noisy) quantum computation
* perform research on application focused quantum algorithms for near-term quantum computers
* perform research on quantum compilation for early quantum computers

**(!) Lernziele (EN)**  
Knowledge
After successful completion of the module students know:
* basics and limitations variational quantum algorithms on near-term (noisy) quantum computers as well as quantum annealers
* how to map applications to near-term (noisy) quantum computers
* basic concepts of quantum compilation

Skills
After successful completion of the module, students will be able to:
* implement quantum algorithms on real quantum computing devices
* assess the computational capabilities of near-term (noisy) quantum computers
* represent and manipulate quantum circuits with the diagrammatic calculus (ZX-calculus)

Competencies
Based on the knowledge and skills acquired in the module, students will be able to:
* orient themselves in field of near-term (noisy) quantum computation
* perform research on application focused quantum algorithms for near-term quantum computers
* perform research on quantum compilation for early quantum computers

**(!) Inhalt (DE)**  
This lecture is an interdisciplinary (computer science, physics, ; mathematics) introduction to modern algorithmic challenges as they occur in the usage of near-term quantum computing hardware. After reviewing the basics of quantum computation and algorithms we will cover the limitation of current quantum computing hardware from a computer ; scientist’s point of view. Next, we will discuss algorithms for near-term quantum computers, before we turn to quantum compilation, i.e. algorithms for the optimal mapping of quantum algorithms to real quantum computers. Finally, we will cover the basics of quantum error correction and mitigation. This lecture is an interdisciplinary (physics, computer science, mathematics) introduction quantum algorithms for near-term quantum computing hardware. After reviewing the basics of quantum computation and algorithms we will cover the limitation of current quantum computing hardware. Next, we will discuss algorithms for near-term quantum computers, which is the main focus of the lecture. Finally, we turn to quantum compilation, i.e. algorithms for the optimal mapping of quantum algorithms to real quantum computers, as well as quantum error mitigation and correction.

**(!) Inhalt (EN)**  
This lecture is an interdisciplinary (computer science, physics, ; mathematics) introduction to modern algorithmic challenges as they occur in the usage of near-term quantum computing hardware. After reviewing the basics of quantum computation and algorithms we will cover the limitation of current quantum computing hardware from a computer ; scientist’s point of view. Next, we will discuss algorithms for near-term quantum computers, before we turn to quantum compilation, i.e. algorithms for the optimal mapping of quantum algorithms to real quantum computers. Finally, we will cover the basics of quantum error correction and mitigation. This lecture is an interdisciplinary (physics, computer science, mathematics) introduction quantum algorithms for near-term quantum computing hardware. After reviewing the basics of quantum computation and algorithms we will cover the limitation of current quantum computing hardware. Next, we will discuss algorithms for near-term quantum computers, which is the main focus of the lecture. Finally, we turn to quantum compilation, i.e. algorithms for the optimal mapping of quantum algorithms to real quantum computers, as well as quantum error mitigation and correction.

**(!) Literatur**  
...



### 5a. SPOen, in denen das Modul verankert werden soll

*[Erläuterungen zu Abschnitt 5a/5b](../Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) Studiengangskürzel | (!) SPO-Version | (!) Modulbereich                   |
|------------------------|-----------------|------------------------------------|
| MSInf                  | 2009            | Wahlpflichtbereiche > Theoretische Informatik |
| MSInf                  | 2023            | Wahlpflichtbereiche > Theoretische Informatik |
| MSSSE                  | 2011            | Wahlpflichtbereiche > Theoretical Foundations of Software Systems Engineering > Wahlpflichtbereich Theoretische Grundlagen des SSE |
| MSDaSci                | 2018            | Vertiefungsbereich >  Computer Science > Wahlpflichtmodule aus Modulkatalog Computer Science |
| MSDaSci                | 2018            | Vertiefungsbereich > Computer Science and Mathematics > Computer Science |




### 5b. SPOen mit abweichenden Verwendungsspezifika


Studiengangskürzel/SPO-Version/Modulbereich:  
...

Abweichende(s) Verwendungsspezifikum/-a:  
...



### Interne Kommentare (gehören NICHT zur Moduldefinition)
Neues Modul für den Wahlpflichtbereich Theorie im Master von Prof. Dominique Unruh.
