
## Brückenkurs Datenbanken (1225948)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSDaSci 2018**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Brückenkurs Datenbanken
| (!) Modultitel (EN)     | Bridge Course Databases
| (!) ECTS                | 4
| (!) Gültig ab           | 2021-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Prof. Dr. Stefan Decker, PD Dr. Ralf Klamma
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Klausur Brückenkurs Datenbanken | Exam Bridge Course Databases | 4 | 0 |
| Vorlesung Brückenkurs Datenbanken | Lecture Bridge Course Databases |  | 4 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSDaSci2018 | Brückenkurse für Profil Mathematik |
| keine Angabe | MSDaSci2018 | Brückenkurse für Profil Physik |
| keine Angabe | MSDaSci2018 | Zusätzliche Prüfungsleistungen |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSDaSci2018 | Brückenkurse für Profil Mathematik |
| keine Angabe | MSDaSci2018 | Brückenkurse für Profil Physik |
| keine Angabe | MSDaSci2018 | Zusätzliche Prüfungsleistungen |

**(!) Prüfungsbedingungen (DE)**  
Mündliche Prüfung (100 %).

**(!) Prüfungsbedingungen (EN)**  
Oral Examination (100 %).


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Keine.

**Empfohlene Voraussetzungen (EN)**  
None.

**(!) Lernziele (DE)**  
Wissen: Nach dem erfolgreichen Abschluss des Kurses wissen die Studierenden über verschiedene fundamentale Datenmodelle. Sie kennen Datenmanipulations- und Anfragesprachen, sowie Datenbankentwurfstheorien und -sprachen. Sie wissen Bescheid über die Verwaltung von Datenbanksystemen und Standards. Fähigkeiten: Die Studierenden können Datenbanken entwerfen, sie verwalten, sie in verschiedenen Fragen anfragen und kleine Programme für Datenbankanwendungen schreiben. Kompetenzen: ; Die Studierenden haben Kompetenzen in formalen Datenbanksprachen und der praktischen Lösung von Aufgaben der Datenverwaltung und -verarbeitung.

**(!) Lernziele (EN)**  
Knowledge: After successful completion of the course, the students know about different fundamental data models, data manipulation and query languages, database design theories and languages, the management of database systems, and standards. Skills: The students are able to design databases, to manage them, querying them in different languages and write small programs for database applications. Competences: The students are able to deal with formal database languages and to solve practical data management and engineering tasks.

**(!) Inhalt (DE)**  
Ein gemischter Brückenlernkurs über Datenbanken für Masterstudierende. Grundlage dieses Brückenkurses über Datenbanken für Masterstudierende ist ein selbst-gesteuerter Kurs, der von Prof. Jennifer Widom von der Stanford Universität angeboten wird. Lernfortschritte und Beratung werden in einer wöchentlichen Sprechstunde angeboten. Mündliche Prüfungen werden in der vorlesungsfreien Zeit angeboten. Wissen über Datenbanken Datenmodelle  Relationale Datenbanken XML Daten JSON Daten  Anfrage Relationaler Datenbanken  Relationale Algebra SQL  Anfrage von XML Datenbanken  XPath und XQuery XLST  Databankentwurf  Relationale Entwurfstheorie Unified Modeling Language (UML)  SQL Fortgeschrittene Eigenschaften  Indexe und Transaktionen Constraints und Triggers Views und Autorisierung On-line Analytical Pro 

**(!) Inhalt (EN)**  
A blended learning database bridge course for master students. Foundation of this database bridge course for master students is the self-paced database course offered online by Jennifer Widom from Stanford University. Feedback and advice will be offered in a weekly office hour. An oral examination is offered in the lecture free time. Knowledge on Database Systems Data Models  Relational Databases XML Data JSON Data  Querying Relational Databases  Relational Algebra SQL  Querying XML Databases  XPath and XQuery XLST  Database Design  Relational Design Theory Unified Modeling Language (UML)  SQL Advanced Features  Indexes and Transactions Constraints and Triggers Views and Authorization On-line Analytical Pro 

**(!) Literatur**  
Elmasri R., Navathe S.B.: ;Fundamentals of Database Systems, Addison Wesley, 6th Edition, 2010


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSDaSci2018 | Brückenkurse für Profil Mathematik |  |
| MSDaSci2018 | Brückenkurse für Profil Physik |  |
| MSDaSci2018 | Zusätzliche Prüfungsleistungen |  |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|