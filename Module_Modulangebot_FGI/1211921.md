
## Computer Vision 2 (1211921)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V2**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Computer Vision 2
| (!) Modultitel (EN)     | Computer Vision 2
| (!) ECTS                | 6
| (!) Gültig ab           | 2018-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Universitätsprofessor Dr. sc. techn. Bastian Leibe
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Vorlesung Computer Vision 2 | Lecture Computer Vision 2 |  | 3 |
| Übung Computer Vision 2 | Exercise Computer Vision 2 | 0 | 1 |
| Masterprüfung Computer Vision 2 | Exam Computer Vision 2 | 6 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSETITTI2018 | Individuelle Module |
| None. | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| None. | MSDaSci2018 | Computer Science |
| None. | MSDaSci2018 | Computer Science |
| keine Angabe | MSRoboSys2018 | Elective Courses |
| keine Angabe | BSInf2018 | Wahlpflicht Angewandte Informatik |
| keine Angabe | MSRoboSys2019 | Elective Courses |
| None. | MSMI2019 | Multimedia-Technologie |
| None | ConRo2020 | Computer Science |
| keine Angabe | MSRoboSys2021 | Individuelle Module |
| keine Angabe | BSInf2022 | Wahlpflicht Angewandte Informatik |
| None. | MSInf2023 | Module aus dem Bereich Grafik und Interaktion |
| keine Angabe | MSETITTI2010 | zusätzliche Prüfungsleistungen |
| Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | BSInf2010 | Wahlpflicht Angewandte Informatik |
| keine Angabe | BSCES2011 | Mastervorzugsfächer |
| keine Angabe | MSALLGMB2011 | Individuelle Module |
| keine Angabe | MSPT2011 | Individuelle Module |
| keine Angabe | MSCES2011 | Individuelle Module |
| keine Angabe | MSCES2011 | Individuelle Module |
| keine Angabe | MSCES2011 | Individuelle Module |
| keine Angabe | MSSiSc2010 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSPSE2013 | Zusätzliche Prüfungsleistungen |
| None. | MSInf2009 | Angewandte Informatik |
| keine Angabe | MSInf2009 | Zusätzliche Prüfungsleistungen |
| Keine. | MSSSE2011 | Applied Computer Science |
| None. | MSMI2005 | Multimedia-Technologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSETITTI2018 | Individuelle Module |
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | MSRoboSys2018 | Elective Courses |
| keine Angabe | BSInf2018 | Wahlpflicht Angewandte Informatik |
| keine Angabe | MSRoboSys2019 | Elective Courses |
| keine Angabe | MSMI2019 | Multimedia-Technologie |
| keine Angabe | ConRo2020 | Computer Science |
| keine Angabe | MSRoboSys2021 | Individuelle Module |
| keine Angabe | BSInf2022 | Wahlpflicht Angewandte Informatik |
| keine Angabe | MSInf2023 | Module aus dem Bereich Grafik und Interaktion |
| keine Angabe | MSETITTI2010 | zusätzliche Prüfungsleistungen |
| keine Angabe | BSInf2010 | Wahlpflicht Angewandte Informatik |
| keine Angabe | BSCES2011 | Mastervorzugsfächer |
| keine Angabe | MSALLGMB2011 | Individuelle Module |
| keine Angabe | MSPT2011 | Individuelle Module |
| keine Angabe | MSCES2011 | Individuelle Module |
| keine Angabe | MSCES2011 | Individuelle Module |
| keine Angabe | MSCES2011 | Individuelle Module |
| keine Angabe | MSSiSc2010 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSPSE2013 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSInf2009 | Angewandte Informatik |
| keine Angabe | MSInf2009 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSSSE2011 | Applied Computer Science |
| keine Angabe | MSMI2005 | Multimedia-Technologie |

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Grundlegende Kenntnisse in Linearer Algebra, Wahrscheinlichkeitstheorie und Statistik werden empfohlen. Die erfolgreiche Teilnahme an der Vorlesung Computer Vision wird empfohlen.

**Empfohlene Voraussetzungen (EN)**  
Basic knowledge of linear algebra, probability theory, and statistics are recommended. Successful completion of the lecture Computer Vision is recommended.

**(!) Lernziele (DE)**  
Kenntnisse: Nach erfolgreicher Teilnahme an den Modulveranstaltungen haben die Vorlesungsteilnehmer Kenntnisse und Fähigkeiten in den Themenfeldern, die unter Inhalt beschrieben werden, erworben. | Fertigkeiten: Vorlesungsteilnehmer können Methoden und Techniken, die es einer Maschine ermöglichen, Bilder und Videos zu analysieren und ihren Inhalt zu verstehen herleiten und erklären. Sie kennen die aktuellen Forschungstrends und -entwicklungen. Dadurch sind sie in der Lage, die grundlegenden Computer Vision Techniken, die für diese Fähigkeiten benötigt werden, auszuwählen. | Kompetenzen: Vorlesungsteilnehmer sind in der Lage, die behandelten Methoden selbstständig auf reale Probleme anzuwenden. Sie sind in der Lage, die vorgestellten Algorithmen selbst zu implementieren und diese in einer Programmiersprache ihrer Wahl umzusetzen.

**(!) Lernziele (EN)**  
Knowledge: On successful completion of this module, lecture participants should be able to recall and explain the theoretical foundations underlying Computer Vision techniques in the areas mentioned under “Content”. | Skills: Lecture participants can derive and explain methods and techniques that enable a machine to analyze the content of images and videos and to derive an understanding of the image content. They know the current research trends and developments. This enables them to select the basic Computer Vision techniques necessary for those capabilities. | Competences: Lecture participants are able to apply the covered methods to real problems on their own. They are able to implement the covered algorithms themselves in a language of their choice.

**(!) Inhalt (DE)**  
Single-Object Tracking, Recursive Bayesian Filtering, Multi-Object Tracking, Visual Odometry, SLAM

**(!) Inhalt (EN)**  
Single-Object Tracking, Recursive Bayesian Filtering, Multi-Object Tracking, Visual Odometry, SLAM

**(!) Literatur**  
R. Szeliski, Computer Vision - Algorithms and Applications, Springer, 2010. | S. Thrun, W. Burgard, D. Fox, Probabilistic Robotics, MIT Press, 2006. | Multiple View Geometry, R. Hartley, A. Zisserman, 2nd edition, Cambridge University Press, 2003 | An Invitation to 3D Vision, Y. Ma, S. Soatto, J. Kosecka, S. Sastry, Springer, 2003.


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSETITTI2018 | Individuelle Module | V2 |
| MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| MSRoboSys2018 | Elective Courses | V2 |
| BSInf2018 | Wahlpflicht Angewandte Informatik | V2 |
| MSRoboSys2019 | Elective Courses | V2 |
| MSMI2019 | Multimedia-Technologie | V2 |
| ConRo2020 | Computer Science | V2 |
| MSRoboSys2021 | Individuelle Module | V2 |
| BSInf2022 | Wahlpflicht Angewandte Informatik | V2 |
| MSInf2023 | Module aus dem Bereich Grafik und Interaktion | V2 |
| MSETITTI2010 | zusätzliche Prüfungsleistungen | V2 |
| BSInf2010 | Wahlpflicht Angewandte Informatik | V2 |
| BSCES2011 | Mastervorzugsfächer | V2 |
| MSALLGMB2011 | Individuelle Module | V2 |
| MSPT2011 | Individuelle Module | V2 |
| MSCES2011 | Individuelle Module | V2 |
| MSCES2011 | Individuelle Module | V2 |
| MSCES2011 | Individuelle Module | V2 |
| MSSiSc2010 | Individuelle Module | V2 |
| MSAT2013 | Individuelle Module | V2 |
| MSAT2013 | Individuelle Module | V2 |
| MSAT2013 | Individuelle Module | V2 |
| MSPSE2013 | Zusätzliche Prüfungsleistungen | V2 |
| MSInf2009 | Angewandte Informatik | V2 |
| MSInf2009 | Zusätzliche Prüfungsleistungen | V2 |
| MSSSE2011 | Applied Computer Science | V2 |
| MSMI2005 | Multimedia-Technologie | V2 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|
| Gültig ab  | 2018-10-01 -> 2019-04-01 | MSRoboSys2018 | Elective Courses | V2
| Gültig ab  | 2018-10-01 -> 2019-04-01 | MSRoboSys2019 | Elective Courses | V2