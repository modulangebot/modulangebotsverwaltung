
## Randomized Algorithms (1212705)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSMath 2018** in der Version **Angelegt über RWTH API als 1**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Randomized Algorithms
| (!) Modultitel (EN)     | Randomized Algorithms
| (!) ECTS                | 7
| (!) Gültig ab           | 2006-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Modulangebotsorganisator: Modulangebotsverantwortlicher InformatikModellierungsteamverantwortlicher: Dr. rer. nat. Katja PetzoldtModulverantworlicher: Universitätsprofessor Dr. rer. nat. Berthold Vöcking
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Vorlesung Randomized Algorithms |  |  | 3 |
| Übung Randomized Algorithms | Exercise Randomized Algorithms | 0 | 2 |
| Prüfungsleistung: Randomized Algorithms | Exam: Randomized Algorithms | 7 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| None | MSMath2018 | Anwendungsfach Informatik |
| Keine | MSMath2018 | Subsidiary Subject Informatics |
| None | MSMath2024 | Anwendungsfach Informatik |
| Keine | BSMath2010 | Anwendungsfach Informatik |
| keine Angabe | MSCES2011 | Individuelle Module |
| Keine | MSMath2009 | Anwendungsfach Informatik |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSMath2018 | Anwendungsfach Informatik |
| keine Angabe | MSMath2018 | Subsidiary Subject Informatics |
| keine Angabe | MSMath2024 | Anwendungsfach Informatik |
| keine Angabe | BSMath2010 | Anwendungsfach Informatik |
| keine Angabe | MSCES2011 | Individuelle Module |
| keine Angabe | MSMath2009 | Anwendungsfach Informatik |

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Keine.

**Empfohlene Voraussetzungen (EN)**  
None.

**(!) Lernziele (DE)**  
Knowledge about the most important techniques from the fields of randomized algorithms and probabilistic analysis of algorithms, Ability to use these techniques to design efficient algorithms in various application contexts

**(!) Lernziele (EN)**  


**(!) Inhalt (DE)**  
Las Vegas algorithms like, e.g., Randomized Quicksort and Randomized LP Solving, Monte Carlo algorithms like, e.g., the FastCut algorithm and Finger Printing, Probability Amplification, Occupancy problems (Balls and Bins, Hashing, Bloom Filters), Randomized Rounding, The Probabilistic Method and the Lovasz Local Lemma

**(!) Inhalt (EN)**  


**(!) Literatur**  
R. Motwani, P. Raghavan. Randomized Algorithms, Cambridge University Press, 1995; M. Mitzenmacher, E. Upfal, Probability and Computing, Cambridge University Press, 2005


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSMath2018 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
| MSMath2018 | Subsidiary Subject Informatics | Angelegt über RWTH API als 1 |
| MSMath2024 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
| BSMath2010 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
| MSCES2011 | Individuelle Module | Angelegt über RWTH API als 1 |
| MSMath2009 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|
| Gültig ab  | 2006-10-01 -> 2010-10-01 | MSMath2018 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2006-10-01 -> 2010-10-01 | MSMath2018 | Subsidiary Subject Informatics | Angelegt über RWTH API als 1
| Gültig ab  | 2006-10-01 -> 2010-10-01 | MSMath2024 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2006-10-01 -> 2010-10-01 | BSMath2010 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2006-10-01 -> 2010-10-01 | MSMath2009 | Anwendungsfach Informatik | Angelegt über RWTH API als 1