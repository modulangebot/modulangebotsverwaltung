
## Infinite Games (1212333)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **Angelegt über RWTH API als 1**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Infinite Games
| (!) Modultitel (EN)     | Infinite Games
| (!) ECTS                | 6
| (!) Gültig ab           | 2010-04-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Modulangebotsorganisator: Modulangebotsverantwortlicher InformatikModellierungsteamverantwortlicher: Dr. rer. nat. Katja PetzoldtModulverantworlicher: Universitätsprofessor i.R. Dr. rer. nat. Dr. h. c. Dr. h. c. Wolfgang Thomas
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Vorlesung Infinite Games | Lecture Infinite Games |  | 3 |
| Prüfung Infinite Games | Exam Infinite Games | 6 | 0 |
| Übung Infinite Games | Exercise Infinite Games |  | 2 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | BSInf2018 | Wahlpflicht Theoretische Informatik |
| None | MSMath2018 | Anwendungsfach Informatik |
| Keine | MSMath2018 | Subsidiary Subject Informatics |
| keine Angabe | BSInf2022 | Wahlpflicht Theoretische Informatik |
| None | MSMath2024 | Anwendungsfach Informatik |
| Courses of Theoretical Computer Science of Bachelor Curriculum Course Infinite Computations | BSInf2010 | Wahlpflicht Theoretische Informatik |
| Keine | BSMath2010 | Anwendungsfach Informatik |
| Keine | MSMath2009 | Anwendungsfach Informatik |
| None. | MSInf2009 | Theoretische Informatik |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | BSInf2018 | Wahlpflicht Theoretische Informatik |
| keine Angabe | MSMath2018 | Anwendungsfach Informatik |
| keine Angabe | MSMath2018 | Subsidiary Subject Informatics |
| keine Angabe | BSInf2022 | Wahlpflicht Theoretische Informatik |
| keine Angabe | MSMath2024 | Anwendungsfach Informatik |
| keine Angabe | BSInf2010 | Wahlpflicht Theoretische Informatik |
| keine Angabe | BSMath2010 | Anwendungsfach Informatik |
| keine Angabe | MSMath2009 | Anwendungsfach Informatik |
| keine Angabe | MSInf2009 | Theoretische Informatik |

**(!) Prüfungsbedingungen (DE)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the examination.

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Kenntnisse aus den Pflichtmodulen der Theoretischen Informatik und aus "Infinite Computations".

**Empfohlene Voraussetzungen (EN)**  
Knowledge from the compulsory modules of theoretical computer science and from "Infinite Computations".

**(!) Lernziele (DE)**  
Knowledge: Algorithmic theory of infinite games, connection between automata theory and the theory of infinite games Skills: Usage of infinite games as a model for reactive systems Competences: Ability to apply game theoretic concepts and algorithms in logic as well as in the verification and synthesis of systems

**(!) Lernziele (EN)**  
 Knowledge of infinite games as a model for reactive systems Understanding of the algorithmic content of the theory of infinite games Ability to apply game theoretic concepts and algorithms in logic as well as in the verification and synthesis of systems 

**(!) Inhalt (DE)**  
 Graph-based games and the associated problem of solution Regular winning conditions for infinite games Solution of reachability games and Büchi games Solution of Muller games and parity games Application to automata on infinite trees Decidability of MSO-logic and other logics over infinite trees Outlook 1: Mean pay-off games Outlook 2: Games on infinite graphs, the Borel hierarchy 

**(!) Inhalt (EN)**  
 Graph-based games and the associated problem of solution Regular winning conditions for infinite games Solution of reachability games and Büchi games Solution of Muller games and parity games Application to automata on infinite trees Decidability of MSO-logic and other logics over infinite trees Outlook 1: Mean pay-off games Outlook 2: Games on infinite graphs, the Borel hierarchy 

**(!) Literatur**  
W. Thomas, Automata and Reactive Systems, Lecture Notes, RWTH Aachen 2003


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| BSInf2018 | Wahlpflicht Theoretische Informatik | Angelegt über RWTH API als 1 |
| MSMath2018 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
| MSMath2018 | Subsidiary Subject Informatics | Angelegt über RWTH API als 1 |
| BSInf2022 | Wahlpflicht Theoretische Informatik | Angelegt über RWTH API als 1 |
| MSMath2024 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
| BSInf2010 | Wahlpflicht Theoretische Informatik | Angelegt über RWTH API als 1 |
| BSMath2010 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
| MSMath2009 | Anwendungsfach Informatik | Angelegt über RWTH API als 1 |
| MSInf2009 | Theoretische Informatik | Angelegt über RWTH API als 1 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|
| ECTS | 6 -> 7 | MSMath2018 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2010-04-01 -> 2016-04-01 | MSMath2018 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| ECTS | 6 -> 7 | MSMath2018 | Subsidiary Subject Informatics | Angelegt über RWTH API als 1
| Gültig ab  | 2010-04-01 -> 2016-04-01 | MSMath2018 | Subsidiary Subject Informatics | Angelegt über RWTH API als 1
| ECTS | 6 -> 7 | MSMath2024 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2010-04-01 -> 2016-04-01 | MSMath2024 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| ECTS | 6 -> 7 | BSMath2010 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2010-04-01 -> 2016-04-01 | BSMath2010 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| ECTS | 6 -> 7 | MSMath2009 | Anwendungsfach Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2010-04-01 -> 2016-04-01 | MSMath2009 | Anwendungsfach Informatik | Angelegt über RWTH API als 1