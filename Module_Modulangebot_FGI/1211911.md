## High-performance Matrix Computations (1211911)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V2**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | High-performance Matrix Computations
| (!) Modultitel (EN)     | High-performance Matrix Computations
| (!) ECTS                | 6
| (!) Gültig ab           | 2018-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Modulangebotsorganisator:Modulangebotsverantwortlicher InformatikModellierungsteamverantwortlicher: Dr. rer. nat. Katja PetzoldtModulverantworlicher: Universitätsprofessor Paolo Bientinesi Ph. D.
| (!) Sprache             | Englisch
| (!) Turnus              | Unregelmäßig
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Lecture High-performance Matrix Computations | Lecture High-performance Matrix Computations |  | 3 |
| Exam High-performance Matrix Computations | Exam High-performance Matrix Computations | 6 | 0 |
| Exercise High-performance Matrix Computations | Exercise High-performance Matrix Computations | 0 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| Voraussetzung:<br>· - Numerical linear algebra.<br>· - Principles of algorithms and programming.<br>· - Familiarity with Matlab and C.<br><br>Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| Numerical linear algebra.<br><br>Principles of algorithms and programming.<br><br>Familiarity with Matlab and C.<br><br>Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflicht Angewandte Informatik |
| Zulassungsvoraussetzung: Lösen von Übungsaufgaben | MSMath2018 | Anwendungsfach Informatik |
| keine Angabe | MSMath2018 | Subsidiary Subject Informatics |
| keine Angabe | BSInf2022 | Wahlpflicht Angewandte Informatik |
| Zulassungsvoraussetzung: Lösen von Übungsaufgaben | MSMath2024 | Anwendungsfach Informatik |
| Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | BSInf2010 | Wahlpflicht Angewandte Informatik |
| keine Angabe | BSMath2010 | Anwendungsfach Informatik |
| keine Angabe | MSCES2011 | Wissenschaftliches Rechnen |
| keine Angabe | MSCES2011 | Wissenschaftliches Rechnen |
| keine Angabe | MSCES2011 | Wissenschaftliches Rechnen |
| keine Angabe | MSSiSc2010 | Computer Science |
| Zulassungsvoraussetzung: Lösen von Übungsaufgaben | MSMath2009 | Anwendungsfach Informatik |
| keine Angabe | MSPhy2013 | Informatik (Computer Science) |
| Voraussetzung: <br>- Numerical linear algebra or equivalent courses.<br>- Principles of algorithms and programming. <br>- Familiarity with Matlab and C.<br><br>Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben.<br> | MSInf2009 | Angewandte Informatik |
| Voraussetzung:  <br>Numerical linear algebra.  <br>Principles of algorithms and programming. <br>Familiarity with Matlab and C.<br><br>Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben.  <br> | MSSSE2011 | Applied Computer Science |
| Voraussetzung: <br>" Numerical linear algebra. <br>" Principles of algorithms and programming. <br>" Familiarity with Matlab and C.<br><br>Students must pass the exercises to be admitted to the examination. Details will be provided in the lecture. | MSMI2005 | Rechner- und Kommunikationstechnologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflicht Angewandte Informatik |
| keine Angabe | MSMath2018 | Anwendungsfach Informatik |
| keine Angabe | MSMath2018 | Subsidiary Subject Informatics |
| keine Angabe | BSInf2022 | Wahlpflicht Angewandte Informatik |
| keine Angabe | MSMath2024 | Anwendungsfach Informatik |
| keine Angabe | BSInf2010 | Wahlpflicht Angewandte Informatik |
| keine Angabe | BSMath2010 | Anwendungsfach Informatik |
| keine Angabe | MSCES2011 | Wissenschaftliches Rechnen |
| keine Angabe | MSCES2011 | Wissenschaftliches Rechnen |
| keine Angabe | MSCES2011 | Wissenschaftliches Rechnen |
| keine Angabe | MSSiSc2010 | Computer Science |
| keine Angabe | MSMath2009 | Anwendungsfach Informatik |
| keine Angabe | MSPhy2013 | Informatik (Computer Science) |
| keine Angabe | MSInf2009 | Angewandte Informatik |
| keine Angabe | MSSSE2011 | Applied Computer Science |
| keine Angabe | MSMI2005 | Rechner- und Kommunikationstechnologie |

**(!) Prüfungsbedingungen (DE)**  
The grading results from 100% of the final exam of this module. The exam can be a written or an oral exam. The final form of the examination is announced at the beginning of the lecture. If it is intended that homework will count for the examination grade, the respective paragraphs of the examination regulations have to be followed. The exam is done at the end of the lecture period.

**(!) Prüfungsbedingungen (EN)**  
The grading results from 100% of the final exam of this module. The exam can be a written or an oral exam. The final form of the examination is announced at the beginning of the lecture. If it is intended that homework will count for the examination grade, the respective paragraphs of the examination regulations have to be followed. The exam is done at the end of the lecture period.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Voraussetzung:
Knowledge from Numerical linear algebra.Principles of algorithms and Knowledge from programming.Familiarity with Matlab and C.

**Empfohlene Voraussetzungen (EN)**  
Voraussetzung:
Knowledge from Numerical linear algebra.Principles of algorithms and Knowledge from programming.Familiarity with Matlab and C.

**(!) Lernziele (DE)**  
Knowledge: On successful completion of this module, students will have knowledge about efficient numerical algorithms for linear systems and eigenproblemsparallel architectures standard numerical linear algebra libraries: BLAS, LAPACK, MKL, Elemental performance metrics: efficiency, strong &; weak scalability, time, space Skills: They should be able to write high-performance code for matrix operations in C design workqueue-based &; multi-threaded parallel algorithms design message-passing-based parallel algorithms Competences: Based on the knowledge and skills acquired, they should be able to identify opportunities for parallelism in matrix operations tailor algorithms for a specific target architecture (shared memory architectures, distributed memory architectures, accelerators)

**(!) Lernziele (EN)**  
Knowledge: On successful completion of this module, students will have knowledge about efficient numerical algorithms for linear systems and eigenproblems parallel architectures standard numerical linear algebra libraries: BLAS, LAPACK, MKL, Elemental performance metrics: efficiency, strong &; weak scalability, time, space Skills: They should be able to write high-performance code for matrix operations in C design workqueue-based &; multi-threaded parallel algorithms design message-passing-based parallel algorithms Competences: Based on the knowledge and skills acquired, they should be able to identify opportunities for parallelism in matrix operations tailor algorithms for a specific target architecture (shared memory architectures, distributed memory architectures, accelerators)

**(!) Inhalt (DE)**  
The course centers around the idea of developing efficient numerical algorithms for matrix computations through a synergy between mathematics and architectures. The focus is on the most common linear algebra operations: linear systems and eigenproblems. The objective is to attain high performance on a variety of parallel architectures: multi-core processors, GPUs, distributed and hybrid systems.

Topics covered: generalized, standard and tridiagonal eigenproblems, matrix factorizations, linear systems.

The students are expected to participate in practical programming exercises. The languages of choice are Matlab and C.

**(!) Inhalt (EN)**  
The course centers around the idea of developing efficient numerical algorithms for matrix computations through a synergy between mathematics and architectures. The focus is on the most common linear algebra operations: linear systems and eigenproblems. The objective is to attain high performance on a variety of parallel architectures: multi-core processors, GPUs, distributed and hybrid systems.

Topics covered: generalized, standard and tridiagonal eigenproblems, matrix factorizations, linear systems.

The students are expected to participate in practical programming exercises. The languages of choice are Matlab and C.

**(!) Literatur**  
Lecture slides Diaries from the computer sessions G. Golub, C. van Loan. Matrix Computations. Third Edition, 1996. G. Meurant. Computer Solution of Large Linear Systems. North Holland, Amsterdam, 1999<


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| BSInf2018 | Wahlpflicht Angewandte Informatik | V2 |
| MSMath2018 | Anwendungsfach Informatik | V2 |
| MSMath2018 | Subsidiary Subject Informatics | V2 |
| BSInf2022 | Wahlpflicht Angewandte Informatik | V2 |
| MSMath2024 | Anwendungsfach Informatik | V2 |
| BSInf2010 | Wahlpflicht Angewandte Informatik | V2 |
| BSMath2010 | Anwendungsfach Informatik | V2 |
| MSCES2011 | Wissenschaftliches Rechnen | V2 |
| MSCES2011 | Wissenschaftliches Rechnen | V2 |
| MSCES2011 | Wissenschaftliches Rechnen | V2 |
| MSSiSc2010 | Computer Science | V2 |
| MSMath2009 | Anwendungsfach Informatik | V2 |
| MSPhy2013 | Informatik (Computer Science) | V2 |
| MSInf2009 | Angewandte Informatik | V2 |
| MSSSE2011 | Applied Computer Science | V2 |
| MSMI2005 | Rechner- und Kommunikationstechnologie | V2 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|
| ECTS | 6 -> 7 | MSMath2018 | Anwendungsfach Informatik | V2
| ECTS | 6 -> 7 | MSMath2018 | Subsidiary Subject Informatics | V2
| ECTS | 6 -> 7 | MSMath2024 | Anwendungsfach Informatik | V2
| ECTS | 6 -> 7 | MSMath2009 | Anwendungsfach Informatik | V2
