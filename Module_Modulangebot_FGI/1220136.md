
## Advanced Process Mining (1220136)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V1**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Advanced Process Mining
| (!) Modultitel (EN)     | Advanced Process Mining
| (!) ECTS                | 6
| (!) Gültig ab           | 2019-04-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Universitätsprofessor Professor h. c. Dr. h. c. Dr. ir. Wil van der Aalst
| (!) Sprache             | Englisch
| (!) Turnus              | Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Advanced Process Mining (Vorlesung) | Advanced Process Mining (Lecture) |  | 3 |
| Advanced Process Mining (Übung) | Advanced Process Mining (Exercise) | 0 | 2 |
| Advanced Process Mining (Klausur) | Advanced Process Mining (Written Exam) | 6 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| None. | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| None. | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflicht Daten- und Informationsmanagement |
| None. | MSMI2019 | Rechner- und Kommunikationstechnologie |
| None | MSCSS2021 | Electives Computer Science |
| None | MSCSS2021 | Electives Computer Science |
| None | MSCSS2021 | Electives Computer Science |
| keine Angabe | BSInf2022 | Wahlpflicht Daten- und Informationsmanagement |
| None. | MSInf2023 | Module aus dem Bereich KI & Daten |
| keine Angabe | BSMTIKI2023 | Mastervorzugsfächer |
| keine Angabe | MSMTIKI2023 | Vertiefungsbereich Daten- und Informationsmanagement |
| keine Angabe | MSMTIKI2023 | Spezialbereich Digitalisierung in der Produktion |
| keine Angabe | MSSiSc2010 | Individuelle Module |
| keine Angabe | MSMMECAME2016 | Zusätzliche Prüfungsleistungen |
| None. | MSInf2009 | Daten- und Informationsmanagement |
| keine Angabe | MSInf2009 | Zusätzliche Prüfungsleistungen |
| None. | MSSSE2011 | Data and Information Management |
| keine Angabe | MSTKI2013 | Vertiefungsbereich Daten- und Informationsmanagement |
| None. | MSMI2005 | Rechner- und Kommunikationstechnologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflicht Daten- und Informationsmanagement |
| keine Angabe | MSMI2019 | Rechner- und Kommunikationstechnologie |
| keine Angabe | MSCSS2021 | Electives Computer Science |
| keine Angabe | MSCSS2021 | Electives Computer Science |
| keine Angabe | MSCSS2021 | Electives Computer Science |
| keine Angabe | BSInf2022 | Wahlpflicht Daten- und Informationsmanagement |
| keine Angabe | MSInf2023 | Module aus dem Bereich KI & Daten |
| keine Angabe | BSMTIKI2023 | Mastervorzugsfächer |
| keine Angabe | MSMTIKI2023 | Vertiefungsbereich Daten- und Informationsmanagement |
| keine Angabe | MSMTIKI2023 | Spezialbereich Digitalisierung in der Produktion |
| keine Angabe | MSSiSc2010 | Individuelle Module |
| keine Angabe | MSMMECAME2016 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSInf2009 | Daten- und Informationsmanagement |
| keine Angabe | MSInf2009 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSSSE2011 | Data and Information Management |
| keine Angabe | MSTKI2013 | Vertiefungsbereich Daten- und Informationsmanagement |
| keine Angabe | MSMI2005 | Rechner- und Kommunikationstechnologie |

**(!) Prüfungsbedingungen (DE)**  
Die Modulprüfung besteht aus den folgenden Teilleistungen: Schriftliche Hausarbeit (40 %); Klausur (60 %). Voraussetzung zum Bestehen des Moduls ist das Bestehen jeder Teilleistung. Es ist nicht möglich, Teilleistungen in ein Folgesemester zu übertragen. ;

**(!) Prüfungsbedingungen (EN)**  
The module examination consists of the following partial qualifications: Written homework (40 %); Written exam or oral examination (60 %). Students must pass all parts of the examination individually to pass the module. It is not possible to transfer parts of the ;


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
 Basic Knowledge of Process Mining Concepts Basic Knowledge of Discrete Mathematics Basic Knowledge of Petri nets   

**Empfohlene Voraussetzungen (EN)**  
 Basic Knowledge of Process Mining Concepts Basic Knowledge of Discrete Mathematics Basic Knowledge of Petri nets   

**(!) Lernziele (DE)**  
Kenntnisse: Nach erfolgreichem Abschluss dieses Moduls sollten die Studierenden in der Lage sein: verschiedene Klassen von Petrinetzen zu verstehen und zu begreifen, z.B. (relaxierte) sound Petrinetze, fortgeschrittene Prozesserkennungsalgorithmen tiefgehend zu verstehen, verschiedene Parametrisierungen der Berechnung von Alignments von Prozessmodellen und Ereignisdaten zu verstehen, die Prinzipien des Decomposed Process Mining und die Prinzipien des Stream Based Process Mining zu verstehen. | Fertigkeiten: Die Studenten sollten in der Lage sein, die verschiedenen Arten von Process Mining Tools, die in einer Vielzahl von Process Mining Tools wie ProM, RapidProM, PM4Py, Disco, Celonis, ProcessGold, etc. verfügbar sind, anzuwenden. Darüber hinaus sind die Studierenden in der Lage, die erzielten Ergebnisse und Gründe für die Grenzen der Anwendbarkeit der verschiedenen Werkzeuge und Algorithmen kritisch zu analysieren. | Kompetenzen: Basierend auf den in diesem Kurs erworbenen Kenntnissen und Fähigkeiten sollten die Studenten in der Lage sein, fortgeschrittene Process Mining Algorithmen auf reale industrielle Probleme und Datensätze anzuwenden und verschiedene damit zusammenhängende Fragen von Geschäftsinhabern zu beantworten, die mit dem Prozess zusammenhängen.

**(!) Lernziele (EN)**  
After taking this course students should:
• have a detailed understanding of the entire process mining spectrum and be able to relate process mining techniques to other analysis techniques (data mining, model checking, simulation, machine learning, etc.),
• understand the positioning of process mining in the context of data science, process management and "big data",
• be able to apply a range of process mining techniques and use tools such as RapidMiner, ProM, and Disco,
• be able to design analysis workflows and execute them on concrete practical datasets (e.g., using RapidMiner and ProM),
• be able to conduct experiments to investigate the influence of noise (infre-quent/deviating behavior) on the process mining results,
• be able to read formal descriptions of process mining techniques and reason about their properties,
• understand the intricate relation between observed behavior (e.g., events logs in XES or MXML format) and modeled behavior (e.g., Petri nets with an initial and final marking),
• understand and apply advanced process discovery techniques using language-based regions (ILP miner),
• be able to discuss all four conformance dimensions (replay fitness, precision, generali-zation, and simplicity), provide metrics for these dimensions, and apply conformance checking using models and logs,
• be able to reason about the strengths and weaknesses of existing process mining algo-rithms and critically evaluate new ones,
• understand and create alignments as a tool for conformance checking and other types of analysis that require the mapping of observed behavior onto modeled behavior,
• understand the limitations of process techniques in terms of computation time, memory use, and data requirements,
• be able to decompose large process mining problems (discovery and conformance checking) into smaller ones (using valid decompositions),
• understand the relation between the results of decomposed process mining and non-decomposed process mining (e.g., what properties are preserved and what guarantees can be given), and
• be able to conduct real-world process mining projects using real data and imprecise questions from stakeholders.

**(!) Inhalt (DE)**  
Process Mining bietet ein neues Tool zur Verbesserung von Prozessen in einer Vielzahl von Anwendungsbereichen. Es gibt zwei Haupttreiber für diese neue Technologie. Zum einen werden immer mehr Vorfälle aufgezeichnet und liefern so detaillierte Informationen über die Prozesshistorie. Andererseits besteht in den meisten Unternehmen die Notwendigkeit, die Prozessleistung (z.B. zur Reduzierung von Kosten und Durchlaufzeiten) und die Compliance (z.B. zur Vermeidung von Abweichungen oder Risiken) zu verbessern. Process Mining schließt die Lücke zwischen modellbasierten Prozessanalysen (z.B. Simulation, Modellprüfung und klassischen BPM-Techniken) und datenorientierten Techniken (z.B. Data-Mining-Techniken wie Klassifizierung, Clustering und Regression). Process-Mining-Techniken können in einer Vielzahl von Bereichen eingesetzt werden. | Schriftliche Hausarbeit (PM Assignment 1) behandelt praktische Erfahrungen mit Process Mining und die Bewertung von Process-Mining-Techniken und der Wirkung von Rauschen. Schriftliche Hausarbeit (PM Assignment 2) behandelt praktisches Process Mining auf Basis des realen Ereignisprotokolls.

**(!) Inhalt (EN)**  
Process mining provides a new means to improve processes in a variety of application domains. There are two main drivers for this new technology. On the one hand, more and more events are being recorded thus providing detailed information about the history of processes. On the other hand, in most organizations, there is a need to improve process performance (e.g. to reduce costs and flow time) and compliance (e.g. to avoid deviations or risks). Process mining bridges the gap between model-based process analyses (e.g., simulation, model checking, and classical BPM techniques) and data-oriented techniques (e.g. data mining techniques like classification, clustering, and regression). Process mining techniques can be applied in a variety of domains. Written homework (PM Assignment 1) includes Hands-on experience with process mining and includes evaluating process mining techniques and the effect of noise. Written homework (PM Assignment 2) includes Real-world process mining based on real-life event log.

**(!) Literatur**  
Examples of mandatory papers that need to be studied in detail:
J.M.E.M. van der Werf, B.F. van Dongen, C.A.J. Hurkens, and A. Serebrenik. Process Discovery using Integer Linear Programming. Fundamenta Informaticae, 94: 387-412, 2010.
W.M.P. van der Aalst, A. Adriansyah, and B. van Dongen. Replaying History on Process Models for Conformance Checking and Performance Analysis. WIREs Data Mining and Knowledge Discovery, 2(2):182-192, 2012.
Selected parts of A. Adriansyah, B. van Dongen, and W.M.P. van der Aalst. Memory-Efficient Alignment of Observed and Modeled Behavior. BPM Center Report BPM-13-03, 2013
W.M.P. van der Aalst. Decomposing Petri Nets for Process Mining: A Generic Ap-proach. Distributed and Parallel Databases, 31(4):471-507, 2013.
The textbook "W.M.P. van der Aalst. Process Mining: Data Science in Action. Springer-Verlag, Berlin, 2016" (http://springer.com/9783662498507) is advised as background information.
Additional background information (optional, just for context or clarification):
W.M.P. van der Aalst. Process Mining. Communications of the ACM, 55(8):76-83, 2012.
IEEE Task Force on Process Mining. Process Mining Manifesto. 2011. http://www.win.tue.nl/ieeetfpm/.
A. Rozinat and W.M.P. van der Aalst. Conformance Checking of Processes Based on Monitoring Real Behavior. Information Systems, 33(1):64-95, 2008.
W.M.P. van der Aalst and C. Stahl. Modeling Business Processes: A Petri Net Oriented Approach. MIT press, Cambridge, MA, 2011.
R. Lorenz, J. Desel, G. Juhás. Models from Scenarios. T. Petri Nets and Other Models of Concurrency 7: 314-371, 2013.
A. Adriansyah. Aligning Observed and Modeled Behavior. PhD Thesis Technische Universiteit Eindhoven, 2014.


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science | V1 |
| MSDaSci2018 | Computer Science | V1 |
| BSInf2018 | Wahlpflicht Daten- und Informationsmanagement | V1 |
| MSMI2019 | Rechner- und Kommunikationstechnologie | V1 |
| MSCSS2021 | Electives Computer Science | V1 |
| MSCSS2021 | Electives Computer Science | V1 |
| MSCSS2021 | Electives Computer Science | V1 |
| BSInf2022 | Wahlpflicht Daten- und Informationsmanagement | V1 |
| MSInf2023 | Module aus dem Bereich KI & Daten | V1 |
| BSMTIKI2023 | Mastervorzugsfächer | V1 |
| MSMTIKI2023 | Vertiefungsbereich Daten- und Informationsmanagement | V1 |
| MSMTIKI2023 | Spezialbereich Digitalisierung in der Produktion | V1 |
| MSSiSc2010 | Individuelle Module | V1 |
| MSMMECAME2016 | Zusätzliche Prüfungsleistungen | V1 |
| MSInf2009 | Daten- und Informationsmanagement | V1 |
| MSInf2009 | Zusätzliche Prüfungsleistungen | V1 |
| MSSSE2011 | Data and Information Management | V1 |
| MSTKI2013 | Vertiefungsbereich Daten- und Informationsmanagement | V1 |
| MSMI2005 | Rechner- und Kommunikationstechnologie | V1 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|
| ECTS | 6 ->  | MSSiSc2010 | Individuelle Module | V1
| Gültig ab  | 2019-04-01 -> 2023-04-01 | MSTKI2013 | Vertiefungsbereich Daten- und Informationsmanagement | V1