
## Außercurriculares Modul im Wahlpflichtbereich Software- Entwicklungsmethoden und -Werkzeuge Master Informatik (1229893)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2023**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Außercurriculares Modul im Wahlpflichtbereich Software- Entwicklungsmethoden und -Werkzeuge Master Informatik
| (!) Modultitel (EN)     | Extracurricular Module in Elective Software methods, tools Master Computer Science
| (!) ECTS                | 
| (!) Gültig ab           | 2023-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| keine Angabe
| (!) Sprache             | keine Angabe
| (!) Turnus              | keine Angabe
| (!) Moduldauer          | keine Angabe
| (!) Modulniveau         | keine Angabe
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Außercurriculares Modul im Wahlpflichtbereich Software- Entwicklungsmethoden und -werkzeuge Master Informatik | Extracurricular Module in Elective Software methods, tools Master Computer Science |  |  |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSInf2023 | Individuelle Module |
| keine Angabe | MSInf2023 | Individuelle Module |
| keine Angabe | MSInf2023 | Individuelle Module |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSInf2023 | Individuelle Module |
| keine Angabe | MSInf2023 | Individuelle Module |
| keine Angabe | MSInf2023 | Individuelle Module |

**(!) Prüfungsbedingungen (DE)**  
keine Angabe

**(!) Prüfungsbedingungen (EN)**  
keine Angabe


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
keine Angabe

**Empfohlene Voraussetzungen (EN)**  
keine Angabe

**(!) Lernziele (DE)**  
keine Angabe

**(!) Lernziele (EN)**  
keine Angabe

**(!) Inhalt (DE)**  
keine Angabe

**(!) Inhalt (EN)**  
keine Angabe

**(!) Literatur**  
keine Angabe


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSInf2023 | Individuelle Module |  |
| MSInf2023 | Individuelle Module |  |
| MSInf2023 | Individuelle Module |  |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|