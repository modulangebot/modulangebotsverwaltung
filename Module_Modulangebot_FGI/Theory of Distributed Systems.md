## Theory of Distributed Systems - NEU (1231551)

### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](../Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Moduleigenschaft            | Inhalt                                                 |
|-----------------------------|--------------------------------------------------------|
| (!) Modultitel (DE)         | Theory of Distributed Systems                               |
| (!) Modultitel (EN)         | Theory of Distributed Systems                               | 
| (!) ECTS                    | 6                                                      |
| (!) Gültig ab               | SoSe 2025                             |
| Gültig bis                  |                                            |
| (!) ModulanbieterIn         | Prof. Dr. Martin Hoefer                                 |
| (!) Sprache                 | Englisch                                               |
| (!) Turnus                  | Unregelmäßig                                           |
| (!) Moduldauer              | Einsemestrig                                           |
| (!) Modulniveau             | Master                                                 |
| (!) Fachsemester            | 1                                                      |



### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](../Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)       | (!) ECTS | (!) Präsenzzeit (SWS) | 
|-------------------------------|-------------------------------------|----------|-----------------------|
| Vorlesung Theory of Distributed Systems | Lecture Theory of Distributed Systems  | 0     | 3         |
| Übung Theory of Distributed Systems | Excercise Theory of Distributed Systems  | 0     | 2         |
| Prüfung Theory of Distributed Systems | Exam Theory of Distributed Systems  | 6     | 0         |

<!-- Zeilen bei Bedarf kopieren -->


### 3. Studien- und Prüfungsleistungen
*[Erläuterungen zu Abschnitt 3](../Anleitungen_Hilfen/3_Leistungen.md)*

**(!) Teilnahmevoraussetzungen (DE)**  
Keine.

**(!) Teilnahmevoraussetzungen (EN)**  
None.

**(!) Prüfungsbedingungen (DE)**  

Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Hausaufgaben.


**(!) Prüfungsbedingungen (EN)**  

Written exam or oral examination (100 %). Students must pass written homework to be admitted to the module examination.



### 4. Informationen für das Modulhandbuch
*[Erläuterungen zu Abschnitt 4](../Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*

<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
-->

**(!) Empfohlene Voraussetzungen (DE)**  

Basic knowledge about algorithms, discrete structures, and probability theory.

**(!) Empfohlene Voraussetzungen (EN)**  

Basic knowledge about algorithms, discrete structures, and probability theory.

**(!) Lernziele (DE)**  
**Knowledge:** After successful completion of the module students know
* how to describe foundational problems arising in distributed systems and explain algorithmic solutions for these problems.

**Skills:** After successful completion of the module, students will be able to
* apply general algorithmic design principles like randomized contention resolution and congestion avoidance and to use techniques like load balancing and randomization to solve problems arising in network contexts.

**Competencies:** Based on the knowledge and skills acquired in the module, students will be able to
* model distributed systems in a formal way and to develop algorithmic solutions enabling the efficient usage of computer networks and other distributed systems.

**(!) Lernziele (EN)**  

**Knowledge:** After successful completion of the module students know
* how to describe foundational problems arising in distributed systems and explain algorithmic solutions for these problems.

**Skills:** After successful completion of the module, students will be able to
* apply general algorithmic design principles like randomized contention resolution and congestion avoidance and to use techniques like load balancing and randomization to solve problems arising in network contexts.

**Competencies:** Based on the knowledge and skills acquired in the module, students will be able to
* model distributed systems in a formal way and to develop algorithmic solutions enabling the efficient usage of computer networks and other distributed systems.

**(!) Inhalt (DE)**  

* Algorithms in message-passing systems
* Leader election and consensus
* Routing in networks: centralized and distributed approaches
* Randomized methods for contention resolution and congestion avoidance
* Contagion and Distributed Network Dynamics
* Game theoretic models and solution concept

**(!) Inhalt (EN)**  

* Algorithms in message-passing systems
* Leader election and consensus
* Routing in networks: centralized and distributed approaches
* Randomized methods for contention resolution and congestion avoidance
* Contagion and Distributed Network Dynamics
* Game theoretic models and solution concept

**(!) Literatur**  
* Leighton. Introduction to Parallel Algorithms and Architectures: Arrays, Trees, Hypercubes
* Kurose, Ross: Computer Networking: A Top-Down Approach Featuring the Internet. Addison Wesley Longman, 1999.
* Kleinberg, Tardos: Algorithm Design, Addison Wesley Pearson, 2005



### 5a. SPOen, in denen das Modul verankert werden soll

*[Erläuterungen zu Abschnitt 5a/5b](../Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) Studiengangskürzel | (!) SPO-Version | (!) Modulbereich                                     |
|------------------------|-----------------|------------------------------------------------------|
| MSDaSci                | 2018            | Vertiefungsbereich > Computer Science and Mathematics > Computer Science |
| MSDaSci                | 2018            | Vertiefungsbereich > Computer Science > Wahlpflichtmodule aus Modulkatalog Computer Science|
| MSInf                  | 2023            | Wahlpflichtbereiche > Theoretische Informatik > Module aus dem Bereich Theoretische Informatik |
| MSInf                  | 2009            | Theoretische Informatik                    |
| BSInf                  | 2022            | Mastervorzug Theoretische Informatik                       |
| BSInf                  | 2018            | Mastervorzug Theoretische Informatik                       |
<!-- Zeilen bei Bedarf kopieren -->



### 5b. SPOen mit abweichenden Verwendungsspezifika

<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

Studiengangskürzel/SPO-Version/Modulbereich:  
...

Abweichende(s) Verwendungsspezifikum/-a:  
...



### Interne Kommentare (gehören NICHT zur Moduldefinition)

Verknüpfungen:
* AK mit GHK 186499
* PK:
    * Übung mit GHK 186500
    * Prüfung mit GHK 186501
