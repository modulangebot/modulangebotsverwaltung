
## Introduction to Numerical Methods and Software (1220996)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V2**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Introduction to Numerical Methods and Software
| (!) Modultitel (EN)     | Introduction to Numerical Methods and Software
| (!) ECTS                | 6
| (!) Gültig ab           | 2021-04-01
|  Gültig bis             | 2024-03-31
| (!)&nbsp;ModulanbieterIn| Universitätsprofessor Dr. rer. nat. Uwe Naumann
| (!) Sprache             | Englisch
| (!) Turnus              | Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Bachelor/Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Introduction to Numerical Methods and Software (Exam) | Introduction to Numerical Methods and Software (Exam) | 6 | 0 |
| Introduction to Numerical Methods and Software (Exercise) | Introduction to Numerical Methods and Software (Exercise) | 0 | 2 |
| Introduction to Numerical Methods and Software (Lecture) | Introduction to Numerical Methods and Software (Lecture) |  | 3 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | BSInf2018 | Wahlpflichtbereich Angewandte Informatik |
| keine Angabe | BSInf2018 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSMI2019 | Rechner- und Kommunikationstechnologie |
| keine Angabe | BSInf2022 | Wahlpflichtbereich Angewandte Informatik |
| keine Angabe | MSInf2009 | Auflagen |
| keine Angabe | MSSSE2011 | Applied Computer Science |
| keine Angabe | MSMI2005 | Rechner- und Kommunikationstechnologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | BSInf2018 | Wahlpflichtbereich Angewandte Informatik |
| keine Angabe | BSInf2018 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSMI2019 | Rechner- und Kommunikationstechnologie |
| keine Angabe | BSInf2022 | Wahlpflichtbereich Angewandte Informatik |
| keine Angabe | MSInf2009 | Auflagen |
| keine Angabe | MSSSE2011 | Applied Computer Science |
| keine Angabe | MSMI2005 | Rechner- und Kommunikationstechnologie |

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100 %).

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %).


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Introduction to Calculus; Introduction to Linear Algebra

**Empfohlene Voraussetzungen (EN)**  
Introduction to Calculus; Introduction to Linear Algebra

**(!) Lernziele (DE)**  
 Knowledge: understanding of fundamental numerical methods. Skills: ability to implement numerical algorithms. Competences: algorithmic aspects of numerical methods 

**(!) Lernziele (EN)**  
 Knowledge: understanding of fundamental numerical methods. Skills: ability to implement numerical algorithms. Competences: algorithmic aspects of numerical methods 

**(!) Inhalt (DE)**  
 Essential Calculus Essential Linear Algebra Computer Arithmetic Error Analysis and Problem Condition Algorithmic Differentiation Systems of Nonlinear Equations Unconstrained Convex Optimization Linear Regression Nonlinear Regression Numerical Software 

**(!) Inhalt (EN)**  
 Essential Calculus Essential Linear Algebra Computer Arithmetic Error Analysis and Problem Condition Algorithmic Differentiation Systems of Nonlinear Equations Unconstrained Convex Optimization Linear Regression Nonlinear Regression Numerical Software 

**(!) Literatur**  
 Set of slides Sample programs References to relevant current literature and online materials 


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| BSInf2018 | Wahlpflichtbereich Angewandte Informatik | V2 |
| BSInf2018 | Zusätzliche Prüfungsleistungen | V2 |
| MSMI2019 | Rechner- und Kommunikationstechnologie | V2 |
| BSInf2022 | Wahlpflichtbereich Angewandte Informatik | V2 |
| MSInf2009 | Auflagen | V2 |
| MSSSE2011 | Applied Computer Science | V2 |
| MSMI2005 | Rechner- und Kommunikationstechnologie | V2 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|