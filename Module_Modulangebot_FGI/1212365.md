
## Einführung in die Informatik (1212365)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSETITTI 2018** in der Version **Angelegt über RWTH API als 1**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Einführung in die Informatik
| (!) Modultitel (EN)     | Introduction to Computer science
| (!) ECTS                | 4
| (!) Gültig ab           | 2006-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Modulangebotsorganisator: Modulangebotsverantwortlicher InformatikModellierungsteamverantwortlicher: Dr. rer. nat. Katja PetzoldtModulverantworlicher: Unbekannt
| (!) Sprache             | Deutsch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Bachelor/Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Vorlesung: ";Einführung in die Informatik"; | Lecture Introduction to Computer science |  | 2 |
| Übung: ";Einführung in die Informatik"; | Tutorial Introduction to Computer Science | 0 | 2 |
| Klausur: ";Einführung in die Informatik"; | Exam Introduction to Computer Science | 4 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSETITTI2018 | Individuelle Module |
| None | BAGW2019 | Perspektive Text und Argument |
| keine Angabe | MSGRM2019 | Zusätzliche Prüfungsleistungen |
| None | BSBau2020 | Zusätzliche Prüfungsleistungen |
| None | BAPLuS2021 | Interdisziplinärer Bereich |
| keine Angabe | LABGyGeInf2023 | Pflichtfächer |
| keine Angabe | LABErwGyGeInf2023 | Pflichtfächer |
| keine Angabe | BSETITTI2017 | Individuelle Module |
| - In den Übungen besteht Anwesenheitspflicht, da die Einübung des wissenschaftlichen Diskurses zu den Lernzielen der Veranstaltung gehört. | BSAngGeo2013 | Nebenfach Informatik |
| - Voraussetzung für die Zulassung zur Modulprüfung ist die erfolgreiche Bearbeitung von Übungs- und Programmieraufgaben <br>- In den Übungen besteht Anwesenheitspflicht, da die Einübung des wissenschaftlichen Diskurses zu den Lernzielen der Veranstaltung gehört | BSAngGeo2013 | Wahlpflichtbereich Informatik |
| In den Übungen besteht Anwesenheitspflicht, da die Einübung des wissenschaftlichen Diskurses zu den Lernzielen der Veranstaltung gehört. | MSAngGeo2013 | Nebenfach Informatik |
| keine | MSWiGeo2013 | Nebenfach Informatik |
| None | BALiSp2017 | Perspektive Informatik |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSETITTI2018 | Individuelle Module |
| keine Angabe | BAGW2019 | Perspektive Text und Argument |
| keine Angabe | MSGRM2019 | Zusätzliche Prüfungsleistungen |
| keine Angabe | BSBau2020 | Zusätzliche Prüfungsleistungen |
| keine Angabe | BAPLuS2021 | Interdisziplinärer Bereich |
| keine Angabe | LABGyGeInf2023 | Pflichtfächer |
| keine Angabe | LABErwGyGeInf2023 | Pflichtfächer |
| keine Angabe | BSETITTI2017 | Individuelle Module |
| keine Angabe | BSAngGeo2013 | Nebenfach Informatik |
| keine Angabe | BSAngGeo2013 | Wahlpflichtbereich Informatik |
| keine Angabe | MSAngGeo2013 | Nebenfach Informatik |
| keine Angabe | MSWiGeo2013 | Nebenfach Informatik |
| keine Angabe | BALiSp2017 | Perspektive Informatik |

**(!) Prüfungsbedingungen (DE)**  
b) Klausur zu aa) und ab) und Lösung von Übungsaufgaben   Die Modulnote wird entsprechend der CP-Verteilung gewichtet.

**(!) Prüfungsbedingungen (EN)**  



### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Lösung von Übungsaufgaben

**Empfohlene Voraussetzungen (EN)**  


**(!) Lernziele (DE)**  
Erwerb der folgenden Kenntnisse und Fähigkeiten:
- spezielles Wissen über Hintergrund, Bedienung und Möglichkeiten aktueller Computersysteme
- Einführung in die prinzipielle Funktionsweise von Rechnern, Grundzüge und Konzepte von Betriebssystemen
- konzeptionelles Wissen über die Benutzung moderner Rechnersysteme anhand der Befehlssprachen von Betriebssystemen
- Umgang mit wichtigen Dienst- und Anwendungsprogrammen, Editoren, Textverarbeitungs- sowie Datenbanksysteme
- moderne Netzwerkdienste
- in Übungen: Betriebssysteme samt spezifischer Anwendungssoftware; Schwerpunkte: Anwendung von Befehls-Prozeduren, E-Mail, Umgang mit dem Internet, Interprozesskommunikation, Datenbanken

**(!) Lernziele (EN)**  


**(!) Inhalt (DE)**  
aa) +ab) Vorlesung/ Übung: "Einführung in die Informatik"  -Was ist Informatik? (Informatik Programmierung) - Grundlagen (u.a. Informations-/Zahlendarstellung, Anwendungsprogramme), - Rechnerstrukturen (u.a. Boolsche Algebra), - Betriebssysteme (am Beispiel von UNIX), - Rechnernetze (u.a. Protokolle und Netze, Netztechnologien), - Internet (u.a. Dienste im Internet, WWW), - Datenbanksysteme (u.a. SQL), - IT-Sicherheit   b) Klausur zu aa) und ab) und Lösung von Übungsaufgaben

**(!) Inhalt (EN)**  


**(!) Literatur**  
Folien und Skripte zur Vorlesung
H. P. GUMM, M. SOMMER (2004): Einführung in die Informatik. Oldenbourg, München (6. Auflage).


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSETITTI2018 | Individuelle Module | Angelegt über RWTH API als 1 |
| BAGW2019 | Perspektive Text und Argument | Angelegt über RWTH API als 1 |
| MSGRM2019 | Zusätzliche Prüfungsleistungen | Angelegt über RWTH API als 1 |
| BSBau2020 | Zusätzliche Prüfungsleistungen | Angelegt über RWTH API als 1 |
| BAPLuS2021 | Interdisziplinärer Bereich | Angelegt über RWTH API als 1 |
| LABGyGeInf2023 | Pflichtfächer | Angelegt über RWTH API als 1 |
| LABErwGyGeInf2023 | Pflichtfächer | Angelegt über RWTH API als 1 |
| BSETITTI2017 | Individuelle Module | Angelegt über RWTH API als 1 |
| BSAngGeo2013 | Nebenfach Informatik | Angelegt über RWTH API als 1 |
| BSAngGeo2013 | Wahlpflichtbereich Informatik | Angelegt über RWTH API als 1 |
| MSAngGeo2013 | Nebenfach Informatik | Angelegt über RWTH API als 1 |
| MSWiGeo2013 | Nebenfach Informatik | Angelegt über RWTH API als 1 |
| BALiSp2017 | Perspektive Informatik | Angelegt über RWTH API als 1 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|
| ECTS | 4 -> 6 | BAGW2019 | Perspektive Text und Argument | Angelegt über RWTH API als 1
| Gültig ab  | 2006-10-01 -> 2021-10-01 | BAGW2019 | Perspektive Text und Argument | Angelegt über RWTH API als 1
| ECTS | 4 -> 8 | BAPLuS2021 | Interdisziplinärer Bereich | Angelegt über RWTH API als 1
| Gültig ab  | 2006-10-01 -> 2017-11-24 | BSAngGeo2013 | Nebenfach Informatik | Angelegt über RWTH API als 1
| ECTS | 4 -> 5 | BSAngGeo2013 | Wahlpflichtbereich Informatik | Angelegt über RWTH API als 1
| ECTS | 4 -> 5 | BALiSp2017 | Perspektive Informatik | Angelegt über RWTH API als 1
| Gültig ab  | 2006-10-01 -> 2021-10-01 | BALiSp2017 | Perspektive Informatik | Angelegt über RWTH API als 1