## Research Focus Class on Data Ecosystems - NEU (1231482)

### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](../Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Moduleigenschaft            | Inhalt                                                 |
|-----------------------------|--------------------------------------------------------|
| (!) Modultitel (DE)         | Research Focus Class on Data Ecosystems                               |
| (!) Modultitel (EN)         | Research Focus Class on Data Ecosystems                               | 
| (!) ECTS                    | 6                                                      |
| (!) Gültig ab               | WiSe 2024/2025                             |
| Gültig bis                  |                                            |
| (!) ModulanbieterIn         | JProf. Dr. Sandra Geisler                                 |
| (!) Sprache                 | Deutsch/Englisch                                               |
| (!) Turnus                  | Unregelmäßig                                           |
| (!) Moduldauer              | Einsemestrig                                           |
| (!) Modulniveau             | Master                                                 |
| (!) Fachsemester            | 1                                                      |



### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](../Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)       | (!) ECTS | (!) Präsenzzeit (SWS) | 
|-------------------------------|-------------------------------------|----------|-----------------------|
| Veranstaltung Research Focus Class on Data Ecosystems| Course Research Focus Class on Data Ecosystems  | 0        | 2                     |
| Prüfung Research Focus Class on Data Ecosystems  | Exam Research Focus Class on Data Ecosystems     | 6        | 0                     |
<!-- Zeilen bei Bedarf kopieren -->


### 3. Studien- und Prüfungsleistungen
*[Erläuterungen zu Abschnitt 3](../Anleitungen_Hilfen/3_Leistungen.md)*

**(!) Teilnahmevoraussetzungen (DE)**  
Keine.

**(!) Teilnahmevoraussetzungen (EN)**  
None.

**(!) Prüfungsbedingungen (DE)**  

Die Modulprüfung besteht aus den folgenden Teilleistungen: Projektarbeit oder Praktikum (70 %); Kolloquium (30 %).


**(!) Prüfungsbedingungen (EN)**  

The module examination consists of the following components: Project work or lab (70 %); colloquium (30 %).



### 4. Informationen für das Modulhandbuch
*[Erläuterungen zu Abschnitt 4](../Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*

<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
-->

**(!) Empfohlene Voraussetzungen (DE)**  

Kenntnisse aus den Veranstaltungen „Datenbanken und Informationssysteme“, „Implementation of Databases“, Data Stream Management and Analysis, oder Seminar „Data Ecosystems“

**(!) Empfohlene Voraussetzungen (EN)**  

Knowledge from the courses “Databases and Information Systems”, “Implementation of Databases”, Data Stream Management and Analysis, or seminar “Data Ecosystems”

**(!) Lernziele (DE)**  
**Kenntnisse:** Nach erfolgreichem Abschluss des Moduls kennen Studierende
* Kenntnisse der wichtigsten Konzepte und aktuellen Trends im Bereich Datenökosysteme. Präsentation des eigenen Forschungsthemas in Form eines wissenschaftlichen Textes, eines Vortrags oder eines Posters.
* Methoden für ein strukturiertes wissenschaftliches Arbeiten an einem gegebenen oder selbstdefinierten Thema. Methoden der systematischen Literaturrecherche und -diskussion zu einem wissenschaftlichen Thema. Konzepte im Forschungsbetrieb (Konferenzen, Journals, andere Veröffentlichungen).

**Fähigkeiten:** Nach erfolgreichem Abschluss des Moduls können Studierende
* Selbständiges strukturiertes wissenschaftliches Arbeiten (sowohl praktisch als auch theoretisch) an einem gegebenen oder selbstdefinierten Thema. Literaturrecherche und -diskussion zu einem wissenschaftlichen Thema.

**Kompetenzen:** Auf der Basis der im Modul erworbenen Kenntnisse und Fähigkeiten sind Studierende in der Lage
* Vertiefte Kenntnis eines ausgewählten, aktuellen Themas
* Fähigkeit zur eigenständigen Einarbeitung in ein Forschungsthema.

**(!) Lernziele (EN)**  

**Knowledge:** After successfully completing the module, students will have
* Knowledge of the most important concepts and current trends in the field of data ecosystems. Presentation of their own research topic in the form of a scientific text, a lecture or a poster. 
* Methods for structured scientific work on a given or self-defined topic. Methods of systematic literature research and discussion on a scientific topic. Concepts in research operations (conferences, journals, other publications).

**Skills:** After successfully completing the module, students will be able to
* Independent structured scientific work (both practical and theoretical) on a given or self-defined topic. Research and discuss literature on a scientific topic.

**Competencies:** Based on the knowledge and skills acquired in the module, students are able to
* In-depth knowledge of a selected, current topic
* ability to independently familiarize themselves with a research topic.

**(!) Inhalt (DE)**  

Dieser forschungsorientierte Kurs richtet sich an Studierende, die Interesse an aktuellen Fragestellungen, Entwicklungen und Forschung im Bereich Datenökosysteme haben. Es wird ein ausgewähltes Thema aus einem der folgenden Bereiche von Datenökosystemen diskutiert: data sovereignty, data exchange, data protection, data security, FAIR data. usw. Es erfolgt zunächst eine Einführung in den aktuellen Stand der Forschung zum ausgewählten Themenbereich. Im Folgenden soll jeder Studierende unter Anleitung eine Fragestellung (Forschungsidee) im Themenbereich identifizieren und sich in diese einarbeiten und den anderen Teilnehmern präsentieren. Nach dieser Konzeptphase folgt eine Praxisphase, in der die Studierenden ihre Forschungsidee ausarbeiten (prototypische Implementierung, Analyse, Simulation, usw.) und evaluieren. Der genaue Ablauf kann von Semester zu Semester sowie je nach Themenbereich variieren.

**(!) Inhalt (EN)**  

This research-oriented course targets students who are interested in current issues, developments and research in the field of data ecosystems. A selected topic from one of the following areas of data ecosystems will be discussed: Data sovereignty, data exchange, data protection, data security, FAIR data. etc. First, an introduction to the current state of research on the selected topic will be given. Subsequently, each student will identify a question (research idea) in the subject area under guidance, familiarize themselves with it, and present it to the other participants. This concept phase is followed by a practical phase in which the students elaborate and evaluate their research idea (prototype implementation, analysis, simulation, etc.). The exact procedure may vary from semester to semester and depending on the subject area.

**(!) Literatur**  




### 5a. SPOen, in denen das Modul verankert werden soll

*[Erläuterungen zu Abschnitt 5a/5b](../Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) Studiengangskürzel | (!) SPO-Version | (!) Modulbereich                                     |
|------------------------|-----------------|------------------------------------------------------|
| MSDaSci                | 2018            | Vertiefungsbereich > Computer Science and Mathematics > Computer Science |
| MSDaSci                | 2018            | Vertiefungsbereich > Computer Science > Wahlpflichtmodule aus Modulkatalog Computer Science|
| MSInf                  | 2023            | Wahlpflichtbereiche > KI & Daten > Module aus dem Bereich KI & Daten |
| MSInf                  | 2009            | Daten- und Informationsmanagement                    |
| MSSSE                  | 2011            | Wahlpflichtbereiche > Data and Information Management|
| BSInf                  | 2022            | Mastervorzug KI & Daten                       |
| BSInf                  | 2018            | Mastervorzug KI & Daten                       |
<!-- Zeilen bei Bedarf kopieren -->



### 5b. SPOen mit abweichenden Verwendungsspezifika

<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

Studiengangskürzel/SPO-Version/Modulbereich:  
...

Abweichende(s) Verwendungsspezifikum/-a:  
...



### Interne Kommentare (gehören NICHT zur Moduldefinition)
