## Industrial Data Security - NEU (1231539)



<!--
=== Allgemeine Hinweise: ===
- PFLICHTfelder sind mit "(!)" markiert.
- Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.  
- Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! -
  nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
- Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->



### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](../Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Moduleigenschaft            | Inhalt                                                 |
|-----------------------------|--------------------------------------------------------|
| (!) Modultitel (DE)         | Industrial Data Security                               |
| (!) Modultitel (EN)         | Industrial Data Security                               | 
| (!) ECTS                    | 6                                                      |
| (!) Gültig ab               | SoSe 2025                             |
| Gültig bis                  |                                            |
| (!) ModulanbieterIn         | Prof. Dr. Martin Henze                                 |
| (!) Sprache                 | Englisch                                               |
| (!) Turnus                  | Unregelmäßig                                           |
| (!) Moduldauer              | Einsemestrig                                           |
| (!) Modulniveau             | Master                                                 |
| (!) Fachsemester            | 1                                                      |



### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](../Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)       | (!) ECTS | (!) Präsenzzeit (SWS) | 
|-------------------------------|-------------------------------------|----------|-----------------------|
| Vorlesung Industrial Data Security| Lecture Industrial Data Security  | 0        | 3                     |
| Übung Industrial Data Security | Exercise Industrial Data Security | 0        | 1       |             
| Prüfung Industrial Data Security  | Exam Industrial Data Security     | 6        | 0                     |
<!-- Zeilen bei Bedarf kopieren -->


### 3. Studien- und Prüfungsleistungen
*[Erläuterungen zu Abschnitt 3](../Anleitungen_Hilfen/3_Leistungen.md)*

**(!) Teilnahmevoraussetzungen (DE)**  
Keine.

**(!) Teilnahmevoraussetzungen (EN)**  
None.

**(!) Prüfungsbedingungen (DE)**  

Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.


**(!) Prüfungsbedingungen (EN)**  

Written exam or oral examination (100 %). Students must pass written homework to be admitted to the examination.



### 4. Informationen für das Modulhandbuch
*[Erläuterungen zu Abschnitt 4](../Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*

<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
-->

**(!) Empfohlene Voraussetzungen (DE)**  

Basic knowledge in IT security, databases, and machine learning e.g., as taught in lectures such as "IT-Security", "Databases and Information Systems ", and "Elements of Machine Learning and Data Science".

**(!) Empfohlene Voraussetzungen (EN)**  

Basic knowledge in IT security, databases, and machine learning e.g., as taught in lectures such as "IT-Security", "Databases and Information Systems ", and "Elements of Machine Learning and Data Science".

**(!) Lernziele (DE)**  
This course will provide students with an understanding of the unique challenges of securely operating on industrial data and provide them with the necessary conceptual tools to properly secure industrial data.

**Knowledge:** Students will be familiar with the security challenges of industrial data, specifically focusing on unique properties of industrial data compared to other settings. Students will know about the various approaches to secure industrial data, covering different aspects of securing industrial data across files, databases, and machine learning. 

**Skills:** After taking this course, students will be able to select and apply suitable conceptual tools to realize comprehensive security of industrial data. Based on an assessment of the individual constraints in a given industrial scenario, students will be able to select those security measures that are best applicable and prioritize their deployment. 

**Competences:** Students will be able to identify and pinpoint challenges of securing industrial data across different domains. They will further be able to identify where standard security approaches can be adapted to secure industrial data and where dedicated security approaches are required.

**(!) Lernziele (EN)**  

This course will provide students with an understanding of the unique challenges of securely operating on industrial data and provide them with the necessary conceptual tools to properly secure industrial data.

**Knowledge:** Students will be familiar with the security challenges of industrial data, specifically focusing on unique properties of industrial data compared to other settings. Students will know about the various approaches to secure industrial data, covering different aspects of securing industrial data across files, databases, and machine learning. 

**Skills:** After taking this course, students will be able to select and apply suitable conceptual tools to realize comprehensive security of industrial data. Based on an assessment of the individual constraints in a given industrial scenario, students will be able to select those security measures that are best applicable and prioritize their deployment. 

**Competences:** Students will be able to identify and pinpoint challenges of securing industrial data across different domains. They will further be able to identify where standard security approaches can be adapted to secure industrial data and where dedicated security approaches are required.

**(!) Inhalt (DE)**  

This course provides an introduction into current security problems surrounding industrial data and state-of-the-art security solutions. After an introduction into industrial data, how it is stored and processes, as well as prevalent security issues surrounding industrial data, students will learn about approaches to secure industrial data across files, databases, and machine learning. Topics include encryption and integrity-protection of structured industrial data in files, database security in industrial contexts, as well as machine learning security for industrial data. Across all topics, a special emphasis will be put on the privacy-preserving processing of industrial data.

**(!) Inhalt (EN)**  

This course provides an introduction into current security problems surrounding industrial data and state-of-the-art security solutions. After an introduction into industrial data, how it is stored and processes, as well as prevalent security issues surrounding industrial data, students will learn about approaches to secure industrial data across files, databases, and machine learning. Topics include encryption and integrity-protection of structured industrial data in files, database security in industrial contexts, as well as machine learning security for industrial data. Across all topics, a special emphasis will be put on the privacy-preserving processing of industrial data.

**(!) Literatur**  

Lecture slides. Pointers to further relevant literature will be provided during the lecture.



### 5a. SPOen, in denen das Modul verankert werden soll

*[Erläuterungen zu Abschnitt 5a/5b](../Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) Studiengangskürzel | (!) SPO-Version | (!) Modulbereich                                     |
|------------------------|-----------------|------------------------------------------------------|
| MSDaSci                | 2018            | Vertiefungsbereich > Computer Science and Mathematics > Computer Science |
| MSDaSci                | 2018            | Vertiefungsbereich > Computer Science > Wahlpflichtmodule aus Modulkatalog Computer Science|
| MSInf                  | 2023            | Wahlpflichtbereiche > KI & Daten > Module aus dem Bereich KI & Daten |
| MSInf                  | 2009            | Daten- und Informationsmanagement                    |
| MSMI                   | 2019            | Rechner- und Kommunikationstechnologie               |
| MSSSE                  | 2011            | Wahlpflichtbereiche > Data and Information Management                      |
| MSSSE                  | 2025            | Wahlpflichtbereiche > KI & Daten > Module aus dem Bereich KI & Daten                      |
| MTKI                   | 2023            | Vertiefungsbereich > Vertiefungsbereich Daten- und Informationsmanagement |
| MTKI                   | 2023            | Vertiefungsbereiche > Spezialbereich Digitalisierung in der Produktion     |
| BSInf                  | 2022            | Mastervorzug KI & Daten                       |
| BSInf                  | 2018            | Mastervorzug KI & Daten                       |
<!-- Zeilen bei Bedarf kopieren -->



### 5b. SPOen mit abweichenden Verwendungsspezifika

<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

Studiengangskürzel/SPO-Version/Modulbereich:  
...

Abweichende(s) Verwendungsspezifikum/-a:  
...



### Interne Kommentare (gehören NICHT zur Moduldefinition)

**Verknüpfungen:**
* AK mit GHK 186175
* PK:
  * Übung mit GHK 186176
  * Prüfung mit GHK 186467