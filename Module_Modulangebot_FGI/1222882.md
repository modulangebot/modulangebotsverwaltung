
## Model-based Systems Engineering (1222882)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V1**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Model-based Systems Engineering
| (!) Modultitel (EN)     | Model-based Systems Engineering
| (!) ECTS                | 6
| (!) Gültig ab           | 2020-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| keine Angabe
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Prüfung Model-based Systems Engineering | Exam Model-based Systems Engineering | 6 | 0 |
| Übung Model-based Systems Engineering | Exercise Model-based Systems Engineering | 0 | 3 |
| Vorlesung Model-based Systems Engineering | Lecture Model-based Systems Engineering |  | 2 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflicht Software und Kommunikation |
| keine Angabe | MSMI2019 | Rechner- und Kommunikationstechnologie |
| keine Angabe | ConRo2020 | Computer Science Basics |
| keine Angabe | NPE2020 | Compulsory Courses |
| keine Angabe | NPE2020 | Individuelle Module |
| keine Angabe | MSMME2020 | Compulsory Elective Courses Engineering |
| keine Angabe | SPE2022 | Compulsory Courses |
| keine Angabe | MEdErwGyGeInf2022 | Module à 6 CP |
| keine Angabe | MEdErwBKInf2022 | Module à 6 CP |
| keine Angabe | BSInf2022 | Wahlpflicht Software und Kommunikation |
| keine Angabe | MSAT2023 | Individuelle Module |
| keine Angabe | MSInf2023 | Module aus dem Bereich Software-Entwicklungsmethoden und -werkzeuge |
| keine Angabe | BSMTIKI2023 | Mastervorzugsfächer |
| keine Angabe | MSMTIKI2023 | Vertiefungsbereich Software und Kommunikation |
| keine Angabe | MEdBKInf2017 | Module à 6 CP |
| keine Angabe | MEdGyGeInf2017 | Module à 6 CP |
| keine Angabe | MSALLGMB2011 | Individuelle Module |
| keine Angabe | MSALLGMB2011 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSInf2009 | Software und Kommunikation |
| keine Angabe | MSSSE2011 | Core Subjects Software Engineering |
| keine Angabe | MSTKI2013 | Vertiefungsbereich Software und Kommunikation |
| keine Angabe | MSMI2005 | Rechner- und Kommunikationstechnologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflicht Software und Kommunikation |
| keine Angabe | MSMI2019 | Rechner- und Kommunikationstechnologie |
| keine Angabe | ConRo2020 | Computer Science Basics |
| keine Angabe | NPE2020 | Compulsory Courses |
| keine Angabe | NPE2020 | Individuelle Module |
| keine Angabe | MSMME2020 | Compulsory Elective Courses Engineering |
| keine Angabe | SPE2022 | Compulsory Courses |
| keine Angabe | MEdErwGyGeInf2022 | Module à 6 CP |
| keine Angabe | MEdErwBKInf2022 | Module à 6 CP |
| keine Angabe | BSInf2022 | Wahlpflicht Software und Kommunikation |
| keine Angabe | MSAT2023 | Individuelle Module |
| keine Angabe | MSInf2023 | Module aus dem Bereich Software-Entwicklungsmethoden und -werkzeuge |
| keine Angabe | BSMTIKI2023 | Mastervorzugsfächer |
| keine Angabe | MSMTIKI2023 | Vertiefungsbereich Software und Kommunikation |
| keine Angabe | MEdBKInf2017 | Module à 6 CP |
| keine Angabe | MEdGyGeInf2017 | Module à 6 CP |
| keine Angabe | MSALLGMB2011 | Individuelle Module |
| keine Angabe | MSALLGMB2011 | Individuelle Module |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSInf2009 | Software und Kommunikation |
| keine Angabe | MSSSE2011 | Core Subjects Software Engineering |
| keine Angabe | MSTKI2013 | Vertiefungsbereich Software und Kommunikation |
| keine Angabe | MSMI2005 | Rechner- und Kommunikationstechnologie |

**(!) Prüfungsbedingungen (DE)**  
Projektarbeit (100%). Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Project (100%). Students must pass written homework to be admitted to the examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Introduction to software engineering or comparable courses.

**Empfohlene Voraussetzungen (EN)**  
Introduction to software engineering or comparable courses.

**(!) Lernziele (DE)**  
Knowledge:  SysML, UML MontiArc Architecture and behavior models Statecharts, finite automata Object diagrams and class diagrams Geometrical models and their connection to software controlling models Use of models in the software and systems engineering process Simulation, code, and test generation Analysis of models Evolution of models and systems  Skills:  Application of models in the development process Ability to read and write own models in appropriate languages  Competences:  Understanding of the use of models Application of models in software and systems engineering Knowledge and practice of SysML and UML Designing systems with a strong software impact by using model-based development techniques 

**(!) Lernziele (EN)**  
Knowledge:  SysML, UML MontiArc Architecture and behavior models Statecharts, finite automata Object diagrams and class diagrams Geometrical models and their connection to software controlling models Use of models in the software and systems engineering process Simulation, code, and test generation Analysis of models Evolution of models and systems  Skills:  Application of models in the development process Ability to read and write own models in appropriate languages  Competences:  Understanding of the use of models Application of models in software and systems engineering Knowledge and practice of SysML and UML Designing systems with a strong software impact by using model-based development techniques 

**(!) Inhalt (DE)**  
After a thorough and detailed introduction of SysML and UML, the possibilities of using models in system development processes are discussed. These include simulation, code and test case generation, analysis, modeling and evolution of systems by refactoring of models.

**(!) Inhalt (EN)**  
After a thorough and detailed introduction of SysML and UML, the possibilities of using models in system development processes are discussed. These include simulation, code and test case generation, analysis, modeling and evolution of systems by refactoring of models.

**(!) Literatur**  
[Rum17] B. Rumpe: Agile Modeling with UML: Code Generation, Testing, Refactoring. Springer International, May 2017. [CFJ+16] B. Combemale, R. France, J. Jézéquel, B. Rumpe, J. Steel, D. Vojtisek: Engineering Modeling Languages: Turning Domain Knowledge into Tools. Chapman &;; Hall/CRC Innovations in Software Engineering and Software Development Series, November 2016.


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science | V1 |
| MSDaSci2018 | Computer Science | V1 |
| BSInf2018 | Wahlpflicht Software und Kommunikation | V1 |
| MSMI2019 | Rechner- und Kommunikationstechnologie | V1 |
| ConRo2020 | Computer Science Basics | V1 |
| NPE2020 | Compulsory Courses | V1 |
| NPE2020 | Individuelle Module | V1 |
| MSMME2020 | Compulsory Elective Courses Engineering | V1 |
| SPE2022 | Compulsory Courses | V1 |
| MEdErwGyGeInf2022 | Module à 6 CP | V1 |
| MEdErwBKInf2022 | Module à 6 CP | V1 |
| BSInf2022 | Wahlpflicht Software und Kommunikation | V1 |
| MSAT2023 | Individuelle Module | V1 |
| MSInf2023 | Module aus dem Bereich Software-Entwicklungsmethoden und -werkzeuge | V1 |
| BSMTIKI2023 | Mastervorzugsfächer | V1 |
| MSMTIKI2023 | Vertiefungsbereich Software und Kommunikation | V1 |
| MEdBKInf2017 | Module à 6 CP | V1 |
| MEdGyGeInf2017 | Module à 6 CP | V1 |
| MSALLGMB2011 | Individuelle Module | V1 |
| MSALLGMB2011 | Individuelle Module | V1 |
| MSAT2013 | Individuelle Module | V1 |
| MSInf2009 | Software und Kommunikation | V1 |
| MSSSE2011 | Core Subjects Software Engineering | V1 |
| MSTKI2013 | Vertiefungsbereich Software und Kommunikation | V1 |
| MSMI2005 | Rechner- und Kommunikationstechnologie | V1 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|