## Basics of Machine Learning - NEU (1231553)

### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](../Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Moduleigenschaft            | Inhalt                                                 |
|-----------------------------|--------------------------------------------------------|
| (!) Modultitel (DE)         | Basics of Machine Learning                             |
| (!) Modultitel (EN)         | Basics of Machine Learning                             | 
| (!) ECTS                    | 3                                                      |
| (!) Gültig ab               | SoSe 2025                                              |
| Gültig bis                  |                                                        |
| (!) ModulanbieterIn         | Prof. Dr. Bastian Leibe                                |
| (!) Sprache                 | Englisch                                               |
| (!) Turnus                  | Unregelmäßig                                           |
| (!) Moduldauer              | Einsemestrig                                           |
| (!) Modulniveau             | Bachelor                                               |
| (!) Fachsemester            | 1                                                      |



### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](../Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)       | (!) ECTS | (!) Präsenzzeit (SWS) | 
|-------------------------------|-------------------------------------|----------|-----------------------|
| Vorlesung/Übung Basics of Machine Learning | Lecture/Excercise Basics of Machine Learning  | 0     | 2         |
| Prüfung Basics of Machine Learning | Exam Basics of Machine Learning  | 3     | 0         |

<!-- Zeilen bei Bedarf kopieren -->


### 3. Studien- und Prüfungsleistungen
*[Erläuterungen zu Abschnitt 3](../Anleitungen_Hilfen/3_Leistungen.md)*

**(!) Teilnahmevoraussetzungen (DE)**  
Keine.

**(!) Teilnahmevoraussetzungen (EN)**  
None.

**(!) Prüfungsbedingungen (DE)**  
Klausur (100 %). Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Hausaufgaben.


**(!) Prüfungsbedingungen (EN)**  
Written exam (100 %). Students must pass written homework to be admitted to the module examination.



### 4. Informationen für das Modulhandbuch
*[Erläuterungen zu Abschnitt 4](../Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*

<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
-->

**(!) Empfohlene Voraussetzungen (DE)**  
Keine.

**(!) Empfohlene Voraussetzungen (EN)**  
None.

**(!) Lernziele (DE)**  
**Kenntnisse:**
Nach erfolgreichem Abschluss des Moduls kennen Studierende  die generellen Themen der Künstlichen Intelligenz (im weiteren Sinne betrachtet), Standardmethoden, -modelle und -konzepte im Bereich Machine Learning und deren mathematische Grundlagen, sowie einige der wichtigsten Einschränkungen von Standardverfahren im Bereich Machine Learning, wie z. B. Over-Fitting.

**Fähigkeiten:**
Nach erfolgreichem Abschluss des Moduls können Studierende Standardalgorithmen im Bereich Machine Learning verstehen, analysieren und implementieren. Dazu gehören Methoden, die auf linearen Modellen (z. B. Regression, logistische Regression) und nichtlinearen Standardmodellen (Kernel-Methoden, Support Vector Machines) basieren. Studierende können die Grundlagen neuronaler Netze verstehen und wissen, wie sie trainiert werden können.

**Kompetenzen:**
Auf der Basis der im Modul erworbenen Kenntnisse und Fähigkeiten sind Studierende in der Lage,  Aufgaben als Probleme im Bereich Machine Learning zu abstrahieren und geeignete Methoden zu deren Lösung auszuwählen und einzusetzen, sowie die Qualität der Lösung zu bewerten. 

**(!) Lernziele (EN)**  
**Knowledge.**
After successful completion of the module students know  the general topics considered in artificial intelligence (considered in a broad sense) standard methods, models and concepts in Machine Learning and their mathematical underpinnings as well as some of the main limitations of standard machine learning procedures such as over-fitting.

**Skills:**
After successful completion of the module, students will be able to  understand, analyse and implement standard machine learning algorithms. This includes methods based on linear models (e.g., regression, logistic regression) and standard non-linear models (kernel methods, support vector machines). Students will be able to understand the basics of neural networks and how they can be trained.

**Competencies:**
Based on the knowledge and skills acquired in the module, students will be able to abstract tasks as machine learning problems and select and implement appropriate methods for their solution assess the quality of the solution 

**(!) Inhalt (DE)**  
**Einführung**
* Überblick über die Bereiche Data Science, Machine Learning und Künstliche Intelligenz im weiteren Sinne.
* Einführung in Standard-ML-Paradigmen: überwachtes Lernen, unüberwachtes Lernen, Verstärkungslernen
* Häufig vorkommende Daten und Vorverarbeitung: Zeitreihen, Bilder, Video

**Grundlagen**
* Selektiver Überblick über die Wahrscheinlichkeitsrechnung: allgemeine univariate und multivariate Verteilungen
* Selektiver Überblick über die Statistik: Maximum-Likelihood-Schätzung, empirische Risikominimierung, Regularisierung, Bayessche vs. Frequentistische Statistik
* Selektiver Überblick über Lineare Algebra und Optimierung: Singulärwertzerlegung, Spektralzerlegung, Gradientenmethoden
* Entscheidungstheorie: Klassifikationsprobleme, Regressionsprobleme

**Lineare Modelle**
* Lineare Diskriminanzanalyse: Naive Bayes, Generative vs. diskriminative Klassifikatoren
* Logistische Regression: Binäre logistische Regression, multinomiale logistische Regression
* Lineare Regression: Kleinste Quadrate, Ridge-Regression

**Nicht-parametrische Modelle**
* Bagging und Boosting

**Kernel-Methoden und Support-Vektor-Maschinen**

**Eine grundlegende Einführung in Deep Networks**
* Perceptron und mehrschichtige Perceptron Training von DNN mit Backpropagation 

**(!) Inhalt (EN)**  
**Introduction**
* Overview of the broader Data Science, Machine Learning, Artificial Intelligence space.
* Introduction to standard ML paradigms: supervised learning, unsupervised learning, reinforcement learning
* Commonly encountered data and preprocessing: time-series, images, video

**Foundations**
* Selective Review of Probability: common univariate and multivariate distributions
* Selective Review of Statistics: Maximum Likelihood estimation, Empirical Risk Minimization, Regularization, Bayesian vs Frequentist statistics
* Selective Review of Linear Algebra and Optimization: singular value decomposition, spectral decomposition, gradient methods
* Decision Theory: Classification problems, Regression problems

**Linear Models**
* Linear Discriminant Analysis: Naive Bayes, Generative vs. discriminative classifiers
* Logistic Regression: binary logistic regression, multinomial logistic regression
* Linear Regression: Least Squares, Ridge Regression

**Non-parametric Models**
* Bagging and Boosting

**Kernel methods and Support Vector machines**

**A shallow Introduction to Deep Networks**
* Perceptron and Multi-layer perceptron Training DNN with backpropagation 

**(!) Literatur**  




### 5a. SPOen, in denen das Modul verankert werden soll

*[Erläuterungen zu Abschnitt 5a/5b](../Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) Studiengangskürzel | (!) SPO-Version | (!) Modulbereich                                     |
|------------------------|-----------------|------------------------------------------------------|
| MSInf                  | 2023            | Auflagen                                             |

<!-- Zeilen bei Bedarf kopieren -->



### 5b. SPOen mit abweichenden Verwendungsspezifika

<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

Studiengangskürzel/SPO-Version/Modulbereich:  
...

Abweichende(s) Verwendungsspezifikum/-a:  
...



### Interne Kommentare (gehören NICHT zur Moduldefinition)

Verknüpfungen:
* AK mit GHK 186505
* PK mit GHK 186506
