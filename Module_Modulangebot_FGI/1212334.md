
## Theory of Constraint Satisfaction Problems (1212334)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSInf 2009** in der Version **V2**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Theory of Constraint Satisfaction Problems
| (!) Modultitel (EN)     | Theory of Constraint Satisfaction Problems
| (!) ECTS                | 6
| (!) Gültig ab           | 2018-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Modulangebotsorganisator: Modulangebotsverantwortlicher InformatikModellierungsteamverantwortlicher: Dr. rer. nat. Katja PetzoldtModulverantworlicher: Universitätsprofessor Dr. rer. nat. Martin Grohe
| (!) Sprache             | Deutsch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Lecture Theory of Constraint Satisfaction Problems | Lecture Theory of Constraint Satisfaction Problems |  | 3 |
| Exercise Theory of Constraint Satisfaction Problems | Exercise Theory of Constraint Satisfaction Problems | 0 | 2 |
| Exam Theory of Constraint Satisfaction Problems | Exam Theory of Constraint Satisfaction Problems | 6 | 0 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| None. | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| None. | MSDaSci2018 | Computer Science |
| None. | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflicht Theoretische Informatik |
| keine Angabe | BSInf2022 | Wahlpflicht Theoretische Informatik |
| None. | MSInf2023 | Module aus dem Bereich Theoretische Informatik |
| Voraussetzung für die Zulassung zur Prüfung ist das Bestehen von Übungsaufgaben. Details werden in der Vorlesung bekanntgegeben. | BSInf2010 | Wahlpflicht Theoretische Informatik |
| None. | MSInf2009 | Theoretische Informatik |
| None. | MSInf2009 | Zusätzliche Prüfungsleistungen |
| None. | MSSSE2011 | Wahlpflichtbereich Theoretische Grundlagen des SSE |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | MSDaSci2018 | Computer Science |
| keine Angabe | BSInf2018 | Wahlpflicht Theoretische Informatik |
| keine Angabe | BSInf2022 | Wahlpflicht Theoretische Informatik |
| keine Angabe | MSInf2023 | Module aus dem Bereich Theoretische Informatik |
| keine Angabe | BSInf2010 | Wahlpflicht Theoretische Informatik |
| keine Angabe | MSInf2009 | Theoretische Informatik |
| keine Angabe | MSInf2009 | Zusätzliche Prüfungsleistungen |
| keine Angabe | MSSSE2011 | Wahlpflichtbereich Theoretische Grundlagen des SSE |

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the module examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Mathematische Grundlagen den Bereichen Diskrete
Strukturen und Lineare Algebra. Informatische Grundlagen in den
Bereichen Datenstrukturen und Algorithmen, Berechenbarkeit und
Komplexität sowie Logik.

**Empfohlene Voraussetzungen (EN)**  
Mathematical foundations from discrete mathematics and linear algebra.
Computer science foundations from data structures and algorithms,
computability and complexity, and logic.

**(!) Lernziele (DE)**  
Kenntnisse:
- Algorithmen für CSPs
- Komplexitätstheoretische Klassifikation von CSPs


Fähigkeiten:
- Erlernen fortgeschrittener Techniken aus verschiedenen Bereichen der
  theoretischen Informatik (Algorithmik, Logik, Komplexitätstheorie)
- Erlernen fortgeschrittener Techniken aus angrenzenden Bereichen der
  Mathematik (diskrete Mathematik, universelle Algebra)


Kompetenzen:
- Kombination und Anwendung dieser Techniken im Kontext eines
  aktuellen Forschungsthemas.

**(!) Lernziele (EN)**  
Knowledge:
- Algorithms for solving CSPs
- Complexity theoretic classification of CSPs


Skills:
- Advanced techniques from different areas of theoretical computer
  science (algorithms, logic, complexity theory)
- Advanced techniques from adjacent areas of mathemtatics (discrete
  mathematics, universal algebra)

Competence:
Combination and application of these techniques on specific research
problems.


**(!) Inhalt (DE)**  
Constraint-Satisfaction Probleme (CSPs) bilden einen konzeptionellen
Rahmen für eine Vielzahl algorithmischer Probleme mit Anwendungen in
verschiedenen Bereichen der Informatik. Inhalt der Vorlesung sind
Algorithmen zur Lösung von CSPs sowie die komplexitätstheoretische
Klassifikation von CSPs.

**(!) Inhalt (EN)**  
Constraint Satisfaction Problems (CSPs) form a conceptual framework
for numerous algorithmic problems with applications in different areas
of computer science. The course covers algorithms for solving CSPs and
a complexity theoretic classification of CSPs.

**(!) Literatur**  
keine Angabe


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSDaSci2018 | Wahlpflichtmodule aus Modulkatalog Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| MSDaSci2018 | Computer Science | V2 |
| BSInf2018 | Wahlpflicht Theoretische Informatik | V2 |
| BSInf2022 | Wahlpflicht Theoretische Informatik | V2 |
| MSInf2023 | Module aus dem Bereich Theoretische Informatik | V2 |
| BSInf2010 | Wahlpflicht Theoretische Informatik | V2 |
| MSInf2009 | Theoretische Informatik | V2 |
| MSInf2009 | Zusätzliche Prüfungsleistungen | V2 |
| MSSSE2011 | Wahlpflichtbereich Theoretische Grundlagen des SSE | V2 |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|