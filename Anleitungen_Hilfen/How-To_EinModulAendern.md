**Wie ändere ich ein Modul aus dem Modulangebot der FGI?**


**1.** Öffne den [Ordner [Repository/Files/Module_Modulangebot_FGI]](../ModuleGen)
       bzw. im [Ordner [Repository/Files/Modul_Listen]](../ModulListen) eine Modulliste 
       und suche darin nach dem Modul, dessen Beschreibung geändert werden soll.  
       
**2.** Klicke in der Toolbar rechts oben auf [Edit].  

**3.** Ändere den Inhalt der gewünschten Felder.  
       -- Beachte dabei bitte die allgemeinen Hinweise / Erläuterungen!  
       -- Der Inhalt des Feldes "Gültig ab" (1. Allgemeine Moduldaten) sollte nicht geändert werden.  
       -- Klicke auf den [Preview] bzw. [Write], um in den Ansichts- bzw. Bearbeitungs-Modus zu wechseln.  
       -- **Soll ein Modul aus dem Angebot der FGI entfernt werden, bitte den Inhalt des Feldes "Gültig bis" (Tab. 1) auf das nächste Semester setzen.**  
       -- **Soll ein Modul in weiteren (oder weniger) Studiengängen angeboten werden, bitte entsprechende SPO in Tab. 5a/5b hinzufügen/entfernen.** 
       
**4.** Klicke unten links auf den grünen Button [Commit changes].  
  
  
  
  
> Im Sinne agilen Teamworks gehen wir in diesem Projekt von 
> wohlwollenden Teammitgliedern aus und bitten entsprechende 
> Vorsicht bei der Bearbeitung walten zu lassen.
> Falls etwas schiefgeht: Bitte via modulangebot.informatik@fb1.rwth-aachen.de die MAO Informatik (Svenja Noichl, David Schmalzing)
> kontaktieren, diese können ältere Versionen rekonstruieren.

