### Veranstaltungsformen

| Veranstaltungsform (DE)       | Type of Course (EN)       |
|-------------------------------|---------------------------|
| (Fachmodul-)Prüfung (FMP)     | Module Examination        |
| Vorlesung (VO)                | Lecture                   |
| Übung (UE)                    | Exercise                  |
| Vorlesung/Übung (VU)          | Lecture/Exercise          |
| Tutorium (TU)                 | Tutorial                  |
| Seminar (SE)                  | Seminar                   |
| Proseminar (PS)               | Introductory Seminar      |
| Hauptseminar (HS)             | Advanced Seminar          |
| Praktikum (PR)                | Laboratory                |
| Programmierübung (PU)         | Programming Course        |
| Laborübung (LU)               | Laboratory Exercise       |
| Kolloquium (KO)               | Colloquium                |
| Forschungskolloquium (FK)     | Research Colloquium       |
| Projekt (PT)                  | Project                   |
| Repetitorium (RE)             | Revision Course           |
| Arbeitsgemeinschaft           | Tutorial                  |
| Intensivkurs (IK)             | Intensive Course          |
| Sprachkurs (SK)               | Language Course           |
| Exkursion (EX)                | Field Trip                |



