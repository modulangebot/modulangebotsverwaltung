`Allgemeine Hinweise:`
- PFLICHTfelder sind mit "(!)" markiert.
- Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.  
- Für ENGLISCHsprachige Module sind nur die Felder mit der ENGLISCHsprachigen 
  Formulierung auszufüllen.

***

### Erläuterung der Moduleigenschaften

* **(!) Modultitel (DE/EN):** Name des Moduls. Empfohlen wird, einen möglichst 
  kurzen Modultitel zu wählen.  
  Im Falle eines englischsprachigen Moduls bitte 
  (abweichend zum Hinweis oben) auch einen deutschen Titel angeben.
* **(!) ECTS:** Summe der Kreditpunkte, die für dieses Modul vergeben werden.  
  Abweichungen für einzelne SPOen sind in Tab. 5b anzugeben!
* **(!) Gültig ab:** Semester, ab dem das neue Modul zum 1. Mal angeboten werden 
  bzw. die Moduländerung in Kraft treten soll,  
  z. B. SoSe 2020 oder WiSe 20/21.
* **Gültig bis:** Semester, in dem das Modul zum letzten Mal angeboten werden soll.
* **(!) ModulanbieterIn:** i. d. R. LeiterIn der anbietenden Organisationseinheit.
* **(!) Sprache:** Sprache, in der das Modul unterrichtet wird.
* **(!) Turnus:** Häufigkeit, mit der das Modul angeboten wird.
* **(!) Moduldauer:** Zeitraum, in dem das Modul abgeschlossen wird (i. d. R. einsemestrig).
* **(!) Modulniveau:** Für Bachelor u./o. Master-Studierende.
* **(!) Fachsemester:** *frühestes* Fachsemester, ab dem der Besuch des Moduls empfohlen ist. 