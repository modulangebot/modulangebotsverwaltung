### Prüfungsformen nach ÜPO §7

| Prüfungsform (DE)             | Examination Type (EN)     |
|-------------------------------|---------------------------|
| Klausur                       | Written exam              |
| Mündliche Prüfung             | Oral Examination          |
| Studienarbeit                 | Course Assignment         |
| Schriftliche Hausarbeit       | Written homework          |
| Projektarbeit                 | Projekt work              |
| Portfolio                     | Portfolio                 |
| Referat                       | Presentation              |
| Kolloquium                    | Colloquium                |
| Praktikum                     | Practical Training/Lab    |

`Hinweis:`  
Durch die SPOen (Studien- und Prüfungsordnungen) der verschiedenen Studiengänge 
können weitere Prüfungsformen zugelassen sein.  
Hierbei ist jedoch zu beachten, dass diese dann **ausschließlich** für 
Studierende dieser SPO zugelassen sind.  
Die KfL empfiehlt, nur die in der ÜPO definierten Prüfungsformen zu verwenden.


