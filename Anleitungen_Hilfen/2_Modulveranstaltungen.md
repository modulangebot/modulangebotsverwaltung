`Allgemeine Hinweise:`
- PFLICHTfelder sind mit "(!)" markiert.
- Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.  
- Für ENGLISCHsprachige Module sind nur die Felder mit der ENGLISCHsprachigen 
  Formulierung auszufüllen.

***

### Erläuterung der Moduleigenschaften

* **(!) Modulveranstaltungen (DE/EN)**: Lehr- und Prüfungsveranstaltungen des Moduls.  
  Die Bezeichnung einer Veranstaltung beinhaltet typischerweise 
  [Veranstaltungsform](Veranstaltungsformen_SPOen.md)
  bzw. [Prüfungsform](Pr%C3%BCfungsformen_%C3%9CPO.md) 
  plus Veranstaltungstitel.
* **(!) ECTS**: Anzahl der Kreditpunkte *je* Veranstaltung.  
* **(!) Präsenzzeit**: Anzahl der wöchentlichen Semesterwochenstunden in Kontakt *je* Veranstaltung.  

> Ein Beispiel für eine ausgefüllte Tabelle 2:
> 
> | (!) Modulveranstaltungen (DE)             | (!) Modulveranstaltungen (EN)            | (!) ECTS | (!) Präsenzzeit (SWS) | 
> |-------------------------------------------|------------------------------------------|----------|-----------------------|
> | Vorlesung Algorithmen und Datenstrukturen | Lecture Algorithms and Data Structures   | 0        | 2                     |
> | Vorlesung Programmierung (Service)        | Lecture Programming (Service)            | 0        | 2                     |
> | Übung Algorithmen und Datenstrukturen     | Exercise Algorithms and Data Structures  | 0        | 1                     |
> | Übung Programmierung (Service)            | Exercise Programming (Service)           | 0        | 2                     |
> | Klausur Algorithmen und Datenstrukturen   | Exam Algorithms and Data Structures      | 5        | 0                     |
> | Klausur Programmierung (Service)          | Exam Programming (Service)               | 6        | 0                     |