Zur korrekten Modellierung von Prüfungsveranstaltungen / Beschreibungen in den 
Feldern Voraussetzungen + Benotung/Dauer ist zwischen  
**Modulprüfungen, Teilprüfungen, Teilleistungen und Modulbausteinen** 
zu unterscheiden.
  
    
Eine **Modulprüfung** ist die Prüfungsleistung eines Moduls. (ÜPO §6.1)

>  "Die Prüfungsordnung unterscheidet zwischen veranstaltungsbegleitenden und
veranstaltungsabschließenden Prüfungen.  
**Veranstaltungsbegleitende Prüfungen** 
sind nach Maßgabe der jeweiligen studiengangspezifischen Prüfungsordnung  
Studienarbeit, schriftliche Hausarbeit, Projektarbeit, Portfolio, Referat, 
Kolloquium und Praktikum.  
**Veranstaltungsabschließende Prüfungen** sind Klausur und 
mündliche Prüfung.  
Einzelheiten sowie gegebenenfalls weitere Prüfungsformen sind 
in den studiengangspezifischen Prüfungsordnungen geregelt." (ÜPO §7)  
"Modulprüfungen können aus Teilprüfungen oder Teilleistungen bestehen." (ÜPO §6.12)  


**Teilprüfung**  
1.  *Beschreibung:* "Eigenständige Prüfungsleistung innerhab eines Moduls, 
    die jeweils mit Kreditpunkten versehen sind und für die gesonderte Noten 
    erfasst werden." (ÜPO §6.12)
2.  *Zu modellieren in Modulkatalog/RWTHonline:* Als eigenständige! Prüfungsveranstaltung (mit ECTS);  
    siehe dazu Tab. 2 "Veranstaltungen".
3.  *Gültigkeit:* Unbeschränkt.
4.  *Wiederholbarkeit:* (je Teilprüfung) 2x
  
  
**Teilleistung** 
1.  *Beschreibung:* "Uneigenständige Bestandteile einer Prüfungsleistung, 
    die nicht mit CP versehen sind und deren Noten mit einer im Modulkatalog zu 
    beschreibenden Gewichtung gemäß § 14 Abs. 5 in die zu erfassende Note der 
    Prüfungsleistung eingehen." (ÜPO §6.12)  
    Anmerkung: Dadurch ist - im Unterschied zur Teilprüfung! - eine
    gegenseitige Kompensation der Teilleistungen möglich.  
    "Findet eine Kompensation von Teilleistungen nicht statt,
    müssen in der Regel alle Teilleistungen wiederholt werden." (ÜPO §14)  
    Ist die Möglichkeit zur gegenseitigen Kompensation NICHT gewünscht, muss
    dies im Modulkatalog EXPLIZIT ausgewiesen werden!
2.  *Zu modellieren in Modulkatalog/RWTHonline:* Im Feld "Benotung/Dauer";  
    siehe dazu die [Formulierungsstandards](3_Leistungen.md) für die Felder "Voraussetzungen" sowie "Benotung/Dauer".
3.  *Gültigkeit:* Innerhalb eines Turnus (d. h. i. d. R. 1 Semester oder 1 Jahr).
4.  *Wiederholbarkeit:* 2x im Rahmen der Modulprüfung.
  
  
**Modulbaustein**
1.  *Beschreibung:* VORleistung, die mit 0-20 % auf die Modulnote angerechnet wird  
    (in der Informatik typischerweise semesterbegleitende Übungsaufgaben).
2.  *Zu modellieren in Modulkatalog/RWTHonline:* Im Feld "Benotung/Dauer";  
    siehe dazu die [Formulierungsstandards](3_Leistungen.md) für die Felder "Voraussetzungen" sowie "Benotung/Dauer".
3.  *Gültigkeit:* Je nach Festlegung.  
    Wenn verfallbarer MB (Standard): 1 Turnus (i. d. R. 1 Semester oder 1 Jahr).  
    Wenn unverfallbarer MB: unbeschränkt.
4.  *Wiederholbarkeit:* Beliebig oft.
