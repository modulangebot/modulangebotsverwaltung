`Allgemeine Hinweise:`
- PFLICHTfelder sind mit "(!)" markiert.
- Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.  
- Für ENGLISCHsprachige Module sind nur die Felder mit der ENGLISCHsprachigen 
  Formulierung auszufüllen.

***

### Erläuterung der Moduleigenschaften

* **(!) Empfohlene Voraussetzungen (DE/EN):** Kenntnisse und Kompetenzen,
  die für die Teilnahme am Modul *empfohlen* werden,   
  z. B. Kenntnisse in Java o. ä. 
* **(!) Lernziele (DE/EN):** Kenntnisse und Kompetenzen,
  welche die Studierenden durch die Teilnahme am Modul erwerben.
* **(!) Inhalt (DE/EN):** Lehrinhalte des Moduls.
* **(!) Literatur:** Verpflichtende/empfohlene Fachliteratur zum Modul.
