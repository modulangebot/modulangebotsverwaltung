**Wie erstelle ich ein neues Modul für das Modulangebot der FGI?**


**1.** Öffne [diesen Link](Vorlage.md) in einem neuen Tab.  
       -- Alternativ klicke auf [Repository > Files > Vorlage.md].  
       
**2.** Klicke in der Toolbar rechts oben auf [Edit].  

**3.** Klicke in das Fenster mit dem Dateiinhalt und drücke [STRG+A] und [STRG+C].  

**4.** Öffne [diesen Link](Module_Modulangebot_FGI) in einem neuen Tab.  
       -- Alternativ klicke unter [Repository > Files > Module_Modulangebot_FGI] neben [master > modulverwaltung > Module_Modulangebot_FGI] auf [+] und wähle [New file].  
       
**5.** Klicke in das Feld [master / File name] und gebe den [Titel des Moduls.md] als Dateinamen ein.  

**6.** Klicke in das weiße Feld und drücke [STRG+V].  

**7.** Klicke unten links auf den grünen Button [Commit changes].  

**8.** Klicke in der Toolbar rechts oben auf [Edit].  

**9.** Befülle die leeren Felder mit den Informationen zum Modul.  
       -- Beachte dabei bitte die allgemeinen Hinweise / Erläuterungen!  
       -- Klicke auf den [Preview] bzw. [Write], um in den Ansichts- bzw. Bearbeitungs-Modus zu wechseln.  
       
**10.** Klicke unten links auf den grünen Button [Commit changes].
  
  
  
  
> Im Sinne agilen Teamworks gehen wir in diesem Projekt von 
> wohlwollenden Teammitgliedern aus und bitten entsprechende 
> Vorsicht bei der Bearbeitung walten zu lassen.
> Falls etwas schiefgeht: Bitte via modulangebot.informatik@fb1.rwth-aachen.de die MAO Informatik (Svenja Noichl, David Schmalzing)
> kontaktieren, diese können ältere Versionen rekonstruieren.