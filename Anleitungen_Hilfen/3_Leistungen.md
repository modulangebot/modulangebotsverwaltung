`Allgemeine Hinweise:`
- PFLICHTfelder sind mit "(!)" markiert.
- Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.  
- Für ENGLISCHsprachige Module sind nur die Felder mit der ENGLISCHsprachigen 
  Formulierung auszufüllen.

***

### Erläuterung der Moduleigenschaften

* **(!) Teilnahmevoraussetzungen (studiengangspezifisch) (DE/EN):** Voraussetzung(en) für den *Zugang zum Modul*.  
  *Anwesenheitspflicht* und *Voraussetzungen für die Zulassung zur Prüfung* werden im Feld "Prüfungsbedingungen" dokumentiert!
* **(!) Prüfungsbedingungen (DE/EN):** Zur Beschreibung der konkreten Prüfungsmodalitäten. Zu den Prüfungsmodalitäten gehören:
  * die [Prüfungsform(en)](Prüfungsformen_ÜPO.md),
  * die Zusammensetzung der Prüfung aus [Teilprüfungen bzw. Teilleistungen](Teilpr%C3%BCfung_Teilleistung_Modulbaustein.md),
  * die Existenz von [Modulbausteinen](Teilpr%C3%BCfung_Teilleistung_Modulbaustein.md) als Voraussetzung für die Zulassung zur Prüfung.

***

### Formulierungsstandards für das Feld "Prüfungsbedingungen"

Im Minimalfall beinhaltet sie die Angabe einer Prüfungsform und einer
Prozentangabe, die aussagt, mit welchem Anteil die jeweilige 
[Form der Prüfung](Pr%C3%BCfungsformen_%C3%9CPO.md)
in die Note des Moduls eingeht:

> [DE] [Prüfungsform] (100 %).  
> [EN] [Examination Type] (100 %). 


**Soll die Modulprüfung aus mehreren TeilPRÜFUNGEN bestehen,**  
ist eine Formulierung nach folgendem Muster zu verwenden 
(Hinweis: Da Teilprüfungen als eigenständige Prüfungsveranstaltungen mit ECTS modelliert werden,
ist hier auf eine prozentuale Angabe zu verzichten):

> [DE] Die Modulprüfung besteht aus den folgenden Teilprüfungen:  
       [Prüfungsform 1]; [Prüfungsform 2]; ... [Prüfungform n].  
> [EN] The module examination consists of the following partial examinations:  
       [Examination Type 1]; [Examination Type 2]; ... [Examination Type n].


**Soll die Modulprüfung aus mehreren TeilLEISTUNGEN bestehen,**  
ist eine Formulierung nach folgendem Muster zu verwenden:

> [DE] Die Modulprüfung besteht aus den folgenden Teilleistungen:  
       [Prüfungsform 1] ([Prozentangabe]); [Prüfungsform 2] ([Prozentangabe]); ... [Prüfungform n] ([Prozentangabe]).  
> [EN] The module examination consists of the following partial examinations:  
       [Examination Type 1] ([Percentage]); [Examination Type 2] ([Percentage]); ... [Examination Type n] ([Percentage]).

Dazu sind die folgenden Ergänzungen möglich:

> [DE] Voraussetzung zum Bestehen des Moduls ist das Bestehen jeder Teilleistung.  
> [EN] Students must pass all parts of the examination individually to pass the module.  
> [DE] Es ist nicht möglich, Teilleistungen in ein Folgesemester zu übertragen.  
> [EN] It is not possible to transfer parts of the examinations in another semester.  


**Ist ein Modulbaustein Voraussetzung für die Zulassung zur Modulprüfung bzw.**  
**eine Teilleistung/Teilprüfung Voraussetzung für die Zulassung zu einer weiteren Teilleistung/Teilprüfung,**  
ist die folgende Formulierung zu verwenden:

> [DE] Voraussetzung für die Zulassung zu ... ist das Bestehen von ....  
> [EN] Students must pass ... to be admitted to ....


**Ist in Lehrveranstaltungen des Typs Übung, Seminar, Proseminar, Kolloquium, (Labor)Praktikum und/oder Exkursion Anwesenheit erforderlich,**  
ist zuletzt die folgende Formulierung zu ergänzen:

> [DE] In der/dem [Art der Lehrveranstaltung] besteht Anwesenheitspflicht.  
> [EN] Attendance is mandatory in [type of education event].


**Beispiele für Inhalte des Feldes "Prüfungsbedingungen":**

> [DE] Klausur (100 %).  
> [EN] Written exam (100 %).

> [DE] Klausur oder mündliche Prüfung (100 %).  
       Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.  
> [EN] Written exam or oral examination (100%).  
       Students must pass written homework to be admitted to the module examination.

> [DE] Die Modulprüfung besteht aus den folgenden Teilleistungen:  
       Mündliche Prüfung (50 %); Schriftliche Hausarbeit (30 %); Referat (20 %).  
       Voraussetzung zum Bestehen des Moduls ist das Bestehen jeder Teilleistung.  
       Es ist nicht möglich, Teilleistungen in ein Folgesemester zu übertragen.  
> [EN] The module examination consists of the following partial qualifications:  
       Oral examination (50 %); Written paper (30 %); Presentation (20 %).  
       Students must pass all parts of the examination individually to pass the module.  
       It is not possible to transfer parts of the examinations in another semester.  

> [DE] Die Modulprüfung besteht aus den folgenden Teilprüfungen:  
       Klausur „Programmierung“; Klausur „Algorithmen und Datenstrukturen“.  
> [EN] The module examination consists of the following partial examinations:  
       Written exam „Programming“; written exam „Algorithms and Data Structures”.  

***

### Formulierungsstandards für das Feld "Teilnahmevoraussetzungen (studiengangsspezifisch)"

Von der KfL empfohlen:
> [DE] Keine.  
> [EN] None.

Bei Bedarf:
> [DE] Das Modul setzt den erfolgreichen Abschluss des Moduls "[Modultitel]" voraus.  
> [EN] The module requires the successful completion of module "[title of the module]".