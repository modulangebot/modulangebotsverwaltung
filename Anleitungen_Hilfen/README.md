**Ist es möglich, nach dem 1. März (bzw. 14. September) noch Modulangebotsänderungen für das kommende Wintersemester (bzw. Sommersemester) vorzunehmen?**
> Grundsätzlich: NEIN.  
> Ausnahme: Änderungen an den Modulbeschreibungsfeldern unter "Informationen für das Modulhandbuch".