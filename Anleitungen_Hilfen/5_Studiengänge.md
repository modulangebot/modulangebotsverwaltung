`Die Tabellen 5a und 5b werden i. d. R. von den Studiengangsorganisatoren
bearbeitet/befüllt.`  
`Vorschläge für Studiengänge, in denen das Modul angeboten werden
soll, können in Spalte Studiengangskürzel eingetragen werden.`
  
  
`Allgemeine Hinweise:`
- PFLICHTfelder sind mit "(!)" markiert.
- Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.  
- Für ENGLISCHsprachige Module sind nur die Felder mit der ENGLISCHsprachigen 
  Formulierung auszufüllen.


***


### Erläuterung der Moduleigenschaften

* **SPO:** Studien- und Prüfungsordnung
* **Studiengangskürzel:** Kurzbezeichnung der Studiengänge; siehe Tabellen unten.
* **SPO-Version:** Jahr der jeweiligen Studien-und Prüfungsordnung (= SPO).  
  Ohne diese Angabe wird das Modul in **allen aktuell gültigen (= laufenden)**
  SPO-Versionen des Studiengangs verankert.
* **Modulbereich:** Bereich innerhalb der SPO, in dem das Modul verankert werden soll,  
  z. B. im MSInf: "Wahlpflichtbereich > Theoretische Informatik"
* **Verwendungsspezifika:** Eigenschaften eines Moduls, die für verschiedene 
  SPO-Versionen verschiedene Inhalte besitzen dürfen - und dann (wenn sie in 
  verschiedenen SPO-Versionen verschiedene Inhalte besitzen) zu einer neuen, 
  SPO-spezifischen Modulvariante (sog. "Vorlagenkopie") führen.
  Welche Moduleigenschaften das sind, siehe Liste unten.


***


### Verwendungsspezifika

Die folgenden Moduleigenschaften können je SPO-Version eines Moduls verschieden
sein:

* **Gültig ab**  
* **Gültig bis**  
* **Fachsemester**  
* **Formale Voraussetzungen (DE/EN)**
* **Modulveranstaltungen (DE/EN) (plus ECTS u. Präsenzzeit (SWS) je Veranstaltung**


***


### Im Modulangebot der FGI häufig genannte Studiengänge und ihre Kurzbezeichnung:  


| **Master-Studiengänge** (alphabetisch sortiert)                    | **Studiengangskürzel** |
|--------------------------------------------------------------------|------------------------|
| Automatisierungstechnik                                            | MSAT                   |
| Computer Aided Conception and Production in Mechanical Engineering | MSCAME                 |
| Computational Engineering Science                                  | MSCES                  |
| Computational Social Systems                                       | MSCSS                  |
| Data Science                                                       | MSDaSci                |
| Elektrotechnik (Lehramt Berufskolleg)                              | MEdBKET                |
| Elektrotechnik, Informationstechnik und Technische Informatik      | MSETITTI               |
| Informatik                                                         | MSInf                  |
| Informatik (Lehramt Berufskolleg)                                  | MEdBKInf               |
| Informatik (Lehramt Gymnasium + Gesamtschule)                      | MEdGyGeInf             |
| Mathematik                                                         | MSMath                 |
| Media Informatics                                                  | MSMI                   |
| Physik                                                             | MSPhy                  |
| Simulation Sciences                                                | MSSiSc                 |
| Software Systems Engineering                                       | MSSSE                  |
| Technische Informatik (Lehramt Berufskolleg)                       | MEdBKTI                |
| Technische Informatik (Lehramt Gymnasium + Gesamtschule)           | MEdGyGeTI              |
| Technik-Kommunikation (Grundlagen der Elektrotechnik)              | MSTKE                  |
| Technik-Kommunikation (Grundlagen der Informatik)                  | MSTKI                  |


| **Bachelor-Studiengänge** (alphabetisch sortiert)                  | **Studiengangskürzel** |
|--------------------------------------------------------------------|------------------------|
| Computational Engineering Science                                  | BSCES                  |
| Elektrotechnik (Lehramt Berufskolleg)                              | LABBKET                |
| Elektrotechnik, Informationstechnik und Technische Informatik      | BSETITTI               |
| Informatik                                                         | BSInf                  |
| Informatik (Lehramt Berufskolleg)                                  | LABBKInf               |
| Informatik (Lehramt Gymnasium + Gesamtschule)                      | LABGyGeInf             |
| Mathematik                                                         | BSMath                 |
| Physik                                                             | BSPhy                  |
| Technische Informatik (Lehramt Berufskolleg)                       | LABBKTI                |
| Technische Informatik (Lehramt Gymnasium + Gesamtschule)           | LABGyGeTI              |
| Technik-Kommunikation (Grundlagen der Elektrotechnik)              | BSTKE                  |
| Technik-Kommunikation (Grundlagen der Informatik)                  | BSTKI                  |