# Modulangebotsverwaltung der Informatik


Dieses GitLab-Projekt dient dazu,
Änderungen im Modulangebot der Fachgruppe Informatik vorzunehmen.  
Verwaltet werden hier **alle Module, die VON der Fachgruppe Informatik (FGI)**
angeboten werden.


Wir unterscheiden 4 Arten von Änderungen an einem Modul:

A. Ein Modul in das Modulangebot der FGI aufnehmen. [#how-to](Anleitungen_Hilfen/How-To_NeuesModulErstellen.md)  
B. Ein Modul aus dem Modulangebot der FGI ändern: [#how-to](Anleitungen_Hilfen/How-To_EinModulAendern.md)  
---B1. Eine Modulbeschreibung ändern.  
---B2. Ein Modul aus dem Modulangebot der FG Informatik entfernen.  
---B3. Ein Modul in weiteren (oder weniger) Studiengängen anbieten.  


Weitere Anleitungen und Hilfestellungen findet ihr [im Ordner "Anleitungen_Hilfen"](Anleitungen_Hilfen).  

`Bei Fragen und Problemen bitte via modulangebot.informatik@fb1.rwth-aachen.de die MAO Informatik (Svenja Noichl) kontaktieren.`
  
  
## Listen der Module, sortiert nach verschiedenen Kriterien: 

*  [Liste aller Module, sortiert nach Titel](ModulListen/ModulListe.md)
*  [Liste der Module, sortiert nach Modulnummer](ModulListen/ModuleNachModulnummerSortiert.md)
*  [Liste der Module, sortiert nach Anbieter](ModulListen/ModuleNachAnbieterSortiert.md)
*  [Liste der Module, sortiert nach Studiengang](ModulListen/ModuleNachStudiengangSortiert.md)
*  [Liste der Module, sortiert nach Änderungsdatum](ModulListen/ModuleNachChangeDatumSortiert.md)
