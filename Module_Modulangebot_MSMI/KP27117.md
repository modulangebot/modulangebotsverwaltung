
## Advanced Methods for Text Mining (KP27117)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSMI 2019**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Advanced Methods for Text Mining
| (!) Modultitel (EN)     | Advanced Methods for Text Mining
| (!) ECTS                | 4
| (!) Gültig ab           | 2022-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| keine Angabe
| (!) Sprache             | Englisch
| (!) Turnus              | Unregelmäßig
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Vorlesung und Übung Advanced Methods for Text Mining | Lecture and Exercise Advanced Methods for Text Mining |  | 4 |
| Prüfung Advanced Methods for Text Mining | Exam Advanced Methods for Text Mining | 4 |  |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSMI2019 | Multimedia-Technologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSMI2019 | Multimedia-Technologie |

**(!) Prüfungsbedingungen (DE)**  
Mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Oral examination (100%). Students must pass written homework to be admitted to the module examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Basic knowledge of AI, data science, machine learning, and pattern recognition; programming skills; good working knowledge in statistics, linear algebra, and optimization.

**Empfohlene Voraussetzungen (EN)**  
Basic knowledge of AI, data science, machine learning, and pattern recognition; programming skills; good working knowledge in statistics, linear algebra, and optimization.

**(!) Lernziele (DE)**  
Knowledge: Students will learn about the basic as well as the advanced methods for processing textual data including the necessary preprocessing steps, representation learning methods to extract embeddings for downstream tasks and build predictive and prescriptive methods for a variety of objectives such as text classification, outlier detection and recommender systems. The students should be able to categorize the methods based on their complexities and their applicability to different text mining problems. Skills: Students should be able to analyze, design as well as reason about existing and new data mining algorithms, theoretically compare algorithms, strengthen their analytical thinking to solve difficult modelling problems, have acquired the necessary mathematical as well as programming/IT skills to systematically plan, design and implement text and data mining projects. Competences: Based on the knowledge and skills acquired in this module, the students will be able to assess certain characteristics of the already existing text mining methods as well as build new solutions to emerging problems. Additionally, the students will be able to transfer their knowledge to other data science areas involving modelling data with sequential dependencies.

**(!) Lernziele (EN)**  
Knowledge: Students will learn about the basic as well as the advanced methods for processing textual data including the necessary preprocessing steps, representation learning methods to extract embeddings for downstream tasks and build predictive and prescriptive methods for a variety of objectives such as text classification, outlier detection and recommender systems. The students should be able to categorize the methods based on their complexities and their applicability to different text mining problems. Skills: Students should be able to analyze, design as well as reason about existing and new data mining algorithms, theoretically compare algorithms, strengthen their analytical thinking to solve difficult modelling problems, have acquired the necessary mathematical as well as programming/IT skills to systematically plan, design and implement text and data mining projects. Competences: Based on the knowledge and skills acquired in this module, the students will be able to assess certain characteristics of the already existing text mining methods as well as build new solutions to emerging problems. Additionally, the students will be able to transfer their knowledge to other data science areas involving modelling data with sequential dependencies.

**(!) Inhalt (DE)**  
Matrix Factorization, Transformer Architectures, Tensor Factorization, Alternating Least Squares, Gradient Descent Individual Dimensional Scaling, DEDICOM, Evolutionary Strategies, K-Means, Support Vector Machines, Decision Trees, Neural Networks, Text Mining Pipelines, Stemming, Lemmatization, TF-IDF, Latent Semantic Indexing, Global Vectors, Recurrent Neural Networks, Sentiment Analysis, Natural Language Inference, Computational Argumentation, Information Extraction, Named Entity Recognition, Text Summarization, Opinion Mining, Text Segmentation, Event Detection.

**(!) Inhalt (EN)**  
Matrix Factorization, Transformer Architectures, Tensor Factorization, Alternating Least Squares, Gradient Descent Individual Dimensional Scaling, DEDICOM, Evolutionary Strategies, K-Means, Support Vector Machines, Decision Trees, Neural Networks, Text Mining Pipelines, Stemming, Lemmatization, TF-IDF, Latent Semantic Indexing, Global Vectors, Recurrent Neural Networks, Sentiment Analysis, Natural Language Inference, Computational Argumentation, Information Extraction, Named Entity Recognition, Text Summarization, Opinion Mining, Text Segmentation, Event Detection.

**(!) Literatur**  
 Pattern Recognition and Machine Learning, Christopher M. Bishop Introduction to Information Retrieval, Christopher D. Manning, Prabhakar Raghavan and Heinrich Schütze Aggarwal, C. C. (2018). Machine learning for text (Vol. 848). Cham: Springer. 


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSMI2019 | Multimedia-Technologie |  |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|