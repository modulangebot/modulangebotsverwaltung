
## Data Science and Big Data (KP20926)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSMI 2019**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Data Science and Big Data
| (!) Modultitel (EN)     | Data Science and Big Data
| (!) ECTS                | 6
| (!) Gültig ab           | 2019-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Universitätsprofessor Dr. rer. nat. Stefan Wrobel
| (!) Sprache             | Englisch
| (!) Turnus              | Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Data Science and Big Data (Prüfung) | Data Science and Big Data (Exam) | 6 |  |
| Data Science and Big Data (Vorlesung) | Data Science and Big Data (Lecture) |  | 4 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| None. | MSMI2019 | Rechner- und Kommunikationstechnologie |
| keine Angabe | MSAT2013 | Individuelle Module |
| None. | MSMI2005 | Rechner- und Kommunikationstechnologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSMI2019 | Rechner- und Kommunikationstechnologie |
| keine Angabe | MSAT2013 | Individuelle Module |
| keine Angabe | MSMI2005 | Rechner- und Kommunikationstechnologie |

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the module examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Kenntnisse aus den Modulen 'Intelligente Lern- und Analysesysteme: Maschinelles Lernen' und 'Intelligente Lern- und Analysesysteme: Data Mining und Wissensfindung'.

**Empfohlene Voraussetzungen (EN)**  
Knowledge of the modules “Intelligent Learning and Analysis Systems: Machine Learning” and “Intelligent Learning and Analysis Systems: Data Mining and Knowledge Discovery”.

**(!) Lernziele (DE)**  
Kenntnisse: Architekturen und Protokolle für große Datensysteme, verteilte Batch- und Stream-Verarbeitungssysteme, Nicht-Standard-Datenbanken für große Daten, Datenbanken für strukturierte Daten, Ähnlichkeitssuche, Synopsen für massive Daten, klassische Data-Mining-Aufgaben für massive Daten- und/oder Datenströme, Mining massiver Grafiken, Anwendungen. | Fertigkeiten: Programmierung für verteilte Systeme wie Apache Spark, Storm, et al. Überlegungen zu formalen Konzepten, formale Analyse von Algorithmen für die Datenwissenschaft und Big Data. | Kompetenzen: Implementierung von sublinearen und subquadratischen praktischen sequentiellen und verteilten Algorithmen auf verteilten Plattformen. Geeignete Auswahl von Algorithmen für verteilte Einstellungen auf der Grundlage einer formalen Analyse.


**(!) Lernziele (EN)**  
Knowledge: Architectures and protocols for big data systems, distributed batch and stream processing systems, non-standard databases for big data, databases for structured data, similarity search, synopses for massive data, classical data mining tasks for massive data and/or data streams, mining massive graphs, applications. | Skills: Programming for distributed systems like Apache Spark, Storm, et al. Reasoning about formal concepts, formal analysis of algorithms for data science and big data. | Competences: Implementing sublinear and subquadratic practical sequential and distributed algorithms on distributed platforms. Appropriate choice of algorithms for distributed settings based on formal analysis.


**(!) Inhalt (DE)**  
Der Kurs bietet ein tiefes Wissen über verschiedene Aspekte der Big Data Analytics und Systeme, einschließlich algorithmischer Techniken zur Analyse strukturierter und unstrukturierter Daten, die nicht in einem einzigen Computer gespeichert werden können, weil sie eine enorme Größe haben und/oder kontinuierlich mit einer so hohen Rate ankommen, dass eine sofortige Verarbeitung erforderlich ist. Zusätzlich zu den algorithmischen Aspekten werden verteilte große Datenverarbeitungs- und Datenbanksysteme vorgestellt und angewendet. Zu den Themen gehören Ähnlichkeitssuche, Synopsen für massive Daten, Mining massive Grafiken, klassische Data-Mining-Aufgaben für massive Daten- und/oder Datenströme, Architekturen und Protokolle für große Datensysteme, verteilte Batch- (Hadoop) und Stream- (Storm) Verarbeitungssysteme, Nicht-Standard-Datenbanken für große Datenmengen (Cassandra).


**(!) Inhalt (EN)**  
The course offers an in-depth knowledge of different aspects of big data analytics and systems, including algorithmic techniques for analysing structured and unstructured data that cannot be stored in a single computer because it has enormous size and/or continuously arrives with such a high rate that requires immediate processing. In addition to the algorithmic aspects, distributed big data processing and database systems will be presented and applied. Topics include similarity search, synopses for massive data, mining massive graphs, classical data mining tasks for massive data and/or data streams, architectures and protocols for big data systems, distributed batch (Hadoop) and stream (Storm) processing systems, non-standard databases for big data (Cassandra).


**(!) Literatur**  
N. Marz and J. Warren: Big Data. Principles and best practices of scalable realtime data systems. Manning Pubn, 2014; T. White: Hadoop The Definitive Guide. O’REILLY, 2012; A. Rajaraman and J.D. Ullman.: Mining of Massive Datasets. Cambridge University Press, 2011; G. Cormode, M. Garofalakis, P.J. Haas, and C. Jermaine: Synopses for Massive Data: Samples, Histograms, Wavelets, Sketches. Foundations and Trends in Databases 4(1-3): 1-294 (2012)


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSMI2019 | Rechner- und Kommunikationstechnologie |  |
| MSAT2013 | Individuelle Module |  |
| MSMI2005 | Rechner- und Kommunikationstechnologie |  |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|