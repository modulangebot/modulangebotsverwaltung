
## Grundlagen der Datenwissenschaft (KP20904)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSMI 2019**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Grundlagen der Datenwissenschaft
| (!) Modultitel (EN)     | Foundations of Data Science
| (!) ECTS                | 8
| (!) Gültig ab           | 2019-04-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Dr. rer. nat. Michael Nüsken  
| (!) Sprache             | Englisch
| (!) Turnus              | Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Grundlagen der Datenwissenschaft | Foundations of Data Science | 8 | 6 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSMI2019 | Data Science |
| None. | MSMI2005 | Rechner- und Kommunikationstechnologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSMI2019 | Data Science |
| keine Angabe | MSMI2005 | Rechner- und Kommunikationstechnologie |

**(!) Prüfungsbedingungen (DE)**  
Klausur oder mündliche Prüfung (100 %). Voraussetzung für die Zulassung zur Modulprüfung ist das Bestehen von Hausaufgaben.

**(!) Prüfungsbedingungen (EN)**  
Written exam or oral examination (100 %). Students must pass written homework to be admitted to the module examination.


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Keine.

**Empfohlene Voraussetzungen (EN)**  
None.

**(!) Lernziele (DE)**  
Beherrschung mathematischer Werkzeuge für die Datenwissenschaft.

**(!) Lernziele (EN)**  
Mastering mathematical tools for data science.

**(!) Inhalt (DE)**  
Die Datenwissenschaft zielt darauf ab, aus großen Datenmengen Sinn zu machen. Zu diesem Zweck müssen verschiedene Werkzeuge verstanden werden, die bei der Analyse der entstehenden Strukturen helfen.
Häufig werden Daten als eine Sammlung von Vektoren mit einer großen Anzahl von Komponenten geliefert. Das Verständnis ihrer gemeinsamen Struktur ist das erste Hauptziel des Datenverständnisses. Die Geometrie und die dahinter liegende lineare Algebra werden relevant und aufschlussreich. Doch die Intuition aus dem nieder-dimensionalen Raum erweist sich oft als irreführend. Wir müssen uns der besonderen Eigenschaften hoch-dimensionaler Räume bei der Arbeit mit solchen Daten bewusst sein. Zu den fruchtbaren Methoden für die Analyse gehören die singuläre Vektor-Zerlegung aus linearer Algebra und das überwachte und unbeaufsichtigte maschinelle Lernen. Wenn es die Zeit erlaubt, betrachten wir auch Zufallsgraphen, die das zweit-häufigste Modell für reale Phänomene sind.

**(!) Inhalt (EN)**  
Data science aims at making sense of big data. To that end various tools have to be understood for helping in analyzing the arising structures.
Often data comes as a collection of vectors with a large number of components. To understand their common structure is the first main objective of understanding the data. The geometry and the linear algebra behind them becomes relevant and enlightning. Yet, the intuition from low-dimensional space turns out to be often misleading. We need to be aware of the particular properties of high-dimensional spaces when working with such data. Fruitful methods for the analysis include singular vector decomposition from linear algebra and supervised and unsupervised machine learning. If time permits we also consider random graphs which are the second most used model for real world phenomena.


**(!) Literatur**  
Avrim Blum, John Hopcroft, and Ravindran Kannan (2018+).  Foundations of Data Science


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSMI2019 | Data Science |  |
| MSMI2005 | Rechner- und Kommunikationstechnologie |  |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|