
## Praktikum (KP21164)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSMI 2019**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Praktikum
| (!) Modultitel (EN)     | Practical Lab
| (!) ECTS                | 9
| (!) Gültig ab           | 2019-10-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Modulangebotsorganisation b-it
| (!) Sprache             | Englisch
| (!) Turnus              | Wintersemester/Sommersemester
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Praktikum | Practical Lab | 9 | 5 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| None. | MSMI2019 | Medien-Informatik Praktika |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSMI2019 | Medien-Informatik Praktika |

**(!) Prüfungsbedingungen (DE)**  
Praktikum (100 %). Im Praktikum besteht Anwesenheitspflicht.


**(!) Prüfungsbedingungen (EN)**  
Practical training (100 %). Attendance in the practical lab is mandatory.



### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Keine.

**Empfohlene Voraussetzungen (EN)**  
None.

**(!) Lernziele (DE)**  
Kenntnisse: Nach erfolgreichem Abschluss dieses Moduls sollten die Studierenden in der Lage sein, die Vor- und Nachteile verschiedener Methoden zur Problemlösung für die jeweilige Anwendungsdomäne darzustellen. | Fertigkeiten: Sie sollten in der Lage sein, Ideen zu brainstormen und zu filtern und iterativ eine Anwendung zu einem bestimmten Thema zu entwickeln. | Kompetenzen: Basierend auf den erworbenen Kenntnissen und Fähigkeiten sollten sie in der Lage sein, mündlich zu kommunizieren, in Teams zu arbeiten, Projekte zu planen und Meilensteine zu erreichen.

**(!) Lernziele (EN)**  
Knowledge: On successful completion of this module, students should be able to state the advantages and disadvantages of various methods used for solving problems for the given application domain. | Skills: They should be able to brainstorm and filter ideas and iteratively develop an application on a given topic. | Competences: Based on the knowledge and skills acquired they should be able to communicate orally, work in teams, plan projects and meet milestones.

**(!) Inhalt (DE)**  
Studierende sollen selbstständig fachspezifische Kenntnisse und Methoden der Konzeption, der Implementierung und dem Test von Soft- und Hardwaresystemen sowie bei der Durchführung von Experimenten und Messungen anwenden. Üblicherweise erfolgt die Bearbeitung einer Aufgabenstellung in Kleingruppen, um die Teamfähigkeit der Studierenden zu trainieren.

**(!) Inhalt (EN)**  
Students should independently apply subject-specific knowledge and methods of conception, implementation and testing of software and hardware systems as well as of experiments and measurements. Usually, a task is carried out in small groups in order to train the ability of the students to work in a team.

**(!) Literatur**  
keine Angabe


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSMI2019 | Medien-Informatik Praktika |  |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|