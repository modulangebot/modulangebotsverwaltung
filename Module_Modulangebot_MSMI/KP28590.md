
## Introduction to Natural Language Processing (KP28590)
Die folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO
**MSMI 2019**.
Weitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.

<!--
=== Allgemeine Hinweise: ===
-  Pflichtfelder sind mit "(!)" markiert.
-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.
-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->


### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Feld                    | Inhalt
| ------------------------|--------------------------------------------------
| (!) Modultitel (DE)     | Introduction to Natural Language Processing
| (!) Modultitel (EN)     | Introduction to Natural Language Processing
| (!) ECTS                | 6
| (!) Gültig ab           | 2023-04-01
|  Gültig bis             | 
| (!)&nbsp;ModulanbieterIn| Prof Dr. rer. nat. Lucie Flek
| (!) Sprache             | Englisch
| (!) Turnus              | Unregelmäßig
| (!) Moduldauer          | Einsemestrig
| (!) Modulniveau         | Master
| (!) Fachsemester        | keine Semesterempfehlung


### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)| (!) ECTS | (!) Präsenzzeit (SWS) |
|-------------------------------|------------------------------|----------|-----------------------|
| Prüfung Introduction to Natural Language Processing | Exam Introduction to Natural Language Processing | 6 |  |
| Vorlesung und Übung Introduction to Natural Language Processing | Lecture and Exercise Introduction to Natural Language Processing |  | 4 |


### 3. Studien- und Prüfungsleistungen  
*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*

**Teilnahmevoraussetzungen (DE)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|---------------------------|----------|------------------|
| keine Angabe | MSMI2019 | Multimedia-Technologie |

**Teilnahmevoraussetzungen (EN)**

| (!) Teilnahmevoraussetzung | (!) SPO | (!) Modulbereich |
|----------------------------|---------|------------------|
| keine Angabe | MSMI2019 | Multimedia-Technologie |

**(!) Prüfungsbedingungen (DE)**  
Die Modulprüfung besteht aus den folgenden Teilleistungen: Klausur (60 %); Projektarbeit (30 %); Schriftliche Hausarbeiten (10 %).

**(!) Prüfungsbedingungen (EN)**  
The module examination consists of the following partial examinations: Written exam (60 %); Project work (30 %); Written homework (10%).


### 4. Informationen für das Modulhandbuch  
*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*
<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
--> 

**Empfohlene Voraussetzungen (DE)**  
Basic programming knowledge in Python is of advantage, Basics of statistics recommended, basics of machine learning are of advantage

**Empfohlene Voraussetzungen (EN)**  
Basic programming knowledge in Python is of advantage, Basics of statistics recommended, basics of machine learning are of advantage

**(!) Lernziele (DE)**  
This class provides a technical perspective on NLP —methods for building computer software that understands and manipulates human language. Contemporary data-driven approaches are emphasized, focusing on machine learning techniques. Group work during programming exercises will allow to work on real-world NLP application projects. At the end of the course, students will be able to develop their own systems interpreting written language. The covered applications vary in complexity, including for example Entity Recognition, Argument Mining, or Emotion Analysis

**(!) Lernziele (EN)**  
This class provides a technical perspective on NLP —methods for building computer software that understands and manipulates human language. Contemporary data-driven approaches are emphasized, focusing on machine learning techniques. Group work during programming exercises will allow to work on real-world NLP application projects. At the end of the course, students will be able to develop their own systems interpreting written language. The covered applications vary in complexity, including for example Entity Recognition, Argument Mining, or Emotion Analysis

**(!) Inhalt (DE)**  
 An overview on NLP goals, challenges and applications Text representation (Words, sentences, paragraphs, documents), word embeddings, word2vec, BERT, word similarity Machine learning / deep learning algorithms for text classification, Transformers Basics of neural language modeling Basics of computational linguistics  transforming words to their baseforms (tokenization, stemming, lemmatization) syntactic analysis (part of speech tagging, chunking and parsing ) techniques for the extraction of meaning from text (semantic analysis) , use of lexical resources in NLP  NLP applications and projects (e.g. Sentiment analysis,Named entity recognition, Question ans- wering, Summarization, Fake news detection, Plagiarism detection, Abusive language detection, Opinion mining...) 

**(!) Inhalt (EN)**  
 An overview on NLP goals, challenges and applications Text representation (Words, sentences, paragraphs, documents), word embeddings, word2vec, BERT, word similarity Machine learning / deep learning algorithms for text classification, Transformers Basics of neural language modeling Basics of computational linguistics  transforming words to their baseforms (tokenization, stemming, lemmatization) syntactic analysis (part of speech tagging, chunking and parsing ) techniques for the extraction of meaning from text (semantic analysis) , use of lexical resources in NLP  NLP applications and projects (e.g. Sentiment analysis,Named entity recognition, Question ans- wering, Summarization, Fake news detection, Plagiarism detection, Abusive language detection, Opinion mining...) 

**(!) Literatur**  
 J. Eisenstein : Introduction to Natural language Processing Jurafsky, Daniel, and James H. Martin. "Speech and Language Processing: An Introduction to Natural Language Processing, Computational Linguistics, and Speech Recognition." S. Bird, E. Klein, E. Loper; Natural Language Processing with Python 


### 5a. Alle SPOen, in denen das Modul verankert ist
*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) SPO | (!) Modulbereich | (!) Version |
|------------------------| --------------------|-----------------------------------|
| MSMI2019 | Multimedia-Technologie |  |
<!--Ende Studiengangsliste-->


#### 5b. SPOen mit abweichenden Verwendungsspezifika:
<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

| (!) Feld | (!) Abweichung | (!) SPO | (!) Modulbereich | (!) Version |
|----------|----------------|---------|------------------|-------------|