## Modul NEU



<!--
=== Allgemeine Hinweise: ===
- PFLICHTfelder sind mit "(!)" markiert.
- Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.  
- Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! -
  nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.
- Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.
-->



### 1. Allgemeine Moduldaten
*[Erläuterungen zu Abschnitt 1](../Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*

| Moduleigenschaft            | Inhalt                                                 |
|-----------------------------|--------------------------------------------------------|
| (!) Modultitel (DE)         |                                                        |
| (!) Modultitel (EN)         |                                                        | 
| (!) ECTS                    |                                                        |
| (!) Gültig ab               | nächstmöglichem Zeitpunkt                              |
| Gültig bis                  | Ende offen                                             |
| (!) ModulanbieterIn         |                                                        |
| (!) Sprache                 | <!-- Deutsch # Englisch # Deutsch/Englisch -->         |
| (!) Turnus                  | <!-- WS # SS # WS+SS # Einmalig # Unregelmäßig -->     |
| (!) Moduldauer              | <!-- Einsemestrig # Zweisemestrig # Mehrsemestrig -->  |
| (!) Modulniveau             | <!-- Bachelor # Master # Bachelor/Master -->           |
| (!) Fachsemester            | 1                                                      |



### 2. Modulveranstaltungen
*[Erläuterungen zu Abschnitt 2](../Anleitungen_Hilfen/2_Modulveranstaltungen.md)*

| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN) | (!) ECTS | (!) Präsenzzeit (SWS) | 
|-------------------------------|-------------------------------|----------|-----------------------|
|                               |                               |          |                       |
|                               |                               |          |                       |
|                               |                               |          |                       |
|                               |                               |          |                       |
|                               |                               |          |                       |
|                               |                               |          |                       |
<!-- Zeilen bei Bedarf kopieren -->



### 3. Studien- und Prüfungsleistungen
*[Erläuterungen zu Abschnitt 3](../Anleitungen_Hilfen/3_Leistungen.md)*

**(!) Teilnahmevoraussetzungen (DE)**  
...

**(!) Teilnahmevoraussetzungen (EN)**  
...

**(!) Prüfungsbedingungen (DE)**  
...

**(!) Prüfungsbedingungen (EN)**  
...



### 4. Informationen für das Modulhandbuch
*[Erläuterungen zu Abschnitt 4](../Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*

<!-- 
=== Allgemeiner Hinweis ===
Wir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung
von Studiengängen benötigt werden.
-->

**(!) Empfohlene Voraussetzungen (DE)**  
...

**(!) Empfohlene Voraussetzungen (EN)**  
...

**(!) Lernziele (DE)**  
...

**(!) Lernziele (EN)**  
...

**(!) Inhalt (DE)**  
...

**(!) Inhalt (EN)**  
...

**(!) Literatur**  
...



### 5a. SPOen, in denen das Modul verankert werden soll

*[Erläuterungen zu Abschnitt 5a/5b](../Anleitungen_Hilfen/5_Studiengänge.md)*

| (!) Studiengangskürzel | (!) SPO-Version | (!) Modulbereich                   |
|------------------------|-----------------|------------------------------------|
|                        |                 |                                    |
|                        |                 |                                    |
|                        |                 |                                    |
|                        |                 |                                    |
|                        |                 |                                    |
|                        |                 |                                    |
<!-- Zeilen bei Bedarf kopieren -->



### 5b. SPOen mit abweichenden Verwendungsspezifika

<!--
=== Allgemeiner Hinweis: ===
Abweichende Verwendungsspezifika nach Möglichkeit vermeiden.
Fall notwendig, !je! Studiengang/SPO-Version/Modulbereich
das oder die abweichende(n) Verwendungsspezifika auflisten.
-->

Studiengangskürzel/SPO-Version/Modulbereich:  
...

Abweichende(s) Verwendungsspezifikum/-a:  
...



### Interne Kommentare (gehören NICHT zur Moduldefinition)
