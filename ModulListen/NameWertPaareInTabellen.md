# Aktuell auftretende Name/Wert-Paare mit Haeufigkeit

Typische sinnvolle (ggf. auch falsch gesetzte Werte)
sind so erkennbar/nutzbar.
Sortiert nach Name.
Bereinigt um stark ver�nderliche Werte (zB Titel)

generiert am:  Fri Feb 16 14:34:21 2024




| Anzahl | Name | Wert | 
| --     | --   | --   | 
  67x  | (!) ECTS |                                                            
   2x  | (!) ECTS | 1                                                          
   4x  | (!) ECTS | 10                                                         
   1x  | (!) ECTS | 15                                                         
   1x  | (!) ECTS | 18                                                         
   3x  | (!) ECTS | 2                                                          
  10x  | (!) ECTS | 3                                                          
   1x  | (!) ECTS | 30                                                         
  42x  | (!) ECTS | 4                                                          
   3x  | (!) ECTS | 5                                                          
 136x  | (!) ECTS | 6                                                          
   7x  | (!) ECTS | 7                                                          
  11x  | (!) ECTS | 8                                                          
 288x  | (!) Fachsemester | keine Semesterempfehlung                           
  52x  | (!) Gültig ab |                                                       
   1x  | (!) Gültig ab | 2001-10-01                                            
   6x  | (!) Gültig ab | 2006-10-01                                            
   2x  | (!) Gültig ab | 2007-04-01                                            
   1x  | (!) Gültig ab | 2007-10-01                                            
   1x  | (!) Gültig ab | 2008-04-01                                            
   1x  | (!) Gültig ab | 2008-10-01                                            
   2x  | (!) Gültig ab | 2009-04-01                                            
  11x  | (!) Gültig ab | 2009-10-01                                            
   3x  | (!) Gültig ab | 2010-04-01                                            
   1x  | (!) Gültig ab | 2010-10-01                                            
   1x  | (!) Gültig ab | 2011-10-01                                            
   1x  | (!) Gültig ab | 2013-10-01                                            
   1x  | (!) Gültig ab | 2014-04-01                                            
   6x  | (!) Gültig ab | 2014-10-01                                            
   1x  | (!) Gültig ab | 2015-04-01                                            
   3x  | (!) Gültig ab | 2015-10-01                                            
   1x  | (!) Gültig ab | 2016-10-01                                            
   1x  | (!) Gültig ab | 2017-04-01                                            
   2x  | (!) Gültig ab | 2017-10-01                                            
  33x  | (!) Gültig ab | 2018-04-01                                            
  94x  | (!) Gültig ab | 2018-10-01                                            
   1x  | (!) Gültig ab | 2018-11-29                                            
   6x  | (!) Gültig ab | 2019-04-01                                            
   2x  | (!) Gültig ab | 2019-10-01                                            
   2x  | (!) Gültig ab | 2020-04-01                                            
   3x  | (!) Gültig ab | 2020-10-01                                            
   6x  | (!) Gültig ab | 2021-04-01                                            
   7x  | (!) Gültig ab | 2021-10-01                                            
   1x  | (!) Gültig ab | 2022-03-31                                            
   2x  | (!) Gültig ab | 2022-04-01                                            
   6x  | (!) Gültig ab | 2022-10-01                                            
   4x  | (!) Gültig ab | 2023-04-01                                            
  19x  | (!) Gültig ab | 2023-10-01                                            
   1x  | (!) Gültig ab | 2024-04-01                                            
   3x  | (!) Gültig ab | 2024-10-01                                            
 207x  | (!) Moduldauer | Einsemestrig                                         
  10x  | (!) Moduldauer | Zweisemestrig                                        
  71x  | (!) Moduldauer | keine Angabe                                         
  25x  | (!) Modulniveau | Bachelor                                            
  51x  | (!) Modulniveau | Bachelor/Master                                     
 142x  | (!) Modulniveau | Master                                              
  70x  | (!) Modulniveau | keine Angabe                                        
  88x  | (!) Sprache | Deutsch                                                 
  20x  | (!) Sprache | Deutsch/Englisch                                        
 110x  | (!) Sprache | Englisch                                                
  70x  | (!) Sprache | keine Angabe                                            
  21x  | (!) Turnus | Sommersemester                                           
  19x  | (!) Turnus | Unregelmäßig                                             
  33x  | (!) Turnus | Wintersemester                                           
 144x  | (!) Turnus | Wintersemester/Sommersemester                            
  71x  | (!) Turnus | keine Angabe                                             
 282x  | Gültig bis |                                                          
   3x  | Gültig bis | 2024-03-31                                               
   3x  | Gültig bis | 2024-09-30                                               

   1x  C.M. Bishop, Pattern Recognition and Machine Learning, Springer, 2006. |
   1x  Dennis Komm, An introduction to online computation: determinism, randomi
   1x  Dieser Kurs beginnt mit einem Überblick über Ansätze und Technologien, d
   1x  Matt Bishop: Introduction to Computer Security, Addison-Wesley. | Ross A
   1x  Process Mining bietet ein neues Tool zur Verbesserung von Prozessen in e
   2x  R. O. Duda, P. E. Hart, D. G. Storck: Pattern Classification. 2nd ed., J
   1x  The lecture conveys concepts, methods and languages for the management o
   1x  This course starts with an overview of approaches and technologies that 
   1x  Tomas Akenine-Möller et al.: Real-Time Rendering (3rd Edition). Taylor &

