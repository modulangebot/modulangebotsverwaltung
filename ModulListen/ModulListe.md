# Liste aktuell verwalteter Module

sortiert nach dem Modulnamen in Deutsch;

generiert am:  Fri Feb 16 14:34:49 2024

* [Actions and Planning in AI: Learning, Models, and Algorithms](../Module_Modulangebot_FGI/1228568.md) (von: Hector Geffner)
* [Advanced Algorithmic Differentiation](../Module_Modulangebot_FGI/1221328.md) (von: Uwe Naumann)
* [Advanced Automata Theory](../Module_Modulangebot_FGI/1211981.md) (von: Christof Löding)
* [Advanced C++](../Module_Modulangebot_FGI/1228566.md) (von: Uwe Naumann)
* [Advanced Data Models](../Module_Modulangebot_FGI/1212673.md) (von: Matthias Jarke)
* [Advanced Internet Technology](../Module_Modulangebot_FGI/1215688.md) (von: Klaus Wehrle)
* [Advanced Machine Learning](../Module_Modulangebot_FGI/1211912.md) (von: Bastian Leibe)
* [Advanced Methods in Automatic Speech Recognition](../Module_Modulangebot_FGI/1211904.md) (von: Hermann Ney)
* [Advanced Process Mining](../Module_Modulangebot_FGI/1220136.md) (von: Wil van der Aalst)
* [Advanced Statistical Classification](../Module_Modulangebot_FGI/1212684.md) (von: Hermann Ney)
* [Advanced Topics in Statistical Natural Language Processing](../Module_Modulangebot_FGI/1212685.md) (von: Hermann Ney)
* [Algorithmen und Datenstrukturen](../Module_Modulangebot_FGI/1212366.md) (von: Fachgruppe Informatik)
* [Algorithmen zur String-Verarbeitung und Techniken zur Datenkompression](../Module_Modulangebot_FGI/1212657.md) (von: Michael Elberfeld)
* [Algorithmic Foundations of Datascience](../Module_Modulangebot_FGI/1216860.md) (von: keine Angabe)
* [Algorithmische Kryptographie](../Module_Modulangebot_FGI/1212358.md) (von: Walter Unger)
* [Algorithmische Lerntheorie](../Module_Modulangebot_FGI/1217537.md) (von: Martin Grohe)
* [Algorithmische Spieltheorie](../Module_Modulangebot_FGI/1212326.md) (von: Gerhard Wöginger)
* [Algorithms for Politics](../Module_Modulangebot_FGI/1223159.md) (von: Gerhard Woeginger)
* [Analyse von Algorithmen](../Module_Modulangebot_FGI/1212337.md) (von: Peter Rossmanith)
* [Anerkennung Zusätzliche Prüfungsleistung](../Module_Modulangebot_FGI/1222464.md) (von: keine Angabe)
* [Anerkennung als Zusätzliche Prüfungsleistung](../Module_Modulangebot_FGI/1222558.md) (von: keine Angabe)
* [Anerkennung aus dem Ausland](../Module_Modulangebot_FGI/1229747.md) (von: keine Angabe)
* [Anerkennung im Anwendungsfach](../Module_Modulangebot_FGI/1219123.md) (von: keine Angabe)
* [Anerkennung im Bereich Kommunikationsfertigkeiten](../Module_Modulangebot_FGI/1219554.md) (von: keine Angabe)
* [Anerkennung im Bereich Medien-Informatik Praktika](../Module_Modulangebot_FGI/1219552.md) (von: keine Angabe)
* [Anerkennung im Bereich Multimedia-Benutzung und -Wirkung](../Module_Modulangebot_FGI/1219550.md) (von: keine Angabe)
* [Anerkennung im Bereich Multimedia-Technologie](../Module_Modulangebot_FGI/1219548.md) (von: keine Angabe)
* [Anerkennung im Bereich Rechner- und Kommunikationstechnologie](../Module_Modulangebot_FGI/1219545.md) (von: keine Angabe)
* [Anerkennung im Kernbereich Software Engineering](../Module_Modulangebot_FGI/1230089.md) (von: keine Angabe)
* [Anerkennung im Nicht-technischen Wahlfach](../Module_Modulangebot_FGI/1218687.md) (von: keine Angabe)
* [Anerkennung im Vertiefungsbereich Applied Computer Science](../Module_Modulangebot_FGI/1219506.md) (von: keine Angabe)
* [Anerkennung im Vertiefungsbereich Communication](../Module_Modulangebot_FGI/1219508.md) (von: keine Angabe)
* [Anerkennung im Vertiefungsbereich Data and Information Management](../Module_Modulangebot_FGI/1219510.md) (von: keine Angabe)
* [Anerkennung im Vertiefungsbereich Software Engineering](../Module_Modulangebot_FGI/1219512.md) (von: keine Angabe)
* [Anerkennung im Vertiefungsbereich Theoretical Foundations of Software Systems Engingeering](../Module_Modulangebot_FGI/1219504.md) (von: keine Angabe)
* [Anerkennung im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1218713.md) (von: keine Angabe)
* [Anerkennung im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1219584.md) (von: keine Angabe)
* [Anerkennung im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1218714.md) (von: keine Angabe)
* [Anerkennung im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1219586.md) (von: keine Angabe)
* [Anerkennung im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1218712.md) (von: keine Angabe)
* [Anerkennung im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1219588.md) (von: keine Angabe)
* [Anerkennung im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1218711.md) (von: keine Angabe)
* [Anerkennung im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1219590.md) (von: keine Angabe)
* [Anerkennung im Wahlpflichtbereich](../Module_Modulangebot_FGI/1218603.md) (von: keine Angabe)
* [Anerkennung im Wahlpflichtbereich](../Module_Modulangebot_FGI/1219220.md) (von: keine Angabe)
* [Anerkennung à 4 CP](../Module_Modulangebot_FGI/1219223.md) (von: keine Angabe)
* [Anerkennung à 6 CP](../Module_Modulangebot_FGI/1219224.md) (von: keine Angabe)
* [Angewandte Softwaretechnik im Lebenszyklus der Automobilelektronik](../Module_Modulangebot_FGI/1215757.md) (von: Ansgar Schleicher Bernhard Rumpe)
* [Automaten, Sprachen, Komplexität](../Module_Modulangebot_FGI/1212373.md) (von: Christof Löding)
* [Automatic Generation and Analysis of Algorithms](../Module_Modulangebot_FGI/1212693.md) (von: Paolo Bientinesi)
* [Automatic Speech Recognition Search](../Module_Modulangebot_FGI/1230105.md) (von: Priv.-Doz. Ralf Schlüter)
* [Automatische Spracherkennung](../Module_Modulangebot_FGI/1215750.md) (von: Hermann Ney)
* [Automatisierung einer Destillationsanlage](../Module_Modulangebot_FGI/1220403.md) (von: keine Angabe)
* [Außercurriculares Modul im Anwendungsfach](../Module_Modulangebot_FGI/1219183.md) (von: keine Angabe)
* [Außercurriculares Modul im Anwendungsfach](../Module_Modulangebot_FGI/1220849.md) (von: keine Angabe)
* [Außercurriculares Modul im Nicht-technischen Wahlfach](../Module_Modulangebot_FGI/1218686.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich Angewandte Informatik Master Informatik](../Module_Modulangebot_FGI/1219018.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1218718.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich Daten- und Informationsmanagement Master Informatik](../Module_Modulangebot_FGI/1219021.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1218717.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich Grafik und Interaktion Master Informatik](../Module_Modulangebot_FGI/1229936.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich Hardware/Software-Systeme Master Informatik](../Module_Modulangebot_FGI/1229937.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich KI & Daten Master Informatik](../Module_Modulangebot_FGI/1229938.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich Software und Kommunikation Master Informatik](../Module_Modulangebot_FGI/1219020.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich Software und Kommuniktion](../Module_Modulangebot_FGI/1218716.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich Software- Entwicklungsmethoden und -Werkzeuge Master Informatik](../Module_Modulangebot_FGI/1229893.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich Theoretische Informatik Master Informatik](../Module_Modulangebot_FGI/1219023.md) (von: keine Angabe)
* [Außercurriculares Modul im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1218715.md) (von: keine Angabe)
* [Bachelorarbeit](../Module_Modulangebot_FGI/1215682.md) (von: Fachgruppe Informatik)
* [Berechenbarkeit und Komplexität](../Module_Modulangebot_FGI/1212004.md) (von: Joost-Pieter Katoen Martin Grohe Peter Rossmanith Jürgen Giesl)
* [Betriebssysteme und Systemsoftware](../Module_Modulangebot_FGI/1214960.md) (von: Klaus Wehrle Hermann Ney)
* [Big Data in Medical Informatics](../Module_Modulangebot_FGI/1212676.md) (von: Stefan Decker)
* [Brückenkurs Algorithmen und Datenstrukturen](../Module_Modulangebot_FGI/1225312.md) (von: Gerhard Woeginger)
* [Brückenkurs Datenbanken](../Module_Modulangebot_FGI/1225948.md) (von: Stefan Decker, PD Ralf Klamma)
* [Business Process Intelligence](../Module_Modulangebot_FGI/1216958.md) (von: Wil van der Aalst)
* [Business Process Modeling & Computation](../Module_Modulangebot_FGI/1229150.md) (von: Leemans)
* [CSCW and Groupware: Concepts and Systems for Computer Supported Cooperative Work](../Module_Modulangebot_FGI/1215691.md) (von: Wolfgang Prinz)
* [Combinatorial Problems in Scientific Computing](../Module_Modulangebot_FGI/1215721.md) (von: Uwe Naumann)
* [Communication Systems Engineering](../Module_Modulangebot_FGI/1212349.md) (von: Klaus Wehrle & Dirk Thißen)
* [Compilerbau](../Module_Modulangebot_FGI/1211978.md) (von: Thomas Noll Joost-Pieter Katoen)
* [Computational Group Theory](../Module_Modulangebot_FGI/1212656.md) (von: Pascal Schweitzer)
* [Computer Vision 2](../Module_Modulangebot_FGI/1211921.md) (von: Bastian Leibe)
* [Computer Vision](../Module_Modulangebot_FGI/1215724.md) (von: Bastian Leibe)
* [Concurrency Theory](../Module_Modulangebot_FGI/1212646.md) (von: Thomas Noll Joost-Pieter Katoen)
* [Current Topics in Media Computing and HCI](../Module_Modulangebot_FGI/1211908.md) (von: Jan Oliver Borchers)
* [Data Analysis and Visualization](../Module_Modulangebot_FGI/1212372.md) (von: Leif Kobbelt)
* [Data Driven Medicine - project-oriented, multidisciplinary introduction](../Module_Modulangebot_FGI/1215842.md) (von: Stefan Decker & Univ.- med. Rainer Röhrig & Oya Deniz Beyan)
* [Datenbanken und Informationssysteme](../Module_Modulangebot_FGI/1211969.md) (von: Stefan Decker &; Matthias Jarke)
* [Datenkommunikation](../Module_Modulangebot_FGI/1211972.md) (von: Klaus Wehrle)
* [Datenstrommanagement und -analyse](../Module_Modulangebot_FGI/1226146.md) (von: Sandra Geisler)
* [Datenstrukturen und Algorithmen](../Module_Modulangebot_FGI/1211971.md) (von: Leif Kobbelt &; Joost-Pieter Katoen &; Hermann Ney &; Peter Rossmanith)
* [Deduktive Programmverifikation](../Module_Modulangebot_FGI/1212659.md) (von: Jürgen Giesl)
* [Der digitale Lebenszyklus von Fahrzeugen als Teil des Internet of Things (IoT)](../Module_Modulangebot_FGI/1215755.md) (von: Ansgar Schleicher; Bernhard Rumpe)
* [Designing Interactive Systems II](../Module_Modulangebot_FGI/1215699.md) (von: Jan Oliver Borchers)
* [Designing Interactive Systems I](../Module_Modulangebot_FGI/1215698.md) (von: Jan Oliver Borchers)
* [Deutschkurs](../Module_Modulangebot_FGI/1215734.md) (von: Sprachenzentrum)
* [Didaktische Zugänge zur informatischen Bildung](../Module_Modulangebot_FGI/1228824.md) (von: Nadine Bergner)
* [Didaktische Zugänge zur informatischen Bildung](../Module_Modulangebot_FGI/1228884.md) (von: Nadine Bergner)
* [Digitalisierung](../Module_Modulangebot_FGI/1220230.md) (von: Bernhard Rumpe)
* [Diskrete Differentialgeometrie](../Module_Modulangebot_FGI/1212694.md) (von: David Bommes)
* [Distributed Ledger Technology](../Module_Modulangebot_FGI/1226006.md) (von: Rose &; Prinz)
* [Dynamical Processes on Networks](../Module_Modulangebot_FGI/1223640.md) (von: keine Angabe)
* [Effiziente Algorithmen](../Module_Modulangebot_FGI/1211977.md) (von: Peter Rossmanith Walter Unger)
* [Einführung in Web Technologien](../Module_Modulangebot_FGI/1211914.md) (von: Ulrik Schroeder)
* [Einführung in das wissenschaftliche Arbeiten (Proseminar Informatik)](../Module_Modulangebot_FGI/1211968.md) (von: Fachgruppe Informatik)
* [Einführung in die Fachdidaktik Informatik](../Module_Modulangebot_FGI/1212317.md) (von: Nadine Bergner)
* [Einführung in die Informatik](../Module_Modulangebot_FGI/1212365.md) (von: Unbekannt)
* [Einführung in die Programmierung für datenbasierte Wissenschaften](../Module_Modulangebot_FGI/1224007.md) (von: Ulrik Schroeder)
* [Einführung in die Programmierung](../Module_Modulangebot_FGI/1215679.md) (von: Uwe Naumann)
* [Einführung in die Technische Informatik](../Module_Modulangebot_FGI/1214958.md) (von: Stefan Kowalewski Gerhard Lakemeyer)
* [Eingebettete Systeme](../Module_Modulangebot_FGI/1215690.md) (von: Stefan Kowalewski)
* [Elements of Machine Learning and Data Science](../Module_Modulangebot_FGI/1226970.md) (von: keine Angabe)
* [Erfüllbarkeitsüberprüfung](../Module_Modulangebot_FGI/1212341.md) (von: Erika Ábrahám)
* [Ersatzveranstaltung - Individuelles Modul](../Module_Modulangebot_FGI/1222030.md) (von: keine Angabe)
* [Exakte Algorithmen](../Module_Modulangebot_FGI/1212658.md) (von: Peter Rossmanith)
* [Fachdidaktik Informatik](../Module_Modulangebot_FGI/1212322.md) (von: Nadine Bergner)
* [Fachdidaktik Informatik](../Module_Modulangebot_FGI/1227857.md) (von: Ulrik Schroeder)
* [Faszination Technik in der Informatik](../Module_Modulangebot_FGI/1212323.md) (von: Ulrik Schroeder)
* [Fixpoints and Induction in Logic and Computer Science](../Module_Modulangebot_FGI/1226911.md) (von: Christof Löding)
* [Formale Grundlagen von UML](../Module_Modulangebot_FGI/1212648.md) (von: Joost-Pieter Katoen)
* [Formale Methoden für Steuerungssoftware](../Module_Modulangebot_FGI/1212666.md) (von: Stefan Kowalewski Hendrik Simon)
* [Formale Systeme, Automaten, Prozesse](../Module_Modulangebot_FGI/1214961.md) (von: Joost-Pieter Katoen Martin Grohe Jürgen Giesl Peter Rossmanith)
* [Forschungsmodul](../Module_Modulangebot_FGI/1211976.md) (von: Fachgruppe Informatik)
* [Fortgeschrittene Methoden der Virtuellen Realität](../Module_Modulangebot_FGI/1212688.md) (von: Torsten Wolfgang Kuhlen)
* [Foundations of Functional Programming](../Module_Modulangebot_FGI/1215684.md) (von: Jürgen Giesl)
* [Foundations of Logic Programming](../Module_Modulangebot_FGI/1212343.md) (von: Jürgen Giesl)
* [Functions of Matrices with Applications](../Module_Modulangebot_FGI/1215727.md) (von: Paolo Bientinesi)
* [Fundamentals of Automatic Speech Recognition](../Module_Modulangebot_FGI/1230106.md) (von: Priv.-Doz. Ralf Schlüter)
* [Fundamentals of Business Process Management](../Module_Modulangebot_FGI/1227457.md) (von: Sander Leemans)
* [Funktionale Sicherheit und Systemzuverlässigkeit](../Module_Modulangebot_FGI/1212353.md) (von: Stefan Kowalewski)
* [Geometrieverarbeitung](../Module_Modulangebot_FGI/1215696.md) (von: Leif Kobbelt)
* [Grafikprogrammierung in OpenGL](../Module_Modulangebot_FGI/1212686.md) (von: Leif Kobbelt)
* [Graphalgorithmen](../Module_Modulangebot_FGI/1212327.md) (von: Walter Unger)
* [Graphzerlegungen und algorithmische Anwendungen](../Module_Modulangebot_FGI/1212710.md) (von: Martin Grohe)
* [Grundlagen der Computergraphik](../Module_Modulangebot_FGI/1212310.md) (von: Leif Kobbelt)
* [Grundzüge der Informatik](../Module_Modulangebot_FGI/1212315.md) (von: keine Angabe)
* [High-Performance Computing](../Module_Modulangebot_FGI/1215720.md) (von: Matthias Müller)
* [High-performance Matrix Computations](../Module_Modulangebot_FGI/1211911.md) (von: Paolo Bientinesi)
* [Höhere Algorithmik](../Module_Modulangebot_FGI/1223638.md) (von: keine Angabe)
* [IT-Sicherheit 1 - Kryptographische Grundlagen und Netzwerksicherheit](../Module_Modulangebot_FGI/1211901.md) (von: Ulrike Meyer)
* [IT-Sicherheit 2 - Computer Security](../Module_Modulangebot_FGI/1211900.md) (von: Ulrike Meyer)
* [IT-Sicherheit](../Module_Modulangebot_FGI/1226971.md) (von: keine Angabe)
* [Implementation of Databases](../Module_Modulangebot_FGI/1215692.md) (von: Matthias Jarke)
* [Indexstrukturen für Datenbanken](../Module_Modulangebot_FGI/1212679.md) (von: Thomas Seidl)
* [Individuelles Modul Zusätzliche Prüfungen](../Module_Modulangebot_FGI/1229767.md) (von: keine Angabe)
* [Individuelles Modul im Bereich Kommunikationsfertigkeiten](../Module_Modulangebot_FGI/1219553.md) (von: keine Angabe)
* [Individuelles Modul im Bereich Medien-Informatik Praktika](../Module_Modulangebot_FGI/1219551.md) (von: keine Angabe)
* [Individuelles Modul im Bereich Multimedia-Benutzung und -Wirkung](../Module_Modulangebot_FGI/1219549.md) (von: keine Angabe)
* [Individuelles Modul im Bereich Multimedia-Technologie](../Module_Modulangebot_FGI/1219547.md) (von: keine Angabe)
* [Individuelles Modul im Bereich Rechner- und Kommunikationstechnologie](../Module_Modulangebot_FGI/1219543.md) (von: keine Angabe)
* [Individuelles Modul im Mastervorzug](../Module_Modulangebot_FGI/1222479.md) (von: keine Angabe)
* [Individuelles Modul im Vertiefungsbereich Applied Computer Science](../Module_Modulangebot_FGI/1219505.md) (von: keine Angabe)
* [Individuelles Modul im Vertiefungsbereich Communication](../Module_Modulangebot_FGI/1219507.md) (von: keine Angabe)
* [Individuelles Modul im Vertiefungsbereich Data and Information Management](../Module_Modulangebot_FGI/1219509.md) (von: keine Angabe)
* [Individuelles Modul im Vertiefungsbereich Software Engineering](../Module_Modulangebot_FGI/1219511.md) (von: keine Angabe)
* [Individuelles Modul im Vertiefungsbereich Theoretical Foundations of Software Systems Engineering](../Module_Modulangebot_FGI/1219503.md) (von: keine Angabe)
* [Individuelles Modul im Vertiefungsbereich](../Module_Modulangebot_FGI/1221648.md) (von: keine Angabe)
* [Individuelles Modul im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1219583.md) (von: keine Angabe)
* [Individuelles Modul im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1219585.md) (von: keine Angabe)
* [Individuelles Modul im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1219587.md) (von: keine Angabe)
* [Individuelles Modul im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1219589.md) (von: keine Angabe)
* [Individuelles Modul im Wahlpflichtbereich](../Module_Modulangebot_FGI/1219222.md) (von: keine Angabe)
* [Industrial Network Security](../Module_Modulangebot_FGI/1227956.md) (von: Martin Henze)
* [Infinite Computations and Games](../Module_Modulangebot_FGI/1212336.md) (von: Christof Löding)
* [Infinite Games](../Module_Modulangebot_FGI/1212333.md) (von: Wolfgang Thomas)
* [Informatik (WP VT)](../Module_Modulangebot_FGI/1212321.md) (von: keine Angabe)
* [Informatik-Praktikum für Mathematiker](../Module_Modulangebot_FGI/1212243.md) (von: Christof Löding)
* [Informatik](../Module_Modulangebot_FGI/1212313.md) (von: Fachgruppe Informatik)
* [Informatik](../Module_Modulangebot_FGI/1212318.md) (von: Unbekannt)
* [Informatik](../Module_Modulangebot_FGI/1212319.md) (von: keine Angabe)
* [Information Management](../Module_Modulangebot_FGI/1211923.md) (von: Matthias Jarke)
* [Inhaltsbasierte Ähnlichkeitssuche](../Module_Modulangebot_FGI/1211929.md) (von: Thomas Seidl)
* [Innovationen im Software Engineering](../Module_Modulangebot_FGI/1212345.md) (von: Bernhard Rumpe)
* [Introduction to Algorithmic Differentiation](../Module_Modulangebot_FGI/1221327.md) (von: Uwe Naumann)
* [Introduction to Artificial Intelligence](../Module_Modulangebot_FGI/1220228.md) (von: MAO Informatik)
* [Introduction to Bioinformatics](../Module_Modulangebot_FGI/1211903.md) (von: Thomas Berlage)
* [Introduction to Data Science](../Module_Modulangebot_FGI/1216861.md) (von: Wil van der Aalst)
* [Introduction to Numerical Methods and Software with C++](../Module_Modulangebot_FGI/1229154.md) (von: Uwe Naumann)
* [Introduction to Numerical Methods and Software](../Module_Modulangebot_FGI/1220996.md) (von: Uwe Naumann)
* [Kombinatorische Optimierung in der wissenschaftlichen Praxis](../Module_Modulangebot_FGI/1228925.md) (von: Christina Büsing)
* [Komplexitätstheorie](../Module_Modulangebot_FGI/1212331.md) (von: Martin Grohe)
* [Konzepte und Modelle der parallelen und datenzentrischen Programmierung](../Module_Modulangebot_FGI/1216838.md) (von: Christian Terboven & Matthias S. Müller)
* [Künstliche Intelligenz](../Module_Modulangebot_FGI/1215694.md) (von: Gerhard Lakemeyer)
* [Learning Technologies](../Module_Modulangebot_FGI/1215751.md) (von: Ulrik Schroeder)
* [Leistungs- und Korrektheitsanalyse paralleler Programme](../Module_Modulangebot_FGI/1215722.md) (von: Matthias Müller)
* [Linux Kernel Programming](../Module_Modulangebot_FGI/1229308.md) (von: Univ.- Redha Gouicem)
* [Machine Learning with Graphs: Foundations and Applications](../Module_Modulangebot_FGI/1227996.md) (von: keine Angabe)
* [Machine Learning](../Module_Modulangebot_FGI/1215744.md) (von: Bastian Leibe)
* [Masterarbeit](../Module_Modulangebot_FGI/1212703.md) (von: Fachgruppe Informatik)
* [Mentoring Informatik](../Module_Modulangebot_FGI/1214959.md) (von: Uwe Naumann)
* [Mikrocontrollerprogrammierung und Fehlersuche](../Module_Modulangebot_FGI/1220524.md) (von: André Stollenwerk)
* [Mobile Internet Technology](../Module_Modulangebot_FGI/1212346.md) (von: Klaus Wehrle & Dirk Thißen)
* [Model Checking](../Module_Modulangebot_FGI/1212328.md) (von: Wolfgang Thomas Joost-Pieter Katoen)
* [Model-based Systems Engineering](../Module_Modulangebot_FGI/1222882.md) (von: keine Angabe)
* [Modeling and Verification of Probabilistic Systems](../Module_Modulangebot_FGI/1212711.md) (von: Joost-Pieter Katoen)
* [Modellbasierte Softwareentwicklung](../Module_Modulangebot_FGI/1215686.md) (von: Bernhard Rumpe)
* [Modelle der Datenexploration](../Module_Modulangebot_FGI/1216218.md) (von: Thomas Seidl)
* [Modellierung und Analyse hybrider Systeme](../Module_Modulangebot_FGI/1212339.md) (von: Erika Abraham)
* [Nicht-technisches Wahlfach Mentoring](../Module_Modulangebot_FGI/1220643.md) (von: keine Angabe)
* [Objektorientierte Softwarekonstruktion](../Module_Modulangebot_FGI/1212354.md) (von: Horst Lichter)
* [Online Algorithmen](../Module_Modulangebot_FGI/1212645.md) (von: Gerhard Wöginger & Walter Unger)
* [Parallele Algorithmen](../Module_Modulangebot_FGI/1212689.md) (von: Martin Bücker)
* [Parallele Programmierung II](../Module_Modulangebot_FGI/1215726.md) (von: Fachgruppe Informatik)
* [Parametrisierte Algorithmen](../Module_Modulangebot_FGI/1212338.md) (von: Peter Rossmanith)
* [Personal Digital Fabrication](../Module_Modulangebot_FGI/1216839.md) (von: Jan Oliver Borchers Simon Völker)
* [Physikalisch-Basierte Animation](../Module_Modulangebot_FGI/1215862.md) (von: Jan Stephen Bender)
* [Physikalische Simulation im Visual Computing](../Module_Modulangebot_FGI/1212692.md) (von: keine Angabe)
* [Post-quantum cryptography](../Module_Modulangebot_FGI/1229347.md) (von: ;Dominique Unruh)
* [Praktikum Angewandte Informatik](../Module_Modulangebot_FGI/1211937.md) (von: Fachgruppe Informatik)
* [Praktikum Daten und Informationsmanagement](../Module_Modulangebot_FGI/1211935.md) (von: Fachgruppe Informatik)
* [Praktikum Inklusion und Heterogenität](../Module_Modulangebot_FGI/1215685.md) (von: Nadine Bergner)
* [Praktikum Software und Kommunikation](../Module_Modulangebot_FGI/1211934.md) (von: Fachgruppe Informatik)
* [Praktikum Theoretische Informatik](../Module_Modulangebot_FGI/1215659.md) (von: Fachgruppe Informatik)
* [Praktikum](../Module_Modulangebot_FGI/1215759.md) (von: Fachgruppe Informatik)
* [Praktische Informatik](../Module_Modulangebot_FGI/1212316.md) (von: Fachgruppe Informatik.)
* [Privacy Enhancing Technologies for Data Science](../Module_Modulangebot_FGI/1212677.md) (von: Stefan Decker)
* [Probabilistic Programming](../Module_Modulangebot_FGI/1212650.md) (von: Joost-Pieter Katoen)
* [Programmierung](../Module_Modulangebot_FGI/1214957.md) (von: Joost-Pieter Katoen &; Ulrik Schroeder &; Jürgen Giesl)
* [Prozess Management](../Module_Modulangebot_FGI/1211902.md) (von: Thomas Rose)
* [Prozesse und Methoden beim Testen von Software](../Module_Modulangebot_FGI/1215732.md) (von: Bernhard Rumpe)
* [Randomized Algorithms](../Module_Modulangebot_FGI/1212705.md) (von: Berthold Vöcking)
* [Real-time Graphics](../Module_Modulangebot_FGI/1215680.md) (von: Leif Kobbelt)
* [Recht in der Informatik](../Module_Modulangebot_FGI/1227459.md) (von: Honorarprofessor jur. Uwe Meiendresch)
* [Regelung und Wahrnehmung in Vernetzten und Autonomen Fahrzeugen](../Module_Modulangebot_FGI/1221329.md) (von: Bassam Alrifaee)
* [Rekursionstheorie](../Module_Modulangebot_FGI/1212335.md) (von: Martin Grohe)
* [Research Focus Class on Communication Systems](../Module_Modulangebot_FGI/1212347.md) (von: keine Angabe)
* [Research Focus Class on Learning Technologies](../Module_Modulangebot_FGI/1222419.md) (von: keine Angabe)
* [Schwerpunktkolloquium Angewandte Informatik](../Module_Modulangebot_FGI/1212704.md) (von: Fachgruppe Informatik)
* [Schwerpunktkolloquium Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212702.md) (von: Fachgruppe Informatik)
* [Schwerpunktkolloquium Software und Kommunikation](../Module_Modulangebot_FGI/1212701.md) (von: Fachgruppe Informatik)
* [Schwerpunktkolloquium Theoretische Informatik](../Module_Modulangebot_FGI/1212700.md) (von: Fachgruppe Informatik)
* [Schwerpunktkolloquium](../Module_Modulangebot_FGI/1222540.md) (von: keine Angabe)
* [Selected Topics in Communication and Distributed Systems](../Module_Modulangebot_FGI/1212665.md) (von: Klaus Wehrle)
* [Semantic Web](../Module_Modulangebot_FGI/1212675.md) (von: Stefan Decker)
* [Semantik und Verifikation von Software](../Module_Modulangebot_FGI/1212329.md) (von: Thomas Noll)
* [Seminar I Angewandte Informatik](../Module_Modulangebot_FGI/1211931.md) (von: Fachgruppe Informatik)
* [Seminar I Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212698.md) (von: Fachgruppe Informatik)
* [Seminar I Software und Kommunikation](../Module_Modulangebot_FGI/1212696.md) (von: Fachgruppe Informatik)
* [Seminar I Theoretische Informatik](../Module_Modulangebot_FGI/1211938.md) (von: Fachgruppe Informatik)
* [Seminar II Angewandte Informatik](../Module_Modulangebot_FGI/1211932.md) (von: Fachgruppe Informatik)
* [Seminar II Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212699.md) (von: Fachgruppe Informatik)
* [Seminar II Software und Kommunikation](../Module_Modulangebot_FGI/1212697.md) (von: Fachgruppe Informatik)
* [Seminar II Theoretische Informatik](../Module_Modulangebot_FGI/1211930.md) (von: Fachgruppe Informatik)
* [Seminar Informatik](../Module_Modulangebot_FGI/1211974.md) (von: Fachgruppe Informatik)
* [Seminar: Topics in Automation, Compilers and Code-Generation](../Module_Modulangebot_FGI/1215729.md) (von: Paolo Bientinesi Prüfungsamt ZPA)
* [Seminar: Topics in High-Performance and Scientific Computing](../Module_Modulangebot_FGI/1215728.md) (von: Paolo Bientinesi)
* [Seminar](../Module_Modulangebot_FGI/1212324.md) (von: Fachgruppe Informatik)
* [Shape Analysis and 3D Deep Learning](../Module_Modulangebot_FGI/1223639.md) (von: keine Angabe)
* [SiSc Laboratory](../Module_Modulangebot_FGI/1212320.md) (von: Uwe Naumann)
* [Sicherheit in der Mobilkommunikation](../Module_Modulangebot_FGI/1212681.md) (von: Ulrike Meyer)
* [Sicherheit und Zuverlässigkeit softwaregesteuerter Systeme (bis WS 14/15)](../Module_Modulangebot_FGI/1212351.md) (von: Stefan Kowalewski)
* [Social Computing](../Module_Modulangebot_FGI/1212678.md) (von: Ralf Klamma)
* [Social and Technological Change](../Module_Modulangebot_FGI/1229157.md) (von: Geffner)
* [Software Language Engineering](../Module_Modulangebot_FGI/1216957.md) (von: Bernhard Rumpe)
* [Software-Architekturen](../Module_Modulangebot_FGI/1215687.md) (von: Manfred Nagl)
* [Software-Projektmanagement](../Module_Modulangebot_FGI/1212355.md) (von: Horst Lichter)
* [Software-Projektpraktikum](../Module_Modulangebot_FGI/1211973.md) (von: Horst Lichter & Fachgruppe Informatik)
* [Software-Qualitätssicherung](../Module_Modulangebot_FGI/1212356.md) (von: Horst Lichter)
* [Softwarepraktikum](../Module_Modulangebot_FGI/1212636.md) (von: Jan Oliver Borchers)
* [Softwaretechnik-Programmiersprache Ada 95](../Module_Modulangebot_FGI/1211982.md) (von: Manfred Nagl)
* [Softwaretechnik](../Module_Modulangebot_FGI/1211965.md) (von: Bernhard Rumpe)
* [Sprachkurs](../Module_Modulangebot_FGI/1225243.md) (von: keine Angabe)
* [Sprachkurs](../Module_Modulangebot_FGI/1225244.md) (von: keine Angabe)
* [Sprachmodul 1](../Module_Modulangebot_FGI/1229768.md) (von: keine Angabe)
* [Sprachmodul 2](../Module_Modulangebot_FGI/1230044.md) (von: keine Angabe)
* [Sprachmodul 3](../Module_Modulangebot_FGI/1230045.md) (von: keine Angabe)
* [Statische Programmanalyse](../Module_Modulangebot_FGI/1212330.md) (von: Thomas Noll)
* [Statistische Klassifikation und Maschinelles Lernen](../Module_Modulangebot_FGI/1215840.md) (von: Hermann Ney)
* [Statistische Methoden zur Verarbeitung natürlicher Sprache](../Module_Modulangebot_FGI/1215695.md) (von: Hermann Ney)
* [Stochastic Games](../Module_Modulangebot_FGI/1219623.md) (von: Priv.-Doz. Christof Löding)
* [Subdivision Kurven und Flächen](../Module_Modulangebot_FGI/1215697.md) (von: Leif Kobbelt)
* [Systemprogrammierung](../Module_Modulangebot_FGI/1211967.md) (von: Stefan Kowalewski)
* [Termersetzungssysteme](../Module_Modulangebot_FGI/1212340.md) (von: Jürgen Giesl)
* [Testen reaktiver Systeme](../Module_Modulangebot_FGI/1212649.md) (von: Joost-Pieter Katoen)
* [The Graph Isomorphism Problem](../Module_Modulangebot_FGI/1212654.md) (von: Martin Grohe)
* [The Logic of Knowledge Bases](../Module_Modulangebot_FGI/1211393.md) (von: Gerhard Lakemeyer)
* [Theory of Constraint Satisfaction Problems](../Module_Modulangebot_FGI/1212334.md) (von: Martin Grohe)
* [Theory of Distributed Systems](../Module_Modulangebot_FGI/1212641.md) (von: keine Angabe)
* [Theory of Distributed and Parallel Systems](../Module_Modulangebot_FGI/1212643.md) (von: Walter Unger)
* [Uncertainty in Robotics](../Module_Modulangebot_FGI/1222468.md) (von: Lakemeyer)
* [Virtuelle Realität](../Module_Modulangebot_FGI/1211909.md) (von: Torsten Wolfgang Kuhlen)
* [Vorbereitungskurs zum Softwareentwicklungspraktikum und Softwareentwicklungspraktikum](../Module_Modulangebot_FGI/1212371.md) (von: Uwe Naumann)
* [Web Science](../Module_Modulangebot_FGI/1212359.md) (von: Matthias Jarke)
* [Wissensrepräsentation](../Module_Modulangebot_FGI/1212361.md) (von: Gerhard Lakemeyer)
* [Zusatzkompetenz 1](../Module_Modulangebot_FGI/1219455.md) (von: keine Angabe)
* [Zusatzkompetenz](../Module_Modulangebot_FGI/1226286.md) (von: keine Angabe)
* [eBusiness - Anwendungen, Architekturen und Standards](../Module_Modulangebot_FGI/1212683.md) (von: Thomas Rose)
* [iOS Application Development](../Module_Modulangebot_FGI/1215681.md) (von: Jan Oliver Borchers &; Simon Völker)

