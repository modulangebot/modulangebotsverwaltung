# Liste verwalteter Module (nach Anbieter)

sortiert nach dem Anbieter sortiert

generiert am:  Fri Feb 16 14:34:49 2024

| Anbieter | Titel | Studiengang | Aenderung am | 
| -- | -- | -- | -- | 
| ;Dominique Unruh | [Post-quantum cryptography](../Module_Modulangebot_FGI/1229347.md) |  | 2024-02-16 |
| André Stollenwerk | [Mikrocontrollerprogrammierung und Fehlersuche](../Module_Modulangebot_FGI/1220524.md) |  | 2024-02-16 |
| Ansgar Schleicher Bernhard Rumpe | [Angewandte Softwaretechnik im Lebenszyklus der Automobilelektronik](../Module_Modulangebot_FGI/1215757.md) |  | 2024-02-16 |
| Ansgar Schleicher; Bernhard Rumpe | [Der digitale Lebenszyklus von Fahrzeugen als Teil des Internet of Things (IoT)](../Module_Modulangebot_FGI/1215755.md) |  | 2024-02-16 |
| Bassam Alrifaee | [Regelung und Wahrnehmung in Vernetzten und Autonomen Fahrzeugen](../Module_Modulangebot_FGI/1221329.md) |  | 2024-02-16 |
| Bastian Leibe | [Advanced Machine Learning](../Module_Modulangebot_FGI/1211912.md) |  | 2024-02-16 |
| Bastian Leibe | [Computer Vision 2](../Module_Modulangebot_FGI/1211921.md) |  | 2024-02-16 |
| Bastian Leibe | [Computer Vision](../Module_Modulangebot_FGI/1215724.md) |  | 2024-02-16 |
| Bastian Leibe | [Machine Learning](../Module_Modulangebot_FGI/1215744.md) |  | 2024-02-16 |
| Bernhard Rumpe | [Digitalisierung](../Module_Modulangebot_FGI/1220230.md) |  | 2024-02-16 |
| Bernhard Rumpe | [Innovationen im Software Engineering](../Module_Modulangebot_FGI/1212345.md) |  | 2024-02-16 |
| Bernhard Rumpe | [Modellbasierte Softwareentwicklung](../Module_Modulangebot_FGI/1215686.md) |  | 2024-02-16 |
| Bernhard Rumpe | [Prozesse und Methoden beim Testen von Software](../Module_Modulangebot_FGI/1215732.md) |  | 2024-02-16 |
| Bernhard Rumpe | [Software Language Engineering](../Module_Modulangebot_FGI/1216957.md) |  | 2024-02-16 |
| Bernhard Rumpe | [Softwaretechnik](../Module_Modulangebot_FGI/1211965.md) |  | 2024-02-16 |
| Berthold Vöcking | [Randomized Algorithms](../Module_Modulangebot_FGI/1212705.md) |  | 2024-02-16 |
| Christian Terboven & Matthias S. Müller | [Konzepte und Modelle der parallelen und datenzentrischen Programmierung](../Module_Modulangebot_FGI/1216838.md) |  | 2024-02-16 |
| Christina Büsing | [Kombinatorische Optimierung in der wissenschaftlichen Praxis](../Module_Modulangebot_FGI/1228925.md) |  | 2024-02-16 |
| Christof Löding | [Advanced Automata Theory](../Module_Modulangebot_FGI/1211981.md) |  | 2024-02-16 |
| Christof Löding | [Automaten, Sprachen, Komplexität](../Module_Modulangebot_FGI/1212373.md) |  | 2024-02-16 |
| Christof Löding | [Fixpoints and Induction in Logic and Computer Science](../Module_Modulangebot_FGI/1226911.md) |  | 2024-02-16 |
| Christof Löding | [Infinite Computations and Games](../Module_Modulangebot_FGI/1212336.md) |  | 2024-02-16 |
| Christof Löding | [Informatik-Praktikum für Mathematiker](../Module_Modulangebot_FGI/1212243.md) |  | 2024-02-16 |
| David Bommes | [Diskrete Differentialgeometrie](../Module_Modulangebot_FGI/1212694.md) |  | 2024-02-16 |
| Erika Abraham | [Modellierung und Analyse hybrider Systeme](../Module_Modulangebot_FGI/1212339.md) |  | 2024-02-16 |
| Erika Ábrahám | [Erfüllbarkeitsüberprüfung](../Module_Modulangebot_FGI/1212341.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Algorithmen und Datenstrukturen](../Module_Modulangebot_FGI/1212366.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Bachelorarbeit](../Module_Modulangebot_FGI/1215682.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Einführung in das wissenschaftliche Arbeiten (Proseminar Informatik)](../Module_Modulangebot_FGI/1211968.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Forschungsmodul](../Module_Modulangebot_FGI/1211976.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Informatik](../Module_Modulangebot_FGI/1212313.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Masterarbeit](../Module_Modulangebot_FGI/1212703.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Parallele Programmierung II](../Module_Modulangebot_FGI/1215726.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Praktikum Angewandte Informatik](../Module_Modulangebot_FGI/1211937.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Praktikum Daten und Informationsmanagement](../Module_Modulangebot_FGI/1211935.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Praktikum Software und Kommunikation](../Module_Modulangebot_FGI/1211934.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Praktikum Theoretische Informatik](../Module_Modulangebot_FGI/1215659.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Praktikum](../Module_Modulangebot_FGI/1215759.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Schwerpunktkolloquium Angewandte Informatik](../Module_Modulangebot_FGI/1212704.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Schwerpunktkolloquium Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212702.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Schwerpunktkolloquium Software und Kommunikation](../Module_Modulangebot_FGI/1212701.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Schwerpunktkolloquium Theoretische Informatik](../Module_Modulangebot_FGI/1212700.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Seminar I Angewandte Informatik](../Module_Modulangebot_FGI/1211931.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Seminar I Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212698.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Seminar I Software und Kommunikation](../Module_Modulangebot_FGI/1212696.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Seminar I Theoretische Informatik](../Module_Modulangebot_FGI/1211938.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Seminar II Angewandte Informatik](../Module_Modulangebot_FGI/1211932.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Seminar II Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212699.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Seminar II Software und Kommunikation](../Module_Modulangebot_FGI/1212697.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Seminar II Theoretische Informatik](../Module_Modulangebot_FGI/1211930.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Seminar Informatik](../Module_Modulangebot_FGI/1211974.md) |  | 2024-02-16 |
| Fachgruppe Informatik | [Seminar](../Module_Modulangebot_FGI/1212324.md) |  | 2024-02-16 |
| Fachgruppe Informatik. | [Praktische Informatik](../Module_Modulangebot_FGI/1212316.md) |  | 2024-02-16 |
| Geffner | [Social and Technological Change](../Module_Modulangebot_FGI/1229157.md) |  | 2024-02-16 |
| Gerhard Lakemeyer | [Künstliche Intelligenz](../Module_Modulangebot_FGI/1215694.md) |  | 2024-02-16 |
| Gerhard Lakemeyer | [The Logic of Knowledge Bases](../Module_Modulangebot_FGI/1211393.md) |  | 2024-02-16 |
| Gerhard Lakemeyer | [Wissensrepräsentation](../Module_Modulangebot_FGI/1212361.md) |  | 2024-02-16 |
| Gerhard Woeginger | [Algorithms for Politics](../Module_Modulangebot_FGI/1223159.md) |  | 2024-02-16 |
| Gerhard Woeginger | [Brückenkurs Algorithmen und Datenstrukturen](../Module_Modulangebot_FGI/1225312.md) |  | 2024-02-16 |
| Gerhard Wöginger & Walter Unger | [Online Algorithmen](../Module_Modulangebot_FGI/1212645.md) |  | 2024-02-16 |
| Gerhard Wöginger | [Algorithmische Spieltheorie](../Module_Modulangebot_FGI/1212326.md) |  | 2024-02-16 |
| Hector Geffner | [Actions and Planning in AI: Learning, Models, and Algorithms](../Module_Modulangebot_FGI/1228568.md) |  | 2024-02-16 |
| Hermann Ney | [Advanced Methods in Automatic Speech Recognition](../Module_Modulangebot_FGI/1211904.md) |  | 2024-02-16 |
| Hermann Ney | [Advanced Statistical Classification](../Module_Modulangebot_FGI/1212684.md) |  | 2024-02-16 |
| Hermann Ney | [Advanced Topics in Statistical Natural Language Processing](../Module_Modulangebot_FGI/1212685.md) |  | 2024-02-16 |
| Hermann Ney | [Automatische Spracherkennung](../Module_Modulangebot_FGI/1215750.md) |  | 2024-02-16 |
| Hermann Ney | [Statistische Klassifikation und Maschinelles Lernen](../Module_Modulangebot_FGI/1215840.md) |  | 2024-02-16 |
| Hermann Ney | [Statistische Methoden zur Verarbeitung natürlicher Sprache](../Module_Modulangebot_FGI/1215695.md) |  | 2024-02-16 |
| Honorarprofessor jur. Uwe Meiendresch | [Recht in der Informatik](../Module_Modulangebot_FGI/1227459.md) |  | 2024-02-16 |
| Horst Lichter & Fachgruppe Informatik | [Software-Projektpraktikum](../Module_Modulangebot_FGI/1211973.md) |  | 2024-02-16 |
| Horst Lichter | [Objektorientierte Softwarekonstruktion](../Module_Modulangebot_FGI/1212354.md) |  | 2024-02-16 |
| Horst Lichter | [Software-Projektmanagement](../Module_Modulangebot_FGI/1212355.md) |  | 2024-02-16 |
| Horst Lichter | [Software-Qualitätssicherung](../Module_Modulangebot_FGI/1212356.md) |  | 2024-02-16 |
| Jan Oliver Borchers &; Simon Völker | [iOS Application Development](../Module_Modulangebot_FGI/1215681.md) |  | 2024-02-16 |
| Jan Oliver Borchers Simon Völker | [Personal Digital Fabrication](../Module_Modulangebot_FGI/1216839.md) |  | 2024-02-16 |
| Jan Oliver Borchers | [Current Topics in Media Computing and HCI](../Module_Modulangebot_FGI/1211908.md) |  | 2024-02-16 |
| Jan Oliver Borchers | [Designing Interactive Systems II](../Module_Modulangebot_FGI/1215699.md) |  | 2024-02-16 |
| Jan Oliver Borchers | [Designing Interactive Systems I](../Module_Modulangebot_FGI/1215698.md) |  | 2024-02-16 |
| Jan Oliver Borchers | [Softwarepraktikum](../Module_Modulangebot_FGI/1212636.md) |  | 2024-02-16 |
| Jan Stephen Bender | [Physikalisch-Basierte Animation](../Module_Modulangebot_FGI/1215862.md) |  | 2024-02-16 |
| Joost-Pieter Katoen &; Ulrik Schroeder &; Jürgen Giesl | [Programmierung](../Module_Modulangebot_FGI/1214957.md) |  | 2024-02-16 |
| Joost-Pieter Katoen Martin Grohe Jürgen Giesl Peter Rossmanith | [Formale Systeme, Automaten, Prozesse](../Module_Modulangebot_FGI/1214961.md) |  | 2024-02-16 |
| Joost-Pieter Katoen Martin Grohe Peter Rossmanith Jürgen Giesl | [Berechenbarkeit und Komplexität](../Module_Modulangebot_FGI/1212004.md) |  | 2024-02-16 |
| Joost-Pieter Katoen | [Formale Grundlagen von UML](../Module_Modulangebot_FGI/1212648.md) |  | 2024-02-16 |
| Joost-Pieter Katoen | [Modeling and Verification of Probabilistic Systems](../Module_Modulangebot_FGI/1212711.md) |  | 2024-02-16 |
| Joost-Pieter Katoen | [Probabilistic Programming](../Module_Modulangebot_FGI/1212650.md) |  | 2024-02-16 |
| Joost-Pieter Katoen | [Testen reaktiver Systeme](../Module_Modulangebot_FGI/1212649.md) |  | 2024-02-16 |
| Jürgen Giesl | [Deduktive Programmverifikation](../Module_Modulangebot_FGI/1212659.md) |  | 2024-02-16 |
| Jürgen Giesl | [Foundations of Functional Programming](../Module_Modulangebot_FGI/1215684.md) |  | 2024-02-16 |
| Jürgen Giesl | [Foundations of Logic Programming](../Module_Modulangebot_FGI/1212343.md) |  | 2024-02-16 |
| Jürgen Giesl | [Termersetzungssysteme](../Module_Modulangebot_FGI/1212340.md) |  | 2024-02-16 |
| Klaus Wehrle & Dirk Thißen | [Communication Systems Engineering](../Module_Modulangebot_FGI/1212349.md) |  | 2024-02-16 |
| Klaus Wehrle & Dirk Thißen | [Mobile Internet Technology](../Module_Modulangebot_FGI/1212346.md) |  | 2024-02-16 |
| Klaus Wehrle Hermann Ney | [Betriebssysteme und Systemsoftware](../Module_Modulangebot_FGI/1214960.md) |  | 2024-02-16 |
| Klaus Wehrle | [Advanced Internet Technology](../Module_Modulangebot_FGI/1215688.md) |  | 2024-02-16 |
| Klaus Wehrle | [Datenkommunikation](../Module_Modulangebot_FGI/1211972.md) |  | 2024-02-16 |
| Klaus Wehrle | [Selected Topics in Communication and Distributed Systems](../Module_Modulangebot_FGI/1212665.md) |  | 2024-02-16 |
| Lakemeyer | [Uncertainty in Robotics](../Module_Modulangebot_FGI/1222468.md) |  | 2024-02-16 |
| Leemans | [Business Process Modeling & Computation](../Module_Modulangebot_FGI/1229150.md) |  | 2024-02-16 |
| Leif Kobbelt &; Joost-Pieter Katoen &; Hermann Ney &; Peter Rossmanith | [Datenstrukturen und Algorithmen](../Module_Modulangebot_FGI/1211971.md) |  | 2024-02-16 |
| Leif Kobbelt | [Data Analysis and Visualization](../Module_Modulangebot_FGI/1212372.md) |  | 2024-02-16 |
| Leif Kobbelt | [Geometrieverarbeitung](../Module_Modulangebot_FGI/1215696.md) |  | 2024-02-16 |
| Leif Kobbelt | [Grafikprogrammierung in OpenGL](../Module_Modulangebot_FGI/1212686.md) |  | 2024-02-16 |
| Leif Kobbelt | [Grundlagen der Computergraphik](../Module_Modulangebot_FGI/1212310.md) |  | 2024-02-16 |
| Leif Kobbelt | [Real-time Graphics](../Module_Modulangebot_FGI/1215680.md) |  | 2024-02-16 |
| Leif Kobbelt | [Subdivision Kurven und Flächen](../Module_Modulangebot_FGI/1215697.md) |  | 2024-02-16 |
| MAO Informatik | [Introduction to Artificial Intelligence](../Module_Modulangebot_FGI/1220228.md) |  | 2024-02-16 |
| Manfred Nagl | [Software-Architekturen](../Module_Modulangebot_FGI/1215687.md) |  | 2024-02-16 |
| Manfred Nagl | [Softwaretechnik-Programmiersprache Ada 95](../Module_Modulangebot_FGI/1211982.md) |  | 2024-02-16 |
| Martin Bücker | [Parallele Algorithmen](../Module_Modulangebot_FGI/1212689.md) |  | 2024-02-16 |
| Martin Grohe | [Algorithmische Lerntheorie](../Module_Modulangebot_FGI/1217537.md) |  | 2024-02-16 |
| Martin Grohe | [Graphzerlegungen und algorithmische Anwendungen](../Module_Modulangebot_FGI/1212710.md) |  | 2024-02-16 |
| Martin Grohe | [Komplexitätstheorie](../Module_Modulangebot_FGI/1212331.md) |  | 2024-02-16 |
| Martin Grohe | [Rekursionstheorie](../Module_Modulangebot_FGI/1212335.md) |  | 2024-02-16 |
| Martin Grohe | [The Graph Isomorphism Problem](../Module_Modulangebot_FGI/1212654.md) |  | 2024-02-16 |
| Martin Grohe | [Theory of Constraint Satisfaction Problems](../Module_Modulangebot_FGI/1212334.md) |  | 2024-02-16 |
| Martin Henze | [Industrial Network Security](../Module_Modulangebot_FGI/1227956.md) |  | 2024-02-16 |
| Matthias Jarke | [Advanced Data Models](../Module_Modulangebot_FGI/1212673.md) |  | 2024-02-16 |
| Matthias Jarke | [Implementation of Databases](../Module_Modulangebot_FGI/1215692.md) |  | 2024-02-16 |
| Matthias Jarke | [Information Management](../Module_Modulangebot_FGI/1211923.md) |  | 2024-02-16 |
| Matthias Jarke | [Web Science](../Module_Modulangebot_FGI/1212359.md) |  | 2024-02-16 |
| Matthias Müller | [High-Performance Computing](../Module_Modulangebot_FGI/1215720.md) |  | 2024-02-16 |
| Matthias Müller | [Leistungs- und Korrektheitsanalyse paralleler Programme](../Module_Modulangebot_FGI/1215722.md) |  | 2024-02-16 |
| Michael Elberfeld | [Algorithmen zur String-Verarbeitung und Techniken zur Datenkompression](../Module_Modulangebot_FGI/1212657.md) |  | 2024-02-16 |
| Nadine Bergner | [Didaktische Zugänge zur informatischen Bildung](../Module_Modulangebot_FGI/1228824.md) |  | 2024-02-16 |
| Nadine Bergner | [Didaktische Zugänge zur informatischen Bildung](../Module_Modulangebot_FGI/1228884.md) |  | 2024-02-16 |
| Nadine Bergner | [Einführung in die Fachdidaktik Informatik](../Module_Modulangebot_FGI/1212317.md) |  | 2024-02-16 |
| Nadine Bergner | [Fachdidaktik Informatik](../Module_Modulangebot_FGI/1212322.md) |  | 2024-02-16 |
| Nadine Bergner | [Praktikum Inklusion und Heterogenität](../Module_Modulangebot_FGI/1215685.md) |  | 2024-02-16 |
| Paolo Bientinesi Prüfungsamt ZPA | [Seminar: Topics in Automation, Compilers and Code-Generation](../Module_Modulangebot_FGI/1215729.md) |  | 2024-02-16 |
| Paolo Bientinesi | [Automatic Generation and Analysis of Algorithms](../Module_Modulangebot_FGI/1212693.md) |  | 2024-02-16 |
| Paolo Bientinesi | [Functions of Matrices with Applications](../Module_Modulangebot_FGI/1215727.md) |  | 2024-02-16 |
| Paolo Bientinesi | [High-performance Matrix Computations](../Module_Modulangebot_FGI/1211911.md) |  | 2024-02-16 |
| Paolo Bientinesi | [Seminar: Topics in High-Performance and Scientific Computing](../Module_Modulangebot_FGI/1215728.md) |  | 2024-02-16 |
| Pascal Schweitzer | [Computational Group Theory](../Module_Modulangebot_FGI/1212656.md) |  | 2024-02-16 |
| Peter Rossmanith Walter Unger | [Effiziente Algorithmen](../Module_Modulangebot_FGI/1211977.md) |  | 2024-02-16 |
| Peter Rossmanith | [Analyse von Algorithmen](../Module_Modulangebot_FGI/1212337.md) |  | 2024-02-16 |
| Peter Rossmanith | [Exakte Algorithmen](../Module_Modulangebot_FGI/1212658.md) |  | 2024-02-16 |
| Peter Rossmanith | [Parametrisierte Algorithmen](../Module_Modulangebot_FGI/1212338.md) |  | 2024-02-16 |
| Priv.-Doz. Christof Löding | [Stochastic Games](../Module_Modulangebot_FGI/1219623.md) |  | 2024-02-16 |
| Priv.-Doz. Ralf Schlüter | [Automatic Speech Recognition Search](../Module_Modulangebot_FGI/1230105.md) |  | 2024-02-16 |
| Priv.-Doz. Ralf Schlüter | [Fundamentals of Automatic Speech Recognition](../Module_Modulangebot_FGI/1230106.md) |  | 2024-02-16 |
| Ralf Klamma | [Social Computing](../Module_Modulangebot_FGI/1212678.md) |  | 2024-02-16 |
| Rose &; Prinz | [Distributed Ledger Technology](../Module_Modulangebot_FGI/1226006.md) |  | 2024-02-16 |
| Sander Leemans | [Fundamentals of Business Process Management](../Module_Modulangebot_FGI/1227457.md) |  | 2024-02-16 |
| Sandra Geisler | [Datenstrommanagement und -analyse](../Module_Modulangebot_FGI/1226146.md) |  | 2024-02-16 |
| Sprachenzentrum | [Deutschkurs](../Module_Modulangebot_FGI/1215734.md) |  | 2024-02-16 |
| Stefan Decker & Univ.- med. Rainer Röhrig & Oya Deniz Beyan | [Data Driven Medicine - project-oriented, multidisciplinary introduction](../Module_Modulangebot_FGI/1215842.md) |  | 2024-02-16 |
| Stefan Decker &; Matthias Jarke | [Datenbanken und Informationssysteme](../Module_Modulangebot_FGI/1211969.md) |  | 2024-02-16 |
| Stefan Decker | [Big Data in Medical Informatics](../Module_Modulangebot_FGI/1212676.md) |  | 2024-02-16 |
| Stefan Decker | [Privacy Enhancing Technologies for Data Science](../Module_Modulangebot_FGI/1212677.md) |  | 2024-02-16 |
| Stefan Decker | [Semantic Web](../Module_Modulangebot_FGI/1212675.md) |  | 2024-02-16 |
| Stefan Decker, PD Ralf Klamma | [Brückenkurs Datenbanken](../Module_Modulangebot_FGI/1225948.md) |  | 2024-02-16 |
| Stefan Kowalewski Gerhard Lakemeyer | [Einführung in die Technische Informatik](../Module_Modulangebot_FGI/1214958.md) |  | 2024-02-16 |
| Stefan Kowalewski Hendrik Simon | [Formale Methoden für Steuerungssoftware](../Module_Modulangebot_FGI/1212666.md) |  | 2024-02-16 |
| Stefan Kowalewski | [Eingebettete Systeme](../Module_Modulangebot_FGI/1215690.md) |  | 2024-02-16 |
| Stefan Kowalewski | [Funktionale Sicherheit und Systemzuverlässigkeit](../Module_Modulangebot_FGI/1212353.md) |  | 2024-02-16 |
| Stefan Kowalewski | [Sicherheit und Zuverlässigkeit softwaregesteuerter Systeme (bis WS 14/15)](../Module_Modulangebot_FGI/1212351.md) |  | 2024-02-16 |
| Stefan Kowalewski | [Systemprogrammierung](../Module_Modulangebot_FGI/1211967.md) |  | 2024-02-16 |
| Thomas Berlage | [Introduction to Bioinformatics](../Module_Modulangebot_FGI/1211903.md) |  | 2024-02-16 |
| Thomas Noll Joost-Pieter Katoen | [Compilerbau](../Module_Modulangebot_FGI/1211978.md) |  | 2024-02-16 |
| Thomas Noll Joost-Pieter Katoen | [Concurrency Theory](../Module_Modulangebot_FGI/1212646.md) |  | 2024-02-16 |
| Thomas Noll | [Semantik und Verifikation von Software](../Module_Modulangebot_FGI/1212329.md) |  | 2024-02-16 |
| Thomas Noll | [Statische Programmanalyse](../Module_Modulangebot_FGI/1212330.md) |  | 2024-02-16 |
| Thomas Rose | [Prozess Management](../Module_Modulangebot_FGI/1211902.md) |  | 2024-02-16 |
| Thomas Rose | [eBusiness - Anwendungen, Architekturen und Standards](../Module_Modulangebot_FGI/1212683.md) |  | 2024-02-16 |
| Thomas Seidl | [Indexstrukturen für Datenbanken](../Module_Modulangebot_FGI/1212679.md) |  | 2024-02-16 |
| Thomas Seidl | [Inhaltsbasierte Ähnlichkeitssuche](../Module_Modulangebot_FGI/1211929.md) |  | 2024-02-16 |
| Thomas Seidl | [Modelle der Datenexploration](../Module_Modulangebot_FGI/1216218.md) |  | 2024-02-16 |
| Torsten Wolfgang Kuhlen | [Fortgeschrittene Methoden der Virtuellen Realität](../Module_Modulangebot_FGI/1212688.md) |  | 2024-02-16 |
| Torsten Wolfgang Kuhlen | [Virtuelle Realität](../Module_Modulangebot_FGI/1211909.md) |  | 2024-02-16 |
| Ulrik Schroeder | [Einführung in Web Technologien](../Module_Modulangebot_FGI/1211914.md) |  | 2024-02-16 |
| Ulrik Schroeder | [Einführung in die Programmierung für datenbasierte Wissenschaften](../Module_Modulangebot_FGI/1224007.md) |  | 2024-02-16 |
| Ulrik Schroeder | [Fachdidaktik Informatik](../Module_Modulangebot_FGI/1227857.md) |  | 2024-02-16 |
| Ulrik Schroeder | [Faszination Technik in der Informatik](../Module_Modulangebot_FGI/1212323.md) |  | 2024-02-16 |
| Ulrik Schroeder | [Learning Technologies](../Module_Modulangebot_FGI/1215751.md) |  | 2024-02-16 |
| Ulrike Meyer | [IT-Sicherheit 1 - Kryptographische Grundlagen und Netzwerksicherheit](../Module_Modulangebot_FGI/1211901.md) |  | 2024-02-16 |
| Ulrike Meyer | [IT-Sicherheit 2 - Computer Security](../Module_Modulangebot_FGI/1211900.md) |  | 2024-02-16 |
| Ulrike Meyer | [Sicherheit in der Mobilkommunikation](../Module_Modulangebot_FGI/1212681.md) |  | 2024-02-16 |
| Unbekannt | [Einführung in die Informatik](../Module_Modulangebot_FGI/1212365.md) |  | 2024-02-16 |
| Unbekannt | [Informatik](../Module_Modulangebot_FGI/1212318.md) |  | 2024-02-16 |
| Univ.- Redha Gouicem | [Linux Kernel Programming](../Module_Modulangebot_FGI/1229308.md) |  | 2024-02-16 |
| Uwe Naumann | [Advanced Algorithmic Differentiation](../Module_Modulangebot_FGI/1221328.md) |  | 2024-02-16 |
| Uwe Naumann | [Advanced C++](../Module_Modulangebot_FGI/1228566.md) |  | 2024-02-16 |
| Uwe Naumann | [Combinatorial Problems in Scientific Computing](../Module_Modulangebot_FGI/1215721.md) |  | 2024-02-16 |
| Uwe Naumann | [Einführung in die Programmierung](../Module_Modulangebot_FGI/1215679.md) |  | 2024-02-16 |
| Uwe Naumann | [Introduction to Algorithmic Differentiation](../Module_Modulangebot_FGI/1221327.md) |  | 2024-02-16 |
| Uwe Naumann | [Introduction to Numerical Methods and Software with C++](../Module_Modulangebot_FGI/1229154.md) |  | 2024-02-16 |
| Uwe Naumann | [Introduction to Numerical Methods and Software](../Module_Modulangebot_FGI/1220996.md) |  | 2024-02-16 |
| Uwe Naumann | [Mentoring Informatik](../Module_Modulangebot_FGI/1214959.md) |  | 2024-02-16 |
| Uwe Naumann | [SiSc Laboratory](../Module_Modulangebot_FGI/1212320.md) |  | 2024-02-16 |
| Uwe Naumann | [Vorbereitungskurs zum Softwareentwicklungspraktikum und Softwareentwicklungspraktikum](../Module_Modulangebot_FGI/1212371.md) |  | 2024-02-16 |
| Walter Unger | [Algorithmische Kryptographie](../Module_Modulangebot_FGI/1212358.md) |  | 2024-02-16 |
| Walter Unger | [Graphalgorithmen](../Module_Modulangebot_FGI/1212327.md) |  | 2024-02-16 |
| Walter Unger | [Theory of Distributed and Parallel Systems](../Module_Modulangebot_FGI/1212643.md) |  | 2024-02-16 |
| Wil van der Aalst | [Advanced Process Mining](../Module_Modulangebot_FGI/1220136.md) |  | 2024-02-16 |
| Wil van der Aalst | [Business Process Intelligence](../Module_Modulangebot_FGI/1216958.md) |  | 2024-02-16 |
| Wil van der Aalst | [Introduction to Data Science](../Module_Modulangebot_FGI/1216861.md) |  | 2024-02-16 |
| Wolfgang Prinz | [CSCW and Groupware: Concepts and Systems for Computer Supported Cooperative Work](../Module_Modulangebot_FGI/1215691.md) |  | 2024-02-16 |
| Wolfgang Thomas Joost-Pieter Katoen | [Model Checking](../Module_Modulangebot_FGI/1212328.md) |  | 2024-02-16 |
| Wolfgang Thomas | [Infinite Games](../Module_Modulangebot_FGI/1212333.md) |  | 2024-02-16 |
| keine Angabe | [Algorithmic Foundations of Datascience](../Module_Modulangebot_FGI/1216860.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung Zusätzliche Prüfungsleistung](../Module_Modulangebot_FGI/1222464.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung als Zusätzliche Prüfungsleistung](../Module_Modulangebot_FGI/1222558.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung aus dem Ausland](../Module_Modulangebot_FGI/1229747.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Anwendungsfach](../Module_Modulangebot_FGI/1219123.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Bereich Kommunikationsfertigkeiten](../Module_Modulangebot_FGI/1219554.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Bereich Medien-Informatik Praktika](../Module_Modulangebot_FGI/1219552.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Bereich Multimedia-Benutzung und -Wirkung](../Module_Modulangebot_FGI/1219550.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Bereich Multimedia-Technologie](../Module_Modulangebot_FGI/1219548.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Bereich Rechner- und Kommunikationstechnologie](../Module_Modulangebot_FGI/1219545.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Kernbereich Software Engineering](../Module_Modulangebot_FGI/1230089.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Nicht-technischen Wahlfach](../Module_Modulangebot_FGI/1218687.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Vertiefungsbereich Applied Computer Science](../Module_Modulangebot_FGI/1219506.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Vertiefungsbereich Communication](../Module_Modulangebot_FGI/1219508.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Vertiefungsbereich Data and Information Management](../Module_Modulangebot_FGI/1219510.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Vertiefungsbereich Software Engineering](../Module_Modulangebot_FGI/1219512.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Vertiefungsbereich Theoretical Foundations of Software Systems Engingeering](../Module_Modulangebot_FGI/1219504.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1218713.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1219584.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1218714.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1219586.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1218712.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1219588.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1218711.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1219590.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Wahlpflichtbereich](../Module_Modulangebot_FGI/1218603.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung im Wahlpflichtbereich](../Module_Modulangebot_FGI/1219220.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung à 4 CP](../Module_Modulangebot_FGI/1219223.md) |  | 2024-02-16 |
| keine Angabe | [Anerkennung à 6 CP](../Module_Modulangebot_FGI/1219224.md) |  | 2024-02-16 |
| keine Angabe | [Automatisierung einer Destillationsanlage](../Module_Modulangebot_FGI/1220403.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Anwendungsfach](../Module_Modulangebot_FGI/1219183.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Anwendungsfach](../Module_Modulangebot_FGI/1220849.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Nicht-technischen Wahlfach](../Module_Modulangebot_FGI/1218686.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich Angewandte Informatik Master Informatik](../Module_Modulangebot_FGI/1219018.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1218718.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich Daten- und Informationsmanagement Master Informatik](../Module_Modulangebot_FGI/1219021.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1218717.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich Grafik und Interaktion Master Informatik](../Module_Modulangebot_FGI/1229936.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich Hardware/Software-Systeme Master Informatik](../Module_Modulangebot_FGI/1229937.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich KI & Daten Master Informatik](../Module_Modulangebot_FGI/1229938.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich Software und Kommunikation Master Informatik](../Module_Modulangebot_FGI/1219020.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich Software und Kommuniktion](../Module_Modulangebot_FGI/1218716.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich Software- Entwicklungsmethoden und -Werkzeuge Master Informatik](../Module_Modulangebot_FGI/1229893.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich Theoretische Informatik Master Informatik](../Module_Modulangebot_FGI/1219023.md) |  | 2024-02-16 |
| keine Angabe | [Außercurriculares Modul im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1218715.md) |  | 2024-02-16 |
| keine Angabe | [Dynamical Processes on Networks](../Module_Modulangebot_FGI/1223640.md) |  | 2024-02-16 |
| keine Angabe | [Elements of Machine Learning and Data Science](../Module_Modulangebot_FGI/1226970.md) |  | 2024-02-16 |
| keine Angabe | [Ersatzveranstaltung - Individuelles Modul](../Module_Modulangebot_FGI/1222030.md) |  | 2024-02-16 |
| keine Angabe | [Grundzüge der Informatik](../Module_Modulangebot_FGI/1212315.md) |  | 2024-02-16 |
| keine Angabe | [Höhere Algorithmik](../Module_Modulangebot_FGI/1223638.md) |  | 2024-02-16 |
| keine Angabe | [IT-Sicherheit](../Module_Modulangebot_FGI/1226971.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul Zusätzliche Prüfungen](../Module_Modulangebot_FGI/1229767.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Bereich Kommunikationsfertigkeiten](../Module_Modulangebot_FGI/1219553.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Bereich Medien-Informatik Praktika](../Module_Modulangebot_FGI/1219551.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Bereich Multimedia-Benutzung und -Wirkung](../Module_Modulangebot_FGI/1219549.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Bereich Multimedia-Technologie](../Module_Modulangebot_FGI/1219547.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Bereich Rechner- und Kommunikationstechnologie](../Module_Modulangebot_FGI/1219543.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Mastervorzug](../Module_Modulangebot_FGI/1222479.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Vertiefungsbereich Applied Computer Science](../Module_Modulangebot_FGI/1219505.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Vertiefungsbereich Communication](../Module_Modulangebot_FGI/1219507.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Vertiefungsbereich Data and Information Management](../Module_Modulangebot_FGI/1219509.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Vertiefungsbereich Software Engineering](../Module_Modulangebot_FGI/1219511.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Vertiefungsbereich Theoretical Foundations of Software Systems Engineering](../Module_Modulangebot_FGI/1219503.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Vertiefungsbereich](../Module_Modulangebot_FGI/1221648.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1219583.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1219585.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1219587.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1219589.md) |  | 2024-02-16 |
| keine Angabe | [Individuelles Modul im Wahlpflichtbereich](../Module_Modulangebot_FGI/1219222.md) |  | 2024-02-16 |
| keine Angabe | [Informatik (WP VT)](../Module_Modulangebot_FGI/1212321.md) |  | 2024-02-16 |
| keine Angabe | [Informatik](../Module_Modulangebot_FGI/1212319.md) |  | 2024-02-16 |
| keine Angabe | [Machine Learning with Graphs: Foundations and Applications](../Module_Modulangebot_FGI/1227996.md) |  | 2024-02-16 |
| keine Angabe | [Model-based Systems Engineering](../Module_Modulangebot_FGI/1222882.md) |  | 2024-02-16 |
| keine Angabe | [Nicht-technisches Wahlfach Mentoring](../Module_Modulangebot_FGI/1220643.md) |  | 2024-02-16 |
| keine Angabe | [Physikalische Simulation im Visual Computing](../Module_Modulangebot_FGI/1212692.md) |  | 2024-02-16 |
| keine Angabe | [Research Focus Class on Communication Systems](../Module_Modulangebot_FGI/1212347.md) |  | 2024-02-16 |
| keine Angabe | [Research Focus Class on Learning Technologies](../Module_Modulangebot_FGI/1222419.md) |  | 2024-02-16 |
| keine Angabe | [Schwerpunktkolloquium](../Module_Modulangebot_FGI/1222540.md) |  | 2024-02-16 |
| keine Angabe | [Shape Analysis and 3D Deep Learning](../Module_Modulangebot_FGI/1223639.md) |  | 2024-02-16 |
| keine Angabe | [Sprachkurs](../Module_Modulangebot_FGI/1225243.md) |  | 2024-02-16 |
| keine Angabe | [Sprachkurs](../Module_Modulangebot_FGI/1225244.md) |  | 2024-02-16 |
| keine Angabe | [Sprachmodul 1](../Module_Modulangebot_FGI/1229768.md) |  | 2024-02-16 |
| keine Angabe | [Sprachmodul 2](../Module_Modulangebot_FGI/1230044.md) |  | 2024-02-16 |
| keine Angabe | [Sprachmodul 3](../Module_Modulangebot_FGI/1230045.md) |  | 2024-02-16 |
| keine Angabe | [Theory of Distributed Systems](../Module_Modulangebot_FGI/1212641.md) |  | 2024-02-16 |
| keine Angabe | [Zusatzkompetenz 1](../Module_Modulangebot_FGI/1219455.md) |  | 2024-02-16 |
| keine Angabe | [Zusatzkompetenz](../Module_Modulangebot_FGI/1226286.md) |  | 2024-02-16 |

