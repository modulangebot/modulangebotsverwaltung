# Liste verwalteter Module (nach Modulnummer)

sortiert nach der Modulnummer sortiert
und fuer jeden Studiengang extra aufgefuehrt

generiert am:  Fri Feb 16 14:34:50 2024

| Modulnummer | Studiengang | Titel | Anbieter | Aenderung am | 
| -- | -- | -- | -- | -- | 
| 1211393 |  | [The Logic of Knowledge Bases](../Module_Modulangebot_FGI/1211393.md) | Gerhard Lakemeyer | 2024-02-16 |
| 1211900 |  | [IT-Sicherheit 2 - Computer Security](../Module_Modulangebot_FGI/1211900.md) | Ulrike Meyer | 2024-02-16 |
| 1211901 |  | [IT-Sicherheit 1 - Kryptographische Grundlagen und Netzwerksicherheit](../Module_Modulangebot_FGI/1211901.md) | Ulrike Meyer | 2024-02-16 |
| 1211902 |  | [Prozess Management](../Module_Modulangebot_FGI/1211902.md) | Thomas Rose | 2024-02-16 |
| 1211903 |  | [Introduction to Bioinformatics](../Module_Modulangebot_FGI/1211903.md) | Thomas Berlage | 2024-02-16 |
| 1211904 |  | [Advanced Methods in Automatic Speech Recognition](../Module_Modulangebot_FGI/1211904.md) | Hermann Ney | 2024-02-16 |
| 1211908 |  | [Current Topics in Media Computing and HCI](../Module_Modulangebot_FGI/1211908.md) | Jan Oliver Borchers | 2024-02-16 |
| 1211909 |  | [Virtuelle Realität](../Module_Modulangebot_FGI/1211909.md) | Torsten Wolfgang Kuhlen | 2024-02-16 |
| 1211911 |  | [High-performance Matrix Computations](../Module_Modulangebot_FGI/1211911.md) | Paolo Bientinesi | 2024-02-16 |
| 1211912 |  | [Advanced Machine Learning](../Module_Modulangebot_FGI/1211912.md) | Bastian Leibe | 2024-02-16 |
| 1211914 |  | [Einführung in Web Technologien](../Module_Modulangebot_FGI/1211914.md) | Ulrik Schroeder | 2024-02-16 |
| 1211921 |  | [Computer Vision 2](../Module_Modulangebot_FGI/1211921.md) | Bastian Leibe | 2024-02-16 |
| 1211923 |  | [Information Management](../Module_Modulangebot_FGI/1211923.md) | Matthias Jarke | 2024-02-16 |
| 1211929 |  | [Inhaltsbasierte Ähnlichkeitssuche](../Module_Modulangebot_FGI/1211929.md) | Thomas Seidl | 2024-02-16 |
| 1211930 |  | [Seminar II Theoretische Informatik](../Module_Modulangebot_FGI/1211930.md) | Fachgruppe Informatik | 2024-02-16 |
| 1211931 |  | [Seminar I Angewandte Informatik](../Module_Modulangebot_FGI/1211931.md) | Fachgruppe Informatik | 2024-02-16 |
| 1211932 |  | [Seminar II Angewandte Informatik](../Module_Modulangebot_FGI/1211932.md) | Fachgruppe Informatik | 2024-02-16 |
| 1211934 |  | [Praktikum Software und Kommunikation](../Module_Modulangebot_FGI/1211934.md) | Fachgruppe Informatik | 2024-02-16 |
| 1211935 |  | [Praktikum Daten und Informationsmanagement](../Module_Modulangebot_FGI/1211935.md) | Fachgruppe Informatik | 2024-02-16 |
| 1211937 |  | [Praktikum Angewandte Informatik](../Module_Modulangebot_FGI/1211937.md) | Fachgruppe Informatik | 2024-02-16 |
| 1211938 |  | [Seminar I Theoretische Informatik](../Module_Modulangebot_FGI/1211938.md) | Fachgruppe Informatik | 2024-02-16 |
| 1211965 |  | [Softwaretechnik](../Module_Modulangebot_FGI/1211965.md) | Bernhard Rumpe | 2024-02-16 |
| 1211967 |  | [Systemprogrammierung](../Module_Modulangebot_FGI/1211967.md) | Stefan Kowalewski | 2024-02-16 |
| 1211968 |  | [Einführung in das wissenschaftliche Arbeiten (Proseminar Informatik)](../Module_Modulangebot_FGI/1211968.md) | Fachgruppe Informatik | 2024-02-16 |
| 1211969 |  | [Datenbanken und Informationssysteme](../Module_Modulangebot_FGI/1211969.md) | Stefan Decker &; Matthias Jarke | 2024-02-16 |
| 1211971 |  | [Datenstrukturen und Algorithmen](../Module_Modulangebot_FGI/1211971.md) | Leif Kobbelt &; Joost-Pieter Katoen &; Hermann Ney &; Peter Rossmanith | 2024-02-16 |
| 1211972 |  | [Datenkommunikation](../Module_Modulangebot_FGI/1211972.md) | Klaus Wehrle | 2024-02-16 |
| 1211973 |  | [Software-Projektpraktikum](../Module_Modulangebot_FGI/1211973.md) | Horst Lichter & Fachgruppe Informatik | 2024-02-16 |
| 1211974 |  | [Seminar Informatik](../Module_Modulangebot_FGI/1211974.md) | Fachgruppe Informatik | 2024-02-16 |
| 1211976 |  | [Forschungsmodul](../Module_Modulangebot_FGI/1211976.md) | Fachgruppe Informatik | 2024-02-16 |
| 1211977 |  | [Effiziente Algorithmen](../Module_Modulangebot_FGI/1211977.md) | Peter Rossmanith Walter Unger | 2024-02-16 |
| 1211978 |  | [Compilerbau](../Module_Modulangebot_FGI/1211978.md) | Thomas Noll Joost-Pieter Katoen | 2024-02-16 |
| 1211981 |  | [Advanced Automata Theory](../Module_Modulangebot_FGI/1211981.md) | Christof Löding | 2024-02-16 |
| 1211982 |  | [Softwaretechnik-Programmiersprache Ada 95](../Module_Modulangebot_FGI/1211982.md) | Manfred Nagl | 2024-02-16 |
| 1212004 |  | [Berechenbarkeit und Komplexität](../Module_Modulangebot_FGI/1212004.md) | Joost-Pieter Katoen Martin Grohe Peter Rossmanith Jürgen Giesl | 2024-02-16 |
| 1212243 |  | [Informatik-Praktikum für Mathematiker](../Module_Modulangebot_FGI/1212243.md) | Christof Löding | 2024-02-16 |
| 1212310 |  | [Grundlagen der Computergraphik](../Module_Modulangebot_FGI/1212310.md) | Leif Kobbelt | 2024-02-16 |
| 1212313 |  | [Informatik](../Module_Modulangebot_FGI/1212313.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212315 |  | [Grundzüge der Informatik](../Module_Modulangebot_FGI/1212315.md) | keine Angabe | 2024-02-16 |
| 1212316 |  | [Praktische Informatik](../Module_Modulangebot_FGI/1212316.md) | Fachgruppe Informatik. | 2024-02-16 |
| 1212317 |  | [Einführung in die Fachdidaktik Informatik](../Module_Modulangebot_FGI/1212317.md) | Nadine Bergner | 2024-02-16 |
| 1212318 |  | [Informatik](../Module_Modulangebot_FGI/1212318.md) | Unbekannt | 2024-02-16 |
| 1212319 |  | [Informatik](../Module_Modulangebot_FGI/1212319.md) | keine Angabe | 2024-02-16 |
| 1212320 |  | [SiSc Laboratory](../Module_Modulangebot_FGI/1212320.md) | Uwe Naumann | 2024-02-16 |
| 1212321 |  | [Informatik (WP VT)](../Module_Modulangebot_FGI/1212321.md) | keine Angabe | 2024-02-16 |
| 1212322 |  | [Fachdidaktik Informatik](../Module_Modulangebot_FGI/1212322.md) | Nadine Bergner | 2024-02-16 |
| 1212323 |  | [Faszination Technik in der Informatik](../Module_Modulangebot_FGI/1212323.md) | Ulrik Schroeder | 2024-02-16 |
| 1212324 |  | [Seminar](../Module_Modulangebot_FGI/1212324.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212326 |  | [Algorithmische Spieltheorie](../Module_Modulangebot_FGI/1212326.md) | Gerhard Wöginger | 2024-02-16 |
| 1212327 |  | [Graphalgorithmen](../Module_Modulangebot_FGI/1212327.md) | Walter Unger | 2024-02-16 |
| 1212328 |  | [Model Checking](../Module_Modulangebot_FGI/1212328.md) | Wolfgang Thomas Joost-Pieter Katoen | 2024-02-16 |
| 1212329 |  | [Semantik und Verifikation von Software](../Module_Modulangebot_FGI/1212329.md) | Thomas Noll | 2024-02-16 |
| 1212330 |  | [Statische Programmanalyse](../Module_Modulangebot_FGI/1212330.md) | Thomas Noll | 2024-02-16 |
| 1212331 |  | [Komplexitätstheorie](../Module_Modulangebot_FGI/1212331.md) | Martin Grohe | 2024-02-16 |
| 1212333 |  | [Infinite Games](../Module_Modulangebot_FGI/1212333.md) | Wolfgang Thomas | 2024-02-16 |
| 1212334 |  | [Theory of Constraint Satisfaction Problems](../Module_Modulangebot_FGI/1212334.md) | Martin Grohe | 2024-02-16 |
| 1212335 |  | [Rekursionstheorie](../Module_Modulangebot_FGI/1212335.md) | Martin Grohe | 2024-02-16 |
| 1212336 |  | [Infinite Computations and Games](../Module_Modulangebot_FGI/1212336.md) | Christof Löding | 2024-02-16 |
| 1212337 |  | [Analyse von Algorithmen](../Module_Modulangebot_FGI/1212337.md) | Peter Rossmanith | 2024-02-16 |
| 1212338 |  | [Parametrisierte Algorithmen](../Module_Modulangebot_FGI/1212338.md) | Peter Rossmanith | 2024-02-16 |
| 1212339 |  | [Modellierung und Analyse hybrider Systeme](../Module_Modulangebot_FGI/1212339.md) | Erika Abraham | 2024-02-16 |
| 1212340 |  | [Termersetzungssysteme](../Module_Modulangebot_FGI/1212340.md) | Jürgen Giesl | 2024-02-16 |
| 1212341 |  | [Erfüllbarkeitsüberprüfung](../Module_Modulangebot_FGI/1212341.md) | Erika Ábrahám | 2024-02-16 |
| 1212343 |  | [Foundations of Logic Programming](../Module_Modulangebot_FGI/1212343.md) | Jürgen Giesl | 2024-02-16 |
| 1212345 |  | [Innovationen im Software Engineering](../Module_Modulangebot_FGI/1212345.md) | Bernhard Rumpe | 2024-02-16 |
| 1212346 |  | [Mobile Internet Technology](../Module_Modulangebot_FGI/1212346.md) | Klaus Wehrle & Dirk Thißen | 2024-02-16 |
| 1212347 |  | [Research Focus Class on Communication Systems](../Module_Modulangebot_FGI/1212347.md) | keine Angabe | 2024-02-16 |
| 1212349 |  | [Communication Systems Engineering](../Module_Modulangebot_FGI/1212349.md) | Klaus Wehrle & Dirk Thißen | 2024-02-16 |
| 1212351 |  | [Sicherheit und Zuverlässigkeit softwaregesteuerter Systeme (bis WS 14/15)](../Module_Modulangebot_FGI/1212351.md) | Stefan Kowalewski | 2024-02-16 |
| 1212353 |  | [Funktionale Sicherheit und Systemzuverlässigkeit](../Module_Modulangebot_FGI/1212353.md) | Stefan Kowalewski | 2024-02-16 |
| 1212354 |  | [Objektorientierte Softwarekonstruktion](../Module_Modulangebot_FGI/1212354.md) | Horst Lichter | 2024-02-16 |
| 1212355 |  | [Software-Projektmanagement](../Module_Modulangebot_FGI/1212355.md) | Horst Lichter | 2024-02-16 |
| 1212356 |  | [Software-Qualitätssicherung](../Module_Modulangebot_FGI/1212356.md) | Horst Lichter | 2024-02-16 |
| 1212358 |  | [Algorithmische Kryptographie](../Module_Modulangebot_FGI/1212358.md) | Walter Unger | 2024-02-16 |
| 1212359 |  | [Web Science](../Module_Modulangebot_FGI/1212359.md) | Matthias Jarke | 2024-02-16 |
| 1212361 |  | [Wissensrepräsentation](../Module_Modulangebot_FGI/1212361.md) | Gerhard Lakemeyer | 2024-02-16 |
| 1212365 |  | [Einführung in die Informatik](../Module_Modulangebot_FGI/1212365.md) | Unbekannt | 2024-02-16 |
| 1212366 |  | [Algorithmen und Datenstrukturen](../Module_Modulangebot_FGI/1212366.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212371 |  | [Vorbereitungskurs zum Softwareentwicklungspraktikum und Softwareentwicklungspraktikum](../Module_Modulangebot_FGI/1212371.md) | Uwe Naumann | 2024-02-16 |
| 1212372 |  | [Data Analysis and Visualization](../Module_Modulangebot_FGI/1212372.md) | Leif Kobbelt | 2024-02-16 |
| 1212373 |  | [Automaten, Sprachen, Komplexität](../Module_Modulangebot_FGI/1212373.md) | Christof Löding | 2024-02-16 |
| 1212636 |  | [Softwarepraktikum](../Module_Modulangebot_FGI/1212636.md) | Jan Oliver Borchers | 2024-02-16 |
| 1212641 |  | [Theory of Distributed Systems](../Module_Modulangebot_FGI/1212641.md) | keine Angabe | 2024-02-16 |
| 1212643 |  | [Theory of Distributed and Parallel Systems](../Module_Modulangebot_FGI/1212643.md) | Walter Unger | 2024-02-16 |
| 1212645 |  | [Online Algorithmen](../Module_Modulangebot_FGI/1212645.md) | Gerhard Wöginger & Walter Unger | 2024-02-16 |
| 1212646 |  | [Concurrency Theory](../Module_Modulangebot_FGI/1212646.md) | Thomas Noll Joost-Pieter Katoen | 2024-02-16 |
| 1212648 |  | [Formale Grundlagen von UML](../Module_Modulangebot_FGI/1212648.md) | Joost-Pieter Katoen | 2024-02-16 |
| 1212649 |  | [Testen reaktiver Systeme](../Module_Modulangebot_FGI/1212649.md) | Joost-Pieter Katoen | 2024-02-16 |
| 1212650 |  | [Probabilistic Programming](../Module_Modulangebot_FGI/1212650.md) | Joost-Pieter Katoen | 2024-02-16 |
| 1212654 |  | [The Graph Isomorphism Problem](../Module_Modulangebot_FGI/1212654.md) | Martin Grohe | 2024-02-16 |
| 1212656 |  | [Computational Group Theory](../Module_Modulangebot_FGI/1212656.md) | Pascal Schweitzer | 2024-02-16 |
| 1212657 |  | [Algorithmen zur String-Verarbeitung und Techniken zur Datenkompression](../Module_Modulangebot_FGI/1212657.md) | Michael Elberfeld | 2024-02-16 |
| 1212658 |  | [Exakte Algorithmen](../Module_Modulangebot_FGI/1212658.md) | Peter Rossmanith | 2024-02-16 |
| 1212659 |  | [Deduktive Programmverifikation](../Module_Modulangebot_FGI/1212659.md) | Jürgen Giesl | 2024-02-16 |
| 1212665 |  | [Selected Topics in Communication and Distributed Systems](../Module_Modulangebot_FGI/1212665.md) | Klaus Wehrle | 2024-02-16 |
| 1212666 |  | [Formale Methoden für Steuerungssoftware](../Module_Modulangebot_FGI/1212666.md) | Stefan Kowalewski Hendrik Simon | 2024-02-16 |
| 1212673 |  | [Advanced Data Models](../Module_Modulangebot_FGI/1212673.md) | Matthias Jarke | 2024-02-16 |
| 1212675 |  | [Semantic Web](../Module_Modulangebot_FGI/1212675.md) | Stefan Decker | 2024-02-16 |
| 1212676 |  | [Big Data in Medical Informatics](../Module_Modulangebot_FGI/1212676.md) | Stefan Decker | 2024-02-16 |
| 1212677 |  | [Privacy Enhancing Technologies for Data Science](../Module_Modulangebot_FGI/1212677.md) | Stefan Decker | 2024-02-16 |
| 1212678 |  | [Social Computing](../Module_Modulangebot_FGI/1212678.md) | Ralf Klamma | 2024-02-16 |
| 1212679 |  | [Indexstrukturen für Datenbanken](../Module_Modulangebot_FGI/1212679.md) | Thomas Seidl | 2024-02-16 |
| 1212681 |  | [Sicherheit in der Mobilkommunikation](../Module_Modulangebot_FGI/1212681.md) | Ulrike Meyer | 2024-02-16 |
| 1212683 |  | [eBusiness - Anwendungen, Architekturen und Standards](../Module_Modulangebot_FGI/1212683.md) | Thomas Rose | 2024-02-16 |
| 1212684 |  | [Advanced Statistical Classification](../Module_Modulangebot_FGI/1212684.md) | Hermann Ney | 2024-02-16 |
| 1212685 |  | [Advanced Topics in Statistical Natural Language Processing](../Module_Modulangebot_FGI/1212685.md) | Hermann Ney | 2024-02-16 |
| 1212686 |  | [Grafikprogrammierung in OpenGL](../Module_Modulangebot_FGI/1212686.md) | Leif Kobbelt | 2024-02-16 |
| 1212688 |  | [Fortgeschrittene Methoden der Virtuellen Realität](../Module_Modulangebot_FGI/1212688.md) | Torsten Wolfgang Kuhlen | 2024-02-16 |
| 1212689 |  | [Parallele Algorithmen](../Module_Modulangebot_FGI/1212689.md) | Martin Bücker | 2024-02-16 |
| 1212692 |  | [Physikalische Simulation im Visual Computing](../Module_Modulangebot_FGI/1212692.md) | keine Angabe | 2024-02-16 |
| 1212693 |  | [Automatic Generation and Analysis of Algorithms](../Module_Modulangebot_FGI/1212693.md) | Paolo Bientinesi | 2024-02-16 |
| 1212694 |  | [Diskrete Differentialgeometrie](../Module_Modulangebot_FGI/1212694.md) | David Bommes | 2024-02-16 |
| 1212696 |  | [Seminar I Software und Kommunikation](../Module_Modulangebot_FGI/1212696.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212697 |  | [Seminar II Software und Kommunikation](../Module_Modulangebot_FGI/1212697.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212698 |  | [Seminar I Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212698.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212699 |  | [Seminar II Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212699.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212700 |  | [Schwerpunktkolloquium Theoretische Informatik](../Module_Modulangebot_FGI/1212700.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212701 |  | [Schwerpunktkolloquium Software und Kommunikation](../Module_Modulangebot_FGI/1212701.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212702 |  | [Schwerpunktkolloquium Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212702.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212703 |  | [Masterarbeit](../Module_Modulangebot_FGI/1212703.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212704 |  | [Schwerpunktkolloquium Angewandte Informatik](../Module_Modulangebot_FGI/1212704.md) | Fachgruppe Informatik | 2024-02-16 |
| 1212705 |  | [Randomized Algorithms](../Module_Modulangebot_FGI/1212705.md) | Berthold Vöcking | 2024-02-16 |
| 1212710 |  | [Graphzerlegungen und algorithmische Anwendungen](../Module_Modulangebot_FGI/1212710.md) | Martin Grohe | 2024-02-16 |
| 1212711 |  | [Modeling and Verification of Probabilistic Systems](../Module_Modulangebot_FGI/1212711.md) | Joost-Pieter Katoen | 2024-02-16 |
| 1214957 |  | [Programmierung](../Module_Modulangebot_FGI/1214957.md) | Joost-Pieter Katoen &; Ulrik Schroeder &; Jürgen Giesl | 2024-02-16 |
| 1214958 |  | [Einführung in die Technische Informatik](../Module_Modulangebot_FGI/1214958.md) | Stefan Kowalewski Gerhard Lakemeyer | 2024-02-16 |
| 1214959 |  | [Mentoring Informatik](../Module_Modulangebot_FGI/1214959.md) | Uwe Naumann | 2024-02-16 |
| 1214960 |  | [Betriebssysteme und Systemsoftware](../Module_Modulangebot_FGI/1214960.md) | Klaus Wehrle Hermann Ney | 2024-02-16 |
| 1214961 |  | [Formale Systeme, Automaten, Prozesse](../Module_Modulangebot_FGI/1214961.md) | Joost-Pieter Katoen Martin Grohe Jürgen Giesl Peter Rossmanith | 2024-02-16 |
| 1215659 |  | [Praktikum Theoretische Informatik](../Module_Modulangebot_FGI/1215659.md) | Fachgruppe Informatik | 2024-02-16 |
| 1215679 |  | [Einführung in die Programmierung](../Module_Modulangebot_FGI/1215679.md) | Uwe Naumann | 2024-02-16 |
| 1215680 |  | [Real-time Graphics](../Module_Modulangebot_FGI/1215680.md) | Leif Kobbelt | 2024-02-16 |
| 1215681 |  | [iOS Application Development](../Module_Modulangebot_FGI/1215681.md) | Jan Oliver Borchers &; Simon Völker | 2024-02-16 |
| 1215682 |  | [Bachelorarbeit](../Module_Modulangebot_FGI/1215682.md) | Fachgruppe Informatik | 2024-02-16 |
| 1215684 |  | [Foundations of Functional Programming](../Module_Modulangebot_FGI/1215684.md) | Jürgen Giesl | 2024-02-16 |
| 1215685 |  | [Praktikum Inklusion und Heterogenität](../Module_Modulangebot_FGI/1215685.md) | Nadine Bergner | 2024-02-16 |
| 1215686 |  | [Modellbasierte Softwareentwicklung](../Module_Modulangebot_FGI/1215686.md) | Bernhard Rumpe | 2024-02-16 |
| 1215687 |  | [Software-Architekturen](../Module_Modulangebot_FGI/1215687.md) | Manfred Nagl | 2024-02-16 |
| 1215688 |  | [Advanced Internet Technology](../Module_Modulangebot_FGI/1215688.md) | Klaus Wehrle | 2024-02-16 |
| 1215690 |  | [Eingebettete Systeme](../Module_Modulangebot_FGI/1215690.md) | Stefan Kowalewski | 2024-02-16 |
| 1215691 |  | [CSCW and Groupware: Concepts and Systems for Computer Supported Cooperative Work](../Module_Modulangebot_FGI/1215691.md) | Wolfgang Prinz | 2024-02-16 |
| 1215692 |  | [Implementation of Databases](../Module_Modulangebot_FGI/1215692.md) | Matthias Jarke | 2024-02-16 |
| 1215694 |  | [Künstliche Intelligenz](../Module_Modulangebot_FGI/1215694.md) | Gerhard Lakemeyer | 2024-02-16 |
| 1215695 |  | [Statistische Methoden zur Verarbeitung natürlicher Sprache](../Module_Modulangebot_FGI/1215695.md) | Hermann Ney | 2024-02-16 |
| 1215696 |  | [Geometrieverarbeitung](../Module_Modulangebot_FGI/1215696.md) | Leif Kobbelt | 2024-02-16 |
| 1215697 |  | [Subdivision Kurven und Flächen](../Module_Modulangebot_FGI/1215697.md) | Leif Kobbelt | 2024-02-16 |
| 1215698 |  | [Designing Interactive Systems I](../Module_Modulangebot_FGI/1215698.md) | Jan Oliver Borchers | 2024-02-16 |
| 1215699 |  | [Designing Interactive Systems II](../Module_Modulangebot_FGI/1215699.md) | Jan Oliver Borchers | 2024-02-16 |
| 1215720 |  | [High-Performance Computing](../Module_Modulangebot_FGI/1215720.md) | Matthias Müller | 2024-02-16 |
| 1215721 |  | [Combinatorial Problems in Scientific Computing](../Module_Modulangebot_FGI/1215721.md) | Uwe Naumann | 2024-02-16 |
| 1215722 |  | [Leistungs- und Korrektheitsanalyse paralleler Programme](../Module_Modulangebot_FGI/1215722.md) | Matthias Müller | 2024-02-16 |
| 1215724 |  | [Computer Vision](../Module_Modulangebot_FGI/1215724.md) | Bastian Leibe | 2024-02-16 |
| 1215726 |  | [Parallele Programmierung II](../Module_Modulangebot_FGI/1215726.md) | Fachgruppe Informatik | 2024-02-16 |
| 1215727 |  | [Functions of Matrices with Applications](../Module_Modulangebot_FGI/1215727.md) | Paolo Bientinesi | 2024-02-16 |
| 1215728 |  | [Seminar: Topics in High-Performance and Scientific Computing](../Module_Modulangebot_FGI/1215728.md) | Paolo Bientinesi | 2024-02-16 |
| 1215729 |  | [Seminar: Topics in Automation, Compilers and Code-Generation](../Module_Modulangebot_FGI/1215729.md) | Paolo Bientinesi Prüfungsamt ZPA | 2024-02-16 |
| 1215732 |  | [Prozesse und Methoden beim Testen von Software](../Module_Modulangebot_FGI/1215732.md) | Bernhard Rumpe | 2024-02-16 |
| 1215734 |  | [Deutschkurs](../Module_Modulangebot_FGI/1215734.md) | Sprachenzentrum | 2024-02-16 |
| 1215744 |  | [Machine Learning](../Module_Modulangebot_FGI/1215744.md) | Bastian Leibe | 2024-02-16 |
| 1215750 |  | [Automatische Spracherkennung](../Module_Modulangebot_FGI/1215750.md) | Hermann Ney | 2024-02-16 |
| 1215751 |  | [Learning Technologies](../Module_Modulangebot_FGI/1215751.md) | Ulrik Schroeder | 2024-02-16 |
| 1215755 |  | [Der digitale Lebenszyklus von Fahrzeugen als Teil des Internet of Things (IoT)](../Module_Modulangebot_FGI/1215755.md) | Ansgar Schleicher; Bernhard Rumpe | 2024-02-16 |
| 1215757 |  | [Angewandte Softwaretechnik im Lebenszyklus der Automobilelektronik](../Module_Modulangebot_FGI/1215757.md) | Ansgar Schleicher Bernhard Rumpe | 2024-02-16 |
| 1215759 |  | [Praktikum](../Module_Modulangebot_FGI/1215759.md) | Fachgruppe Informatik | 2024-02-16 |
| 1215840 |  | [Statistische Klassifikation und Maschinelles Lernen](../Module_Modulangebot_FGI/1215840.md) | Hermann Ney | 2024-02-16 |
| 1215842 |  | [Data Driven Medicine - project-oriented, multidisciplinary introduction](../Module_Modulangebot_FGI/1215842.md) | Stefan Decker & Univ.- med. Rainer Röhrig & Oya Deniz Beyan | 2024-02-16 |
| 1215862 |  | [Physikalisch-Basierte Animation](../Module_Modulangebot_FGI/1215862.md) | Jan Stephen Bender | 2024-02-16 |
| 1216218 |  | [Modelle der Datenexploration](../Module_Modulangebot_FGI/1216218.md) | Thomas Seidl | 2024-02-16 |
| 1216838 |  | [Konzepte und Modelle der parallelen und datenzentrischen Programmierung](../Module_Modulangebot_FGI/1216838.md) | Christian Terboven & Matthias S. Müller | 2024-02-16 |
| 1216839 |  | [Personal Digital Fabrication](../Module_Modulangebot_FGI/1216839.md) | Jan Oliver Borchers Simon Völker | 2024-02-16 |
| 1216860 |  | [Algorithmic Foundations of Datascience](../Module_Modulangebot_FGI/1216860.md) | keine Angabe | 2024-02-16 |
| 1216861 |  | [Introduction to Data Science](../Module_Modulangebot_FGI/1216861.md) | Wil van der Aalst | 2024-02-16 |
| 1216957 |  | [Software Language Engineering](../Module_Modulangebot_FGI/1216957.md) | Bernhard Rumpe | 2024-02-16 |
| 1216958 |  | [Business Process Intelligence](../Module_Modulangebot_FGI/1216958.md) | Wil van der Aalst | 2024-02-16 |
| 1217537 |  | [Algorithmische Lerntheorie](../Module_Modulangebot_FGI/1217537.md) | Martin Grohe | 2024-02-16 |
| 1218603 |  | [Anerkennung im Wahlpflichtbereich](../Module_Modulangebot_FGI/1218603.md) | keine Angabe | 2024-02-16 |
| 1218686 |  | [Außercurriculares Modul im Nicht-technischen Wahlfach](../Module_Modulangebot_FGI/1218686.md) | keine Angabe | 2024-02-16 |
| 1218687 |  | [Anerkennung im Nicht-technischen Wahlfach](../Module_Modulangebot_FGI/1218687.md) | keine Angabe | 2024-02-16 |
| 1218711 |  | [Anerkennung im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1218711.md) | keine Angabe | 2024-02-16 |
| 1218712 |  | [Anerkennung im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1218712.md) | keine Angabe | 2024-02-16 |
| 1218713 |  | [Anerkennung im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1218713.md) | keine Angabe | 2024-02-16 |
| 1218714 |  | [Anerkennung im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1218714.md) | keine Angabe | 2024-02-16 |
| 1218715 |  | [Außercurriculares Modul im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1218715.md) | keine Angabe | 2024-02-16 |
| 1218716 |  | [Außercurriculares Modul im Wahlpflichtbereich Software und Kommuniktion](../Module_Modulangebot_FGI/1218716.md) | keine Angabe | 2024-02-16 |
| 1218717 |  | [Außercurriculares Modul im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1218717.md) | keine Angabe | 2024-02-16 |
| 1218718 |  | [Außercurriculares Modul im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1218718.md) | keine Angabe | 2024-02-16 |
| 1219018 |  | [Außercurriculares Modul im Wahlpflichtbereich Angewandte Informatik Master Informatik](../Module_Modulangebot_FGI/1219018.md) | keine Angabe | 2024-02-16 |
| 1219020 |  | [Außercurriculares Modul im Wahlpflichtbereich Software und Kommunikation Master Informatik](../Module_Modulangebot_FGI/1219020.md) | keine Angabe | 2024-02-16 |
| 1219021 |  | [Außercurriculares Modul im Wahlpflichtbereich Daten- und Informationsmanagement Master Informatik](../Module_Modulangebot_FGI/1219021.md) | keine Angabe | 2024-02-16 |
| 1219023 |  | [Außercurriculares Modul im Wahlpflichtbereich Theoretische Informatik Master Informatik](../Module_Modulangebot_FGI/1219023.md) | keine Angabe | 2024-02-16 |
| 1219123 |  | [Anerkennung im Anwendungsfach](../Module_Modulangebot_FGI/1219123.md) | keine Angabe | 2024-02-16 |
| 1219183 |  | [Außercurriculares Modul im Anwendungsfach](../Module_Modulangebot_FGI/1219183.md) | keine Angabe | 2024-02-16 |
| 1219220 |  | [Anerkennung im Wahlpflichtbereich](../Module_Modulangebot_FGI/1219220.md) | keine Angabe | 2024-02-16 |
| 1219222 |  | [Individuelles Modul im Wahlpflichtbereich](../Module_Modulangebot_FGI/1219222.md) | keine Angabe | 2024-02-16 |
| 1219223 |  | [Anerkennung à 4 CP](../Module_Modulangebot_FGI/1219223.md) | keine Angabe | 2024-02-16 |
| 1219224 |  | [Anerkennung à 6 CP](../Module_Modulangebot_FGI/1219224.md) | keine Angabe | 2024-02-16 |
| 1219455 |  | [Zusatzkompetenz 1](../Module_Modulangebot_FGI/1219455.md) | keine Angabe | 2024-02-16 |
| 1219503 |  | [Individuelles Modul im Vertiefungsbereich Theoretical Foundations of Software Systems Engineering](../Module_Modulangebot_FGI/1219503.md) | keine Angabe | 2024-02-16 |
| 1219504 |  | [Anerkennung im Vertiefungsbereich Theoretical Foundations of Software Systems Engingeering](../Module_Modulangebot_FGI/1219504.md) | keine Angabe | 2024-02-16 |
| 1219505 |  | [Individuelles Modul im Vertiefungsbereich Applied Computer Science](../Module_Modulangebot_FGI/1219505.md) | keine Angabe | 2024-02-16 |
| 1219506 |  | [Anerkennung im Vertiefungsbereich Applied Computer Science](../Module_Modulangebot_FGI/1219506.md) | keine Angabe | 2024-02-16 |
| 1219507 |  | [Individuelles Modul im Vertiefungsbereich Communication](../Module_Modulangebot_FGI/1219507.md) | keine Angabe | 2024-02-16 |
| 1219508 |  | [Anerkennung im Vertiefungsbereich Communication](../Module_Modulangebot_FGI/1219508.md) | keine Angabe | 2024-02-16 |
| 1219509 |  | [Individuelles Modul im Vertiefungsbereich Data and Information Management](../Module_Modulangebot_FGI/1219509.md) | keine Angabe | 2024-02-16 |
| 1219510 |  | [Anerkennung im Vertiefungsbereich Data and Information Management](../Module_Modulangebot_FGI/1219510.md) | keine Angabe | 2024-02-16 |
| 1219511 |  | [Individuelles Modul im Vertiefungsbereich Software Engineering](../Module_Modulangebot_FGI/1219511.md) | keine Angabe | 2024-02-16 |
| 1219512 |  | [Anerkennung im Vertiefungsbereich Software Engineering](../Module_Modulangebot_FGI/1219512.md) | keine Angabe | 2024-02-16 |
| 1219543 |  | [Individuelles Modul im Bereich Rechner- und Kommunikationstechnologie](../Module_Modulangebot_FGI/1219543.md) | keine Angabe | 2024-02-16 |
| 1219545 |  | [Anerkennung im Bereich Rechner- und Kommunikationstechnologie](../Module_Modulangebot_FGI/1219545.md) | keine Angabe | 2024-02-16 |
| 1219547 |  | [Individuelles Modul im Bereich Multimedia-Technologie](../Module_Modulangebot_FGI/1219547.md) | keine Angabe | 2024-02-16 |
| 1219548 |  | [Anerkennung im Bereich Multimedia-Technologie](../Module_Modulangebot_FGI/1219548.md) | keine Angabe | 2024-02-16 |
| 1219549 |  | [Individuelles Modul im Bereich Multimedia-Benutzung und -Wirkung](../Module_Modulangebot_FGI/1219549.md) | keine Angabe | 2024-02-16 |
| 1219550 |  | [Anerkennung im Bereich Multimedia-Benutzung und -Wirkung](../Module_Modulangebot_FGI/1219550.md) | keine Angabe | 2024-02-16 |
| 1219551 |  | [Individuelles Modul im Bereich Medien-Informatik Praktika](../Module_Modulangebot_FGI/1219551.md) | keine Angabe | 2024-02-16 |
| 1219552 |  | [Anerkennung im Bereich Medien-Informatik Praktika](../Module_Modulangebot_FGI/1219552.md) | keine Angabe | 2024-02-16 |
| 1219553 |  | [Individuelles Modul im Bereich Kommunikationsfertigkeiten](../Module_Modulangebot_FGI/1219553.md) | keine Angabe | 2024-02-16 |
| 1219554 |  | [Anerkennung im Bereich Kommunikationsfertigkeiten](../Module_Modulangebot_FGI/1219554.md) | keine Angabe | 2024-02-16 |
| 1219583 |  | [Individuelles Modul im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1219583.md) | keine Angabe | 2024-02-16 |
| 1219584 |  | [Anerkennung im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1219584.md) | keine Angabe | 2024-02-16 |
| 1219585 |  | [Individuelles Modul im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1219585.md) | keine Angabe | 2024-02-16 |
| 1219586 |  | [Anerkennung im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1219586.md) | keine Angabe | 2024-02-16 |
| 1219587 |  | [Individuelles Modul im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1219587.md) | keine Angabe | 2024-02-16 |
| 1219588 |  | [Anerkennung im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1219588.md) | keine Angabe | 2024-02-16 |
| 1219589 |  | [Individuelles Modul im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1219589.md) | keine Angabe | 2024-02-16 |
| 1219590 |  | [Anerkennung im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1219590.md) | keine Angabe | 2024-02-16 |
| 1219623 |  | [Stochastic Games](../Module_Modulangebot_FGI/1219623.md) | Priv.-Doz. Christof Löding | 2024-02-16 |
| 1220136 |  | [Advanced Process Mining](../Module_Modulangebot_FGI/1220136.md) | Wil van der Aalst | 2024-02-16 |
| 1220228 |  | [Introduction to Artificial Intelligence](../Module_Modulangebot_FGI/1220228.md) | MAO Informatik | 2024-02-16 |
| 1220230 |  | [Digitalisierung](../Module_Modulangebot_FGI/1220230.md) | Bernhard Rumpe | 2024-02-16 |
| 1220403 |  | [Automatisierung einer Destillationsanlage](../Module_Modulangebot_FGI/1220403.md) | keine Angabe | 2024-02-16 |
| 1220524 |  | [Mikrocontrollerprogrammierung und Fehlersuche](../Module_Modulangebot_FGI/1220524.md) | André Stollenwerk | 2024-02-16 |
| 1220643 |  | [Nicht-technisches Wahlfach Mentoring](../Module_Modulangebot_FGI/1220643.md) | keine Angabe | 2024-02-16 |
| 1220849 |  | [Außercurriculares Modul im Anwendungsfach](../Module_Modulangebot_FGI/1220849.md) | keine Angabe | 2024-02-16 |
| 1220996 |  | [Introduction to Numerical Methods and Software](../Module_Modulangebot_FGI/1220996.md) | Uwe Naumann | 2024-02-16 |
| 1221327 |  | [Introduction to Algorithmic Differentiation](../Module_Modulangebot_FGI/1221327.md) | Uwe Naumann | 2024-02-16 |
| 1221328 |  | [Advanced Algorithmic Differentiation](../Module_Modulangebot_FGI/1221328.md) | Uwe Naumann | 2024-02-16 |
| 1221329 |  | [Regelung und Wahrnehmung in Vernetzten und Autonomen Fahrzeugen](../Module_Modulangebot_FGI/1221329.md) | Bassam Alrifaee | 2024-02-16 |
| 1221648 |  | [Individuelles Modul im Vertiefungsbereich](../Module_Modulangebot_FGI/1221648.md) | keine Angabe | 2024-02-16 |
| 1222030 |  | [Ersatzveranstaltung - Individuelles Modul](../Module_Modulangebot_FGI/1222030.md) | keine Angabe | 2024-02-16 |
| 1222419 |  | [Research Focus Class on Learning Technologies](../Module_Modulangebot_FGI/1222419.md) | keine Angabe | 2024-02-16 |
| 1222464 |  | [Anerkennung Zusätzliche Prüfungsleistung](../Module_Modulangebot_FGI/1222464.md) | keine Angabe | 2024-02-16 |
| 1222468 |  | [Uncertainty in Robotics](../Module_Modulangebot_FGI/1222468.md) | Lakemeyer | 2024-02-16 |
| 1222479 |  | [Individuelles Modul im Mastervorzug](../Module_Modulangebot_FGI/1222479.md) | keine Angabe | 2024-02-16 |
| 1222540 |  | [Schwerpunktkolloquium](../Module_Modulangebot_FGI/1222540.md) | keine Angabe | 2024-02-16 |
| 1222558 |  | [Anerkennung als Zusätzliche Prüfungsleistung](../Module_Modulangebot_FGI/1222558.md) | keine Angabe | 2024-02-16 |
| 1222882 |  | [Model-based Systems Engineering](../Module_Modulangebot_FGI/1222882.md) | keine Angabe | 2024-02-16 |
| 1223159 |  | [Algorithms for Politics](../Module_Modulangebot_FGI/1223159.md) | Gerhard Woeginger | 2024-02-16 |
| 1223638 |  | [Höhere Algorithmik](../Module_Modulangebot_FGI/1223638.md) | keine Angabe | 2024-02-16 |
| 1223639 |  | [Shape Analysis and 3D Deep Learning](../Module_Modulangebot_FGI/1223639.md) | keine Angabe | 2024-02-16 |
| 1223640 |  | [Dynamical Processes on Networks](../Module_Modulangebot_FGI/1223640.md) | keine Angabe | 2024-02-16 |
| 1224007 |  | [Einführung in die Programmierung für datenbasierte Wissenschaften](../Module_Modulangebot_FGI/1224007.md) | Ulrik Schroeder | 2024-02-16 |
| 1225243 |  | [Sprachkurs](../Module_Modulangebot_FGI/1225243.md) | keine Angabe | 2024-02-16 |
| 1225244 |  | [Sprachkurs](../Module_Modulangebot_FGI/1225244.md) | keine Angabe | 2024-02-16 |
| 1225312 |  | [Brückenkurs Algorithmen und Datenstrukturen](../Module_Modulangebot_FGI/1225312.md) | Gerhard Woeginger | 2024-02-16 |
| 1225948 |  | [Brückenkurs Datenbanken](../Module_Modulangebot_FGI/1225948.md) | Stefan Decker, PD Ralf Klamma | 2024-02-16 |
| 1226006 |  | [Distributed Ledger Technology](../Module_Modulangebot_FGI/1226006.md) | Rose &; Prinz | 2024-02-16 |
| 1226146 |  | [Datenstrommanagement und -analyse](../Module_Modulangebot_FGI/1226146.md) | Sandra Geisler | 2024-02-16 |
| 1226286 |  | [Zusatzkompetenz](../Module_Modulangebot_FGI/1226286.md) | keine Angabe | 2024-02-16 |
| 1226911 |  | [Fixpoints and Induction in Logic and Computer Science](../Module_Modulangebot_FGI/1226911.md) | Christof Löding | 2024-02-16 |
| 1226970 |  | [Elements of Machine Learning and Data Science](../Module_Modulangebot_FGI/1226970.md) | keine Angabe | 2024-02-16 |
| 1226971 |  | [IT-Sicherheit](../Module_Modulangebot_FGI/1226971.md) | keine Angabe | 2024-02-16 |
| 1227457 |  | [Fundamentals of Business Process Management](../Module_Modulangebot_FGI/1227457.md) | Sander Leemans | 2024-02-16 |
| 1227459 |  | [Recht in der Informatik](../Module_Modulangebot_FGI/1227459.md) | Honorarprofessor jur. Uwe Meiendresch | 2024-02-16 |
| 1227857 |  | [Fachdidaktik Informatik](../Module_Modulangebot_FGI/1227857.md) | Ulrik Schroeder | 2024-02-16 |
| 1227956 |  | [Industrial Network Security](../Module_Modulangebot_FGI/1227956.md) | Martin Henze | 2024-02-16 |
| 1227996 |  | [Machine Learning with Graphs: Foundations and Applications](../Module_Modulangebot_FGI/1227996.md) | keine Angabe | 2024-02-16 |
| 1228566 |  | [Advanced C++](../Module_Modulangebot_FGI/1228566.md) | Uwe Naumann | 2024-02-16 |
| 1228568 |  | [Actions and Planning in AI: Learning, Models, and Algorithms](../Module_Modulangebot_FGI/1228568.md) | Hector Geffner | 2024-02-16 |
| 1228824 |  | [Didaktische Zugänge zur informatischen Bildung](../Module_Modulangebot_FGI/1228824.md) | Nadine Bergner | 2024-02-16 |
| 1228884 |  | [Didaktische Zugänge zur informatischen Bildung](../Module_Modulangebot_FGI/1228884.md) | Nadine Bergner | 2024-02-16 |
| 1228925 |  | [Kombinatorische Optimierung in der wissenschaftlichen Praxis](../Module_Modulangebot_FGI/1228925.md) | Christina Büsing | 2024-02-16 |
| 1229150 |  | [Business Process Modeling & Computation](../Module_Modulangebot_FGI/1229150.md) | Leemans | 2024-02-16 |
| 1229154 |  | [Introduction to Numerical Methods and Software with C++](../Module_Modulangebot_FGI/1229154.md) | Uwe Naumann | 2024-02-16 |
| 1229157 |  | [Social and Technological Change](../Module_Modulangebot_FGI/1229157.md) | Geffner | 2024-02-16 |
| 1229308 |  | [Linux Kernel Programming](../Module_Modulangebot_FGI/1229308.md) | Univ.- Redha Gouicem | 2024-02-16 |
| 1229347 |  | [Post-quantum cryptography](../Module_Modulangebot_FGI/1229347.md) | ;Dominique Unruh | 2024-02-16 |
| 1229747 |  | [Anerkennung aus dem Ausland](../Module_Modulangebot_FGI/1229747.md) | keine Angabe | 2024-02-16 |
| 1229767 |  | [Individuelles Modul Zusätzliche Prüfungen](../Module_Modulangebot_FGI/1229767.md) | keine Angabe | 2024-02-16 |
| 1229768 |  | [Sprachmodul 1](../Module_Modulangebot_FGI/1229768.md) | keine Angabe | 2024-02-16 |
| 1229893 |  | [Außercurriculares Modul im Wahlpflichtbereich Software- Entwicklungsmethoden und -Werkzeuge Master Informatik](../Module_Modulangebot_FGI/1229893.md) | keine Angabe | 2024-02-16 |
| 1229936 |  | [Außercurriculares Modul im Wahlpflichtbereich Grafik und Interaktion Master Informatik](../Module_Modulangebot_FGI/1229936.md) | keine Angabe | 2024-02-16 |
| 1229937 |  | [Außercurriculares Modul im Wahlpflichtbereich Hardware/Software-Systeme Master Informatik](../Module_Modulangebot_FGI/1229937.md) | keine Angabe | 2024-02-16 |
| 1229938 |  | [Außercurriculares Modul im Wahlpflichtbereich KI & Daten Master Informatik](../Module_Modulangebot_FGI/1229938.md) | keine Angabe | 2024-02-16 |
| 1230044 |  | [Sprachmodul 2](../Module_Modulangebot_FGI/1230044.md) | keine Angabe | 2024-02-16 |
| 1230045 |  | [Sprachmodul 3](../Module_Modulangebot_FGI/1230045.md) | keine Angabe | 2024-02-16 |
| 1230089 |  | [Anerkennung im Kernbereich Software Engineering](../Module_Modulangebot_FGI/1230089.md) | keine Angabe | 2024-02-16 |
| 1230105 |  | [Automatic Speech Recognition Search](../Module_Modulangebot_FGI/1230105.md) | Priv.-Doz. Ralf Schlüter | 2024-02-16 |
| 1230106 |  | [Fundamentals of Automatic Speech Recognition](../Module_Modulangebot_FGI/1230106.md) | Priv.-Doz. Ralf Schlüter | 2024-02-16 |

