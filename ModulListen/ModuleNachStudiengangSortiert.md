# Liste verwalteter Module (nach Studiengang)

sortiert nach dem Studiengang sortiert

generiert am:  Fri Feb 16 14:34:49 2024

| Studiengang | Titel | Anbieter | Aenderung am | 
| -- | -- | -- | -- | 
|  | [Actions and Planning in AI: Learning, Models, and Algorithms](../Module_Modulangebot_FGI/1228568.md) | Hector Geffner | 2024-02-16 |
|  | [Advanced Algorithmic Differentiation](../Module_Modulangebot_FGI/1221328.md) | Uwe Naumann | 2024-02-16 |
|  | [Advanced Automata Theory](../Module_Modulangebot_FGI/1211981.md) | Christof Löding | 2024-02-16 |
|  | [Advanced C++](../Module_Modulangebot_FGI/1228566.md) | Uwe Naumann | 2024-02-16 |
|  | [Advanced Data Models](../Module_Modulangebot_FGI/1212673.md) | Matthias Jarke | 2024-02-16 |
|  | [Advanced Internet Technology](../Module_Modulangebot_FGI/1215688.md) | Klaus Wehrle | 2024-02-16 |
|  | [Advanced Machine Learning](../Module_Modulangebot_FGI/1211912.md) | Bastian Leibe | 2024-02-16 |
|  | [Advanced Methods in Automatic Speech Recognition](../Module_Modulangebot_FGI/1211904.md) | Hermann Ney | 2024-02-16 |
|  | [Advanced Process Mining](../Module_Modulangebot_FGI/1220136.md) | Wil van der Aalst | 2024-02-16 |
|  | [Advanced Statistical Classification](../Module_Modulangebot_FGI/1212684.md) | Hermann Ney | 2024-02-16 |
|  | [Advanced Topics in Statistical Natural Language Processing](../Module_Modulangebot_FGI/1212685.md) | Hermann Ney | 2024-02-16 |
|  | [Algorithmen und Datenstrukturen](../Module_Modulangebot_FGI/1212366.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Algorithmen zur String-Verarbeitung und Techniken zur Datenkompression](../Module_Modulangebot_FGI/1212657.md) | Michael Elberfeld | 2024-02-16 |
|  | [Algorithmic Foundations of Datascience](../Module_Modulangebot_FGI/1216860.md) | keine Angabe | 2024-02-16 |
|  | [Algorithmische Kryptographie](../Module_Modulangebot_FGI/1212358.md) | Walter Unger | 2024-02-16 |
|  | [Algorithmische Lerntheorie](../Module_Modulangebot_FGI/1217537.md) | Martin Grohe | 2024-02-16 |
|  | [Algorithmische Spieltheorie](../Module_Modulangebot_FGI/1212326.md) | Gerhard Wöginger | 2024-02-16 |
|  | [Algorithms for Politics](../Module_Modulangebot_FGI/1223159.md) | Gerhard Woeginger | 2024-02-16 |
|  | [Analyse von Algorithmen](../Module_Modulangebot_FGI/1212337.md) | Peter Rossmanith | 2024-02-16 |
|  | [Anerkennung Zusätzliche Prüfungsleistung](../Module_Modulangebot_FGI/1222464.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung als Zusätzliche Prüfungsleistung](../Module_Modulangebot_FGI/1222558.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung aus dem Ausland](../Module_Modulangebot_FGI/1229747.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Anwendungsfach](../Module_Modulangebot_FGI/1219123.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Bereich Kommunikationsfertigkeiten](../Module_Modulangebot_FGI/1219554.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Bereich Medien-Informatik Praktika](../Module_Modulangebot_FGI/1219552.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Bereich Multimedia-Benutzung und -Wirkung](../Module_Modulangebot_FGI/1219550.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Bereich Multimedia-Technologie](../Module_Modulangebot_FGI/1219548.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Bereich Rechner- und Kommunikationstechnologie](../Module_Modulangebot_FGI/1219545.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Kernbereich Software Engineering](../Module_Modulangebot_FGI/1230089.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Nicht-technischen Wahlfach](../Module_Modulangebot_FGI/1218687.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Vertiefungsbereich Applied Computer Science](../Module_Modulangebot_FGI/1219506.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Vertiefungsbereich Communication](../Module_Modulangebot_FGI/1219508.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Vertiefungsbereich Data and Information Management](../Module_Modulangebot_FGI/1219510.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Vertiefungsbereich Software Engineering](../Module_Modulangebot_FGI/1219512.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Vertiefungsbereich Theoretical Foundations of Software Systems Engingeering](../Module_Modulangebot_FGI/1219504.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1218713.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1219584.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1218714.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1219586.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1218712.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1219588.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1218711.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1219590.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Wahlpflichtbereich](../Module_Modulangebot_FGI/1218603.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung im Wahlpflichtbereich](../Module_Modulangebot_FGI/1219220.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung à 4 CP](../Module_Modulangebot_FGI/1219223.md) | keine Angabe | 2024-02-16 |
|  | [Anerkennung à 6 CP](../Module_Modulangebot_FGI/1219224.md) | keine Angabe | 2024-02-16 |
|  | [Angewandte Softwaretechnik im Lebenszyklus der Automobilelektronik](../Module_Modulangebot_FGI/1215757.md) | Ansgar Schleicher Bernhard Rumpe | 2024-02-16 |
|  | [Automaten, Sprachen, Komplexität](../Module_Modulangebot_FGI/1212373.md) | Christof Löding | 2024-02-16 |
|  | [Automatic Generation and Analysis of Algorithms](../Module_Modulangebot_FGI/1212693.md) | Paolo Bientinesi | 2024-02-16 |
|  | [Automatic Speech Recognition Search](../Module_Modulangebot_FGI/1230105.md) | Priv.-Doz. Ralf Schlüter | 2024-02-16 |
|  | [Automatische Spracherkennung](../Module_Modulangebot_FGI/1215750.md) | Hermann Ney | 2024-02-16 |
|  | [Automatisierung einer Destillationsanlage](../Module_Modulangebot_FGI/1220403.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Anwendungsfach](../Module_Modulangebot_FGI/1219183.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Anwendungsfach](../Module_Modulangebot_FGI/1220849.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Nicht-technischen Wahlfach](../Module_Modulangebot_FGI/1218686.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich Angewandte Informatik Master Informatik](../Module_Modulangebot_FGI/1219018.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1218718.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich Daten- und Informationsmanagement Master Informatik](../Module_Modulangebot_FGI/1219021.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1218717.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich Grafik und Interaktion Master Informatik](../Module_Modulangebot_FGI/1229936.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich Hardware/Software-Systeme Master Informatik](../Module_Modulangebot_FGI/1229937.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich KI & Daten Master Informatik](../Module_Modulangebot_FGI/1229938.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich Software und Kommunikation Master Informatik](../Module_Modulangebot_FGI/1219020.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich Software und Kommuniktion](../Module_Modulangebot_FGI/1218716.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich Software- Entwicklungsmethoden und -Werkzeuge Master Informatik](../Module_Modulangebot_FGI/1229893.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich Theoretische Informatik Master Informatik](../Module_Modulangebot_FGI/1219023.md) | keine Angabe | 2024-02-16 |
|  | [Außercurriculares Modul im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1218715.md) | keine Angabe | 2024-02-16 |
|  | [Bachelorarbeit](../Module_Modulangebot_FGI/1215682.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Berechenbarkeit und Komplexität](../Module_Modulangebot_FGI/1212004.md) | Joost-Pieter Katoen Martin Grohe Peter Rossmanith Jürgen Giesl | 2024-02-16 |
|  | [Betriebssysteme und Systemsoftware](../Module_Modulangebot_FGI/1214960.md) | Klaus Wehrle Hermann Ney | 2024-02-16 |
|  | [Big Data in Medical Informatics](../Module_Modulangebot_FGI/1212676.md) | Stefan Decker | 2024-02-16 |
|  | [Brückenkurs Algorithmen und Datenstrukturen](../Module_Modulangebot_FGI/1225312.md) | Gerhard Woeginger | 2024-02-16 |
|  | [Brückenkurs Datenbanken](../Module_Modulangebot_FGI/1225948.md) | Stefan Decker, PD Ralf Klamma | 2024-02-16 |
|  | [Business Process Intelligence](../Module_Modulangebot_FGI/1216958.md) | Wil van der Aalst | 2024-02-16 |
|  | [Business Process Modeling & Computation](../Module_Modulangebot_FGI/1229150.md) | Leemans | 2024-02-16 |
|  | [CSCW and Groupware: Concepts and Systems for Computer Supported Cooperative Work](../Module_Modulangebot_FGI/1215691.md) | Wolfgang Prinz | 2024-02-16 |
|  | [Combinatorial Problems in Scientific Computing](../Module_Modulangebot_FGI/1215721.md) | Uwe Naumann | 2024-02-16 |
|  | [Communication Systems Engineering](../Module_Modulangebot_FGI/1212349.md) | Klaus Wehrle & Dirk Thißen | 2024-02-16 |
|  | [Compilerbau](../Module_Modulangebot_FGI/1211978.md) | Thomas Noll Joost-Pieter Katoen | 2024-02-16 |
|  | [Computational Group Theory](../Module_Modulangebot_FGI/1212656.md) | Pascal Schweitzer | 2024-02-16 |
|  | [Computer Vision 2](../Module_Modulangebot_FGI/1211921.md) | Bastian Leibe | 2024-02-16 |
|  | [Computer Vision](../Module_Modulangebot_FGI/1215724.md) | Bastian Leibe | 2024-02-16 |
|  | [Concurrency Theory](../Module_Modulangebot_FGI/1212646.md) | Thomas Noll Joost-Pieter Katoen | 2024-02-16 |
|  | [Current Topics in Media Computing and HCI](../Module_Modulangebot_FGI/1211908.md) | Jan Oliver Borchers | 2024-02-16 |
|  | [Data Analysis and Visualization](../Module_Modulangebot_FGI/1212372.md) | Leif Kobbelt | 2024-02-16 |
|  | [Data Driven Medicine - project-oriented, multidisciplinary introduction](../Module_Modulangebot_FGI/1215842.md) | Stefan Decker & Univ.- med. Rainer Röhrig & Oya Deniz Beyan | 2024-02-16 |
|  | [Datenbanken und Informationssysteme](../Module_Modulangebot_FGI/1211969.md) | Stefan Decker &; Matthias Jarke | 2024-02-16 |
|  | [Datenkommunikation](../Module_Modulangebot_FGI/1211972.md) | Klaus Wehrle | 2024-02-16 |
|  | [Datenstrommanagement und -analyse](../Module_Modulangebot_FGI/1226146.md) | Sandra Geisler | 2024-02-16 |
|  | [Datenstrukturen und Algorithmen](../Module_Modulangebot_FGI/1211971.md) | Leif Kobbelt &; Joost-Pieter Katoen &; Hermann Ney &; Peter Rossmanith | 2024-02-16 |
|  | [Deduktive Programmverifikation](../Module_Modulangebot_FGI/1212659.md) | Jürgen Giesl | 2024-02-16 |
|  | [Der digitale Lebenszyklus von Fahrzeugen als Teil des Internet of Things (IoT)](../Module_Modulangebot_FGI/1215755.md) | Ansgar Schleicher; Bernhard Rumpe | 2024-02-16 |
|  | [Designing Interactive Systems II](../Module_Modulangebot_FGI/1215699.md) | Jan Oliver Borchers | 2024-02-16 |
|  | [Designing Interactive Systems I](../Module_Modulangebot_FGI/1215698.md) | Jan Oliver Borchers | 2024-02-16 |
|  | [Deutschkurs](../Module_Modulangebot_FGI/1215734.md) | Sprachenzentrum | 2024-02-16 |
|  | [Didaktische Zugänge zur informatischen Bildung](../Module_Modulangebot_FGI/1228824.md) | Nadine Bergner | 2024-02-16 |
|  | [Didaktische Zugänge zur informatischen Bildung](../Module_Modulangebot_FGI/1228884.md) | Nadine Bergner | 2024-02-16 |
|  | [Digitalisierung](../Module_Modulangebot_FGI/1220230.md) | Bernhard Rumpe | 2024-02-16 |
|  | [Diskrete Differentialgeometrie](../Module_Modulangebot_FGI/1212694.md) | David Bommes | 2024-02-16 |
|  | [Distributed Ledger Technology](../Module_Modulangebot_FGI/1226006.md) | Rose &; Prinz | 2024-02-16 |
|  | [Dynamical Processes on Networks](../Module_Modulangebot_FGI/1223640.md) | keine Angabe | 2024-02-16 |
|  | [Effiziente Algorithmen](../Module_Modulangebot_FGI/1211977.md) | Peter Rossmanith Walter Unger | 2024-02-16 |
|  | [Einführung in Web Technologien](../Module_Modulangebot_FGI/1211914.md) | Ulrik Schroeder | 2024-02-16 |
|  | [Einführung in das wissenschaftliche Arbeiten (Proseminar Informatik)](../Module_Modulangebot_FGI/1211968.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Einführung in die Fachdidaktik Informatik](../Module_Modulangebot_FGI/1212317.md) | Nadine Bergner | 2024-02-16 |
|  | [Einführung in die Informatik](../Module_Modulangebot_FGI/1212365.md) | Unbekannt | 2024-02-16 |
|  | [Einführung in die Programmierung für datenbasierte Wissenschaften](../Module_Modulangebot_FGI/1224007.md) | Ulrik Schroeder | 2024-02-16 |
|  | [Einführung in die Programmierung](../Module_Modulangebot_FGI/1215679.md) | Uwe Naumann | 2024-02-16 |
|  | [Einführung in die Technische Informatik](../Module_Modulangebot_FGI/1214958.md) | Stefan Kowalewski Gerhard Lakemeyer | 2024-02-16 |
|  | [Eingebettete Systeme](../Module_Modulangebot_FGI/1215690.md) | Stefan Kowalewski | 2024-02-16 |
|  | [Elements of Machine Learning and Data Science](../Module_Modulangebot_FGI/1226970.md) | keine Angabe | 2024-02-16 |
|  | [Erfüllbarkeitsüberprüfung](../Module_Modulangebot_FGI/1212341.md) | Erika Ábrahám | 2024-02-16 |
|  | [Ersatzveranstaltung - Individuelles Modul](../Module_Modulangebot_FGI/1222030.md) | keine Angabe | 2024-02-16 |
|  | [Exakte Algorithmen](../Module_Modulangebot_FGI/1212658.md) | Peter Rossmanith | 2024-02-16 |
|  | [Fachdidaktik Informatik](../Module_Modulangebot_FGI/1212322.md) | Nadine Bergner | 2024-02-16 |
|  | [Fachdidaktik Informatik](../Module_Modulangebot_FGI/1227857.md) | Ulrik Schroeder | 2024-02-16 |
|  | [Faszination Technik in der Informatik](../Module_Modulangebot_FGI/1212323.md) | Ulrik Schroeder | 2024-02-16 |
|  | [Fixpoints and Induction in Logic and Computer Science](../Module_Modulangebot_FGI/1226911.md) | Christof Löding | 2024-02-16 |
|  | [Formale Grundlagen von UML](../Module_Modulangebot_FGI/1212648.md) | Joost-Pieter Katoen | 2024-02-16 |
|  | [Formale Methoden für Steuerungssoftware](../Module_Modulangebot_FGI/1212666.md) | Stefan Kowalewski Hendrik Simon | 2024-02-16 |
|  | [Formale Systeme, Automaten, Prozesse](../Module_Modulangebot_FGI/1214961.md) | Joost-Pieter Katoen Martin Grohe Jürgen Giesl Peter Rossmanith | 2024-02-16 |
|  | [Forschungsmodul](../Module_Modulangebot_FGI/1211976.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Fortgeschrittene Methoden der Virtuellen Realität](../Module_Modulangebot_FGI/1212688.md) | Torsten Wolfgang Kuhlen | 2024-02-16 |
|  | [Foundations of Functional Programming](../Module_Modulangebot_FGI/1215684.md) | Jürgen Giesl | 2024-02-16 |
|  | [Foundations of Logic Programming](../Module_Modulangebot_FGI/1212343.md) | Jürgen Giesl | 2024-02-16 |
|  | [Functions of Matrices with Applications](../Module_Modulangebot_FGI/1215727.md) | Paolo Bientinesi | 2024-02-16 |
|  | [Fundamentals of Automatic Speech Recognition](../Module_Modulangebot_FGI/1230106.md) | Priv.-Doz. Ralf Schlüter | 2024-02-16 |
|  | [Fundamentals of Business Process Management](../Module_Modulangebot_FGI/1227457.md) | Sander Leemans | 2024-02-16 |
|  | [Funktionale Sicherheit und Systemzuverlässigkeit](../Module_Modulangebot_FGI/1212353.md) | Stefan Kowalewski | 2024-02-16 |
|  | [Geometrieverarbeitung](../Module_Modulangebot_FGI/1215696.md) | Leif Kobbelt | 2024-02-16 |
|  | [Grafikprogrammierung in OpenGL](../Module_Modulangebot_FGI/1212686.md) | Leif Kobbelt | 2024-02-16 |
|  | [Graphalgorithmen](../Module_Modulangebot_FGI/1212327.md) | Walter Unger | 2024-02-16 |
|  | [Graphzerlegungen und algorithmische Anwendungen](../Module_Modulangebot_FGI/1212710.md) | Martin Grohe | 2024-02-16 |
|  | [Grundlagen der Computergraphik](../Module_Modulangebot_FGI/1212310.md) | Leif Kobbelt | 2024-02-16 |
|  | [Grundzüge der Informatik](../Module_Modulangebot_FGI/1212315.md) | keine Angabe | 2024-02-16 |
|  | [High-Performance Computing](../Module_Modulangebot_FGI/1215720.md) | Matthias Müller | 2024-02-16 |
|  | [High-performance Matrix Computations](../Module_Modulangebot_FGI/1211911.md) | Paolo Bientinesi | 2024-02-16 |
|  | [Höhere Algorithmik](../Module_Modulangebot_FGI/1223638.md) | keine Angabe | 2024-02-16 |
|  | [IT-Sicherheit 1 - Kryptographische Grundlagen und Netzwerksicherheit](../Module_Modulangebot_FGI/1211901.md) | Ulrike Meyer | 2024-02-16 |
|  | [IT-Sicherheit 2 - Computer Security](../Module_Modulangebot_FGI/1211900.md) | Ulrike Meyer | 2024-02-16 |
|  | [IT-Sicherheit](../Module_Modulangebot_FGI/1226971.md) | keine Angabe | 2024-02-16 |
|  | [Implementation of Databases](../Module_Modulangebot_FGI/1215692.md) | Matthias Jarke | 2024-02-16 |
|  | [Indexstrukturen für Datenbanken](../Module_Modulangebot_FGI/1212679.md) | Thomas Seidl | 2024-02-16 |
|  | [Individuelles Modul Zusätzliche Prüfungen](../Module_Modulangebot_FGI/1229767.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Bereich Kommunikationsfertigkeiten](../Module_Modulangebot_FGI/1219553.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Bereich Medien-Informatik Praktika](../Module_Modulangebot_FGI/1219551.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Bereich Multimedia-Benutzung und -Wirkung](../Module_Modulangebot_FGI/1219549.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Bereich Multimedia-Technologie](../Module_Modulangebot_FGI/1219547.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Bereich Rechner- und Kommunikationstechnologie](../Module_Modulangebot_FGI/1219543.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Mastervorzug](../Module_Modulangebot_FGI/1222479.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Vertiefungsbereich Applied Computer Science](../Module_Modulangebot_FGI/1219505.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Vertiefungsbereich Communication](../Module_Modulangebot_FGI/1219507.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Vertiefungsbereich Data and Information Management](../Module_Modulangebot_FGI/1219509.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Vertiefungsbereich Software Engineering](../Module_Modulangebot_FGI/1219511.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Vertiefungsbereich Theoretical Foundations of Software Systems Engineering](../Module_Modulangebot_FGI/1219503.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Vertiefungsbereich](../Module_Modulangebot_FGI/1221648.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Wahlpflichtbereich Angewandte Informatik](../Module_Modulangebot_FGI/1219583.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Wahlpflichtbereich Daten- und Informationsmanagement](../Module_Modulangebot_FGI/1219585.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Wahlpflichtbereich Software und Kommunikation](../Module_Modulangebot_FGI/1219587.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Wahlpflichtbereich Theoretische Informatik](../Module_Modulangebot_FGI/1219589.md) | keine Angabe | 2024-02-16 |
|  | [Individuelles Modul im Wahlpflichtbereich](../Module_Modulangebot_FGI/1219222.md) | keine Angabe | 2024-02-16 |
|  | [Industrial Network Security](../Module_Modulangebot_FGI/1227956.md) | Martin Henze | 2024-02-16 |
|  | [Infinite Computations and Games](../Module_Modulangebot_FGI/1212336.md) | Christof Löding | 2024-02-16 |
|  | [Infinite Games](../Module_Modulangebot_FGI/1212333.md) | Wolfgang Thomas | 2024-02-16 |
|  | [Informatik (WP VT)](../Module_Modulangebot_FGI/1212321.md) | keine Angabe | 2024-02-16 |
|  | [Informatik-Praktikum für Mathematiker](../Module_Modulangebot_FGI/1212243.md) | Christof Löding | 2024-02-16 |
|  | [Informatik](../Module_Modulangebot_FGI/1212313.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Informatik](../Module_Modulangebot_FGI/1212318.md) | Unbekannt | 2024-02-16 |
|  | [Informatik](../Module_Modulangebot_FGI/1212319.md) | keine Angabe | 2024-02-16 |
|  | [Information Management](../Module_Modulangebot_FGI/1211923.md) | Matthias Jarke | 2024-02-16 |
|  | [Inhaltsbasierte Ähnlichkeitssuche](../Module_Modulangebot_FGI/1211929.md) | Thomas Seidl | 2024-02-16 |
|  | [Innovationen im Software Engineering](../Module_Modulangebot_FGI/1212345.md) | Bernhard Rumpe | 2024-02-16 |
|  | [Introduction to Algorithmic Differentiation](../Module_Modulangebot_FGI/1221327.md) | Uwe Naumann | 2024-02-16 |
|  | [Introduction to Artificial Intelligence](../Module_Modulangebot_FGI/1220228.md) | MAO Informatik | 2024-02-16 |
|  | [Introduction to Bioinformatics](../Module_Modulangebot_FGI/1211903.md) | Thomas Berlage | 2024-02-16 |
|  | [Introduction to Data Science](../Module_Modulangebot_FGI/1216861.md) | Wil van der Aalst | 2024-02-16 |
|  | [Introduction to Numerical Methods and Software with C++](../Module_Modulangebot_FGI/1229154.md) | Uwe Naumann | 2024-02-16 |
|  | [Introduction to Numerical Methods and Software](../Module_Modulangebot_FGI/1220996.md) | Uwe Naumann | 2024-02-16 |
|  | [Kombinatorische Optimierung in der wissenschaftlichen Praxis](../Module_Modulangebot_FGI/1228925.md) | Christina Büsing | 2024-02-16 |
|  | [Komplexitätstheorie](../Module_Modulangebot_FGI/1212331.md) | Martin Grohe | 2024-02-16 |
|  | [Konzepte und Modelle der parallelen und datenzentrischen Programmierung](../Module_Modulangebot_FGI/1216838.md) | Christian Terboven & Matthias S. Müller | 2024-02-16 |
|  | [Künstliche Intelligenz](../Module_Modulangebot_FGI/1215694.md) | Gerhard Lakemeyer | 2024-02-16 |
|  | [Learning Technologies](../Module_Modulangebot_FGI/1215751.md) | Ulrik Schroeder | 2024-02-16 |
|  | [Leistungs- und Korrektheitsanalyse paralleler Programme](../Module_Modulangebot_FGI/1215722.md) | Matthias Müller | 2024-02-16 |
|  | [Linux Kernel Programming](../Module_Modulangebot_FGI/1229308.md) | Univ.- Redha Gouicem | 2024-02-16 |
|  | [Machine Learning with Graphs: Foundations and Applications](../Module_Modulangebot_FGI/1227996.md) | keine Angabe | 2024-02-16 |
|  | [Machine Learning](../Module_Modulangebot_FGI/1215744.md) | Bastian Leibe | 2024-02-16 |
|  | [Masterarbeit](../Module_Modulangebot_FGI/1212703.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Mentoring Informatik](../Module_Modulangebot_FGI/1214959.md) | Uwe Naumann | 2024-02-16 |
|  | [Mikrocontrollerprogrammierung und Fehlersuche](../Module_Modulangebot_FGI/1220524.md) | André Stollenwerk | 2024-02-16 |
|  | [Mobile Internet Technology](../Module_Modulangebot_FGI/1212346.md) | Klaus Wehrle & Dirk Thißen | 2024-02-16 |
|  | [Model Checking](../Module_Modulangebot_FGI/1212328.md) | Wolfgang Thomas Joost-Pieter Katoen | 2024-02-16 |
|  | [Model-based Systems Engineering](../Module_Modulangebot_FGI/1222882.md) | keine Angabe | 2024-02-16 |
|  | [Modeling and Verification of Probabilistic Systems](../Module_Modulangebot_FGI/1212711.md) | Joost-Pieter Katoen | 2024-02-16 |
|  | [Modellbasierte Softwareentwicklung](../Module_Modulangebot_FGI/1215686.md) | Bernhard Rumpe | 2024-02-16 |
|  | [Modelle der Datenexploration](../Module_Modulangebot_FGI/1216218.md) | Thomas Seidl | 2024-02-16 |
|  | [Modellierung und Analyse hybrider Systeme](../Module_Modulangebot_FGI/1212339.md) | Erika Abraham | 2024-02-16 |
|  | [Nicht-technisches Wahlfach Mentoring](../Module_Modulangebot_FGI/1220643.md) | keine Angabe | 2024-02-16 |
|  | [Objektorientierte Softwarekonstruktion](../Module_Modulangebot_FGI/1212354.md) | Horst Lichter | 2024-02-16 |
|  | [Online Algorithmen](../Module_Modulangebot_FGI/1212645.md) | Gerhard Wöginger & Walter Unger | 2024-02-16 |
|  | [Parallele Algorithmen](../Module_Modulangebot_FGI/1212689.md) | Martin Bücker | 2024-02-16 |
|  | [Parallele Programmierung II](../Module_Modulangebot_FGI/1215726.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Parametrisierte Algorithmen](../Module_Modulangebot_FGI/1212338.md) | Peter Rossmanith | 2024-02-16 |
|  | [Personal Digital Fabrication](../Module_Modulangebot_FGI/1216839.md) | Jan Oliver Borchers Simon Völker | 2024-02-16 |
|  | [Physikalisch-Basierte Animation](../Module_Modulangebot_FGI/1215862.md) | Jan Stephen Bender | 2024-02-16 |
|  | [Physikalische Simulation im Visual Computing](../Module_Modulangebot_FGI/1212692.md) | keine Angabe | 2024-02-16 |
|  | [Post-quantum cryptography](../Module_Modulangebot_FGI/1229347.md) | ;Dominique Unruh | 2024-02-16 |
|  | [Praktikum Angewandte Informatik](../Module_Modulangebot_FGI/1211937.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Praktikum Daten und Informationsmanagement](../Module_Modulangebot_FGI/1211935.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Praktikum Inklusion und Heterogenität](../Module_Modulangebot_FGI/1215685.md) | Nadine Bergner | 2024-02-16 |
|  | [Praktikum Software und Kommunikation](../Module_Modulangebot_FGI/1211934.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Praktikum Theoretische Informatik](../Module_Modulangebot_FGI/1215659.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Praktikum](../Module_Modulangebot_FGI/1215759.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Praktische Informatik](../Module_Modulangebot_FGI/1212316.md) | Fachgruppe Informatik. | 2024-02-16 |
|  | [Privacy Enhancing Technologies for Data Science](../Module_Modulangebot_FGI/1212677.md) | Stefan Decker | 2024-02-16 |
|  | [Probabilistic Programming](../Module_Modulangebot_FGI/1212650.md) | Joost-Pieter Katoen | 2024-02-16 |
|  | [Programmierung](../Module_Modulangebot_FGI/1214957.md) | Joost-Pieter Katoen &; Ulrik Schroeder &; Jürgen Giesl | 2024-02-16 |
|  | [Prozess Management](../Module_Modulangebot_FGI/1211902.md) | Thomas Rose | 2024-02-16 |
|  | [Prozesse und Methoden beim Testen von Software](../Module_Modulangebot_FGI/1215732.md) | Bernhard Rumpe | 2024-02-16 |
|  | [Randomized Algorithms](../Module_Modulangebot_FGI/1212705.md) | Berthold Vöcking | 2024-02-16 |
|  | [Real-time Graphics](../Module_Modulangebot_FGI/1215680.md) | Leif Kobbelt | 2024-02-16 |
|  | [Recht in der Informatik](../Module_Modulangebot_FGI/1227459.md) | Honorarprofessor jur. Uwe Meiendresch | 2024-02-16 |
|  | [Regelung und Wahrnehmung in Vernetzten und Autonomen Fahrzeugen](../Module_Modulangebot_FGI/1221329.md) | Bassam Alrifaee | 2024-02-16 |
|  | [Rekursionstheorie](../Module_Modulangebot_FGI/1212335.md) | Martin Grohe | 2024-02-16 |
|  | [Research Focus Class on Communication Systems](../Module_Modulangebot_FGI/1212347.md) | keine Angabe | 2024-02-16 |
|  | [Research Focus Class on Learning Technologies](../Module_Modulangebot_FGI/1222419.md) | keine Angabe | 2024-02-16 |
|  | [Schwerpunktkolloquium Angewandte Informatik](../Module_Modulangebot_FGI/1212704.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Schwerpunktkolloquium Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212702.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Schwerpunktkolloquium Software und Kommunikation](../Module_Modulangebot_FGI/1212701.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Schwerpunktkolloquium Theoretische Informatik](../Module_Modulangebot_FGI/1212700.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Schwerpunktkolloquium](../Module_Modulangebot_FGI/1222540.md) | keine Angabe | 2024-02-16 |
|  | [Selected Topics in Communication and Distributed Systems](../Module_Modulangebot_FGI/1212665.md) | Klaus Wehrle | 2024-02-16 |
|  | [Semantic Web](../Module_Modulangebot_FGI/1212675.md) | Stefan Decker | 2024-02-16 |
|  | [Semantik und Verifikation von Software](../Module_Modulangebot_FGI/1212329.md) | Thomas Noll | 2024-02-16 |
|  | [Seminar I Angewandte Informatik](../Module_Modulangebot_FGI/1211931.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Seminar I Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212698.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Seminar I Software und Kommunikation](../Module_Modulangebot_FGI/1212696.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Seminar I Theoretische Informatik](../Module_Modulangebot_FGI/1211938.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Seminar II Angewandte Informatik](../Module_Modulangebot_FGI/1211932.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Seminar II Daten und Informationsmanagement](../Module_Modulangebot_FGI/1212699.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Seminar II Software und Kommunikation](../Module_Modulangebot_FGI/1212697.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Seminar II Theoretische Informatik](../Module_Modulangebot_FGI/1211930.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Seminar Informatik](../Module_Modulangebot_FGI/1211974.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Seminar: Topics in Automation, Compilers and Code-Generation](../Module_Modulangebot_FGI/1215729.md) | Paolo Bientinesi Prüfungsamt ZPA | 2024-02-16 |
|  | [Seminar: Topics in High-Performance and Scientific Computing](../Module_Modulangebot_FGI/1215728.md) | Paolo Bientinesi | 2024-02-16 |
|  | [Seminar](../Module_Modulangebot_FGI/1212324.md) | Fachgruppe Informatik | 2024-02-16 |
|  | [Shape Analysis and 3D Deep Learning](../Module_Modulangebot_FGI/1223639.md) | keine Angabe | 2024-02-16 |
|  | [SiSc Laboratory](../Module_Modulangebot_FGI/1212320.md) | Uwe Naumann | 2024-02-16 |
|  | [Sicherheit in der Mobilkommunikation](../Module_Modulangebot_FGI/1212681.md) | Ulrike Meyer | 2024-02-16 |
|  | [Sicherheit und Zuverlässigkeit softwaregesteuerter Systeme (bis WS 14/15)](../Module_Modulangebot_FGI/1212351.md) | Stefan Kowalewski | 2024-02-16 |
|  | [Social Computing](../Module_Modulangebot_FGI/1212678.md) | Ralf Klamma | 2024-02-16 |
|  | [Social and Technological Change](../Module_Modulangebot_FGI/1229157.md) | Geffner | 2024-02-16 |
|  | [Software Language Engineering](../Module_Modulangebot_FGI/1216957.md) | Bernhard Rumpe | 2024-02-16 |
|  | [Software-Architekturen](../Module_Modulangebot_FGI/1215687.md) | Manfred Nagl | 2024-02-16 |
|  | [Software-Projektmanagement](../Module_Modulangebot_FGI/1212355.md) | Horst Lichter | 2024-02-16 |
|  | [Software-Projektpraktikum](../Module_Modulangebot_FGI/1211973.md) | Horst Lichter & Fachgruppe Informatik | 2024-02-16 |
|  | [Software-Qualitätssicherung](../Module_Modulangebot_FGI/1212356.md) | Horst Lichter | 2024-02-16 |
|  | [Softwarepraktikum](../Module_Modulangebot_FGI/1212636.md) | Jan Oliver Borchers | 2024-02-16 |
|  | [Softwaretechnik-Programmiersprache Ada 95](../Module_Modulangebot_FGI/1211982.md) | Manfred Nagl | 2024-02-16 |
|  | [Softwaretechnik](../Module_Modulangebot_FGI/1211965.md) | Bernhard Rumpe | 2024-02-16 |
|  | [Sprachkurs](../Module_Modulangebot_FGI/1225243.md) | keine Angabe | 2024-02-16 |
|  | [Sprachkurs](../Module_Modulangebot_FGI/1225244.md) | keine Angabe | 2024-02-16 |
|  | [Sprachmodul 1](../Module_Modulangebot_FGI/1229768.md) | keine Angabe | 2024-02-16 |
|  | [Sprachmodul 2](../Module_Modulangebot_FGI/1230044.md) | keine Angabe | 2024-02-16 |
|  | [Sprachmodul 3](../Module_Modulangebot_FGI/1230045.md) | keine Angabe | 2024-02-16 |
|  | [Statische Programmanalyse](../Module_Modulangebot_FGI/1212330.md) | Thomas Noll | 2024-02-16 |
|  | [Statistische Klassifikation und Maschinelles Lernen](../Module_Modulangebot_FGI/1215840.md) | Hermann Ney | 2024-02-16 |
|  | [Statistische Methoden zur Verarbeitung natürlicher Sprache](../Module_Modulangebot_FGI/1215695.md) | Hermann Ney | 2024-02-16 |
|  | [Stochastic Games](../Module_Modulangebot_FGI/1219623.md) | Priv.-Doz. Christof Löding | 2024-02-16 |
|  | [Subdivision Kurven und Flächen](../Module_Modulangebot_FGI/1215697.md) | Leif Kobbelt | 2024-02-16 |
|  | [Systemprogrammierung](../Module_Modulangebot_FGI/1211967.md) | Stefan Kowalewski | 2024-02-16 |
|  | [Termersetzungssysteme](../Module_Modulangebot_FGI/1212340.md) | Jürgen Giesl | 2024-02-16 |
|  | [Testen reaktiver Systeme](../Module_Modulangebot_FGI/1212649.md) | Joost-Pieter Katoen | 2024-02-16 |
|  | [The Graph Isomorphism Problem](../Module_Modulangebot_FGI/1212654.md) | Martin Grohe | 2024-02-16 |
|  | [The Logic of Knowledge Bases](../Module_Modulangebot_FGI/1211393.md) | Gerhard Lakemeyer | 2024-02-16 |
|  | [Theory of Constraint Satisfaction Problems](../Module_Modulangebot_FGI/1212334.md) | Martin Grohe | 2024-02-16 |
|  | [Theory of Distributed Systems](../Module_Modulangebot_FGI/1212641.md) | keine Angabe | 2024-02-16 |
|  | [Theory of Distributed and Parallel Systems](../Module_Modulangebot_FGI/1212643.md) | Walter Unger | 2024-02-16 |
|  | [Uncertainty in Robotics](../Module_Modulangebot_FGI/1222468.md) | Lakemeyer | 2024-02-16 |
|  | [Virtuelle Realität](../Module_Modulangebot_FGI/1211909.md) | Torsten Wolfgang Kuhlen | 2024-02-16 |
|  | [Vorbereitungskurs zum Softwareentwicklungspraktikum und Softwareentwicklungspraktikum](../Module_Modulangebot_FGI/1212371.md) | Uwe Naumann | 2024-02-16 |
|  | [Web Science](../Module_Modulangebot_FGI/1212359.md) | Matthias Jarke | 2024-02-16 |
|  | [Wissensrepräsentation](../Module_Modulangebot_FGI/1212361.md) | Gerhard Lakemeyer | 2024-02-16 |
|  | [Zusatzkompetenz 1](../Module_Modulangebot_FGI/1219455.md) | keine Angabe | 2024-02-16 |
|  | [Zusatzkompetenz](../Module_Modulangebot_FGI/1226286.md) | keine Angabe | 2024-02-16 |
|  | [eBusiness - Anwendungen, Architekturen und Standards](../Module_Modulangebot_FGI/1212683.md) | Thomas Rose | 2024-02-16 |
|  | [iOS Application Development](../Module_Modulangebot_FGI/1215681.md) | Jan Oliver Borchers &; Simon Völker | 2024-02-16 |

