#### Hinweis

Die obenstehenden Listen sortieren die Module aus dem Modulangebot der FGI nach 
verschiedenen Kriterien (Modultitel, Modulnummer, Anbieter etc.).  
Die in den Listen enthaltenen Module verlinken auf die Moduldateien im Ordner
[Module_Modulangebot_FGI](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/tree/master/Module_Modulangebot_FGI).
