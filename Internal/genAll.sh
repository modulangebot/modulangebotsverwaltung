#!/bin/bash
#  MontiCore stable
# Ok.

MHBList=`find . -print | grep "Internal/Modulhandbuchexporte/" | grep ".xml"`
MHBamount=$(wc -w <<< "$MHBList")

#cleanUp
mkdir -p Module_Modulangebot_FGI
mkdir -p Internal/ModuleRaw

rm -rfv Module_Modulangebot_FGI/*
rm -rfv Internal/ModuleRaw/*

for export in $MHBList
do
	MHBCounter=$(( MHBCounter + 1 ))
	echo
	echo "===================================================================="
	echo 'MHB '$MHBCounter' / '$MHBamount
	echo  $export
	echo "===================================================================="
	echo

	./Internal/genModule.sh $export
done

echo "===================================================================="
echo "Postprocessing"
echo "===================================================================="

./Internal/genTheOverviews.sh 
#./Internal/genOverview.sh
./Internal/someSanityChecks.sh

echo "===================================================================="
echo "Done"
echo "===================================================================="

