#!/bin/bash
# MontiCore stable
#
# Dieses Skript parsed den XML export des Modulkatalogs von RWTHonline
# Die einzelnen Module werden in dem Ordner ModuleGen abgelegt.
#
# Dieses Skript wird im Root-verzeichnis des Repositories ausgeführt.
#


convert_param_module () {
	local O='<!--  File originally created on '$(date -u)' TEST 1 -->'
			
	O+='\n## '${M_NAME}' ('${M_KENNUNG}')'
	O+='\nDie folgende Modulbeschreibung basiert auf der Beschreibung des Moduls in der SPO  '
	O+='\n**'$M_SPO' '$M_SPO_VER' ('$M_BEREICH')**.  '
	O+='\nWeitere SPOen und dort ggf. abweichende Moduleigenschaften sind in Abschnitt 5 angeführt.  '
	O+='\n'
	O+='\n<!--'
	O+='\n=== Allgemeine Hinweise: ==='
	O+='\n-  Pflichtfelder sind mit "(!)" markiert.  '
	O+='\n-  Für DEUTSCHsprachige Module ist eine Übersetzung in ENGLISCH anzugeben.  '
	O+='\n-  Für ENGLISCHsprachige Module sind - mit Ausnahme des Modultitels! - nur die Felder mit der ENGLISCHsprachigen Formulierung auszufüllen.  '
    O+='\n-  Innerhalb von Tabellen KEINE ZEILEN UMBRECHEN.  '
	O+='\n-->'
	O+='\n'
	O+='\n'
#--------------------------------------------------------------
# ALLG Moduldaten
#--------------------------------------------------------------
	O+='\n### 1. Allgemeine Moduldaten'
	O+='\n*[Erläuterungen zu Abschnitt 1](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/1_Allgemeine_Moduldaten.md)*'
	O+='\n'
	O+='\n| Feld                    | Inhalt'
	O+='\n| ------------------------|--------------------------------------------------'
	O+='\n| (!) Modultitel (DE)     | '${M_NAME}''
	O+='\n| (!) Modultitel (EN)     | '${M_NAME_EN}'' 
	O+='\n| (!) ECTS                | '${M_ECTS}''
	O+='\n| (!) Gültig ab           | '${M_VALIDFROM}''
	O+='\n|  Gültig bis             | '${M_VALIDUNTIL}''
	O+='\n| (!)&nbsp;ModulanbieterIn| '${M_RESPONSIBLE}''
	O+='\n| (!) Sprache             | '${M_LANGUAGE}''
	O+='\n| (!) Turnus              | '${M_FREQUENCY}''
	O+='\n| (!) Moduldauer          | '${M_MODULDAUER}''
	O+='\n| (!) Modulniveau         | '${M_LEVEL}''
	O+='\n| (!) Fachsemester        | '${M_SEM}''
	O+='\n'
	O+='\n'

#--------------------------------------------------------------
# Lehrveranstaltungen (LV) und Prüfungsveranstaltungen (PV)
#--------------------------------------------------------------
	O+='\n### 2. Modulveranstaltungen'
	O+='\n*[Erläuterungen zu Abschnitt 2](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/2_Modulveranstaltungen.md)*'
	O+='\n'
	O+='\n| (!) Modulveranstaltungen (DE) | (!) Modulveranstaltungen (EN)|(!) ECTS | (!) Präsenzzeit (SWS) |'
	O+='\n|-------------------------------|------------------------------|---------|-----------------------|'
	O+=' '${LVPV_EINTRAG}''
	O+='\n'
	O+='\n'

#--------------------------------------------------------------
# Studien- und Prüfungsleistungen
#--------------------------------------------------------------
	O+='\n### 3. Studien- und Prüfungsleistungen  '
	O+='\n*[Erläuterungen zu Abschnitt 3](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/3_Leistungen.md)*'
	O+='\n'
	O+='\n**Formale Voraussetzungen (DE)**  '
	O+='\n*Siehe Modulkatalog der jeweiligen SPO.*'
	O+='\n'
	O+='\n**Formale Voraussetzungen (EN)**  '
	O+='\n*See module catalogue of correponding SPO.*'
	O+='\n'
	O+='\n**(!) Benotung (DE)**  '
	O+='\n'${M_GRADING_DE}''
	O+='\n'
	O+='\n**(!) Benotung (EN)**  '
	O+='\n'${M_GRADING_EN}''
	O+='\n'
	O+='\n'

#--------------------------------------------------------------
# Informationen für das Modulhandbuch
#--------------------------------------------------------------
	O+='\n### 4. Informationen für das Modulhandbuch  '
	O+='\n*[Erläuterungen zu Abschnitt 4](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/4_Infos_Modulhandbuch.md)*'
	O+='\n<!-- '
	O+='\n=== Allgemeiner Hinweis ==='
	O+='\nWir bitten Angaben in allen Punkten zu machen, da diese für die (Re-)Akkreditierung'
	O+='\nvon Studiengängen benötigt werden.'
	O+='\n--> '
	O+='\n'
	O+='\n**Empfohlene Voraussetzungen (DE)**  '
	O+='\n'${M_PREV_KNOW_DE}''
	O+='\n'
	O+='\n**Empfohlene Voraussetzungen (EN)**  '
	O+='\n'${M_PREV_KNOW_EN}''
	O+='\n'
	O+='\n**(!) Lernziele (DE)**  '
	O+='\n'${M_LERNZIELE_DE}''
	O+='\n'
	O+='\n**(!) Lernziele (EN)**  '
	O+='\n'${M_LERNZIELE_EN}''
	O+='\n'
	O+='\n**(!) Inhalt (DE)**  '
	O+='\n'${M_CONTENT_DE}''
	O+='\n'
	O+='\n**(!) Inhalt (EN)**  '
	O+='\n'${M_CONTENT_EN}''
	O+='\n'
	O+='\n**(!) Literatur**  '
	O+='\n'${M_LITERATUR}''
	O+='\n'	
	O+='\n'

#--------------------------------------------------------------
# Studien- und Prüfungsordnungen, in denen das Modul verankert ist
#--------------------------------------------------------------
	O+='\n### 5a. Alle SPOen, in denen das Modul verankert ist'
	O+='\n*[Erläuterungen zu Abschnitt 5a/5b](https://git.rwth-aachen.de/modulangebot/modulangebotsverwaltung/blob/master/Anleitungen_Hilfen/5_Studiengänge.md)*'
	O+='\n'
	O+='\n| (!) Studiengangskürzel | (!) SPO-Version     | (!) Modulbereich'
	O+='\n|------------------------| --------------------|------------------------------------'
	O+='\n| '$M_SPO'               | '$M_SPO_VER'        | '$M_BEREICH''
	O+='\n<!--Ende Studiengangsliste-->'
	O+='\n'
	O+='\n'
	O+='\n#### 5b. SPOen mit abweichenden Verwendungsspezifika:'
	O+='\n<!--'
	O+='\n=== Allgemeiner Hinweis: ==='
	O+='\nAbweichende Verwendungsspezifika nach Möglichkeit vermeiden.'
	O+='\nFall notwendig, !je! Studiengang/SPO-Version/Modulbereich'
	O+='\ndas oder die abweichende(n) Verwendungsspezifika auflisten.'
	O+='\n-->'


	OUTPUT=$O;
}




#--------------------------------------------------------------------------------------------------------


init () {

	IS_SPO='false'
	LVPV_EINTRAG='';
	MODULE_COUNTER=0;
	M_C_TMP=0;
	ID=0;
	H1='*wert-schlummert-noch-in-R.O.-DB*'
	M_BEREICH='keine Zuordnung'
}

getProf () {
	if [[ $M_RESPONSIBLE = "*keine Angabe*" ]] ; then
		M_PROF='_Kein_Verantwortlicher_Eingetragen_'
	else
		# Bereinigung Modulanbieter
		M_PROF=${M_RESPONSIBLE#*Modulverantworlicher:*}


		M_PROF=${M_PROF//&lt;br/}
		M_PROF=${M_PROF//&lt;br \/&gt;/}
		M_PROF=${M_PROF//&gt;/}
		M_PROF=${M_PROF//&lt;br&gt;}
		M_PROF=${M_PROF//\<br\>/}
		M_PROF=${M_PROF//\<br \/\>/}
		M_PROF=${M_PROF//Universitätsprofessorin /}
		M_PROF=${M_PROF//Universitätsprofessor /}
		M_PROF=${M_PROF//Professorin /}
		M_PROF=${M_PROF//Professor /}
		M_PROF=${M_PROF//als Juniorprofessor /}
		M_PROF=${M_PROF//als Juniorprofessorin /}
		M_PROF=${M_PROF//Dr\. h\. c\. /}
		M_PROF=${M_PROF//h\. c \. /}
		M_PROF=${M_PROF//techn\. /}

		M_PROF=${M_PROF//Dr\. rer\. nat\. /}
		M_PROF=${M_PROF//Dr\. rer\. pol\. /}
		M_PROF=${M_PROF//Dr\.-Ing\. /}
		M_PROF=${M_PROF//Dr\. Ing\. /}
		M_PROF=${M_PROF//Dr\. ing\. /}
		M_PROF=${M_PROF//Dr\. ir\. /}
		M_PROF=${M_PROF//Ph\. D\. /}
		M_PROF=${M_PROF//Ph\. D\./}
		M_PROF=${M_PROF//\(*\) /}
		M_PROF=${M_PROF//h\. c\. /}
		M_PROF=${M_PROF//i\. R\. /}
		M_PROF=${M_PROF//i\.R\. /}
		M_PROF=${M_PROF//Dr\. /}
		M_PROF=${M_PROF//apl\. /}
		M_PROF=${M_PROF//Prof\. /}
		M_PROF=${M_PROF//Dipl\/form\. /}
		M_PROF=${M_PROF//M\. Sc\. RWTH /}
		M_PROF=${M_PROF//B\. Sc\. /}
		M_PROF=${M_PROF//M\. Sc\. /}
		M_PROF=${M_PROF//LL\.M /}
		M_PROF=${M_PROF//sc\. /}
		M_PROF=${M_PROF//Modulangebotsorganisation: /}
		M_PROF=${M_PROF//Stephanie Schrader /}
		M_PROF=${M_PROF//Modellierungsteamverantwortlicher: /}
		M_PROF=${M_PROF//Sebastiaan Wouters /}
		M_PROF=${M_PROF//sc\. techn\. /}
		M_PROF=${M_PROF//  +/}
		M_PROF=${M_PROF//^ +/}
		M_PROF=${M_PROF//  / }

	fi
}


# Definiert trennzeichen des XML-dokument
next_xml_tag () {
    local IFS=\>
    read -d \< ENTITY CONTENT
    TAG_NAME=${ENTITY%% *}
    ATTRIBUTES=${ENTITY#* }

	#Restore HTML TAGS
	CONTENT=${CONTENT//\&apos\;/\'}
	CONTENT=${CONTENT/\&amp\;/\&}
	CONTENT=${CONTENT/\&quot\;/\"}
	CONTENT=${CONTENT//\&gt\;/\>}
	CONTENT=${CONTENT//\&lt\;/\<}

    if [ -z "$CONTENT" ]; then 
    	CONTENT='*keine Angabe*' 
    fi

    if [ -z "$ENTITY" ]; then 
    	echo 'EOF'
    	exit 1;
    fi
    return $ret
}




# Hauptschleife
init;
while next_xml_tag; do

		# Der MHB-Export ist in resourcen gegliedert, jede resource hat einen Typ
		# Wenn es sich um ein Modul handelt speichern wir die daten zwischen und 
		# Erzeugen eine Datei beim wechsel zur nächsten resource.
		#
		# Den resourcen-Typ erfahren wir nicht am anfang der resource, daher müssen
		# wir einige werte vormerken und ggf. wieder verwerfen.

	  	# Klassifiziert den Knotentyp im XML
		if [[ $ENTITY = "nodeTypeId" ]] ; then
			if [ $CONTENT = "7" ] ||  [ $CONTENT = "6" ]; then
				M_BEREICH=$M_NAME_DE_TMP
			fi

			if [ $CONTENT = "1" ] ; then
				IS_SPO="true"
			else
				IS_SPO="false"
			fi

			# 9: PflichModul 10: Wahlpflichmodul
			if [ $CONTENT = "10" ] || [ $CONTENT = "9" ] ; then 
				IS_MODULE="true"; IS_AK="false"; IS_PK="false"
				MODULE_COUNTER=$(( MODULE_COUNTER + 1 ))
				if [ -z "$M_NAME" ]; then #Name nicht initialisiert
			  		M_NAME=$M_NAME_DE_TMP
			  	fi
				echo 'M ' $M_NAME_DE_TMP 

				if [[ $M_KENNUNG == 12* ]]; then #Informatik Modul
		  			convert_param_module;
		  			echo
		  			echo '   '$M_KENNUNG ',' $M_NAME ',' $M_PROF

					FILENAME=$M_KENNUNG'.md'

					# Clean up File name
		  			FILENAME=${FILENAME// /_}
		  			FILENAME=${FILENAME//\\/-}
		  			FILENAME=${FILENAME//\*/_}
		  			FILENAME=${FILENAME//\//-}
		  			FILENAME=${FILENAME//\|/_}
		  			FILENAME=${FILENAME//\?/_}
		  			FILENAME=${FILENAME//\:/-}
		  			FILENAME=${FILENAME//\>:/_}
		  			FILENAME=${FILENAME//\<:/_}
		  			FILENAME=${FILENAME//\":/_}


		  			FILENAME_PUBLIC='Module_Modulangebot_FGI/'$FILENAME''
		  			FILENAME_INTERN='Internal/ModuleRaw/'$M_KENNUNG'_'$M_SPO'_'$(date +%M%H%S)'.md'
		  			echo -e "$OUTPUT" > $FILENAME_INTERN


		  			# Check if Module-File already exists
		  			if [ -f $FILENAME_PUBLIC ]; then
    					echo "    Modul Existiert bereits, füge SPO Hinzu: "$M_SPO



    					#MOTHER_MODULE=$(<$FILENAME_PUBLIC)    					    			
    					MOTHER_MODULE="$(sed '2,/^.*SPOen, in denen das Modul verankert ist.*/!d' $FILENAME_PUBLIC)"
    					#MOTHER_MODULE=$(sed '1,/^.*SPOen, in denen das Modul verankert ist.*/!d' "$MOTHER_MODULE")

    					#echo "MOTHER_MODULE: $MOTHER_MODULE"


    					#CHILD_MODULE="$OUTPUT" | sed -n '/^.*SPOen, in denen das Modul verankert ist.*/q;p' 
    					#echo -e "$OUTPUT" > 'tmp_modul.md'
    					CHILD_MODULE="$(echo -e \"$OUTPUT\" | sed '2,/^.*SPOen, in denen das Modul verankert ist.*/!d')"
    					#CHILD_MODULE=$(sed '1,/^.*SPOen, in denen das Modul verankert ist.*/!d' "$OUTPUT")
    							
    					#echo -e "CHILD_MODULE: $CHILD_MODULE"

						DIFFREPORT='\n \n### '$M_SPO' '$M_SPO_VER' '$M_BEREICH'\n```\n'
						DIFFREPORT+="$(diff --show-function-line='^###.*' -C 0 -tEZbwBd --speed-large-files <(echo "$CHILD_MODULE") <(echo "$MOTHER_MODULE") | sed '/^\*.*###.*/ a\```'| sed '/^\*.*###.*/ i\```' | sed 's/^\*.*###/#####/')"
						DIFFREPORT+='\n``` \n'
						echo -e "$DIFFREPORT" >> $FILENAME_PUBLIC



    					STUDIENGANG='| '${M_SPO}' | '${M_SPO_VER}' | '${M_BEREICH}''
    					sed -i "/Ende Studiengangsliste/i $STUDIENGANG" $FILENAME_PUBLIC
    					#echo -e "$OUTPUT" > $FILENAME_PUBLIC
    					echo
    				else
    					echo '   Erzeuge Modul: '$FILENAME_PUBLIC
						echo -e "$OUTPUT" > $FILENAME_PUBLIC
						echo
    				fi					
				fi	

				M_NAME=$M_NAME_DE_TMP
				M_NAME_EN=$M_NAME_EN_TMP
				LVPV_EINTRAG=''
				M_C_TMP=$(( M_C_TMP + 1 ))
			else 
				IS_MODULE="false"
			fi
			#Lecture / Vorlesung (AK)
			if [ $CONTENT = "140" ] || [ $CONTENT = "150" ] ; then
				IS_MODULE="false"; IS_AK="true"; IS_PK="false"
				#echo 'AK '${ID}'' 
			else
				IS_AK="false"
			fi
			#Exersise / Übung / Exam / Prüfung (PK)
			if [ $CONTENT = "13" ] || [ $CONTENT = "12" ] ; then
				IS_MODULE="false"; IS_AK="false"; IS_PK="true"
				#echo 'PK '${ID}''
			else
				IS_PK="false"
			fi
		fi

		#---------------------------------------------------
		# Lines before ModuleTypeField
	
		#Speichert den namen der Aktuellen resource zwischen
		if [[ $ENTITY = "name" ]] ; then 
			#Skip name bracket
			next_xml_tag;
			LV_NAME=$CONTENT  #name
			next_xml_tag;
			next_xml_tag;
			next_xml_tag; #translation DE
			M_NAME_DE_TMP=$CONTENT
			next_xml_tag; next_xml_tag #translation EN
			M_NAME_EN_TMP=$CONTENT
		fi	 	

		# id zum Debuggen
		if [[ $ENTITY = "id" ]]	 ; then 
			ID=$CONTENT;
		fi

		#----------------------------------------------------
		# Lines after ModuleTypeField
		
		#Modulkekennung
		if [[ $ENTITY = "nodeIdentification" && $IS_MODULE = "true" ]]  ; then
			M_KENNUNG=$CONTENT
		fi
		if [[ $ENTITY = "nodeIdentification" && $IS_SPO = "true" ]]  ; then
			M_SPO_VER=$CONTENT
		fi
		
		#ECTS
		if [[ $ENTITY = "ectsCredits" ]] ; then
			if [[ $IS_MODULE = "true" ]] ; then
				M_ECTS=$CONTENT
			elif [[ $IS_AK = "true" || $IS_PK = "true" ]] ; then 
			#	if [[ $CONTENT =~ ^-?[0-9]+$ ]] ; then
			#		LV_ECTS=$CONTENT
			#	else
			#		LV_ECTS=0;
			#	fi
				LV_ECTS=$CONTENT
			fi
		fi

		#Valid from:
		if [[ $ENTITY = "nodeValidFrom" && $IS_MODULE = "true" ]]  ; then
			next_xml_tag
			M_VALIDFROM=$CONTENT
		fi

		#Valid until
		if [[ $ENTITY = "nodeValidUntil" && $IS_MODULE = "true" ]]  ; then
			next_xml_tag
			M_VALIDUNTIL=$CONTENT
			    if [ -z "$CONTENT" ]; then 
			    	CONTENT='*unbeschränkt*' 
			    fi
		fi

		#Moduldauer
		if [[ $ENTITY = "moduleDuration" && $IS_MODULE = "true" ]]  ; then
			next_xml_tag
			M_MODULDAUER=$CONTENT
		fi

		#ModuleLevel
		if [[ $ENTITY = "moduleLevel" && $IS_MODULE = "true" ]]  ; then
			next_xml_tag
			M_LEVEL=$CONTENT
		fi

		#frequency
		if [[ $ENTITY = "frequency" && $IS_MODULE = "true" ]]  ; then
			next_xml_tag
			M_FREQUENCY=$CONTENT
		fi

		#Sprache languageOfInstruction
		if [[ $ENTITY = "languageOfInstruction" && $IS_MODULE = "true" ]]  ; then
			next_xml_tag
			M_LANGUAGE=$CONTENT
		fi

		# Semester recommendedSemInCurriculum
		if [[ $ENTITY = "recommendedSemInCurriculum" && $IS_AK = "true" ]]  ; then
			next_xml_tag
			M_SEM=$CONTENT
		fi

		#Modulverantwortlicher personResponsibleForTheModule
		if [[ $ENTITY = "personResponsibleForTheModule" && $IS_MODULE = "true" ]]  ; then
			M_RESPONSIBLE=$CONTENT
			getProf
		fi

		#Studiengang spoCurriculumId
		if [[ $ENTITY = "spoCurriculumId" && $IS_MODULE = "true" ]]  ; then
			M_SPO=$CONTENT
		fi

		#Benotungsdauer examDuration
		if [[ $ENTITY = "examDuration" && $IS_MODULE = "true" ]]  ; then
			M_EXAM_DURATION=$CONTENT
		fi

		#Gesamtstungen totalHours
		if [[ $ENTITY = "totalHours" && $IS_MODULE = "true" ]]  ; then
			M_TOTALHOURS=$CONTENT
		fi

		#Kontaktzeit contactHours
		if [[ $ENTITY = "contactHours" && $IS_MODULE = "true" ]]  ; then
			M_CONTACTHOURS=$CONTENT
		fi

		#Selbststudium selfStudyHours
		if [[ $ENTITY = "selfStudyHours" && $IS_MODULE = "true" ]]  ; then
			M_SELFSTUDY=$CONTENT
		fi

		#----------------------------------------------------
		# Bilingual fields

		#Benotung grading
		if [[ $ENTITY = "grading" && $IS_MODULE = "true" ]]  ; then
			next_xml_tag; next_xml_tag; #name
			next_xml_tag; next_xml_tag; #translation DE
			if [[ $ENTITY = 'translation lang="de"' ]] ; then
				M_GRADING_DE=$CONTENT
				next_xml_tag; 
			else
				M_GRADING_DE='*keine Angabe*';
			fi
			next_xml_tag; #translation EN
			if [[ $ENTITY = 'translation lang="en"' ]] ; then
				M_GRADING_EN=$CONTENT
			else
				M_GRADING_EN='*keine Angabe*';
			fi
		fi

		#Literatur readingList
		if [[ $ENTITY = "readingList" && $IS_MODULE = "true" ]]  ; then
			next_xml_tag; next_xml_tag; #name
			next_xml_tag; next_xml_tag; #translation DE
			if [[ $ENTITY = 'translation lang="de"' ]] ; then
				M_LITERATUR=$CONTENT
			else
				M_LITERATUR='*keine Angabe*'
			fi
			next_xml_tag; #translation EN
			if [[ $ENTITY = 'translation lang="en"' ]] ; then
				M_LITERATUR=$CONTENT
			fi
		fi

		#Inhalt contents
		if [[ $ENTITY = "contents" && $IS_MODULE = "true" ]]  ; then
			next_xml_tag; next_xml_tag; #name
			next_xml_tag; next_xml_tag; #translation DE
			if [[ $ENTITY = 'translation lang="de"' ]] ; then
				M_CONTENT_DE=$CONTENT
				next_xml_tag; 
			else 
				M_CONTENT_DE='*keine Angabe*' 
			fi 
			next_xml_tag; #translation EN
			if [[ $ENTITY = 'translation lang="en"' ]] ; then
				M_CONTENT_EN=$CONTENT
			else 
				M_CONTENT_EN='*keine Angabe*' 
			fi 
		fi

		#Lehrnziele intendedLearningOutcomes
		if [[ $ENTITY = "intendedLearningOutcomes" && $IS_MODULE = "true" ]]  ; then
			next_xml_tag; next_xml_tag; #name
			next_xml_tag; next_xml_tag; #translation DE
			if [[ $ENTITY = 'translation lang="de"' ]] ; then
				M_LERNZIELE_DE=$CONTENT
				next_xml_tag; 
			else 
				M_LERNZIELE_DE='*keine Angabe*' 
			fi 
			next_xml_tag; #translation EN
			if [[ $ENTITY = 'translation lang="en"' ]] ; then
				M_LERNZIELE_EN=$CONTENT
			else 
				M_LERNZIELE_EN='*keine Angabe*' 
			fi
		fi

		#Empfohlne Vorraussetzungen previousKnowledgeExpected
		if [[ $ENTITY = "previousKnowledgeExpected" && $IS_MODULE = "true" ]]  ; then
			next_xml_tag; next_xml_tag; #name
			next_xml_tag; next_xml_tag; #translation DE
			if [[ $ENTITY = 'translation lang="de"' ]] ; then
				M_PREV_KNOW_DE=$CONTENT
				next_xml_tag;
			else 
				M_PREV_KNOW_DE='*keine Angabe*' 
			fi 
			 next_xml_tag; #translation EN
			if [[ $ENTITY = 'translation lang="en"' ]] ; then
				M_PREV_KNOW_EN=$CONTENT
			else 
				M_PREV_KNOW_EN='*keine Angabe*'
			fi
		fi

		#Präsenzzeit duration
		if [[ $ENTITY = "duration" ]]  ; then
		#		if [[ $CONTENT =~ ^-?[0-9]+$ ]] ; then
		#			LV_DURATION=$CONTENT
		#		else
		#			LV_DURATION=0;
		#		fi
		LV_DURATION=$CONTENT	
		fi

		#-----------------------------------------------------
		# Write to file 
				
		#increment resource
		if [[ $ENTITY = "resource" ]] ; then #neuer Ressourcenblock
			#LV of a Module
			if [[ $IS_AK = "true" || $IS_PK = "true" ]] ; then
				LVPV_EINTRAG+='\n| '${LV_NAME}' | '${LV_NAME_EN}'  | '${LV_ECTS}' | '${LV_DURATION}''
				#LV_ECTS_SUM+=$((LV_ECTS_SUM + LV_ECTS ))
				#LV_DURATION_SUM+=$((LV_DURATION_SUM + LV_DURATION ))
				RESOURCE_COUNTER=$((RESOURCE_COUNTER + 1 ))
			fi
		fi
			
done < $1
echo 'Done'
