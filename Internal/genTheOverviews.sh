#!/bin/bash
# MontiCore stable
#
# Dieses Script generiert die ein oder andere
# kleine Uebersicht ins Repository
#
# Startpunkt des Script: root des Repository
#
# Ok.

# Ergebnisse zu speichern:
target=./ModulListen/

# ################################
# Phase 1: Extraktion der Daten nach $rawlist

# ################################
# rawlist: Erläuterung
# Hier landet die rohe Liste von Modulen mit 
# je einer Zeile pro modul.
# Ein typischer Eintrag hat folgendes Aussehen: 
# Modellbasierte Softwareentwicklung~#~Bernhard Rumpe~#~keineKennung~#~2019-07-21~#~MEdGyGeInf:2017~#~ModuleGen/1215686.md
# Dies sind sechs Felder, getrennt durch "~#~" wie folgt:
# 1. Titel des Moduls
# 2. Name des Anbieters (Prof.)
# 3. Kurztitel (zB MBSE)
# 4. Datum der letzten Änderung oder "--"
# 5. Name und Jahrgang der Studienordnung
# 6. Quelle der Info (Dateiname)
#
rawlist=$target/rawlist.txt

## aktuell unbenutzt: ##  moduls1=`find . -print | grep "Module/" | grep ".md$"`
moduls2=`find . -print | grep "Module_Modulangebot_FGI/" | grep ".md$"`
#TEST moduls2=`find . -print | grep "Module_Modulangebot_FGI/1220" | grep ".md$"` # testzwecke
moduls=`echo $moduls1 $moduls2`

rm -f $rawlist	# neu initialisieren
touch $rawlist

for m in $moduls
do 
  ##echo "-------------------------   FILE:" $m
  changedate=`stat --format="%y" $m`
  cat $m \
  | gawk '
    /^\|/ { fModTitelDE=0; fModAnbieter=0; fKennung=0; }

    ####### Modultitel  und   Modulkennung
    # erste Überschriftszeile "## ..."
    /^##.*\([0-9][0-9]*\)/ {
      sub(/^#* */,"");
      contKennung=$0;
      sub(/.*\(/,"",contKennung);
      sub(/\).*/,"",contKennung);
      sub(/ *\([0-9][0-9]*\).*/,"");
      contModTitelDE=$0; next
    }

    ####### ModulanbieterIn (sogar mehrzeilig akzeptiert)
    /^\|.*&nbsp;ModulanbieterIn\|/ {
      sub(/\|.*\| */,""); contModAnbieter=$0; fModAnbieter=1; next;
    }
    {if(fModAnbieter) { contModAnbieter= contModAnbieter " " $0; next}}

    ####### Studiengaenge  (das wird eine Liste gezaehlt mit anzStudiengang)
    /^\|.*Studiengangsk.rzel.*\|/ {
      fStudiengang=1; anzStudiengang=0; next;
    }
    # Extraktion einer Zeile der Art:   | BSInf | 2010 |
    /^\| *[a-zA-Z]* *\| *[0-9][0-9][0-9][0-9] *\|/ { 
      if(fStudiengang) {
        kuerzel=$0;  # + bereinigen
	sub(/^\| */, "", kuerzel);
        jahr=kuerzel;  
	sub(/ *\|.*/, "", kuerzel);
	sub(/[^\|]*\| */, "", jahr);
	sub(/ *\|.*/, "", jahr);
        contStudiengang[anzStudiengang] = kuerzel ":" jahr;
        anzStudiengang++; 
        next
      }
    }
    /^[^\|]/ {fStudiengang=0;}

    ####### Ausgabe am Ende
    END { 
      if(!contModTitelDE) { contModTitelDE="keinTitel"; }
      if(!contModAnbieter) { contModAnbieter="keinAnbieter"; }
      if(!contKennung) { contKennung="keineKennung"; }
      if(anzStudiengang==0) {
        contStudiengang[anzStudiengang] = "keinStudiengang";
	anzStudiengang++;
      }

      # Bereinigung des Modulanbieters
      # cut the initial part (MAOvs. MOVerwalter)
      sub(/.*Modulverantworlicher: */,"",contModAnbieter);
      sub(/.*Modulverantwortlicher: */,"",contModAnbieter);
      gsub(/&lt;br/,"",contModAnbieter);
      gsub(/&gt;/," ",contModAnbieter);
      gsub(/Universit.tsprofessorin */," ",contModAnbieter);
      gsub(/Universit.tsprofessor */," ",contModAnbieter);
      gsub(/Professorin */," ",contModAnbieter);
      gsub(/Professor */," ",contModAnbieter);
      gsub(/als Juniorprofessor */," ",contModAnbieter);
      gsub(/als Juniorprofessorin */," ",contModAnbieter);
      gsub(/Dr\. h\. c\. */," ",contModAnbieter);
      gsub(/Dr\. rer\. nat\. */," ",contModAnbieter);
      gsub(/Dr\. rer\. pol\. */," ",contModAnbieter);
      gsub(/Dr\.-Ing\. */," ",contModAnbieter);
      gsub(/Dr\. Ing\. */," ",contModAnbieter);
      gsub(/Dr\. ing\. */," ",contModAnbieter);
      gsub(/Dr\. ir\. */," ",contModAnbieter);
      gsub(/Ph\. D\. */," ",contModAnbieter);
      gsub(/\(.*\) */," ",contModAnbieter);
      gsub(/h\. *c *\. */," ",contModAnbieter);
      gsub(/i\. *R\. */," ",contModAnbieter);
      gsub(/Dr\. */," ",contModAnbieter);
      gsub(/apl\. */," ",contModAnbieter);
      gsub(/Prof\. */," ",contModAnbieter);
      gsub(/Dipl\.-Inform\. */," ",contModAnbieter);
      gsub(/M\. Sc\. RWTH */," ",contModAnbieter);
      gsub(/LL\.M\. */," ",contModAnbieter);
      gsub(/sc\. techn\. */," ",contModAnbieter);
      gsub(/Modulangebotsorganisation: */," ",contModAnbieter);
      gsub(/Stephanie Schrader */," ",contModAnbieter);
      gsub(/Modellierungsteamverantwortlicher: */," ",contModAnbieter);
      gsub(/Sebastiaan Wouters */," ",contModAnbieter);
      gsub(/sc\. techn\. */," ",contModAnbieter);
      gsub(/<br>/," ",contModAnbieter);
      gsub(/<br \/>/," ",contModAnbieter);
      gsub(/  +/," ",contModAnbieter);
      gsub(/^ /,"",contModAnbieter);
      gsub(/ $/,"",contModAnbieter);

      # Filename, Änderungsdatum aus der Umgebung holen
      contFilename="'"$m"'";
      sub(/^\.\//,"",contFilename);
      contChangedate="'"$changedate"'";
      sub(/ .*/,"",contChangedate);
      sub(/2019-07-21/,"--",contChangedate);    # ungeändertes Datum

      for(i = 0; i < anzStudiengang; i++) {
        printf "%s~#~%s~#~%s~#~%s~#~%s~#~%s\n", 
		contModTitelDE, contModAnbieter, contKennung,
		contChangedate, contStudiengang[i], contFilename;
      }
    }
  ' >> $rawlist
done



# ################################
# Phase 2: Aufbereitung der Daten aus $rawlist

# ################################
# Liste der Module (dt) sortiert
targetfile=$target/ModulListe.md

echo "# Liste aktuell verwalteter Module" > $targetfile
echo "" >> $targetfile
echo "sortiert nach dem Modulnamen in Deutsch;" >> $targetfile
echo "" >> $targetfile
echo "generiert am: " `date` >> $targetfile
#echo "Anzahl Module:" `cat $rawlist | wc -l` >> $targetfile
echo "" >> $targetfile

cat $rawlist \
| gawk 'BEGIN {FS="~#~"}; { printf "* [%s](../%s) (von: %s)\n", $1, $6, $2; }' \
| sort -u \
>> $targetfile

echo "" >> $targetfile


# ################################
targetfile=$target/ModuleNachChangeDatumSortiert.md

echo "# Liste  verwalteter Module (nach Datum)" > $targetfile
echo "" >> $targetfile
echo "sortiert nach dem Datum letzter Aenderung" >> $targetfile
echo "" >> $targetfile
echo "generiert am: " `date` >> $targetfile
#echo "Anzahl Module:" `cat $rawlist | wc -l` >> $targetfile
echo "" >> $targetfile

cat $rawlist \
| gawk 'BEGIN {FS="~#~"}; { printf "* %s: [%s](../%s) (von: %s)\n", $4, $1, $6, $2; }' \
| sort -u -r \
>> $targetfile

echo "" >> $targetfile


# ################################
targetfile=$target/ModuleNachAnbieterSortiert.md

echo "# Liste verwalteter Module (nach Anbieter)" > $targetfile
echo "" >> $targetfile
echo "sortiert nach dem Anbieter sortiert" >> $targetfile
echo "" >> $targetfile
echo "generiert am: " `date` >> $targetfile
#echo "Anzahl Module:" `cat $rawlist | wc -l` >> $targetfile
echo "" >> $targetfile

echo "| Anbieter | Titel | Studiengang | Aenderung am | " >> $targetfile
echo "| -- | -- | -- | -- | " >> $targetfile
cat $rawlist \
| gawk 'BEGIN {FS="~#~"}; { printf "| %s | [%s](../%s) | %s | %s |\n", $2, $1, $6, $5, $4; }' \
| sort -u \
>> $targetfile

echo "" >> $targetfile


# ################################
targetfile=$target/ModuleNachStudiengangSortiert.md

echo "# Liste verwalteter Module (nach Studiengang)" > $targetfile
echo "" >> $targetfile
echo "sortiert nach dem Studiengang sortiert" >> $targetfile
echo "" >> $targetfile
echo "generiert am: " `date` >> $targetfile
#echo "Anzahl Module:" `cat $rawlist | wc -l` >> $targetfile
echo "" >> $targetfile

echo "| Studiengang | Titel | Anbieter | Aenderung am | " >> $targetfile
echo "| -- | -- | -- | -- | " >> $targetfile
cat $rawlist \
| gawk 'BEGIN {FS="~#~"}; { printf "| %s | [%s](../%s) | %s | %s |\n", $5, $1, $6, $2, $4; }' \
| sort -u \
>> $targetfile

echo "" >> $targetfile


# ################################
targetfile=$target/ModuleNachModulnummerSortiert.md

echo "# Liste verwalteter Module (nach Modulnummer)" > $targetfile
echo "" >> $targetfile
echo "sortiert nach der Modulnummer sortiert" >> $targetfile
echo "und fuer jeden Studiengang extra aufgefuehrt" >> $targetfile
echo "" >> $targetfile
echo "generiert am: " `date` >> $targetfile
#echo "Anzahl Module:" `cat $rawlist | wc -l` >> $targetfile
echo "" >> $targetfile

echo "| Modulnummer | Studiengang | Titel | Anbieter | Aenderung am | " >> $targetfile
echo "| -- | -- | -- | -- | -- | " >> $targetfile
cat $rawlist \
| gawk 'BEGIN {FS="~#~"}; { printf "| %s | %s | [%s](../%s) | %s | %s |\n", $3, $5, $1, $6, $2, $4; }' \
| sort -u \
>> $targetfile

echo "" >> $targetfile



