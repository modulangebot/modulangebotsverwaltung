#!/bin/bash
# MontiCore stable
#
# Dieses Script generiert Name/Wert Paare
# als Hilfe f�r Modulanbieter
#
# Ok;

# Liste der Module, die untersucht werden
## aktuell unbenutzt: ##  moduls1=`find . -print | grep "Module/" | grep ".md$"`
moduls2=`find . -print | grep "Module_Modulangebot_FGI/" | grep ".md$"`
#XXX moduls2=`find . -print | grep "ModuleGen/1220" | grep ".md$"` # testzwecke
moduls=`echo $moduls1 $moduls2`


# Ergebnisse zu speichern:
target=./ModulListen/

# ################################
targetfile=$target/NameWertPaareInTabellen.md

echo "# Aktuell auftretende Name/Wert-Paare mit Haeufigkeit" > $targetfile
echo "" >> $targetfile
echo "Typische sinnvolle (ggf. auch falsch gesetzte Werte)" >> $targetfile
echo "sind so erkennbar/nutzbar." >> $targetfile
echo "Sortiert nach Name." >> $targetfile
echo "Bereinigt um stark ver�nderliche Werte (zB Titel)" >> $targetfile
echo "" >> $targetfile
echo "generiert am: " `date` >> $targetfile
echo "" >> $targetfile
echo "" >> $targetfile
echo "" >> $targetfile
echo "" >> $targetfile


echo "| Anzahl | Name | Wert | " >> $targetfile
echo "| --     | --   | --   | " >> $targetfile

# Vertikale Tabellen: Zeilen
# Suche alle Tabellen-Zeilen und gib sie mit #auftreten aus
# (das hilft irregul�r auftretende Zeilen zu finden)

# Teil 1: mit f�hrendem "|"
cat $moduls \
  | grep "^| " \
  | grep -v "|.*|.*|.*" \
  | grep -v "ModulanbieterIn" \
  | grep -v "Modultitel" \
  | grep -v "K.rzel" \
  | grep -v "Untertitel" \
  | grep -v "Feld.*Inhalt" \
  | grep -v ".-------------" \
  | tr " \011\015" "   " \
  | sed "s/  */ /g" \
  | sed "s/<!--.*-->//g" \
  | gawk '{a[$0]++}
          END {for(k in a) printf "%4sx  %-72.72s\n",  a[k],k }' \
  | sort -k2 \
  >> $targetfile
echo "" >> $targetfile

# Teil 2: ohne f�hrendem "|" (aber wenigstens einem) Strich
cat $moduls \
  | grep -v "^| " \
  | grep "|" \
  | grep -v "|.*|.*" \
  | grep -v ".*|.*|" \
  | grep -v "ModulanbieterIn" \
  | grep -v "Modultitel" \
  | grep -v "K.rzel" \
  | grep -v "Untertitel" \
  | tr " \011\015" "   " \
  | sed "s/  */ /g" \
  | sed "s/^ *//g" \
  | sed "s/ *$//g" \
  | sed "s/<!--.*-->//g" \
  | gawk '{a[$0]++}
          END {for(k in a) printf "%4sx  %-72.72s\n",  a[k],k }' \
  | sort -k2 \
  >> $targetfile
echo "" >> $targetfile

