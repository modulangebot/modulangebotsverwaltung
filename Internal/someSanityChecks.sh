#!/bin/bash
# MontiCore stable
#
# Dieses Script fuehrt einige kleine Checks durch, die 
# zeigen, was der Datenbestand so beinhaltet
#
# Ok.

# Liste der Module, die untersucht werden
## aktuell unbenutzt: ##  moduls1=`find . -print | grep "Module/" | grep ".md$"`
moduls2=`find . -print | grep "Module_Modulangebot_FGI/" | grep ".md$"`
#TEST moduls2=`find . -print | grep "ModuleGen/1220" | grep ".md$"` # testzwecke
moduls=`echo $moduls1 $moduls2`

# Anzahl gefundener Module
echo "Sanity checks fuer den modulkatalog am" `date`
echo "" 
echo "Es sind aktuell" `echo $moduls| wc -w`  "Module"
echo "mit" `cat $moduls| wc -w`  "Zeilen"
echo "" 
# evtl. rausnehmen: TEST
#TEST echo $moduls
echo "" 

# Neueste Dateien finden
# Suche alle Dateien, die in den letzten 3 Tagen ge�ndert wurden
echo "----------------------------------------------------------"
echo "-- Diese Dateien haben sich in den letzten Tagen geaendert:"
echo ""

echo "#----- gestern ------------------------------"
find . -mtime -1 \
    ! -path "*/.git*" \
    ! -name "*.swp" ! -name "*.bak" \
    ! -name "Module*Sortiert*.md" \
    ! -name "x*.txt" \
    ! -name "find.dateienliste.txt" \
    ! -name "rawlist.txt" \
    ! -name "ModulListe.md" \
    ! -name "NameWertPaareInTabellen.md" \
    ! -path "./ModulListen" \
    -printf " %p\n" \
  | sort
echo "#----- 2 Tage ------------------------------"
find . -mtime 2 \
    ! -path "*/.git*" \
    ! -name "*.swp" ! -name "*.bak" \
    ! -name "Module*Sortiert*.md" \
    ! -name "x*.txt" \
    ! -name "find.dateienliste.txt" \
    ! -name "rawlist.txt" \
    ! -name "ModulListe.md" \
    ! -name "NameWertPaareInTabellen.md" \
    ! -path "./ModulListen" \
    -printf " %p\n" \
  | sort
echo ""


# Header-Zeilen
# Suche alle Header-Zeilen und gib sie mit #auftreten aus
# (das hilft irregul�r auftretende Header zu finden)
echo "----------------------------------------------------------"
echo "-- Diese Header-Zeilen treten auf:"
echo ""
cat $moduls \
  | grep "^#" \
  | grep -v "([0-9][0-9]*)" \
  | gawk '{a[$0]++}
          END {for(k in a) printf " %5sx  %s\n",  a[k],k }' \
  | sort
echo ""


# Vertikale Tabellen: Eintraege in Spalte 1
# (haben nur zwei Zeilen "| Attributname | Wert |" )
# Suche alle Tabellen Eintraege (Spalte 1) und gib sie mit #auftreten aus
echo "----------------------------------------------------------"
echo "-- Diese Tabellen-Attributnamen treten auf (in vertikalen Tabellen):"
echo ""
# Teil 1: mit f�hrendem "|"
cat $moduls \
  | grep "^| " \
  | grep -v "|.*|.*|.*" \
  | sed "s/^| *//g" \
  | sed "s/ *|.*//g" \
  | sed "s/^ *//g" \
  | sed "s/ *$//g" \
  | gawk '{a[$0]++}
          END {for(k in a) printf "%4sx  %s\n",  a[k],k }' \
  | sort 
echo ""
# Teil 2: ohne f�hrendem "|" (aber wenigstens einem) Strich
cat $moduls \
  | grep -v "^| " \
  | grep "|" \
  | grep -v "|.*|.*" \
  | grep -v ".*|.*|" \
  | sed "s/|.*//g" \
  | sed "s/^ *//g" \
  | sed "s/ *$//g" \
  | gawk '{a[$0]++}
          END {for(k in a) printf "%4sx  %s\n",  a[k],k }' \
  | sort 
echo ""

# Irregulaere L�nge (zu klein / zu gross)
echo "----------------------------------------------------------"
echo "-- Gibt es Dateien, die sehr klein oder gross sind:"
echo ""
find . -printf "  %7s %p\n" | grep Module | grep -v ModulListen | grep -v Internal |grep ".md$" | sort -n | head -4
echo "..."
find . -printf "  %7s %p\n" | grep Module | grep -v ModulListen | grep -v Internal | grep ".md$" | sort -n | tail -4

echo ""


# Formatierungszeichen zB aus HTML
# auf diese sollte eher verzichtet werden
# Beispiele: &lt;/div&gt;&lt;ul &gt;   
echo "----------------------------------------------------------"
echo "-- Diese Tags treten auf:"
echo ""
cat $moduls \
  | sed "s/;/; /g" \
  | sed "s/&/ &/g" \
  | tr " \011\015" "\012\012\012" \
  | grep "&" \
  | gawk '{a[$0]++}
          END {for(k in a) printf " %5sx  %s\n",  a[k],k }' \
  | sort
echo ""

# Formatierungszeichen zB aus HTML (in etwas gr��eren Einheiten)
cat $moduls \
  | sed "s/<!--/<!-- /g" \
  | sed "s/ *>/> /g" \
  | sed "s/ *\/>/> /g" \
  | sed "s/ *&gt;/> /g" \
  | sed "s/&lt; */ </g" \
  | sed "s/< */ </g" \
  | sed "s/<\/ */ </g" \
  | tr " \011\015" "\012\012\012" \
  | grep "<" \
  | gawk '{a[$0]++}
          END {for(k in a) printf " %5sx  %s\n",  a[k],k }' \
  | sort
echo ""


# Stimmen Dateienamen und Modulkennung ueberein
# (da ist Redundanz)
echo "----------------------------------------------------------"
echo "-- Abweichungen zwischen Modulkennung und Dateinamen"
echo ""
for m in $moduls
do
  #TEST echo "-------------------------   FILE:" $m
  kennung=`cat $m \
  | gawk '
    ####### Modulkennung
    # erste �berschriftszeile "## ..."
    /^##.*\([0-9][0-9]*\)/ {
      sub(/.*\(/,"");
      sub(/\).*/,"");
      print;
    }
  '`
  expectedFile=`basename $m ".md"`
  if test "$expectedFile" != "$kennung"; then
    echo "Stress in " $m
  fi
done

# Sind die Liste der RAW und der generated Modules gleich?
echo "----------------------------------------------------------"
echo "-- Fehlen Raw oder Generated Module"
echo ""
rawsource="Internal/ModuleRaw/"
gensource="Module_Modulangebot_FGI/"
rawmoduls=`find . -print | grep $rawsource | grep ".md$"`
genmoduls=`find . -print | grep $gensource | grep ".md$"`

# ################################
# Liste der Modulnummern aus Dateinamen zusammen sammeln
echo $genmoduls \
  | sed "s!"$gensource"!!g" \
  | sed "s!\./!!g" \
  | sed "s!_[^ ]*md!�!g" \
  | sed "s!\.md!�!g" \
  | tr "�" "\012" \
  | sed "s/^ *//g" \
  | sort -u \
  > xgenmodulkennungen.txt

echo $rawmoduls \
  | sed "s!"$rawsource"!!g" \
  | sed "s!\./!!g" \
  | sed "s!_[^ ]*md!�!g" \
  | sed "s!\.md!�!g" \
  | tr "�" "\012" \
  | sed "s/^ *//g" \
  | sort -u \
  > xrawmodulkennungen.txt

diff xgenmodulkennungen.txt xrawmodulkennungen.txt \
  | grep -a "^[<>] " \
  | sed "s/^>/New (only in gen. Modules):/g" \
  | sed "s/^</Missing (only in raw): /g"


echo ""
echo "-- Ende."
echo ""


